// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	future: {
		compatibilityVersion: 4,
	},
	compatibilityDate: "2024-12-31",

	ssr: false,
	css: ["./app/assets/index.css"],
	modules: [
		"@pinia/nuxt",
		"@nuxtjs/i18n",
		"@vee-validate/nuxt",
		"@vite-pwa/nuxt",
		"@nuxt/test-utils/module",
		"@nuxtjs/storybook",
	],
	devtools: { enabled: true },
	postcss: {
		plugins: {
			tailwindcss: {},
			autoprefixer: {},
		},
	},
	pinia: {
		storesDirs: ["./app/stores/**"],
	},
	i18n: {
		vueI18n: "./locales/i18n.config.ts",
	},
	veeValidate: {
		autoImports: true,
		componentNames: {
			Form: "VeeForm",
			Field: "VeeField",
			FieldArray: "VeeFieldArray",
			ErrorMessage: "VeeErrorMessage",
		},
	},
	pwa: {
		registerType: "autoUpdate",
		manifest: {
			name: "Web Libros",
			short_name: "Web Libros",
			description: "Take ownership off your digital books",
			theme_color: "",
			icons: [
				{
					src: "pwa-192x192.png",
					sizes: "192x192",
					type: "image/png",
				},
				{
					src: "pwa-512x512.png",
					sizes: "512x512",
					type: "image/png",
				},
				{
					src: "pwa-512x512.png",
					sizes: "512x512",
					type: "image/png",
					purpose: "any maskable",
				},
			],
		},
	},
	app: {
		head: {
			link: [
				{
					rel: "icon",
					type: "image/x-icon",
					href: "favicon.ico",
				},
			],
		},
	},
});
