import bookCard from "@/components/bookCard.vue";

export default {
	component: bookCard,
};

export const Default = {
	args: {
		title: "Hello world",
		coverUrl: "https://tecdn.b-cdn.net/img/new/standard/nature/184.jpg",
	},
};
