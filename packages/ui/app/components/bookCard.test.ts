import { mount } from "@vue/test-utils";
import { it, expect, describe } from "vitest";
import bookCard from "@/components/bookCard.vue";

describe("bookCard", () => {
	it("can mount the component", async () => {
		expect(bookCard).toBeTruthy();
		const component = mount(bookCard, {
			props: { coverUrl: "http//fake.com", title: "Hello world" },
		});
		expect(component.html()).toContain('src="http//fake.com"');
		expect(component.html()).toContain('<h5 class="mb-2">Hello world</h5>');
	});
});
