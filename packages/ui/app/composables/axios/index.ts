import axios from "axios";
import JwtToken from "../jwt";
import { useJwtStore } from "@/stores/jwt";
import { apiUrlV1 } from "@/composables/urls";

const defaultHeaders = {
	"Content-Type": "application/json",
	"Access-Control-Allow-Origin": "*",
};

export enum TokenCookiesName {
	accessToken = "webLibrosJwtAccessToken",
	refreshToken = "webLibrosJwtRefreshToken",
}

const axiosApiV1 = axios.create({
	baseURL: apiUrlV1,
	headers: defaultHeaders,
});

const axiosApiV1NoBaseUrl = axios.create({
	headers: defaultHeaders,
});

const axiosApiV1NoTokenForm = axios.create({
	baseURL: apiUrlV1,
	headers: {
		"content-type": "application/x-www-form-urlencoded",
	},
});
const axiosApiV1NoToken = axios.create({
	baseURL: apiUrlV1,
	headers: defaultHeaders,
});

/* The interceptor here ensures that we check for the token in local storage every time an axios request is made
 */

const valid_access_token = async () => {
	const raw_accesstoken = useCookie(TokenCookiesName.accessToken).value;
	if (!raw_accesstoken) {
		//no token stored in cookies
		console.log("No access tokens found in cookies");
		return Promise.reject;
	}

	const accesstoken = new JwtToken(raw_accesstoken);
	if (!accesstoken.is_expired()) {
		// access token is valid
		return Promise.resolve(raw_accesstoken);
	}

	const refresh_token = new JwtToken(
		useCookie(TokenCookiesName.refreshToken).value || "notValid"
	);
	if (!refresh_token.is_expired()) {
		const store = useJwtStore();
		store.refreshTokens();
		return Promise.resolve(store.getAccessToken);
	}
	// both tokes expired -> force reload
	remove_cookies_and_reload();
};

const remove_cookies_and_reload = () => {
	useCookie(TokenCookiesName.accessToken).value = null;
	useCookie(TokenCookiesName.refreshToken).value = null;
	location.reload();
};

axiosApiV1.interceptors.request.use(
	async config => {
		const accesstoken = await valid_access_token();
		config.headers.Authorization = `Bearer ${accesstoken}`;
		return config;
	},
	function (error) {
		return Promise.reject(error);
	}
);

axiosApiV1NoBaseUrl.interceptors.request.use(
	async config => {
		const accesstoken = await valid_access_token();
		config.headers.Authorization = `Bearer ${accesstoken}`;
		return config;
	},
	function (error) {
		return Promise.reject(error);
	}
);

export default axiosApiV1;
export { axiosApiV1NoTokenForm };
export { axiosApiV1NoToken };
export { axiosApiV1NoBaseUrl };
