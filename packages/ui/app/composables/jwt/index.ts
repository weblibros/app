import { jwtDecode } from "jwt-decode";
import { DateTime } from "luxon";
import type { JwtPayload } from "@/composables/jwt/types";

const ten_seconds = 10;
const EXPIRE_MARGIN = ten_seconds;

class JwtToken {
	token: string;
	payload: JwtPayload | undefined;
	constructor(token: string) {
		this.token = token;
		this.payload = undefined;
	}

	decode(): JwtPayload {
		const decoded_payload: JwtPayload = jwtDecode(this.token);
		this.payload = decoded_payload;
		return decoded_payload;
	}

	is_expired(): boolean {
		const payload = this.decode();

		const exp = payload.exp;
		const now_epoche = DateTime.now().toUnixInteger();
		return exp <= now_epoche + EXPIRE_MARGIN;
	}
}

export default JwtToken;
