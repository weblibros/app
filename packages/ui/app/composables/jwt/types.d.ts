export type JwtPayload = {
	exp: number;
	iat: number;
	iss: string;
	jti: string;
	user: string;
	type?: string;
	isActive?: boolean;
};
