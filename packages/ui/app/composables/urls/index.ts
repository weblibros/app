export const urlBackend = import.meta.env.VITE_API_URL
	? import.meta.env.VITE_API_URL
	: "";

export const apiUrlV1 = `${urlBackend}/api/v1`;
