export enum UploadUrls {
	ebook = "/file/upload/ebook",
}

export type EbookUploadResponse = {
	uuid: string;
	path: string;
	size_bytes: number;
	content_type: string;
};

export type EbookUploadForm = {
	file: File;
};
