import { defineStore, acceptHMRUpdate } from "pinia";
import axiosApiV1 from "@/composables/axios";
import {
	UploadUrls,
	type EbookUploadResponse,
	type EbookUploadForm,
} from "@/stores/upload/types.d.js";
export type UploadRootState = {
	loading: boolean;
	errorList: Array<string>;
};

export const useUploadEbookStore = defineStore("UploadEbookStore", {
	state: () => {
		return {
			loading: false,
			errorList: [],
		} as UploadRootState;
	},
	getters: {
		isLoading: state => state.loading,
		errorMessages: state => state.errorList,
	},
	actions: {
		async uploadEbook(formData: EbookUploadForm) {
			this.loading = true;
			this.clearErrorMessages();
			return await axiosApiV1
				.post<EbookUploadResponse>(UploadUrls.ebook, formData, {
					headers: {
						"Content-Type": "multipart/form-data",
					},
				})
				.then(
					response => {
						this.loading = false;
						Promise.resolve(response);
					},
					error => {
						this.loading = false;
						this.appendErrorMessage("Oops Something went wrong");
						Promise.reject(error);
					}
				);
		},
		async appendErrorMessage(errorMessage: string) {
			this.errorList.push(errorMessage);
		},
		async clearErrorMessages() {
			this.errorList = [];
		},
	},
});

if (import.meta.hot) {
	import.meta.hot.accept(acceptHMRUpdate(useUploadEbookStore, import.meta.hot));
}
