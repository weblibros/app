export enum BookUrls {
	myBooks = "/books",
}

interface BookAction {
	name: string;
	link: string;
	display_text: string;
}

interface BookResponse {
	uuid: string;
	title: string;
	cover_path: string;
	actions: Array<BookAction>;
}

interface Book extends BookResponse {
	coverUrl: string;
}

export type MyBooksResponse = {
	items: Array<BookResponse>;
	total: number;
	page: number;
	size: number;
	pages: number;
};
