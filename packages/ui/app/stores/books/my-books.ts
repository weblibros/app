import { defineStore, acceptHMRUpdate } from "pinia";
import axiosApiV1 from "@/composables/axios";
import {
	type MyBooksResponse,
	BookUrls,
	type Book,
	type BookResponse,
} from "@/stores/books/types.d.js";
import { urlBackend } from "@/composables/urls";
export type BookRootState = {
	books: Book[];
	loading: boolean;
};
const processBookResponse = function (booksResponse: BookResponse[]): Book[] {
	const books: Book[] = [];
	booksResponse.forEach(el => {
		books.push({ ...el, coverUrl: urlBackend + el.cover_path });
	});
	return books;
};

export const useMyBooksStore = defineStore("myBooksStore", {
	state: () => {
		return {
			books: [],
			loading: false,
		} as BookRootState;
	},
	getters: {
		getMyBooks: state => state.books,
		isLoading: state => state.loading,
	},
	actions: {
		async requestMyBooks() {
			this.loading = true;
			return await axiosApiV1.get<MyBooksResponse>(BookUrls.myBooks).then(
				response => {
					this.books = processBookResponse(response.data.items);
					this.loading = false;
					Promise.resolve(response);
				},
				error => {
					this.loading = false;
					Promise.reject(error);
				}
			);
		},
	},
});

if (import.meta.hot) {
	import.meta.hot.accept(acceptHMRUpdate(useMyBooksStore, import.meta.hot));
}
