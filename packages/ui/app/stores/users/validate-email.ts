import { defineStore, acceptHMRUpdate } from "pinia";
import { axiosApiV1NoToken } from "@/composables/axios";
import { UserUrls } from "./types.d.js";
import { useJwtStore } from "../jwt";
export const useValidateEmailStore = defineStore("ValidateEmailStore", {
	state: () => {
		return {
			loading: false,
			validResponse: false,
		};
	},
	getters: {
		isLoading: state => state.loading,
		isValidResponse: state => state.validResponse,
	},
	actions: {
		async validateEmailRequest() {
			const jwtStore = useJwtStore();

			this.loading = true;
			this.validResponse = false;
			return await axiosApiV1NoToken
				.post(UserUrls.userActivateReRequest, {
					access_token: jwtStore.getAccessToken,
				})
				.then(
					response => {
						this.loading = false;
						this.validResponse = true;
						Promise.resolve(response);
					},
					error => {
						this.loading = false;
						this.validResponse = false;
						Promise.reject(error);
					}
				);
		},
		resetValidResponse() {
			this.validResponse = false;
		},
	},
});

if (import.meta.hot) {
	import.meta.hot.accept(
		acceptHMRUpdate(useValidateEmailStore, import.meta.hot)
	);
}
