export enum UserUrls {
	userMe = "/user/me",
	userSignup = "/user/signup",
	userActivate = "/user/email/validate",
	userActivateReRequest = "/user/email/validate/re-request",
	passwordResetByToken = "/user/reset-password/token",
	passwordResetEmail = "/user/reset-password/email-request",
}

export type UserMeResponse = {
	uuid: string;
	email: string;
};

export type UserPasswordResetEmailRequest = {
	message: string;
};

export type UserPasswordResetByToken = {
	reset_token: string;
	password: string;
};

export type UserPasswordResetByTokenResponse = {
	user_uuid: string;
};

export type SignupForm = {
	password: string;
	email: string;
};
