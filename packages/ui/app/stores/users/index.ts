import { defineStore, acceptHMRUpdate } from "pinia";
import axiosApiV1 from "@/composables/axios";
import { type UserMeResponse, UserUrls } from "@/stores/users/types.d.js";
export const useUserStore = defineStore("userStore", {
	state: () => {
		return {
			email: "",
			loading: false,
		};
	},
	getters: {
		getMyEmail: state => state.email,
		isLoading: state => state.loading,
	},
	actions: {
		async requestUserMe() {
			this.loading = true;
			return await axiosApiV1.get<UserMeResponse>(UserUrls.userMe).then(
				response => {
					this.email = response.data.email;
					this.loading = false;
					Promise.resolve(response);
				},
				error => {
					this.loading = false;
					Promise.reject(error);
				}
			);
		},
	},
});

if (import.meta.hot) {
	import.meta.hot.accept(acceptHMRUpdate(useUserStore, import.meta.hot));
}
