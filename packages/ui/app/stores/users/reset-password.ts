import { defineStore, acceptHMRUpdate } from "pinia";
import { axiosApiV1NoToken } from "@/composables/axios";
import {
	type UserPasswordResetByToken,
	type UserPasswordResetByTokenResponse,
	UserUrls,
} from "@/stores/users/types.d.js";

export const usePasswordResetByTokenStore = defineStore(
	"passwordResetByTokenStore",
	{
		state: () => {
			return {
				loading: false,
				validResponse: false,
			};
		},
		getters: {
			isLoading: state => state.loading,
			isValidResponse: state => state.validResponse,
		},
		actions: {
			async resetPasswordByToken(body: UserPasswordResetByToken) {
				this.loading = true;
				this.validResponse = false;
				return await axiosApiV1NoToken
					.post<UserPasswordResetByTokenResponse>(
						UserUrls.passwordResetByToken,
						body
					)
					.then(
						response => {
							this.loading = false;
							this.validResponse = true;
							Promise.resolve(response);
						},
						error => {
							this.loading = false;
							this.validResponse = false;
							Promise.reject(error);
						}
					);
			},
			resetValidResponse() {
				this.validResponse = false;
			},
		},
	}
);

if (import.meta.hot) {
	import.meta.hot.accept(
		acceptHMRUpdate(usePasswordResetByTokenStore, import.meta.hot)
	);
}
