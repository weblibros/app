import { defineStore, acceptHMRUpdate } from "pinia";
import { axiosApiV1NoToken } from "@/composables/axios";
import { type SignupForm, UserUrls } from "@/stores/users/types.d.js";

export const useSignupStore = defineStore("signupStore", {
	state: () => {
		return {
			loading: false,
			validResponse: false,
			errorMessage: "",
		};
	},
	getters: {
		isLoading: state => state.loading,
		isValidResponse: state => state.validResponse,
		getErrorMessage: state => state.errorMessage,
	},
	actions: {
		async signupRequest(form: SignupForm) {
			this.errorMessage = "";
			this.loading = true;
			this.validResponse = false;
			return await axiosApiV1NoToken.post(UserUrls.userSignup, form).then(
				response => {
					this.loading = false;
					this.validResponse = true;
					Promise.resolve(response);
				},
				error => {
					this.loading = false;
					this.validResponse = false;
					this.errorMessage = error;
					Promise.reject(error);
				}
			);
		},
		resetValidResponse() {
			this.validResponse = false;
		},
	},
});

if (import.meta.hot) {
	import.meta.hot.accept(acceptHMRUpdate(useSignupStore, import.meta.hot));
}
