import { defineStore, acceptHMRUpdate } from "pinia";
import { axiosApiV1NoToken } from "@/composables/axios";
import {
	type UserPasswordResetEmailRequest,
	UserUrls,
} from "@/stores/users/types.d.js";
export const usepasswordResetEmailRequest = defineStore(
	"passwordResetEmailRequestStore",
	{
		state: () => {
			return {
				loading: false,
				validResponse: false,
			};
		},
		getters: {
			isLoading: state => state.loading,
			isValidResponse: state => state.validResponse,
		},
		actions: {
			async requestPasswordResetEmail(email: string) {
				this.loading = true;
				this.validResponse = false;
				return await axiosApiV1NoToken
					.post<UserPasswordResetEmailRequest>(UserUrls.passwordResetEmail, {
						email: email,
					})
					.then(
						response => {
							this.loading = false;
							this.validResponse = true;
							Promise.resolve(response);
						},
						error => {
							this.loading = false;
							this.validResponse = false;
							Promise.reject(error);
						}
					);
			},
			resetValidResponse() {
				this.validResponse = false;
			},
		},
	}
);

if (import.meta.hot) {
	import.meta.hot.accept(
		acceptHMRUpdate(usepasswordResetEmailRequest, import.meta.hot)
	);
}
