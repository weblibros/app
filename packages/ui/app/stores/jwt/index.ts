import { defineStore, acceptHMRUpdate } from "pinia";
import JwtToken from "@/composables/jwt";
import {
	axiosApiV1NoTokenForm,
	axiosApiV1NoToken,
	TokenCookiesName,
} from "@/composables/axios";
import {
	TokenUrls,
	type LoginForm,
	type TokenPairResponse,
} from "@/stores/jwt/types.d.js";

const setTokenInCookies = (accessToken: string, refreshToken: string) => {
	const keepForWeek = 3600 * 24 * 7;
	useCookie(TokenCookiesName.accessToken, {
		maxAge: keepForWeek,
		sameSite: "strict",
	}).value = accessToken;
	useCookie(TokenCookiesName.refreshToken, {
		maxAge: keepForWeek,
		sameSite: "strict",
	}).value = refreshToken;
};
function delete_cookie(name: string) {
	document.cookie = name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
}
const removeTokenFromCookies = () => {
	delete_cookie(TokenCookiesName.accessToken);
	delete_cookie(TokenCookiesName.refreshToken);
	refreshCookie(TokenCookiesName.accessToken);
	refreshCookie(TokenCookiesName.refreshToken);
};

export const useJwtStore = defineStore("jwtStore", {
	state: () => {
		return {
			accesstoken: {
				token: useCookie(TokenCookiesName.accessToken).value || "",
				payload: useCookie(TokenCookiesName.accessToken).value
					? new JwtToken(
							useCookie(TokenCookiesName.accessToken).value || "unvalidToken"
						).decode()
					: undefined,
			},
			refreshtoken: {
				token: useCookie(TokenCookiesName.refreshToken).value || "",
				payload: useCookie(TokenCookiesName.refreshToken).value
					? new JwtToken(
							useCookie(TokenCookiesName.refreshToken).value || "unvalidToken"
						).decode()
					: undefined,
			},
			loading: false,
			errorMessage: "",
		};
	},
	getters: {
		getAccessToken: state => state.accesstoken.token,
		getRefreshToken: state => state.refreshtoken.token,
		getErrorMessage: state => state.errorMessage,
		isLoading: state => state.loading,
		isAuth: state => !!state.accesstoken.payload?.user || false,
		isActive: state => !!state.accesstoken.payload?.isActive || false,
	},
	actions: {
		async requestToken(data: LoginForm) {
			this.errorMessage = "";
			this.loading = true;

			return await axiosApiV1NoTokenForm
				.post<TokenPairResponse>(TokenUrls.requestToken, data)
				.then(
					response => {
						const accesstoken = response.data.access_token;
						const refreshtoken = response.data.refresh_token;
						this.setAccessToken(accesstoken);
						this.setRefreshToken(refreshtoken);
						setTokenInCookies(accesstoken, refreshtoken);
						this.loading = false;
						Promise.resolve(response);
					},
					error => {
						this.errorMessage = error;
						this.loading = false;
						Promise.reject(error);
					}
				);
		},
		async refreshTokens() {
			const refresh_token = useCookie(TokenCookiesName.refreshToken).value;
			return await axiosApiV1NoToken
				.post<TokenPairResponse>(TokenUrls.refreshTokenPair, {
					refresh_token: refresh_token,
				})
				.then(
					response => {
						const accesstoken = response.data.access_token;
						const refreshtoken = response.data.refresh_token;
						this.setAccessToken(accesstoken);
						this.setRefreshToken(refreshtoken);
						setTokenInCookies(accesstoken, refreshtoken);
						Promise.resolve(response);
					},
					error => {
						Promise.reject(error);
					}
				);
		},

		setAccessToken(token: string) {
			const JwtAccessToken = new JwtToken(token);
			this.accesstoken = {
				token: token,
				payload: JwtAccessToken.decode(),
			};
		},
		setRefreshToken(token: string) {
			const JwtRefreshToken = new JwtToken(token);
			this.refreshtoken = {
				token: token,
				payload: JwtRefreshToken.decode(),
			};
		},
		deleteTokens() {
			removeTokenFromCookies();
			this.refreshtoken = { token: "", payload: undefined };
			this.accesstoken = { token: "", payload: undefined };
		},
	},
});

if (import.meta.hot) {
	import.meta.hot.accept(acceptHMRUpdate(useJwtStore, import.meta.hot));
}
