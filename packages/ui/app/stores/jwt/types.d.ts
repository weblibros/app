export enum TokenUrls {
	requestToken = "/auth/login",
	refreshTokenPair = "/auth/token/refresh",
}

export type LoginForm = {
	password: string;
	username: string;
	scope?: string;
	client_id?: string;
	client_secret?: string;
};

export type TokenPairResponse = {
	access_token: string;
	refresh_token: string;
};
