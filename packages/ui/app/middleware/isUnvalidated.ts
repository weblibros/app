export default defineNuxtRouteMiddleware(() => {
	const store = useJwtStore();

	if (!store.isAuth) {
		return navigateTo("/login");
	} else if (store.isActive) {
		return navigateTo("/");
	} else {
		return;
	}
});
