export default defineI18nConfig(() => ({
	legacy: false,
	locale: "en",
	messages: {
		en: {
			pages: {
				home: "Home",
				login: "Login page",
				"not-found": "Page not found",
				"forgot-password": {
					"explain-text":
						"Enter the email adress you used when you joined. We will send you an email with further instruction to reset your password",
					button: "Send reset instructions",
					"request-recieved-text":
						"Your reset password was succesfully recieved please check your email for futher instructions",
				},
				"reset-password": {
					title: "Reset password",
					"label-password": "Password",
					"label-confirm-password": "Confirm Password",
					"submit-button": "Submit",
				},
				"activate-user": {
					title: "Activate User",
					loading: "...loading",
				},
				"validate-email": {
					"intro-title": "Oops it seems your email has not been validated",
					"help-text":
						"check your email inbox or request a new activation email",
					button: "request activation email",
					"succes-response":
						"A new activation email has been send to your email, please check your inbox",
				},
				logout: {
					text: "Logged out",
				},
				"upload-page": "Upload Book",
				other: { desc: "other page" },
			},
			action: {
				bookUpload: {
					errorMessage: {
						noFileSelected: "No file was selected on the upload form",
						onlyOneFile: "Only one file can selected",
					},
					formLabel: "Choose an ebook:",
					submitButtonText: "Submit",
				},
			},
		},
	},
}));
