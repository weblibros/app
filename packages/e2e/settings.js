export const BASE_URL = process.env.E2E_BASE_URL || "http://0.0.0.0:5500";
export const API_URL = process.env.E2E_API_URL || "http://0.0.0.0:8500";
export const EMAIL_URL = process.env.E2E_EMAIL_URL || "http://0.0.0.0:1080";
