export const ACCESS_TOKEN_COOKIE_NAME = "webLibrosJwtAccessToken";
export const REFRESH_TOKEN_COOKIE_NAME = "webLibrosJwtRefreshToken";

export const USER_EMAIL = "user@fake.com";
export const UNACTIVE_USER_EMAIL = "non_active_user@fake.com";
export const NO_BOOKS_USER = "no_books_user@fake.com";
export const DUMMY_PASSWORD = "Q12345678!";
