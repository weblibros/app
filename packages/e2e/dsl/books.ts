import type { Page } from "@playwright/test";
import type { UploadBookDto } from "./dto";
import { UploadBookPage } from "../pages/uploadBookPage";
import { BookListPage } from "../pages/bookListPage";

class BookDsl {
	private _page: Page;
	uploadBookPage: UploadBookPage;
	bookListPage: BookListPage;

	constructor(page: Page) {
		this._page = page;
		this.uploadBookPage = new UploadBookPage(this._page);
		this.bookListPage = new BookListPage(this._page);
	}

	async uploadBook(book: UploadBookDto) {
		await this.uploadBookPage.goto();
		await this.uploadBookPage.isOnCurrentpage();
		await this.uploadBookPage.uploadBook(book);
		await this._page.waitForTimeout(500);
	}

	async bookIsUploaded(book: UploadBookDto) {
		await this.bookListPage.goto();
		await this.bookListPage.isOnCurrentpage();
		await this.bookListPage.bookIsOnPage(book.title);
	}

	async downloadBook(bookTitle: string) {
		await this.bookListPage.goto();
		await this.bookListPage.isOnCurrentpage();
		return await this.bookListPage.downloadBook(bookTitle);
	}
}
export default BookDsl;
