import { expect, type Page } from "@playwright/test";
import { SignupPage } from "../pages/signupPage";
import { LoginPage } from "../pages/loginPage";
import { RemoveAccountPage } from "../pages/removeAccount";
import { LogoutPage } from "../pages/logoutPage";
import { emailClearAll, emailGrabActivationLink } from "../services/devMail";
import {
	ACCESS_TOKEN_COOKIE_NAME,
	REFRESH_TOKEN_COOKIE_NAME,
	USER_EMAIL,
	DUMMY_PASSWORD,
} from "../constants";
import { BASE_URL } from "../settings";

class AccountDsl {
	private _page: Page;
	signupPage: SignupPage;
	removeAccountPage: RemoveAccountPage;
	logoutPage: LogoutPage;
	loginPage: LoginPage;

	constructor(page: Page) {
		this._page = page;
		this.signupPage = new SignupPage(page);
		this.loginPage = new LoginPage(page);
		this.logoutPage = new LogoutPage(page);
		this.removeAccountPage = new RemoveAccountPage(page);
	}

	async createUser(email: string, password: string) {
		await emailClearAll();
		await this.signupUser(email, password);
		await this.followActivationEmail(email);
		await this.loginUser(email, password);
		await this.isLoggedIn();
	}

	async followActivationEmail(email: string) {
		await this._page.waitForLoadState("networkidle");
		const activationLink = await emailGrabActivationLink(email);
		expect(activationLink).toContain(BASE_URL);
		await this._page.goto(activationLink);
		await this._page.waitForLoadState("networkidle");
	}

	async signupUser(email: string, password: string) {
		await this.signupPage.goto();
		await this._page.waitForLoadState("networkidle");
		await this.signupPage.isOnCurrentpage();
		await this.signupPage.signup(email, password);
	}

	async loginUser(email: string, password: string) {
		await this.loginPage.goto();
		await this._page.waitForLoadState("networkidle");
		await this.loginPage.isOnCurrentpage();
		await this.loginPage.loginUser(email, password);
	}

	async loginAsDefaultUser() {
		await this.loginUser(USER_EMAIL, DUMMY_PASSWORD);
		await this.isLoggedIn();
	}

	async removeUser() {
		await this.removeAccountPage.goto();
		await this._page.waitForLoadState("networkidle");
		await this.removeAccountPage.isOnCurrentpage();
		await this.removeAccountPage.clickRemoveAccount();
		await this._page.waitForLoadState("networkidle");
	}
	async isLoggedIn() {
		// wait for the cookie to be readin
		await this._page.waitForTimeout(50);

		const cookies = await this._page.context().cookies();
		const accessTokenCookie = cookies.filter(el => {
			return el.name === ACCESS_TOKEN_COOKIE_NAME;
		});
		const refreshTokenCookie = cookies.filter(el => {
			return el.name === REFRESH_TOKEN_COOKIE_NAME;
		});
		expect(accessTokenCookie[0].name).toBe(ACCESS_TOKEN_COOKIE_NAME);
		expect(refreshTokenCookie[0].name).toBe(REFRESH_TOKEN_COOKIE_NAME);
	}
	async isLoggedOut() {
		// wait for the cookie to be deleted
		await this._page.waitForTimeout(50);

		const cookies = await this._page.context().cookies();
		const accessTokenCookie = cookies.filter(el => {
			return el.name === ACCESS_TOKEN_COOKIE_NAME;
		});
		const refreshTokenCookie = cookies.filter(el => {
			return el.name === REFRESH_TOKEN_COOKIE_NAME;
		});
		expect(accessTokenCookie).toStrictEqual([]);
		expect(refreshTokenCookie).toStrictEqual([]);
	}
	async logout() {
		await this.logoutPage.goto();
		await this.isLoggedOut();
	}
}

export default AccountDsl;
