export interface UploadBookDto {
	title: string;
	path: string;
}
