import type { Page } from "@playwright/test";
import AccountDsl from "./account";
import BookDsl from "./books";

export default class Dsl {
	page: Page;
	account: AccountDsl;
	books: BookDsl;

	constructor(page: Page) {
		this.page = page;
		this.account = new AccountDsl(page);
		this.books = new BookDsl(page);
	}
}
