import { expect, test } from "@playwright/test";
import Dsl from "../../dsl";
import path from "node:path";
import fs from "node:fs";

test.beforeEach(async ({ page }) => {
	const dsl = new Dsl(page);
	await dsl.account.loginAsDefaultUser();
	await dsl.account.isLoggedIn();
});

test.afterEach(async ({ page }) => {
	const dsl = new Dsl(page);
	await dsl.account.logout();
});

test("User can download a book", async ({ page }) => {
	const dsl = new Dsl(page);
	const bookTitle = "Epic Tales of Adventure";
	const bookFilePath = path.join(
		path.dirname(path.dirname(__dirname)),
		"dummy_files",
		"ebooks",
		"Epic Tales of Adventure - Jeremy L. Lumpkin.epub"
	);

	const download = await dsl.books.downloadBook(bookTitle);
	const orginalBookFile = fs.readFileSync(bookFilePath);
	const downloadedBookFile = fs.readFileSync(await download.path());

	expect(orginalBookFile).toStrictEqual(downloadedBookFile);
});

test("User can read title in the filename of the downloaded a book", async ({
	page,
}) => {
	const dsl = new Dsl(page);
	const bookTitle = "Epic Tales of Adventure";

	const download = await dsl.books.downloadBook(bookTitle);
	expect(download.suggestedFilename()).toStrictEqual(
		"epic_tales_of_adventure.epub"
	);
});
