import { test } from "@playwright/test";
import Dsl from "../../dsl";
import path from "node:path";

test.beforeEach(async ({ page }) => {
	const dsl = new Dsl(page);
	await dsl.account.createUser("can_download_book@example.com", "password");
	await dsl.account.isLoggedIn();
});

test.afterEach(async ({ page }) => {
	const dsl = new Dsl(page);
	await dsl.account.removeUser();
	await dsl.account.isLoggedOut();
});

test("User can upload a book", async ({ page }) => {
	const dsl = new Dsl(page);
	const uploadBook = {
		title: "The Boy Who Ate Too Much Chocolate and Became a Wizard",
		path: path.join(
			path.dirname(path.dirname(__dirname)),
			"dummy_files",
			"ebooks",
			"Boy_Who_Ate_Too_Much_Chocolate_and_Became_a_Wizard__The_Harry_Darkbrown.epub"
		),
	};
	await dsl.books.uploadBook(uploadBook);
	await dsl.books.bookIsUploaded(uploadBook);
});
