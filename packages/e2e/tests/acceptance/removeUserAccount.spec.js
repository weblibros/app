import { test } from "@playwright/test";
import Dsl from "../../dsl";

test("A user can remove his account", async ({ page }) => {
	const dsl = new Dsl(page);
	await dsl.account.createUser("can_remove_account@example.com", "password");
	await dsl.account.removeUser();
	await dsl.account.isLoggedOut();
	await dsl.account.loginPage.isOnCurrentpage();
});
test("A anonymous user cannot access the remove page", async ({ page }) => {
	const dsl = new Dsl(page);
	await dsl.account.removeAccountPage.goto();
	await dsl.account.loginPage.isOnCurrentpage();
});
