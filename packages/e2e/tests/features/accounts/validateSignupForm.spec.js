import { expect, test } from "@playwright/test";
import { SignupPage } from "../../../pages/signupPage";

test.describe("Signup of user behaviour", () => {
	test("cannot submit at load page", async ({ page }) => {
		const signupPage = new SignupPage(page);
		await signupPage.goto();
		await expect(signupPage.submitButton).toBeDisabled();
	});
	test("cannot submit when password is only filled", async ({ page }) => {
		const signupPage = new SignupPage(page);
		await signupPage.goto();
		await signupPage.passwordInput.type("new-secret-password1!");
		await expect(signupPage.submitButton).toBeDisabled();
	});
	test("cannot submit when email is only filled", async ({ page }) => {
		const signupPage = new SignupPage(page);
		await signupPage.goto();
		await signupPage.emailInput.type("onlyEmailFilledIn@fake.com");
		await expect(signupPage.submitButton).toBeDisabled();
	});
	test("cannot submit when password is to short", async ({ page }) => {
		const signupPage = new SignupPage(page);
		await signupPage.goto();
		await signupPage.emailInput.type("onlyEmailFilledIn@fake.com");
		await signupPage.passwordInput.type("short");
		await expect(signupPage.submitButton).toBeDisabled();
	});
	test("cannot submit when email is not correct", async ({ page }) => {
		const signupPage = new SignupPage(page);
		await signupPage.goto();
		await signupPage.passwordInput.type("new-secret-password1!");
		await signupPage.emailInput.type("thisIsNotAnEmail");
		await expect(signupPage.submitButton).toBeDisabled();
	});
});
