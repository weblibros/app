import { expect, test } from "@playwright/test";
import { LoginPage } from "../../../pages/loginPage";
import { ActivatePage } from "../../../pages/activatePage";
import { BASE_URL } from "../../../settings";

test.describe("activation of user account", () => {
	test("send activation token to validate api url on page load", async ({
		page,
	}) => {
		const activatePage = new ActivatePage(page);
		await activatePage.mockValidateRequest();
		const responsePromise = page.waitForResponse(
			response => {
				return (
					response.url().includes(`${activatePage.validateApiUrl}`) &&
					response.request().postDataJSON()?.activation_token ===
						activatePage.mockedToken
				);
			},
			{ timeout: 60000 }
		);
		await activatePage.goto();
		await responsePromise;
	});
	test("redirect to login after validate api url request when no user is stored in cookies", async ({
		page,
	}) => {
		const activatePage = new ActivatePage(page);
		await activatePage.mockValidateRequest();
		await activatePage.goto();
		await page.waitForURL("/login");
		expect(page.url()).toBe(`${BASE_URL}/login`);
	});
	test("redirect to home after validate api url request when no user is stored in cookies", async ({
		page,
	}) => {
		await new LoginPage(page).login();
		const activatePage = new ActivatePage(page);
		await activatePage.mockValidateRequest();
		await activatePage.goto();
		await page.waitForURL("/");
		expect(page.url()).toBe(`${BASE_URL}/`);
	});
	test("refresh jwt token after activation request for active user", async ({
		page,
	}) => {
		await new LoginPage(page).login();
		const activatePage = new ActivatePage(page);
		await activatePage.mockValidateRequest();
		const responsePromise = page.waitForResponse(
			response => {
				return (
					response.url().includes("/api/v1/auth/token/refresh") &&
					response.status() === 200
				);
			},
			{ timeout: 60000 }
		);
		await activatePage.goto();
		await responsePromise;
	});
	test("refresh jwt token after activation request for  unactive user", async ({
		page,
	}) => {
		await new LoginPage(page).login_unactived_user();
		const activatePage = new ActivatePage(page);
		await activatePage.mockValidateRequest();
		const responsePromise = page.waitForResponse(
			response => {
				return response.url().includes("/api/v1/auth/token/refresh");
			},
			{ timeout: 60000 }
		);
		await activatePage.goto();
		await responsePromise;
	});
	test("no token refreshed when no user was saved in cookies", async ({
		page,
	}) => {
		let refreshTokenHasBeenCalled = false;
		page.on("request", request => {
			if (request.url().includes("/api/v1/auth/token/refresh")) {
				refreshTokenHasBeenCalled = true;
			}
		});
		const activatePage = new ActivatePage(page);
		await activatePage.mockValidateRequest();
		await activatePage.goto();
		expect(refreshTokenHasBeenCalled).toBe(false);
	});
});
