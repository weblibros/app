import { expect, test } from "@playwright/test";
import { BASE_URL } from "../../../settings";

test("redirected to login page if user is not loged in", async ({ page }) => {
	await page.goto("/");
	await page.waitForURL("/login");
	expect(page.url()).toBe(`${BASE_URL}/login`);
});
