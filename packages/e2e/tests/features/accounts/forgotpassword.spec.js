import { expect, test } from "@playwright/test";
import { LoginPage } from "../../../pages/loginPage";
import { ForgotPasswordPage } from "../../../pages/forgotPasswordPage";

import { BASE_URL } from "../../../settings";
import { USER_EMAIL } from "../../../constants";
import {
	emailClearAll,
	emailSubjectLatestEmailTo,
} from "../../../services/devMail";

test("route to forgot password page after clicking link", async ({ page }) => {
	const loginPage = new LoginPage(page);
	await loginPage.goto();
	await loginPage.clickForgotPasswordLink();
	await page.waitForURL("/forgot-password");
	expect(page.url()).toBe(`${BASE_URL}/forgot-password`);
});

test("recieve email request after sunbmiting useremail", async ({ page }) => {
	await emailClearAll();
	const forgotPasswordPage = new ForgotPasswordPage(page);
	await forgotPasswordPage.goto();
	await forgotPasswordPage.fillinEmail(USER_EMAIL);
	const emailSubject = await emailSubjectLatestEmailTo(USER_EMAIL);
	expect(emailSubject).toEqual("Reset User Password WebLibros");
	await emailClearAll();
});
