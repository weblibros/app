import { expect, test } from "@playwright/test";
import { SignupPage } from "../../../pages/signupPage";
import {
	emailClearAll,
	emailSubjectLatestEmailTo,
} from "../../../services/devMail";

const uniqueUserEmail = function () {
	const uid = Date.now().toString(36) + Math.random().toString(36).substr(2);
	return `new-user-${uid}@fake.com`;
};

test.beforeEach(async () => {
	await emailClearAll();
});
test.afterEach(async () => {
	await emailClearAll();
});
test.describe("Signup of user behaviour", () => {
	test("user can signup", async ({ page }) => {
		const signupPage = new SignupPage(page);
		await signupPage.goto();
		await signupPage.signup(uniqueUserEmail(), "devPassword");
	});
	test("user recieved activation email after signup", async ({ page }) => {
		const userEmail = uniqueUserEmail();
		const signupPage = new SignupPage(page);
		await signupPage.goto();
		await signupPage.signup(userEmail, "devPassword");
		const emailSubject = await emailSubjectLatestEmailTo(userEmail);
		expect(emailSubject).toEqual("Activate user WebLibros");
	});
});
