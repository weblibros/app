import { expect, test } from "@playwright/test";
import { LoginPage } from "../../../pages/loginPage";
import { BASE_URL } from "../../../settings";

test("redirected to detail page after login", async ({ page }) => {
	await new LoginPage(page).login();
	await page.waitForURL("/user/me");
	expect(page.url()).toBe(`${BASE_URL}/user/me`);
});
