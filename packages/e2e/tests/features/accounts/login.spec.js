import { test, expect } from "@playwright/test";
import { LoginPage } from "../../../pages/loginPage";
import {
	ACCESS_TOKEN_COOKIE_NAME,
	REFRESH_TOKEN_COOKIE_NAME,
	USER_EMAIL,
	DUMMY_PASSWORD,
} from "../../../constants";
import { BASE_URL } from "../../../settings";

test("test user can login", async ({ page }) => {
	await page.goto("/login");
	await page.getByTestId("login-input-email").type(USER_EMAIL);
	await page.getByTestId("login-input-password").type(DUMMY_PASSWORD);

	const responsePromise = page.waitForResponse(
		response =>
			response.url().includes("api/v1/auth/login") && response.status() == 200,
		{ timeout: 60000 }
	);
	await page.getByTestId("login-submit-form").click();
	await responsePromise;
	await page.waitForTimeout(50);
	const cookies = await page.context().cookies();
	const accessTokenCookie = cookies.filter(el => {
		return el.name === ACCESS_TOKEN_COOKIE_NAME;
	});
	const refreshTokenCookie = cookies.filter(el => {
		return el.name === REFRESH_TOKEN_COOKIE_NAME;
	});
	expect(accessTokenCookie[0].name).toBe(ACCESS_TOKEN_COOKIE_NAME);
	expect(refreshTokenCookie[0].name).toBe(REFRESH_TOKEN_COOKIE_NAME);
});

test("test get notification on failed login", async ({ page }) => {
	await page.goto("/login");
	await page.getByTestId("login-input-email").type(USER_EMAIL);
	await page.getByTestId("login-input-password").type("wrong password");

	const responsePromise = page.waitForResponse(
		response =>
			response.url().includes("api/v1/auth/login") && response.status() == 403,
		{ timeout: 60000 }
	);
	await page.getByTestId("login-submit-form").click();
	await responsePromise;
	await expect(page.getByTestId("login-error-message")).toContainText("failed");
});

test("user is able to resubmit login after failed attempt", async ({
	page,
}) => {
	await page.goto("/login");
	const passwordInput = page.getByTestId("login-input-password");
	const submitInput = page.getByTestId("login-submit-form");
	await page.getByTestId("login-input-email").type(USER_EMAIL);
	await passwordInput.type("wrong password");

	await expect(submitInput).toBeEnabled();

	const responsePromise = page.waitForResponse(
		response =>
			response.url().includes("api/v1/auth/login") && response.status() == 403,
		{ timeout: 60000 }
	);
	await page.getByTestId("login-submit-form").click();
	await responsePromise;
	await expect(page.getByTestId("login-error-message")).toContainText("failed");

	await passwordInput.clear();
	await expect(submitInput).toBeDisabled();
	await passwordInput.type(DUMMY_PASSWORD);
	await expect(submitInput).toBeEnabled();

	const responsePromiseSecond = page.waitForResponse(
		response =>
			response.url().includes("api/v1/auth/login") && response.status() == 200,
		{ timeout: 60000 }
	);
	await page.getByTestId("login-submit-form").click();
	await responsePromiseSecond;

	await page.waitForTimeout(50);
	const cookies = await page.context().cookies();
	const accessTokenCookie = cookies.filter(el => {
		return el.name === ACCESS_TOKEN_COOKIE_NAME;
	});
	const refreshTokenCookie = cookies.filter(el => {
		return el.name === REFRESH_TOKEN_COOKIE_NAME;
	});
	expect(accessTokenCookie[0]).toHaveProperty("name", ACCESS_TOKEN_COOKIE_NAME);
	expect(refreshTokenCookie[0]).toHaveProperty(
		"name",
		REFRESH_TOKEN_COOKIE_NAME
	);
});
test("route to signup page after clicking link", async ({ page }) => {
	const loginPage = new LoginPage(page);
	await loginPage.goto();
	await loginPage.clickSignupLink();
	await page.waitForURL("/signup");
	expect(page.url()).toBe(`${BASE_URL}/signup`);
});
