import { expect, test } from "@playwright/test";
import { LoginPage } from "../../../pages/loginPage";
import { LogoutPage } from "../../../pages/logoutPage";
import { BASE_URL } from "../../../settings";
import {
	ACCESS_TOKEN_COOKIE_NAME,
	REFRESH_TOKEN_COOKIE_NAME,
} from "../../../constants";

test.describe("Logout of user behaviour", () => {
	test("anoymous user can visit logout page", async ({ page }) => {
		const logoutPage = new LogoutPage(page);
		await logoutPage.goto();
		expect(page.url()).toContain(`${BASE_URL}/logout`);
	});
	test("jwt token cookies are deleted", async ({ page }) => {
		await new LoginPage(page).login();
		await page.waitForTimeout(50);
		const cookies = await page.context().cookies();
		const accessTokenCookie = cookies.filter(el => {
			return el.name === ACCESS_TOKEN_COOKIE_NAME;
		});
		const refreshTokenCookie = cookies.filter(el => {
			return el.name === REFRESH_TOKEN_COOKIE_NAME;
		});
		expect(accessTokenCookie[0]).toHaveProperty(
			"name",
			ACCESS_TOKEN_COOKIE_NAME
		);
		expect(refreshTokenCookie[0]).toHaveProperty(
			"name",
			REFRESH_TOKEN_COOKIE_NAME
		);

		const logoutPage = new LogoutPage(page);
		await logoutPage.goto();

		await page.waitForTimeout(50);
		const cookiesAfter = await page.context().cookies();
		const accessTokenCookieAfter = cookiesAfter.filter(el => {
			return el.name === ACCESS_TOKEN_COOKIE_NAME;
		});
		const refreshTokenCookieAfter = cookiesAfter.filter(el => {
			return el.name === REFRESH_TOKEN_COOKIE_NAME;
		});
		expect(accessTokenCookieAfter).toStrictEqual([]);
		expect(refreshTokenCookieAfter).toStrictEqual([]);
	});
});
