import { expect, test } from "@playwright/test";
import { ResetPasswordPage } from "../../../pages/resetPasswordPage";
import { BASE_URL } from "../../../settings";

test.describe("Reset User password", () => {
	test("User can fill in reset password for,", async ({ page }) => {
		const resetPasswordPage = new ResetPasswordPage(page);
		await resetPasswordPage.goto();
		await resetPasswordPage.mockresetPasswordRequest();
		await resetPasswordPage.typePassword("dummypassword");
		await resetPasswordPage.submit();
		await page.waitForURL("/login");
		expect(page.url()).toBe(`${BASE_URL}/login`);
	});
	test("token and password are send", async ({ page }) => {
		const newPassword = "dummypassword2";
		const resetPasswordPage = new ResetPasswordPage(page);
		await resetPasswordPage.goto();
		await resetPasswordPage.mockresetPasswordRequest();
		await resetPasswordPage.typePassword(newPassword);
		const responsePromise = page.waitForResponse(
			response => {
				return (
					response.url().includes(`${resetPasswordPage.resetPasswordApiUrl}`) &&
					response.request().postDataJSON()?.reset_token ===
						resetPasswordPage.mockedToken &&
					response.request().postDataJSON()?.password === newPassword
				);
			},
			{ timeout: 60000 }
		);
		await resetPasswordPage.submit();
		await responsePromise;
		await page.waitForURL("/login");
		expect(page.url()).toBe(`${BASE_URL}/login`);
	});
});

test("is redirect once token is not present in url", async ({ page }) => {
	const resetPasswordPage = new ResetPasswordPage(page);
	await resetPasswordPage.gotoNoToken();

	await page.waitForURL("/login");
	expect(page.url()).toBe(`${BASE_URL}/login`);
});

test.describe("Submit button is oly active when all condition are met", () => {
	test("submit is disable on page load", async ({ page }) => {
		const resetPasswordPage = new ResetPasswordPage(page);
		await resetPasswordPage.goto();
		await expect(resetPasswordPage.submitButton).toBeDisabled();
	});
	test("submit is disable if only the primary password is presented", async ({
		page,
	}) => {
		const resetPasswordPage = new ResetPasswordPage(page);
		await resetPasswordPage.goto();
		await resetPasswordPage.primaryPasswordInput.type("funpassword");
		await expect(resetPasswordPage.submitButton).toBeDisabled();
	});
	test("submit is disable if only the confirm password is presented", async ({
		page,
	}) => {
		const resetPasswordPage = new ResetPasswordPage(page);
		await resetPasswordPage.goto();
		await resetPasswordPage.confirmPasswordInput.type("funpassword");
		await expect(resetPasswordPage.submitButton).toBeDisabled();
	});
	test("submit is disable if the primary and confirm password are not the same", async ({
		page,
	}) => {
		const resetPasswordPage = new ResetPasswordPage(page);
		await resetPasswordPage.goto();
		await resetPasswordPage.primaryPasswordInput.type("funpassword");
		await resetPasswordPage.confirmPasswordInput.type("funpasswordDiff");
		await expect(resetPasswordPage.submitButton).toBeDisabled();
	});
	test("submit is enable if the primary and confirm password are the same and present", async ({
		page,
	}) => {
		const resetPasswordPage = new ResetPasswordPage(page);
		await resetPasswordPage.goto();
		await resetPasswordPage.primaryPasswordInput.type("funpassword");
		await resetPasswordPage.confirmPasswordInput.type("funpassword");
		await expect(resetPasswordPage.submitButton).toBeEnabled();
	});
});
