import { expect, test } from "@playwright/test";
import { LoginPage } from "../../../pages/loginPage";
import { IndexPage } from "../../../pages/indexPage";

test.describe("check ebooks on index page", () => {
	test("User has  6 books", async ({ page }) => {
		const loginPage = new LoginPage(page);
		await loginPage.login();
		const indexPage = new IndexPage(page);
		await indexPage.goto();
		await page.waitForLoadState("networkidle");
		await expect(indexPage.getBookElement()).toHaveCount(6);
	});
});
