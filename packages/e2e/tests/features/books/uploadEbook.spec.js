import { expect, test } from "@playwright/test";
import { LoginPage } from "../../../pages/loginPage";
import { UploadPage } from "../../../pages/uploadFormPage";
import { IndexPage } from "../../../pages/indexPage";
import { cleanupBooks } from "../../../services/cleanupBooks";

test.describe("Upload ebook", () => {
	test.afterEach(async ({ page }) => {
		await cleanupBooks(page);
	});
	test("One Book Is uploaded", async ({ page }) => {
		const loginPage = new LoginPage(page);
		await loginPage.login_no_books_user();
		const indexPage = new IndexPage(page);
		await indexPage.goto();
		await page.waitForLoadState("networkidle");
		await expect(indexPage.getBookElement()).toHaveCount(0);
		const uploadPage = new UploadPage(page);
		await uploadPage.goto();
		await page.waitForLoadState("networkidle");
		await uploadPage.uploadFile();
		await page.waitForLoadState("networkidle");
		await page.waitForTimeout(1000);
		await indexPage.goto();
		await page.waitForLoadState("networkidle");
		await expect(indexPage.getBookElement()).toHaveCount(1);
	});
});
