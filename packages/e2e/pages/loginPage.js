import { expect } from "@playwright/test";

const {
	USER_EMAIL,
	UNACTIVE_USER_EMAIL,
	DUMMY_PASSWORD,
	NO_BOOKS_USER,
} = require("../constants");

export class LoginPage {
	/**
	 * @param {import('@playwright/test').Page} page
	 */
	constructor(page) {
		this.page = page;
		this.url = "/login";
		this.passwordInput = this.page.getByTestId("login-input-password");
		this.emailInput = this.page.getByTestId("login-input-email");
		this.submitButton = this.page.getByTestId("login-submit-form");
		this.forgotPasswordLink = this.page.getByTestId("route-forgot-password");
		this.signUpLink = this.page.getByTestId("route-signup");
	}

	async goto() {
		await this.page.goto(this.url);
	}

	async login() {
		await this.login_with_user_password(USER_EMAIL, DUMMY_PASSWORD);
	}

	async login_unactived_user() {
		await this.login_with_user_password(UNACTIVE_USER_EMAIL, DUMMY_PASSWORD);
	}

	async login_no_books_user() {
		await this.login_with_user_password(NO_BOOKS_USER, DUMMY_PASSWORD);
	}

	async loginUser(userName, password) {
		await this.login_with_user_password(userName, password);
	}
	async login_with_user_password(userName, password) {
		await this.goto();
		await this.passwordInput.type(password);
		await this.emailInput.type(userName);
		const responsePromise = this.page.waitForResponse(
			response =>
				response.url().includes("api/v1/auth/login") &&
				response.status() == 200,
			{ timeout: 60000 }
		);
		await this.submitButton.click();
		await responsePromise;
	}

	async clickForgotPasswordLink() {
		await this.forgotPasswordLink.click();
	}

	async clickSignupLink() {
		await this.signUpLink.click();
	}

	async isOnCurrentpage() {
		await this.page.waitForLoadState("networkidle");
		expect(this.page.url()).toContain(this.url);
	}
}
