const path = require("node:path");

export class UploadPage {
	/**
	 * @param {import('@playwright/test').Page} page
	 */
	constructor(page) {
		this.page = page;
		this.uploadInput = this.page.getByTestId("upload-input-file");
		this.submitButton = this.page.getByTestId("upload-submit-form");
	}

	async goto() {
		await this.page.waitForLoadState("networkidle");
		await this.page.goto("/upload-page");
		await this.page.waitForLoadState("networkidle");
	}

	async uploadFile() {
		const ebookFile = path.join(
			path.dirname(__dirname),
			"dummy_files",
			"ebooks",
			"Boy_Who_Ate_Too_Much_Chocolate_and_Became_a_Wizard__The_Harry_Darkbrown.epub"
		);
		await this.uploadInput.setInputFiles(ebookFile);
		await this.submitButton.click();
	}
}
