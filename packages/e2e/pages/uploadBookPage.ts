import { expect, type Locator, type Page } from "@playwright/test";
import type { UploadBookDto } from "../dsl/dto";

export class UploadBookPage {
	private page: Page;
	private uploadInput: Locator;
	private submitButton: Locator;
	url: string;

	constructor(page: Page) {
		this.page = page;
		this.url = "/upload-page";
		this.uploadInput = this.page.getByTestId("upload-input-file");
		this.submitButton = this.page.getByTestId("upload-submit-form");
	}

	async goto() {
		await this.page.waitForLoadState("networkidle");
		await this.page.goto(this.url);
		await this.page.waitForLoadState("networkidle");
	}

	async uploadBook(book: UploadBookDto) {
		await this.uploadInput.setInputFiles(book.path);
		const responsePromise = this.page.waitForResponse(
			response =>
				response.url().includes("/api/v1/file/upload/ebook") &&
				response.status() == 201,
			{ timeout: 60000 }
		);
		await this.submitButton.click();
		await responsePromise;
	}
	async isOnCurrentpage() {
		await this.page.waitForLoadState("networkidle");
		expect(new URL(this.page.url()).pathname).toContain(this.url);
	}
}
