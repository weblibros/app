export class ResetPasswordPage {
	/**
	 * @param {import('@playwright/test').Page} page
	 */
	constructor(page) {
		this.page = page;
		this.mockedToken = "faketoken";
		this.primaryPasswordInput = this.page.getByTestId(
			"reset-password-input-password"
		);
		this.confirmPasswordInput = this.page.getByTestId(
			"reset-password-input-confirm-password"
		);
		this.submitButton = this.page.getByTestId("reset-password-button");
		this.resetPasswordApiUrl = "api/v1/user/reset-password/token";
	}
	async mockresetPasswordRequest() {
		await this.page.route(`*/**/${this.resetPasswordApiUrl}`, async route => {
			const json = { user_uuid: "mocked" };
			await route.fulfill({ json });
		});
	}

	async goto() {
		await this.page.waitForLoadState("networkidle");
		await this.page.goto(`/reset-password?token=${this.mockedToken}`);
		await this.page.waitForLoadState("networkidle");
	}
	async gotoNoToken() {
		await this.page.waitForLoadState("networkidle");
		await this.page.goto(`/reset-password`);
		await this.page.waitForLoadState("networkidle");
	}

	async typePassword(password) {
		await this.primaryPasswordInput.type(password);
		await this.confirmPasswordInput.type(password);
	}

	async submit() {
		await this.submitButton.click();
	}
}
