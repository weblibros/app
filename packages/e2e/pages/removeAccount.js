import { expect } from "@playwright/test";

export class RemoveAccountPage {
	/**
	 * @param {import('@playwright/test').Page} page
	 */
	constructor(page) {
		this.page = page;
		this.url = "/user/remove";
		this.removeAccountButton = this.page.getByTestId("remove-account-button");
	}

	async goto() {
		await this.page.waitForLoadState("networkidle");
		await this.page.goto(this.url);
		await this.page.waitForLoadState("networkidle");
	}

	async clickRemoveAccount() {
		const responsePromise = this.page.waitForResponse(
			response =>
				response.url().includes("/api/v1/user/me") && response.status() == 202,
			{ timeout: 60000 }
		);
		await this.removeAccountButton.click();
		await responsePromise;
	}

	async isOnCurrentpage() {
		await this.page.waitForLoadState("networkidle");
		expect(this.page.url()).toContain(this.url);
	}
}
