export class ForgotPasswordPage {
	/**
	 * @param {import('@playwright/test').Page} page
	 */
	constructor(page) {
		this.page = page;
		this.emailInput = this.page.getByTestId("forgot-password-input-email");
		this.submitButton = this.page.getByTestId("forgot-password-button");
	}

	async goto() {
		await this.page.waitForLoadState("networkidle");
		await this.page.goto("/forgot-password");
		await this.page.waitForLoadState("networkidle");
	}

	async fillinEmail(email) {
		await this.emailInput.type(email);
		await this.submitButton.click();
	}
}
