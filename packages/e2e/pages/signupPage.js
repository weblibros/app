import { expect } from "@playwright/test";

export class SignupPage {
	/**
	 * @param {import('@playwright/test').Page} page
	 */
	constructor(page) {
		this.page = page;
		this.url = "/signup";
		this.passwordInput = this.page.getByTestId("signup-input-password");
		this.emailInput = this.page.getByTestId("signup-input-email");
		this.submitButton = this.page.getByTestId("signup-submit-form");
	}

	async goto() {
		await this.page.waitForLoadState("networkidle");
		await this.page.goto(this.url);
		await this.page.waitForLoadState("networkidle");
	}

	async signup(userName, password) {
		await this.passwordInput.type(password);
		await this.emailInput.type(userName);
		const responsePromise = this.page.waitForResponse(
			response =>
				response.url().includes("api/v1/user/signup") &&
				response.status() == 201,
			{ timeout: 60000 }
		);
		await this.submitButton.click();
		await responsePromise;
	}

	async isOnCurrentpage() {
		await this.page.waitForLoadState("networkidle");
		expect(this.page.url()).toContain(this.url);
	}
}
