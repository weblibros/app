export class IndexPage {
	/**
	 * @param {import('@playwright/test').Page} page
	 */
	constructor(page) {
		this.page = page;
		this.bookElement = this.page.getByTestId("bookElement");
	}

	async goto() {
		await this.page.waitForLoadState("networkidle");
		await this.page.goto("/");
		await this.page.waitForLoadState("networkidle");
	}

	getBookElement() {
		return this.bookElement;
	}
}
