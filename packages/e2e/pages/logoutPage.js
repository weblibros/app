export class LogoutPage {
	/**
	 * @param {import('@playwright/test').Page} page
	 */
	constructor(page) {
		this.page = page;
	}

	async goto() {
		await this.page.waitForLoadState("networkidle");
		await this.page.goto("/logout");
		await this.page.waitForLoadState("networkidle");
	}
}
