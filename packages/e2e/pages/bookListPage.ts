import { expect, type Locator, type Page } from "@playwright/test";

export class BookListPage {
	private page: Page;
	url: string;
	bookElement: Locator;
	bookList: Locator;

	constructor(page: Page) {
		this.page = page;
		this.url = "/";
		this.bookElement = this.page.getByTestId("bookElement");
	}

	async goto() {
		await this.page.goto(this.url);
		await this.page.waitForLoadState("networkidle");
	}

	async downloadBook(title: string) {
		await this.bookIsOnPage(title);
		const downloadButton = this.bookElement
			.filter({ hasText: title })
			.first()
			.getByTestId("actionElement-download");
		const downloadPromise = this.page.waitForEvent("download");
		await downloadButton.click();
		const download = await downloadPromise;
		return download;
	}

	async bookIsOnPage(title: string) {
		const bookTitle = await this.bookElement
			.getByRole("heading", { name: title })
			.textContent();
		expect(bookTitle).toStrictEqual(title);
	}

	async isOnCurrentpage() {
		await this.page.waitForLoadState("networkidle");
		expect(new URL(this.page.url()).pathname).toEqual(this.url);
	}
}
