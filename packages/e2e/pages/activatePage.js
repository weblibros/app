export class ActivatePage {
	/**
	 * @param {import('@playwright/test').Page} page
	 */
	constructor(page) {
		this.page = page;
		this.mockedToken = "faketoken";
		this.validateApiUrl = "api/v1/user/email/validate";
	}

	async mockValidateRequest() {
		await this.page.route(`*/**/${this.validateApiUrl}`, async route => {
			const json = [{ message: "mocked" }];
			await route.fulfill({ json });
		});
	}

	async goto() {
		await this.page.waitForLoadState("networkidle");
		await this.page.goto(`/user/activate?token=${this.mockedToken}`);
		await this.page.waitForLoadState("networkidle");
	}
}
