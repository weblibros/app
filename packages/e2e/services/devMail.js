const { EMAIL_URL, BASE_URL } = require("../settings");

const sleep = ms => new Promise(r => setTimeout(r, ms));

class EmailAdress {
	constructor(address, name) {
		this.address = address;
		this.name = name;
	}
}

const emailAddressFactory = function (rawData) {
	const address = rawData.address;
	const name = rawData.name;
	return new EmailAdress(address, name);
};

class EmailMessage {
	constructor(id, time, to, subject, htmlBody) {
		this.id = id;
		this.time = time;
		this.to = to;
		this.subject = subject;
		this.htmlBody = htmlBody;
	}
	wasSendTo(toEmailAddress) {
		return this.to.find(
			emailAddress => emailAddress.address === toEmailAddress
		);
	}
}

const emailMessageFactory = function (rawData) {
	const to = rawData.to.map(rawAdress => {
		return emailAddressFactory(rawAdress);
	});
	return new EmailMessage(
		rawData.id,
		rawData.time,
		to,
		rawData.subject,
		rawData.html
	);
};

const emailSendTo = async function (emailAdress, retrySend = 0) {
	const response = await fetch(`${EMAIL_URL}/email`);
	if (response.status !== 200) {
		throw new Error(
			`wrong email response ${response.status} for request to devmail`
		);
	}
	const responseJson = await response.json();
	const emailList = responseJson
		.map(rawMessage => emailMessageFactory(rawMessage))
		.reverse();
	for (let i = 0; i < emailList.length; i++) {
		const emailMessage = emailList[i];
		if (emailMessage.wasSendTo(emailAdress)) {
			return emailMessage;
		}
	}
	if (retrySend < 5) {
		await sleep(1000);
		const retrySendPlus = retrySend + 1;
		return await emailSendTo(emailAdress, retrySendPlus);
	}
};

export const emailSubjectLatestEmailTo = async function (emailAdress) {
	const emailMessage = await emailSendTo(emailAdress);
	if (!emailMessage) {
		throw new Error(`no email found for ${emailAdress}`);
	}
	return emailMessage.subject;
};

const replaceHostWithTestHost = function (link) {
	const url = new URL(link);
	return `${BASE_URL}${url.pathname}${url.search}`;
};

export const emailGrabActivationLink = async function (emailAdress) {
	const emailMessage = await emailSendTo(emailAdress);
	if (!emailMessage) {
		throw new Error(`no email found for ${emailAdress}`);
	}
	if (!emailMessage.subject.includes("Activate")) {
		throw new Error(
			`no activation email found for ${emailAdress} with subject ${emailMessage.subject}`
		);
	}
	const htmlBody = emailMessage.htmlBody;
	const activationLink = htmlBody.match(/href="([^"]*)"/)[1];
	return replaceHostWithTestHost(activationLink);
};

export const emailClearAll = async function () {
	const response = await fetch(`${EMAIL_URL}/email/all`, { method: "DELETE" });
	if (response.status !== 200) {
		throw new Error(`wrong clear all response ${response.status}`);
	}
};
