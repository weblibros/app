const { API_URL } = require("../settings");

class BookItem {
	constructor(uuid, title) {
		this.uuid = uuid;
		this.title = title;
	}
}
class BookPage {
	constructor(items, total, page, size, pages) {
		this.items = items;
		this.total = total;
		this.page = page;
		this.size = size;
		this.pages = pages;
	}
	book_uuids() {
		return this.items.map(el => el.uuid);
	}
}
const findBooks = async function (auth_header) {
	const getMethod = {
		method: "GET",
		headers: {
			"Content-type": "application/json; charset=UTF-8",
			...auth_header,
		},
	};
	const response = await fetch(`${API_URL}/api/v1/books?size=100`, getMethod);
	if (response.status !== 200) {
		throw new Error("wrong response");
	}
	const responseJson = await response.json();
	const book_page = new BookPage(
		responseJson.items.map(el => new BookItem(el.uuid, el.title)),
		responseJson.total,
		responseJson.page,
		responseJson.size,
		responseJson.pages
	);
	return book_page.book_uuids();
};
const removeBook = async function (book_uuid, auth_header) {
	const deleteMethod = {
		method: "DELETE", // Method itself
		headers: {
			"Content-type": "application/json; charset=UTF-8",
			...auth_header,
		},
	};
	const url = `${API_URL}/api/v1/books/${book_uuid}`;
	await fetch(url, deleteMethod);
};

const get_auth_headers = async function (page) {
	const cookies = await page.context().cookies();
	const auth_token = cookies.find(
		el => el.name === "webLibrosJwtAccessToken"
	).value;

	return {
		Authorization: "Bearer " + auth_token,
	};
};
export const cleanupBooks = async function (page) {
	const auth_header = await get_auth_headers(page);
	const books = await findBooks(auth_header);
	for (let i = 0; i < books.length; i++) {
		await removeBook(books[i], auth_header);
	}
};
