from dataclasses import dataclass
from datetime import datetime
from typing import TYPE_CHECKING, Optional, Self
from uuid import UUID

from fastapi_events.typing import Event

if TYPE_CHECKING:
    from src.books.repositories.models import BookModel


@dataclass(frozen=True)
class BookItemDTO:
    uuid: UUID
    title: str
    cover_path: Optional[str]
    download_url: Optional[str]


@dataclass(frozen=True)
class BookDetailDTO:
    uuid: UUID
    title: str

    @classmethod
    def from_model(cls, book_model: "BookModel") -> Self:
        return cls(uuid=book_model.uuid, title=book_model.title)


@dataclass(frozen=True)
class AuthorBookUploadEventDTO:
    first_name: str
    last_name: str

    @classmethod
    def from_dict(cls, raw_dict: dict) -> Self:
        return cls(first_name=raw_dict["first_name"], last_name=raw_dict["last_name"])


@dataclass(frozen=True)
class IdentifierBookUploadEventDTO:
    key: str
    value: str

    @classmethod
    def from_dict(cls, raw_dict: dict) -> Self:
        return cls(key=raw_dict["key"], value=raw_dict["value"])


@dataclass(frozen=True)
class LanguageDTO:
    value: str


@dataclass(frozen=True)
class BookUploadedEventDTO:
    file_uuid: UUID
    user_uuid: UUID
    book_uuid: UUID
    library_uuid: UUID
    authors: list[AuthorBookUploadEventDTO]
    identifiers: list[IdentifierBookUploadEventDTO]
    title: str
    title_sort: Optional[str]
    rights: Optional[str]
    published: Optional[datetime]
    language: Optional[LanguageDTO]

    @classmethod
    def from_event(cls, event: Event) -> Self:
        _, payload = event

        return cls(
            file_uuid=payload["file_uuid"],
            user_uuid=payload["user_uuid"],
            book_uuid=payload["book_uuid"],
            library_uuid=payload["library_uuid"],
            authors=[AuthorBookUploadEventDTO.from_dict(raw_dict) for raw_dict in payload.get("authors", [])],
            identifiers=[
                IdentifierBookUploadEventDTO.from_dict(raw_dict) for raw_dict in payload.get("identifiers", [])
            ],
            title=payload["title"],
            title_sort=payload.get("title_sort", None),
            rights=payload.get("rights", None),
            published=payload.get("published", None),
            language=(LanguageDTO(payload["language"]) if payload.get("language") else None),
        )
