from typing import TYPE_CHECKING, Callable, Type
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import RedirectResponse
from pydantic import BaseModel

from src.books import urls
from src.books.domain.events import DownloadLinkBookCmd
from src.books.permissions import BookPermissions
from src.books.unit_of_work import AbstractUnitOfWork as UnitOfWork
from src.books.unit_of_work import SqlAlchemyUnitOfWork
from src.services.authentication import active_cookie_user
from src.services.jwt import JWTUserDownloadBookToken
from src.tags import ApiTags

if TYPE_CHECKING:
    from src.accounts.users import User
    from src.books.domain import Book
    from src.services.jwt import JWTToken

book_download_router = APIRouter()


class BookDownloadLink(BaseModel):
    url: str


class BookNotFoundException(Exception):
    def __init__(self, book_uuid: UUID):
        super().__init__(f"Book with uuid {book_uuid} not found")


class BookHasNoFilesException(Exception):
    def __init__(self, book: "Book"):
        super().__init__(f"Book with uuid {book.uuid} has no files")


class LinkConstructor:
    def __init__(self, token_class: Type["JWTToken"]):
        self._token_class = token_class

    def __call__(self, book: "Book", user_uuid: UUID) -> str:
        if book_file := book.get_file():
            token = self._token_class.encode(
                {
                    "user": str(user_uuid),
                    "book": str(book.uuid),
                    "file": str(book_file.uuid),
                    "title": book.title,
                }
            )
            return f"{book_file.href}?token={token}"
        else:
            raise BookHasNoFilesException(book)


class BookDownloadLinkHandler:
    def __init__(self, uow: UnitOfWork, link_constructor_strategy: Callable[["Book", UUID], str]):
        self._uow = uow
        self._link_constructor_strategy = link_constructor_strategy

    async def __call__(self, cmd: DownloadLinkBookCmd) -> BookDownloadLink:
        async with self._uow as uow:
            if book := await uow.books.get(cmd.book_uuid):
                download_link = BookDownloadLink(url=self._link_constructor_strategy(book, cmd.requested_by_user_uuid))
            else:
                raise BookNotFoundException(cmd.book_uuid)
        return download_link


def handler_factory() -> BookDownloadLinkHandler:
    return BookDownloadLinkHandler(
        uow=SqlAlchemyUnitOfWork(),
        link_constructor_strategy=LinkConstructor(JWTUserDownloadBookToken),
    )


async def user_who_can_download_book(book_uuid: UUID, user: "User" = Depends(active_cookie_user)) -> "User":
    if await BookPermissions().user_can_download_book(user, book_uuid):
        return user
    else:
        raise HTTPException(status_code=403, detail=f"No permissions to download book {book_uuid}")


@book_download_router.get(urls.DOWNLOAD_BOOK_URL, tags=[ApiTags.TAG_BOOKS])
async def download_book(
    book_uuid: UUID,
    user: "User" = Depends(user_who_can_download_book),
    handler: BookDownloadLinkHandler = Depends(handler_factory),
) -> RedirectResponse:
    cmd = DownloadLinkBookCmd(book_uuid=book_uuid, requested_by_user_uuid=user.uuid)
    redirect_link = await handler(cmd)
    return RedirectResponse(redirect_link.url)
