from unittest.mock import MagicMock
from uuid import uuid4

from src.accounts.users import User
from src.books import urls

from ..router import (
    BookDownloadLink,
    BookDownloadLinkHandler,
    DownloadLinkBookCmd,
    handler_factory,
    user_who_can_download_book,
)


def fake_user_can_download_book() -> User:
    return User(uuid=uuid4(), is_active=True, memberships=[])


def fake_factory_handler():
    class FakeBookDownloadLinkHandler(BookDownloadLinkHandler):
        def __init__(self):
            pass

        async def __call__(self, cmd: DownloadLinkBookCmd) -> BookDownloadLink:
            return BookDownloadLink(url=f"/api/v1/fake-link/{cmd.book_uuid}/download")

    return FakeBookDownloadLinkHandler()


class TestDownloadBookEndpoint:
    async def test_endpoint_download_book_return_307(self, client_factory):
        async with client_factory(
            {
                user_who_can_download_book: fake_user_can_download_book,
                handler_factory: fake_factory_handler,
            }
        ) as client:
            reponse = await client.get(urls.DOWNLOAD_BOOK_URL.format(book_uuid=uuid4()))
        assert reponse.status_code == 307

    async def test_endpoint_download_book_return_to_set_redirect_url(self, client_factory):
        book_uuid = uuid4()
        async with client_factory(
            {
                user_who_can_download_book: fake_user_can_download_book,
                handler_factory: fake_factory_handler,
            }
        ) as client:
            reponse = await client.get(urls.DOWNLOAD_BOOK_URL.format(book_uuid=book_uuid))
        assert reponse.status_code == 307
        assert reponse.headers["Location"] == f"/api/v1/fake-link/{book_uuid}/download"

    async def test_handler_in_endpoint_is_called_with_correct_command(self, client_factory):
        book_uuid = uuid4()
        user_uuid = uuid4()
        mocked_handler = MagicMock()

        def fake_mocked_handler_factory() -> BookDownloadLinkHandler:
            class FakeBookDownloadLinkHandler(BookDownloadLinkHandler):
                def __init__(self):
                    pass

                async def __call__(self, cmd: DownloadLinkBookCmd) -> BookDownloadLink:
                    mocked_handler(cmd)
                    return BookDownloadLink(url=f"/api/v1/fake-link/{cmd.book_uuid}/download")

            return FakeBookDownloadLinkHandler()

        def fake_user_can_download_book_with_injected_uuid() -> User:
            return User(uuid=user_uuid, is_active=True, memberships=[])

        async with client_factory(
            {
                user_who_can_download_book: fake_user_can_download_book_with_injected_uuid,
                handler_factory: fake_mocked_handler_factory,
            }
        ) as client:
            reponse = await client.get(urls.DOWNLOAD_BOOK_URL.format(book_uuid=book_uuid))
        assert reponse.status_code == 307
        mocked_handler.assert_called_once_with(
            DownloadLinkBookCmd(book_uuid=book_uuid, requested_by_user_uuid=user_uuid)
        )
