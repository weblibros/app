from uuid import uuid4

import pytest

from src.books.domain import Book, BookFile, BookMimeType
from src.services.jwt import JWTUserDownloadBookToken

from ..router import BookHasNoFilesException, LinkConstructor


@pytest.fixture
def book() -> Book:
    return Book(
        uuid=uuid4(),
        title="fake book title",
        title_sort="fake title sort",
        isbn=None,
        published_at=None,
        authors=[],
        identifiers=[],
        cover=None,
        files=[BookFile(uuid=uuid4(), href="/test/epub", mime_type=BookMimeType.EPUB)],
    )


@pytest.fixture
def book_without_files() -> Book:
    return Book(
        uuid=uuid4(),
        title="fake book title",
        title_sort="fake title sort",
        isbn=None,
        published_at=None,
        authors=[],
        identifiers=[],
        cover=None,
        files=[],
    )


@pytest.fixture
def link_constructor():
    return LinkConstructor(JWTUserDownloadBookToken)


def test_link_constructor_returns_a_link(link_constructor, book):
    user_uuid = uuid4()
    link = link_constructor(book, user_uuid)
    assert link.startswith("/test/epub?token=")


def test_link_constructor_returns_a_download_token(link_constructor, book):
    user_uuid = uuid4()
    link = link_constructor(book, user_uuid)
    assert link.startswith("/test/epub?token=")
    token = link.split("token=")[1]
    assert JWTUserDownloadBookToken(token).is_valid() is True


def test_contructor_raises_exception_when_book_has_no_files(link_constructor, book_without_files):
    with pytest.raises(BookHasNoFilesException) as exc:
        link_constructor(book_without_files, uuid4())
    assert str(exc.value) == f"Book with uuid {book_without_files.uuid} has no files"
