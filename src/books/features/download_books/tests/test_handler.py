from uuid import UUID, uuid4

import pytest

from src.books.domain import Book, BookFile, BookMimeType
from src.books.unit_of_work import InMemoryUnitOfWork

from ..router import BookDownloadLink, BookDownloadLinkHandler, BookNotFoundException, DownloadLinkBookCmd


def fake_link_constructor_strategy(book: Book, user_uuid: UUID) -> str:
    if book_file := book.get_file():
        return f"/fake/{book_file.uuid}"
    else:
        raise Exception("Book has no files")


@pytest.fixture
def book() -> Book:
    return Book(
        uuid=uuid4(),
        title="fake book title",
        title_sort="fake title sort",
        isbn=None,
        published_at=None,
        authors=[],
        identifiers=[],
        cover=None,
        files=[BookFile(uuid=uuid4(), href="/test/epub", mime_type=BookMimeType.EPUB)],
    )


@pytest.fixture
def book_without_files() -> Book:
    return Book(
        uuid=uuid4(),
        title="fake book title",
        title_sort="fake title sort",
        isbn=None,
        published_at=None,
        authors=[],
        identifiers=[],
        cover=None,
        files=[],
    )


class TestBookDownloadLinkHandler:
    async def test_handler_returns_back_a_link(self, book):
        handler = BookDownloadLinkHandler(
            uow=InMemoryUnitOfWork(books_in_memory=[book]),
            link_constructor_strategy=fake_link_constructor_strategy,
        )
        command = DownloadLinkBookCmd(book_uuid=book.uuid, requested_by_user_uuid=uuid4())
        link = await handler(command)
        assert isinstance(link, BookDownloadLink)
        assert link.url == f"/fake/{book.get_file().uuid}"

    async def test_handler_raises_exception_when_book_not_found(self):
        handler = BookDownloadLinkHandler(
            uow=InMemoryUnitOfWork(books_in_memory=[]),
            link_constructor_strategy=fake_link_constructor_strategy,
        )
        command = DownloadLinkBookCmd(book_uuid=uuid4(), requested_by_user_uuid=uuid4())
        with pytest.raises(BookNotFoundException) as exc:
            await handler(command)
        assert str(exc.value) == f"Book with uuid {command.book_uuid} not found"
