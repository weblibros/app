from fastapi_events.handlers.local import local_handler
from fastapi_events.typing import Event

from src.books.unit_of_work import SqlAlchemyUnitOfWork
from src.file_management.domain.events import EbookFileImported, FileManagmentEvents

from .factory import BookFactory
from .handlers import CreateBookHandler


@local_handler.register(event_name=str(FileManagmentEvents.EBOOK_FILE_IMPORTED))
async def store_book_info_from_uploaded_file(event: Event):
    _, payload = event

    ebook_file_import_event = EbookFileImported(**payload)

    handler = CreateBookHandler(SqlAlchemyUnitOfWork(), BookFactory())
    await handler(ebook_file_import_event)
