from typing import Optional, Sequence, Type, TypeVar
from uuid import UUID, uuid4

from src.books.constants import get_articles_by_languages
from src.books.domain import ISBN, Author, Book, BookFile, BookMimeType, Cover, CoverPath, Identifier
from src.file_management.domain.events import (
    EbookFileImported,
    ProcessedExtractCoverFromEbookStep,
    ProcessedImportStep,
    ProcessedRetrieveEbookMetaDataStep,
    ProcessedTransformTempToEbookStep,
)

T = TypeVar("T", bound=ProcessedImportStep)


class BookFactory:
    def __init__(self):
        pass

    def __call__(self, event: EbookFileImported):
        book = self._map_event_to_book(event)
        return book

    def _map_event_to_book(self, event: EbookFileImported) -> Book:
        processed_step = self._find_processed_step(event.steps_processed, ProcessedRetrieveEbookMetaDataStep)
        if processed_step.result.data.title_sort:
            title_sort = processed_step.result.data.title_sort
        else:
            title_sort = self._move_arctile_to_back_from_title(
                processed_step.result.data.title, processed_step.result.data.language
            )
        book = Book(
            uuid=event.book_uuid,
            title=processed_step.result.data.title,
            title_sort=title_sort,
            published_at=processed_step.result.data.published,
            isbn=self._extract_isbn_from_event(processed_step),
            authors=[
                Author(
                    first_name=author.first_name,
                    last_name=author.last_name,
                )
                for author in processed_step.result.data.authors
            ],
            identifiers=[
                Identifier(
                    key=identifier.key,
                    value=identifier.value,
                )
                for identifier in processed_step.result.data.identifiers
            ],
            cover=self._map_event_to_cover(event),
            files=self._map_event_to_book_files(event),
        )
        return book

    def _map_event_to_cover(self, event: EbookFileImported) -> Optional[Cover]:
        processed_step = self._find_processed_step(event.steps_processed, ProcessedExtractCoverFromEbookStep)
        if processed_step.result.image_uuid:
            cover = Cover(
                uuid=uuid4(),
                cover_path=self._cover_path(processed_step.result.image_uuid),
                image_uuid=processed_step.result.image_uuid,
            )
            return cover
        else:
            return None

    def _cover_path(self, image_uuid: UUID) -> CoverPath:
        # TODO; move logic to cover domain
        return CoverPath(f"/api/v1/file/cover/{image_uuid}/image")

    def _map_event_to_book_files(self, event: EbookFileImported) -> list[BookFile]:
        processed_step = self._find_processed_step(event.steps_processed, ProcessedTransformTempToEbookStep)
        return [
            BookFile(
                uuid=processed_step.result.book_file_uuid,
                href=processed_step.result.href,
                mime_type=BookMimeType.from_str(processed_step.result.mime_type),
            )
        ]

    def _find_processed_step(self, steps_processed: Sequence[ProcessedImportStep], process_class: Type[T]) -> T:
        for step in steps_processed:
            if isinstance(step, process_class):
                return step
        raise ValueError(f"{process_class.__name__} not found in steps processed")

    def _move_arctile_to_back_from_title(self, title: str, language: str | None):
        split_words = title.split(" ")
        first_word = split_words[0]
        arcticles_by_langauge = get_articles_by_languages(language)
        if first_word.lower() in arcticles_by_langauge:
            title_without_article = " ".join(split_words[1:])
            return f"{title_without_article}, {split_words[0]}"
        else:
            return title

    def _extract_isbn_from_event(self, event: ProcessedImportStep) -> Optional[ISBN]:
        # TODO
        return None
