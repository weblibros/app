from functools import singledispatchmethod
from typing import Callable

from src.books.domain import Book
from src.books.domain.events import BookAddedEvent
from src.books.unit_of_work import AbstractUnitOfWork
from src.file_management.domain.events import EbookFileImported

from .factory import BookFactory


class CreateBookHandler:
    def __init__(self, uow: AbstractUnitOfWork, book_factory: Callable[[EbookFileImported], Book] = BookFactory()):
        self.uow = uow
        self._book_factory = book_factory

    async def __call__(self, event) -> Book:
        return await self.handle(event)

    @singledispatchmethod
    async def handle(event):
        raise NotImplementedError("event cannot be handled")

    @handle.register
    async def _(self, event: EbookFileImported):
        book = self._book_factory(event)

        book.events.append(
            BookAddedEvent(
                book_uuid=book.uuid,
                library_uuid=event.library_uuid,
                stored_by_user_uuid=event.user_uuid,
                title=book.title,
                title_sort=book.title_sort,
                published_at=book.published_at,
            )
        )
        async with self.uow:
            await self.uow.books.add(book)
            await self.uow.commit()
        return book
