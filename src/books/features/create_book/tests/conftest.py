from datetime import datetime
from pathlib import Path
from uuid import uuid4

import pytest

from src.file_management.domain.events import (
    AuthorMetaData,
    EbookFileImported,
    EbookMetaData,
    ExtractCoverFromEbookStepResult,
    FileContextLoadedInMemoryResult,
    FileIsEbookStepResult,
    IdentifierMetaData,
    ProcessedExtractCoverFromEbookStep,
    ProcessedFileContextLoadedInMemoryStep,
    ProcessedFileIsEbookStep,
    ProcessedRetrieveEbookMetaDataStep,
    ProcessedTransformTempToEbookStep,
    RetrieveEbookMetaDataStepResult,
    TransformTempToEbookStepResult,
)


@pytest.fixture
def ebook_file_import_event():
    book_uuid = uuid4()
    temp_file_uuid = uuid4()
    event = EbookFileImported(
        user_uuid=uuid4(),
        library_uuid=uuid4(),
        book_uuid=book_uuid,
        started_at=datetime.now(),
        finished_at=datetime.now(),
        steps_processed=[
            ProcessedFileContextLoadedInMemoryStep(
                uuid=uuid4(),
                import_step_name="FileContextLoadedInMemoryStep",
                started_at=datetime.now(),
                finished_at=datetime.now(),
                result=FileContextLoadedInMemoryResult(
                    file_uuid=temp_file_uuid,
                    loaded=True,
                ),
            ),
            ProcessedFileIsEbookStep(
                uuid=uuid4(),
                import_step_name="FileIsEbookStep",
                started_at=datetime.now(),
                finished_at=datetime.now(),
                result=FileIsEbookStepResult(
                    is_ebook=True,
                    file_uuid=uuid4(),
                ),
            ),
            ProcessedExtractCoverFromEbookStep(
                uuid=uuid4(),
                import_step_name="ExtractCoverFromEbookStep",
                started_at=datetime.now(),
                finished_at=datetime.now(),
                result=ExtractCoverFromEbookStepResult(
                    image_uuid=uuid4(),
                    file_uuid=uuid4(),
                    image_path=Path("image_path"),
                    book_uuid=book_uuid,
                ),
            ),
            ProcessedRetrieveEbookMetaDataStep(
                uuid=uuid4(),
                import_step_name="RetrieveEbookMetaDataStep",
                started_at=datetime.now(),
                finished_at=datetime.now(),
                result=RetrieveEbookMetaDataStepResult(
                    book_uuid=book_uuid,
                    data=EbookMetaData(
                        title="title",
                        title_sort="title_sort",
                        authors=[
                            AuthorMetaData(
                                first_name="first_name",
                                last_name="last_name",
                            )
                        ],
                        identifiers=[
                            IdentifierMetaData(
                                key="key",
                                value="value",
                            )
                        ],
                        language="eng",
                        published=datetime.now(),
                    ),
                ),
            ),
            ProcessedTransformTempToEbookStep(
                uuid=uuid4(),
                import_step_name="TransformTempToEbookStep",
                started_at=datetime.now(),
                finished_at=datetime.now(),
                result=TransformTempToEbookStepResult(
                    book_file_uuid=uuid4(),
                    book_uuid=book_uuid,
                    temp_file_uuid=uuid4(),
                    href="/fake/path",
                    mime_type="application/epub+zip",
                ),
            ),
        ],
    )
    return event
