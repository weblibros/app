from src.books.unit_of_work import InMemoryUnitOfWork

from ..factory import BookFactory
from ..handlers import CreateBookHandler


class TestCreateBookHandler:
    async def test_create_book_from_event(self, ebook_file_import_event):
        memory = []
        event = ebook_file_import_event
        handler = CreateBookHandler(InMemoryUnitOfWork(memory), BookFactory())
        await handler(event)
        assert handler.uow.committed is True
        assert len(memory) == 1
        [book] = memory
        assert book.uuid == event.book_uuid
