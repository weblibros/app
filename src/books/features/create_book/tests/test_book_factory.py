from ..factory import BookFactory


class TestCreateFromFactory:
    async def test_create_book_from_event(self, ebook_file_import_event):
        event = ebook_file_import_event
        book_factory = BookFactory()
        book = book_factory(event)
        assert book.uuid == event.book_uuid
