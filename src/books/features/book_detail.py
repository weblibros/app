from typing import TYPE_CHECKING
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel

from src.books import urls
from src.books.permissions import BookPermissions
from src.books.repositories import SqlBookReadRepo
from src.services.authentication import active_user
from src.tags import ApiTags

book_detail_router = APIRouter()

if TYPE_CHECKING:
    from src.accounts.users import User


class BookDetailResponseSchema(BaseModel):
    uuid: UUID
    title: str


@book_detail_router.get(urls.DETAIL_BOOK_URL, tags=[ApiTags.TAG_BOOKS])
async def get_book(
    book_uuid: UUID,
    user: "User" = Depends(active_user),
) -> BookDetailResponseSchema:
    can_read_book = await BookPermissions().user_can_read_book(user, book_uuid)
    if can_read_book is True:
        book = await SqlBookReadRepo().get_book_detail(book_uuid)
    else:
        raise HTTPException(status_code=403, detail="No permissions to get book details")
    return BookDetailResponseSchema.model_validate(book, from_attributes=True)
