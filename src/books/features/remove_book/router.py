from typing import TYPE_CHECKING
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel

from src.books import urls
from src.books.domain.events import RemoveBookCmd
from src.books.permissions import BookPermissions
from src.books.unit_of_work import SqlAlchemyUnitOfWork
from src.services.authentication import active_user
from src.tags import ApiTags

from .handlers import BookNotFoundException, RemoveBookHandler

if TYPE_CHECKING:
    from src.accounts.users import User

book_remove_router = APIRouter()


class DeleteResponse(BaseModel):
    book_uuid: UUID
    message: str = "Book was succesfully removed"


@book_remove_router.delete(urls.REMOVE_BOOK_URL, tags=[ApiTags.TAG_BOOKS])
async def remove_book(
    book_uuid: UUID,
    user: "User" = Depends(active_user),
) -> DeleteResponse:
    http_error_403 = HTTPException(status_code=403, detail=f"No permissions to remove book {book_uuid}")

    can_remove_book = await BookPermissions().user_can_remove_book(user, book_uuid)
    if can_remove_book is False:
        raise http_error_403

    cmd = RemoveBookCmd(book_uuid=book_uuid, requested_by_user_uuid=user.uuid)
    handler = RemoveBookHandler(SqlAlchemyUnitOfWork())
    try:
        await handler(cmd)
    except BookNotFoundException:
        raise http_error_403
    else:
        return DeleteResponse(book_uuid=book_uuid)
