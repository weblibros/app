from uuid import uuid4

import pytest
from httpx import Headers
from sqlalchemy import delete, exists

from src.accounts.users.user import Membership, User
from src.books import urls
from src.books.repositories.models import BookModel
from src.config.database import async_session_maker
from src.libraries.membership_roles import MembershipRole
from src.libraries.repositories.models import Library, LibraryBook
from src.services.jwt import JWTUserAccesToken


@pytest.fixture()
async def user_with_book_uuid():
    book = BookModel(uuid=uuid4(), title="fake book", title_sort="fake book")
    library = Library(uuid=uuid4(), name="fake lib")
    library_book = LibraryBook(library_uuid=library.uuid, book_uuid=book.uuid)
    user = User(
        uuid=uuid4(),
        is_active=True,
        memberships=[
            Membership(
                library_uuid=library.uuid,
                membership_role=MembershipRole.ADMIN,
            ),
        ],
    )
    async with async_session_maker() as session:
        session.add_all(
            [
                book,
                library,
                library_book,
            ]
        )
        await session.commit()

    yield user, book.uuid
    async with async_session_maker() as session:
        await session.execute(delete(LibraryBook).filter_by(book_uuid=book.uuid))
        await session.execute(delete(BookModel).filter_by(uuid=book.uuid))
        await session.execute(delete(Library).filter_by(uuid=library.uuid))
        await session.commit()


@pytest.fixture()
async def user_with_no_permissons_book_uuid():
    book = BookModel(uuid=uuid4(), title="fake book")
    library = Library(uuid=uuid4(), name="fake lib")
    library_book = LibraryBook(library_uuid=library.uuid, book_uuid=book.uuid)
    user = User(
        uuid=uuid4(),
        is_active=True,
        memberships=[
            Membership(
                library_uuid=library.uuid,
                membership_role=MembershipRole.VIEWER,
            ),
        ],
    )
    async with async_session_maker() as session:
        session.add_all(
            [
                book,
                library,
                library_book,
            ]
        )
        await session.commit()

    yield user, book.uuid
    async with async_session_maker() as session:
        await session.execute(delete(LibraryBook).filter_by(book_uuid=book.uuid))
        await session.execute(delete(BookModel).filter_by(uuid=book.uuid))
        await session.execute(delete(Library).filter_by(uuid=library.uuid))
        await session.commit()


def user_auth_header(user):
    return Headers({"Authorization": f"Bearer {JWTUserAccesToken.encode_user(user)}"})


class TestBooksRemoveBooks:
    async def test_remove_book_endpoint(self, client, user_with_book_uuid):
        user, book_uuid = user_with_book_uuid
        response = await client.delete(
            urls.REMOVE_BOOK_URL.format(book_uuid=book_uuid),
            headers=user_auth_header(user),
        )
        assert response.status_code == 200
        book = response.json()
        assert isinstance(book, dict)
        assert book["book_uuid"] == str(book_uuid)
        async with async_session_maker() as session:
            query = exists(BookModel).where(BookModel.uuid == book_uuid).select()
            result = await session.scalar(query)
            assert result is False

    async def test_remove_no_book(self, client, user_with_book_uuid):
        user, _ = user_with_book_uuid
        book_uuid = uuid4()
        response = await client.delete(
            urls.REMOVE_BOOK_URL.format(book_uuid=book_uuid),
            headers=user_auth_header(user),
        )
        assert response.status_code == 403
        async with async_session_maker() as session:
            query = exists(BookModel).where(BookModel.uuid == book_uuid).select()
            result = await session.scalar(query)
            assert result is False

    async def test_remove_book_endpoint_no_permissions(self, client, user_with_no_permissons_book_uuid):
        user, book_uuid = user_with_no_permissons_book_uuid
        response = await client.delete(
            urls.REMOVE_BOOK_URL.format(book_uuid=book_uuid),
            headers=user_auth_header(user),
        )
        assert response.status_code == 403
        async with async_session_maker() as session:
            query = exists(BookModel).where(BookModel.uuid == book_uuid).select()
            result = await session.scalar(query)
            assert result is True
