from functools import singledispatchmethod

from src.books.domain.events import BookRemovedEvent, RemoveBookCmd
from src.books.unit_of_work import AbstractUnitOfWork


class BookNotFoundException(Exception):
    pass


class RemoveBookHandler:
    def __init__(self, uow: AbstractUnitOfWork):
        self.uow = uow

    async def __call__(self, event):
        return await self.handle(event)

    @singledispatchmethod
    async def handle(event):
        raise NotImplementedError("event cannot be handled")

    @handle.register
    async def _(self, event: RemoveBookCmd):
        async with self.uow:
            book = await self.uow.books.get(event.book_uuid)
            if book:
                book.events.append(
                    BookRemovedEvent(
                        book_uuid=book.uuid,
                        removed_by_user_uuid=event.requested_by_user_uuid,
                        image_uuid_list=book.image_uuids,
                        file_uuid_list=book.file_uuids,
                    )
                )
                await self.uow.books.remove(book)
                await self.uow.commit()
            else:
                raise BookNotFoundException()
