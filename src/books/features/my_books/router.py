from dataclasses import dataclass
from enum import StrEnum
from functools import partial
from math import ceil
from typing import TYPE_CHECKING, Optional
from uuid import UUID

from fastapi import APIRouter, Depends, Query
from pydantic import BaseModel
from sqlalchemy import func, select
from sqlalchemy.ext.asyncio import AsyncSession

from src.books import urls
from src.books.repositories.models import BookFileRecords, BookModel, CoverModel
from src.config.database import async_session_manager
from src.libraries.repositories.models import LibraryBook
from src.services.authentication import active_user
from src.tags import ApiTags

if TYPE_CHECKING:
    from src.accounts.users import User

book_list_router = APIRouter()


class BookActionName(StrEnum):
    DOWNLOAD = "download"


class BookAction(BaseModel):
    name: BookActionName
    link: str
    display_text: str


class BookItem(BaseModel):
    uuid: UUID
    title: str
    cover_path: str
    actions: list[BookAction]


class Params(BaseModel):
    page: int = Query(1, ge=1, description="Page number")
    size: int = Query(50, ge=1, le=100, description="Page size")

    @property
    def limit(self) -> int:
        return self.size

    @property
    def offset(self) -> int:
        return self.size * (self.page - 1)


class MyBooksPage(BaseModel):
    items: list[BookItem]
    total: int
    page: int
    size: int
    pages: int


@dataclass
class ReadModelResponse:
    books: list[BookItem]
    total: int


class MyBooksReadModel:
    async_session_manager = partial(async_session_manager)

    def __init__(self, session_overwrite: Optional[AsyncSession] = None) -> None:
        self.session_overwrite = session_overwrite

    async def read_paginated_data(self, libraries_list: list[UUID], params: Params) -> ReadModelResponse:
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            raw_books = await session.execute(
                self.books_by_library_query(libraries_list).limit(params.limit).offset(params.offset)
            )
            books = [self._as_book_item(item) for item in raw_books]
            books_total = (await session.execute(self.books_count_query(libraries_list))).scalar()

        return ReadModelResponse(books=books, total=books_total)

    def _as_book_item(self, item: tuple[UUID, str, str, str, str]) -> BookItem:
        if item[3]:
            actions = [
                BookAction(
                    name=BookActionName.DOWNLOAD,
                    link=urls.DOWNLOAD_BOOK_URL.format(book_uuid=item[0]),
                    display_text="Download",
                )
            ]
        else:
            actions = []
        return BookItem(
            uuid=item[0],
            title=item[1],
            cover_path=item[2] or "/books/covers/unknown",
            actions=actions,
        )

    def books_by_library_query(self, libraries: list[UUID]):
        sub_query_book_list = select(LibraryBook.book_uuid.label("book_uuid")).where(
            LibraryBook.library_uuid.in_(libraries)
        )

        query_book = (
            select(
                BookModel.uuid.label("uuid"),
                BookModel.title.label("title"),
                CoverModel.static_file_path.label("static_file_path"),
                BookFileRecords.file_record_uuid.label("file_uuid"),
            )
            .join(CoverModel, isouter=True)
            .join_from(
                BookModel,
                BookFileRecords,
                BookModel.uuid == BookFileRecords.book_uuid,
                isouter=True,
            )
            .where(BookModel.uuid.in_(sub_query_book_list))
        )
        return query_book

    def books_count_query(self, libraries: list[UUID]):
        sub_query_book_list = select(LibraryBook.book_uuid.label("book_uuid")).where(
            LibraryBook.library_uuid.in_(libraries)
        )

        query_book = select(
            func.count(BookModel.uuid).label("count"),
        ).where(BookModel.uuid.in_(sub_query_book_list))
        return query_book


class MyBooksPaginator:
    async_session_manager = partial(async_session_manager)

    def __init__(self, read_model: MyBooksReadModel) -> None:
        self.read_model = read_model

    async def paginated_user_books(self, user: "User", params: Params) -> MyBooksPage:
        libraries_uuid_list = user.get_libraries()
        read_model_response = await self.read_model.read_paginated_data(libraries_uuid_list, params)
        books = read_model_response.books
        books_total = read_model_response.total

        return MyBooksPage(
            items=books,
            total=books_total,
            page=params.page,
            size=params.size,
            pages=ceil(books_total / params.size),
        )


def paginator_factory():
    return MyBooksPaginator(MyBooksReadModel())


@book_list_router.get(urls.MY_BOOKS_URL, tags=[ApiTags.TAG_BOOKS])
async def my_books(
    params: Params = Depends(),
    user: "User" = Depends(active_user),
    paginator: MyBooksPaginator = Depends(paginator_factory),
) -> MyBooksPage:
    return await paginator.paginated_user_books(user, params)
