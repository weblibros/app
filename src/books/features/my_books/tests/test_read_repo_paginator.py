from typing import Callable, Tuple
from uuid import UUID, uuid4

import pytest

from src.accounts.users import Membership, MembershipRole, User
from src.books.repositories.models import BookModel
from src.config.database import async_session_maker
from src.libraries.repositories.models import Library, LibraryBook

from ..router import MyBooksPage, MyBooksPaginator, MyBooksReadModel, Params


@pytest.fixture
def library_factory() -> Callable[[str], Library]:
    def _library_factory(name: str):
        library = Library(uuid=uuid4(), name=name)  # type: ignore
        return library

    return _library_factory


@pytest.fixture
def book_factory() -> Callable[[str, UUID], Tuple[BookModel, LibraryBook]]:
    def _book_factory(title: str, library_uuid: UUID):
        book = BookModel(uuid=uuid4(), title=title)  # type: ignore
        library_book = LibraryBook(library_uuid=library_uuid, book_uuid=book.uuid)  # type: ignore
        return book, library_book

    return _book_factory


@pytest.fixture
def user_factory() -> Callable[[UUID], User]:
    def _user_factory(library_uuid: UUID):
        user = User(
            uuid=uuid4(),
            is_active=True,
            memberships=[
                Membership(library_uuid=library_uuid, membership_role=MembershipRole.OWNER),
            ],
        )
        return user

    return _user_factory


class TestPaginatesBooksByLibraries:
    paginator_class = MyBooksPaginator
    read_model_class = MyBooksReadModel

    async def test_select_book(self, library_factory, book_factory, user_factory):
        library = library_factory("fake lib")
        book, library_book = book_factory("book", library.uuid)
        user = user_factory(library.uuid)
        async with async_session_maker() as session:
            paginator = self.paginator_class(self.read_model_class(session))
            session.add_all([book, library, library_book])
            await session.flush()
            paginated_books = await paginator.paginated_user_books(user, Params())
            await session.rollback()
        assert isinstance(paginated_books, MyBooksPage)
        assert paginated_books.total == 1
        assert paginated_books.page == 1
        assert paginated_books.size == 50
        assert paginated_books.pages == 1
        assert len(paginated_books.items) == 1
        assert paginated_books.items[0].uuid == book.uuid

    async def test_filter_book_by_libraries(self, library_factory, book_factory, user_factory):
        first_library = library_factory("fake first lib")
        second_library = library_factory("fake second lib")
        first_book, first_library_book = book_factory("first book", first_library.uuid)
        second_book, second_library_book = book_factory("second book", second_library.uuid)
        third_book, third_library_book = book_factory("third book", first_library.uuid)
        user = user_factory(first_library.uuid)
        async with async_session_maker() as session:
            paginator = self.paginator_class(self.read_model_class(session))
            session.add_all(
                [
                    first_book,
                    first_library,
                    first_library_book,
                    second_book,
                    second_library,
                    second_library_book,
                    third_book,
                    third_library_book,
                ]
            )
            await session.flush()
            paginated_books = await paginator.paginated_user_books(user, Params())
        assert isinstance(paginated_books, MyBooksPage)
        assert paginated_books.total == 2
        assert paginated_books.page == 1
        assert paginated_books.size == 50
        assert paginated_books.pages == 1
        assert len(paginated_books.items) == 2
        book_uuids = [item.uuid for item in paginated_books.items]
        assert first_book.uuid in book_uuids
        assert second_book.uuid not in book_uuids
        assert third_book.uuid in book_uuids

    @pytest.mark.parametrize(
        "page,size,pages,items",
        [
            (1, 1, 3, 1),
            (1, 2, 2, 2),
            (1, 3, 1, 3),
            (1, 5, 1, 3),
            (2, 1, 3, 1),
            (2, 2, 2, 1),
            (2, 3, 1, 0),
            (2, 5, 1, 0),
            (3, 1, 3, 1),
            (3, 2, 2, 0),
            (3, 3, 1, 0),
            (3, 5, 1, 0),
        ],
    )
    async def test_pagination_with_3_items(self, page, size, pages, items, library_factory, book_factory, user_factory):
        library = library_factory("fake first lib")
        first_book, first_library_book = book_factory("first book", library.uuid)
        second_book, second_library_book = book_factory("second book", library.uuid)
        third_book, third_library_book = book_factory("third book", library.uuid)
        user = user_factory(library.uuid)
        params = Params(size=size, page=page)
        async with async_session_maker() as session:
            paginator = self.paginator_class(self.read_model_class(session))
            session.add_all(
                [
                    first_book,
                    library,
                    first_library_book,
                    second_book,
                    second_library_book,
                    third_book,
                    third_library_book,
                ]
            )
            await session.flush()
            paginated_books = await paginator.paginated_user_books(user, params)
        assert isinstance(paginated_books, MyBooksPage)
        assert paginated_books.total == 3
        assert paginated_books.page == page
        assert paginated_books.size == size
        assert paginated_books.pages == pages
        assert len(paginated_books.items) == items
