from uuid import uuid4

import pytest
from httpx import Headers

from src.accounts.users.user import Membership, User
from src.books import urls
from src.books.repositories.models import BookFileRecords, BookModel
from src.config.database import async_session_maker
from src.file_management.repositories.models import FileRecord
from src.libraries.membership_roles import MembershipRole
from src.libraries.repositories.models import Library, LibraryBook
from src.services.jwt import JWTUserAccesToken


@pytest.fixture()
async def user_with_books():
    first_book = BookModel(uuid=uuid4(), title="first book")
    first_library = Library(uuid=uuid4(), name="fake first lib")
    first_library_book = LibraryBook(library_uuid=first_library.uuid, book_uuid=first_book.uuid)
    second_book = BookModel(uuid=uuid4(), title="second book")
    second_library = Library(uuid=uuid4(), name="fake second lib")
    second_library_book = LibraryBook(library_uuid=second_library.uuid, book_uuid=second_book.uuid)
    third_book = BookModel(uuid=uuid4(), title="third book")
    third_library_book = LibraryBook(library_uuid=first_library.uuid, book_uuid=third_book.uuid)
    user = User(
        uuid=uuid4(),
        is_active=True,
        memberships=[
            Membership(
                library_uuid=first_library.uuid,
                membership_role=MembershipRole.OWNER,
            ),
            Membership(
                library_uuid=second_library.uuid,
                membership_role=MembershipRole.ADMIN,
            ),
        ],
    )
    async with async_session_maker() as session:
        session.add_all(
            [
                first_book,
                first_library,
                first_library_book,
                second_book,
                second_library,
                second_library_book,
                third_book,
                third_library_book,
            ]
        )
        await session.commit()

    yield user
    async with async_session_maker() as session:
        await session.delete(first_library_book)
        await session.delete(second_library_book)
        await session.delete(third_library_book)
        await session.delete(first_book)
        await session.delete(first_library)
        await session.delete(second_book)
        await session.delete(second_library)
        await session.delete(third_book)
        await session.commit()


@pytest.fixture()
async def user_with_single_book():
    book = BookModel(uuid=uuid4(), title="first book")
    library = Library(uuid=uuid4(), name="fake first lib")
    library_book = LibraryBook(library_uuid=library.uuid, book_uuid=book.uuid)
    file_record = FileRecord(
        uuid=uuid4(),
        path="fakepath",
        size_bytes=1,
        extension="fake",
        content_type="fake",
        storage_service_name="test",
        entity_class_name="File",
    )
    book_file_record = BookFileRecords(
        uuid=uuid4(),
        book_uuid=book.uuid,
        file_record_uuid=file_record.uuid,
    )
    user = User(
        uuid=uuid4(),
        is_active=True,
        memberships=[
            Membership(
                library_uuid=library.uuid,
                membership_role=MembershipRole.OWNER,
            ),
        ],
    )
    async with async_session_maker() as session:
        session.add_all(
            [
                book,
                library,
                file_record,
            ]
        )
        await session.flush()
        session.add_all(
            [
                library_book,
                book_file_record,
            ]
        )
        await session.commit()

    yield user
    async with async_session_maker() as session:
        await session.delete(book_file_record)
        await session.delete(file_record)
        await session.delete(library_book)
        await session.delete(book)
        await session.delete(library)
        await session.commit()


@pytest.fixture()
async def user_with_books_auth_header(user_with_books):
    return Headers({"Authorization": f"Bearer {JWTUserAccesToken.encode_user(user_with_books)}"})


@pytest.fixture()
async def user_with_single_auth_header(user_with_single_book):
    return Headers({"Authorization": f"Bearer {JWTUserAccesToken.encode_user(user_with_single_book)}"})


class TestMyBooksEndpoint:
    async def test_books_endpoint(self, client, user_with_books_auth_header):
        response = await client.get(urls.MY_BOOKS_URL, headers=user_with_books_auth_header)
        assert response.status_code == 200
        page_books = response.json()
        books = page_books["items"]
        assert isinstance(books, list)
        assert len(books) == 3
        assert isinstance(books[0], dict)

    async def test_books_data_structure(self, client, user_with_books_auth_header):
        response = await client.get(urls.MY_BOOKS_URL, headers=user_with_books_auth_header)
        assert response.status_code == 200
        page_books = response.json()
        books = page_books["items"]
        assert isinstance(books, list)
        assert len(books) == 3
        assert isinstance(books[0], dict)
        assert "uuid" in books[0]
        assert "title" in books[0]
        assert "cover_path" in books[0]
        assert "actions" in books[0]

    async def test_books_actions_data_structure_with_file_books(self, client, user_with_single_auth_header):
        response = await client.get(urls.MY_BOOKS_URL, headers=user_with_single_auth_header)
        assert response.status_code == 200
        page_books = response.json()
        books = page_books["items"]
        assert isinstance(books, list)
        assert "actions" in books[0]
        assert isinstance(books[0]["actions"], list)
        assert len(books[0]["actions"]) == 1
        assert isinstance(books[0]["actions"][0], dict)
        assert "name" in books[0]["actions"][0]
        assert "link" in books[0]["actions"][0]
        assert "display_text" in books[0]["actions"][0]
        assert books[0]["actions"][0]["name"] == "download"
        assert "/api/v1/books/" in books[0]["actions"][0]["link"]
        assert "download" in books[0]["actions"][0]["link"]
        assert books[0]["actions"][0]["display_text"] == "Download"

    async def test_books_actions_data_structure_without_file_books(self, client, user_with_books_auth_header):
        response = await client.get(urls.MY_BOOKS_URL, headers=user_with_books_auth_header)
        assert response.status_code == 200
        page_books = response.json()
        books = page_books["items"]
        assert isinstance(books, list)
        assert "actions" in books[0]
        assert isinstance(books[0]["actions"], list)
        download_actions = [action for action in books[0]["actions"] if action["name"] == "download"]
        assert len(download_actions) == 0
