from uuid import uuid4

import pytest
from httpx import Headers

from src.accounts.users.user import Membership, User
from src.books import urls
from src.books.repositories.models import BookModel
from src.config.database import async_session_maker
from src.libraries.membership_roles import MembershipRole
from src.libraries.repositories.models import Library, LibraryBook
from src.services.jwt import JWTUserAccesToken


@pytest.fixture()
async def user_with_book_uuid():
    book = BookModel(uuid=uuid4(), title="fake book")
    library = Library(uuid=uuid4(), name="fake lib")
    library_book = LibraryBook(library_uuid=library.uuid, book_uuid=book.uuid)
    user = User(
        uuid=uuid4(),
        is_active=True,
        memberships=[
            Membership(
                library_uuid=uuid4(),
                membership_role=MembershipRole.OWNER,
            ),
            Membership(
                library_uuid=library.uuid,
                membership_role=MembershipRole.VIEWER,
            ),
        ],
    )
    async with async_session_maker() as session:
        session.add_all(
            [
                book,
                library,
                library_book,
            ]
        )
        await session.commit()

    yield user, book.uuid
    async with async_session_maker() as session:
        await session.delete(library_book)
        await session.delete(book)
        await session.delete(library)
        await session.commit()


def user_auth_header(user):
    return Headers({"Authorization": f"Bearer {JWTUserAccesToken.encode_user(user)}"})


class TestBooksDetailSelector:
    async def test_detail_book_endpoint(self, client, user_with_book_uuid):
        user, book_uuid = user_with_book_uuid
        response = await client.get(
            urls.DETAIL_BOOK_URL.format(book_uuid=book_uuid),
            headers=user_auth_header(user),
        )
        assert response.status_code == 200
        book = response.json()
        assert isinstance(book, dict)
        assert book["uuid"] == str(book_uuid)

    async def test_detail_book_data_structure(self, client, user_with_book_uuid):
        user, book_uuid = user_with_book_uuid
        response = await client.get(
            urls.DETAIL_BOOK_URL.format(book_uuid=book_uuid),
            headers=user_auth_header(user),
        )
        assert response.status_code == 200
        book = response.json()
        assert isinstance(book, dict)
        assert "uuid" in book
        assert "title" in book

    async def test_get_no_book(self, client, user_with_book_uuid):
        user, _ = user_with_book_uuid
        response = await client.get(
            urls.DETAIL_BOOK_URL.format(book_uuid=uuid4()),
            headers=user_auth_header(user),
        )
        assert response.status_code == 403
