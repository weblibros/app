from dataclasses import dataclass
from typing import TYPE_CHECKING
from uuid import UUID, uuid4

import pytest

from src.accounts.users import Membership, MembershipRole, User
from src.books.permissions import BookPermissions
from src.books.repositories import BookReadRepo

if TYPE_CHECKING:
    from src.books.dto import BookDetailDTO


@dataclass
class FakeLibrary:
    uuid: UUID


@pytest.fixture
def library_owner():
    return FakeLibrary(uuid=uuid4())


@pytest.fixture
def library_admin():
    return FakeLibrary(uuid=uuid4())


@pytest.fixture
def library_contributor():
    return FakeLibrary(uuid=uuid4())


@pytest.fixture
def library_viewer():
    return FakeLibrary(uuid=uuid4())


@pytest.fixture
def user(library_owner, library_admin, library_contributor, library_viewer) -> User:
    return User(
        uuid=uuid4(),
        is_active=True,
        memberships=[
            Membership(library_owner.uuid, MembershipRole.OWNER),
            Membership(library_admin.uuid, MembershipRole.ADMIN),
            Membership(library_contributor.uuid, MembershipRole.CONTRIBUTOR),
            Membership(library_viewer.uuid, MembershipRole.VIEWER),
        ],
    )


class FakeReadRepo(BookReadRepo):
    def __init__(self, lib_book_mappings: dict[UUID, list[UUID]]):
        self._lib_book_mappings = lib_book_mappings

    async def book_exists_in_libraries(self, book_uuid, libraries_uuid_list):
        for library_uuid in libraries_uuid_list:
            if book_uuid in self._lib_book_mappings.get(library_uuid, []):
                return True
        else:
            return False

    async def get_book_detail(self, book_uuid: UUID) -> "BookDetailDTO":
        raise NotImplementedError


class TestBookPermissionsCanDownloadBook:
    async def test_user_can_download_book_when_user_is_the_library_owner(self, user, library_owner):
        book_uuid = uuid4()
        read_repo = FakeReadRepo({library_owner.uuid: [book_uuid]})
        book_permissions = BookPermissions(read_repo=read_repo)
        assert await book_permissions.user_can_download_book(user, book_uuid) is True

    async def test_user_can_download_book_when_user_is_the_library_admin(self, user, library_admin):
        book_uuid = uuid4()
        read_repo = FakeReadRepo({library_admin.uuid: [book_uuid]})
        book_permissions = BookPermissions(read_repo=read_repo)
        assert await book_permissions.user_can_download_book(user, book_uuid) is True

    async def test_user_can_download_book_when_user_is_the_library_contributor(self, user, library_contributor):
        book_uuid = uuid4()
        read_repo = FakeReadRepo({library_contributor.uuid: [book_uuid]})
        book_permissions = BookPermissions(read_repo=read_repo)
        assert await book_permissions.user_can_download_book(user, book_uuid) is True

    async def test_user_can_download_book_when_user_is_the_library_viewer(self, user, library_viewer):
        book_uuid = uuid4()
        read_repo = FakeReadRepo({library_viewer.uuid: [book_uuid]})
        book_permissions = BookPermissions(read_repo=read_repo)
        assert await book_permissions.user_can_download_book(user, book_uuid) is True

    async def test_user_cannot_download_book_when_user_does_not_have_the_book_in_its_libraries(self, user):
        book_uuid = uuid4()
        read_repo = FakeReadRepo({uuid4(): [book_uuid]})
        book_permissions = BookPermissions(read_repo=read_repo)
        assert await book_permissions.user_can_download_book(user, book_uuid) is False
