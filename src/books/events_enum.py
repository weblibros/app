from enum import StrEnum


class BookEvents(StrEnum):
    BOOK_STORED = "BOOK_STORED"
    BOOK_COVER_STORED = "BOOK_COVER_STORED"
    BOOK_REMOVED = "BOOK_REMOVED"
