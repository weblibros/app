from uuid import uuid4

import pytest
from sqlalchemy import select

from src.books.domain import Author, Book
from src.books.domain.events import BookAddedEvent
from src.books.repositories import InMemoryBookRepo, SqlBookRepo
from src.books.repositories.models import AuthorBookModel, AuthorModel, BookModel
from src.config.database import async_session_maker


@pytest.fixture
def book():
    first_author = Author(
        first_name="fake 1",
        last_name="fake 1",
    )
    second_author = Author(
        first_name="fake 2",
        last_name="fake 2",
    )
    book = Book(
        uuid=uuid4(),
        title="fake plain title",
        title_sort="fake title sort",
        isbn=None,
        published_at=None,
        authors=[first_author, second_author],
        identifiers=[],
        cover=None,
        files=[],
    )
    book.events.append(
        BookAddedEvent(
            book_uuid=book.uuid,
            library_uuid=uuid4(),
            title=book.title,
            title_sort=book.title_sort,
            published_at=book.published_at,
            stored_by_user_uuid=uuid4(),
        )
    )
    return book


class TestInMemoryBookRepo:
    async def test_add_authors(self, book):
        [first_author, second_author] = book.authors
        in_memory_books = []
        book_repo = InMemoryBookRepo(in_memory_books)

        await book_repo.add(book)
        [book_model] = in_memory_books
        [first_author_model, second_author_model] = book_model.authors
        assert book_model.uuid == book.uuid
        assert first_author_model.first_name == first_author.first_name
        assert first_author_model.last_name == first_author.last_name
        assert second_author_model.first_name == second_author.first_name
        assert second_author_model.last_name == second_author.last_name


class TestSQLBookRepo:
    async def test_add_authors(self, book):
        [first_author, second_author] = book.authors

        async with async_session_maker() as session:
            book_repo = SqlBookRepo(session_overwrite=session)

            await book_repo.add(book)
            await session.flush()
            book_model = (
                await session.execute(select(BookModel).where(BookModel.uuid == book.uuid))
            ).scalar_one_or_none()
            first_author_model = (
                await session.execute(
                    select(AuthorModel).where(
                        AuthorModel.first_name == first_author.first_name,
                        AuthorModel.last_name == first_author.last_name,
                    )
                )
            ).scalar_one_or_none()
            first_author_uuid = first_author_model.uuid
            second_author_model = (
                await session.execute(
                    select(AuthorModel).where(
                        AuthorModel.first_name == second_author.first_name,
                        AuthorModel.last_name == second_author.last_name,
                    )
                )
            ).scalar_one_or_none()
            second_author_uuid = second_author_model.uuid
            author_book_link_models = await session.scalars(
                select(AuthorBookModel).where(AuthorBookModel.book_uuid == book.uuid)
            )

            assert book_model.uuid == book.uuid
            assert first_author_model.first_name == first_author.first_name
            assert first_author_model.last_name == first_author.last_name
            assert second_author_model.first_name == second_author.first_name
            assert second_author_model.last_name == second_author.last_name
            author_book_link_author_uuids = [link.author_uuid for link in author_book_link_models]
            assert len(author_book_link_author_uuids) == 2
            assert first_author_uuid in author_book_link_author_uuids
            assert second_author_uuid in author_book_link_author_uuids

            await session.rollback()
