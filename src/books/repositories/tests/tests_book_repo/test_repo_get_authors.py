from uuid import uuid4

import pytest

from src.books.domain import Author, Book
from src.books.domain.events import BookAddedEvent
from src.books.repositories import InMemoryBookRepo, SqlBookRepo
from src.config.database import async_session_maker


@pytest.fixture
def book():
    first_author = Author(
        first_name="fake 1",
        last_name="fake 1",
    )
    second_author = Author(
        first_name="fake 2",
        last_name="fake 2",
    )
    book = Book(
        uuid=uuid4(),
        title="fake plain title",
        title_sort="fake title sort",
        isbn=None,
        published_at=None,
        authors=[first_author, second_author],
        identifiers=[],
        cover=None,
        files=[],
    )
    book.events.append(
        BookAddedEvent(
            book_uuid=book.uuid,
            library_uuid=uuid4(),
            title=book.title,
            title_sort=book.title_sort,
            published_at=book.published_at,
            stored_by_user_uuid=uuid4(),
        )
    )
    return book


class TestInMemoryBookRepo:
    async def test_get_authors(self, book):
        [first_author, second_author] = book.authors

        in_memory_books = []
        book_repo = InMemoryBookRepo(in_memory_books)

        await book_repo.add(book)
        book_found = await book_repo.get(book.uuid)
        assert isinstance(book_found, Book)
        assert isinstance(book_found.authors, list)
        assert len(book_found.authors) == 2
        assert first_author in book_found.authors
        assert second_author in book_found.authors


class TestSQLBookRepo:
    async def test_get_authors(self, book):
        [first_author, second_author] = book.authors

        async with async_session_maker() as session:
            book_repo = SqlBookRepo(session_overwrite=session)

            await book_repo.add(book)
            await session.flush()
            book_found = await book_repo.get(book.uuid)
            assert isinstance(book_found, Book)
            assert isinstance(book_found.authors, list)
            assert len(book_found.authors) == 2
            assert first_author in book_found.authors
            assert second_author in book_found.authors

            await session.rollback()
