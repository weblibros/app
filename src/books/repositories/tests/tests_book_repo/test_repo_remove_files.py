from uuid import uuid4

from sqlalchemy import exists

from src.books.domain import Book, BookFile, BookMimeType
from src.books.domain.events import BookAddedEvent
from src.books.repositories import SqlBookRepo
from src.books.repositories.models import BookFileRecords
from src.books.repositories.queries.selector import book_does_exists
from src.config.database import async_session_maker
from src.file_management.repositories.models import FileRecord


class TestSQLBookRepo:
    async def fake_file(self, session):
        file_record = FileRecord(
            uuid=uuid4(),
            path="fakepath",
            size_bytes=1,
            extension="fake",
            content_type="fake",
            storage_service_name="test",
            entity_class_name="File",
        )
        session.add(file_record)
        await session.flush()
        return file_record

    async def test_add_files(self):
        book_uuid = uuid4()

        async with async_session_maker() as session:
            fake_file = await self.fake_file(session)

            book = Book(
                uuid=book_uuid,
                title="fake book title",
                title_sort="fake title sort",
                isbn=None,
                published_at=None,
                authors=[],
                identifiers=[],
                cover=None,
                files=[BookFile(uuid=fake_file.uuid, href="/todo/epub", mime_type=BookMimeType.EPUB)],
            )
            book.events.append(
                BookAddedEvent(
                    book_uuid=book.uuid,
                    library_uuid=uuid4(),
                    title=book.title,
                    title_sort=book.title_sort,
                    published_at=book.published_at,
                    stored_by_user_uuid=uuid4(),
                )
            )
            book_repo = SqlBookRepo(session_overwrite=session)

            await book_repo.add(book)
            await session.flush()

            assert await book_does_exists(session, book_uuid) is True
            assert (
                await session.scalar(exists().where(BookFileRecords.file_record_uuid == fake_file.uuid).select())
                is True
            )

            await book_repo.remove(book)
            await session.flush()

            assert await book_does_exists(session, book_uuid) is False
            assert (
                await session.scalar(exists().where(BookFileRecords.file_record_uuid == fake_file.uuid).select())
                is False
            )
