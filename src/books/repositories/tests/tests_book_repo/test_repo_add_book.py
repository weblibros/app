from uuid import uuid4

import pytest
from sqlalchemy import select

from src.books.domain import Book
from src.books.domain.events import BookAddedEvent
from src.books.repositories import InMemoryBookRepo, SqlBookRepo
from src.books.repositories.models import BookModel
from src.config.database import async_session_maker


@pytest.fixture
def book():
    book = Book(
        uuid=uuid4(),
        title="fake plain title",
        title_sort="fake title sort",
        isbn=None,
        published_at=None,
        authors=[],
        identifiers=[],
        cover=None,
        files=[],
    )
    book.events.append(
        BookAddedEvent(
            book_uuid=book.uuid,
            library_uuid=uuid4(),
            title=book.title,
            title_sort=book.title_sort,
            published_at=book.published_at,
            stored_by_user_uuid=uuid4(),
        )
    )
    return book


class TestInMemoryBookRepo:
    async def test_add_plain_book(self, book):
        in_memory_books = []
        book_repo = InMemoryBookRepo(in_memory_books)

        await book_repo.add(book)
        [book_model] = in_memory_books

        assert book_model.uuid == book.uuid
        assert book_model.title == book.title


class TestSQLBookRepo:
    async def test_add_plain_book(self, book):
        async with async_session_maker() as session:
            book_repo = SqlBookRepo(session_overwrite=session)

            await book_repo.add(book)
            await session.flush()
            query = select(BookModel).where(BookModel.uuid == book.uuid)
            result = await session.execute(query)
            book_model = result.scalar_one_or_none()

            assert book_model.uuid == book.uuid
            assert book_model.title == book.title

            await session.rollback()
