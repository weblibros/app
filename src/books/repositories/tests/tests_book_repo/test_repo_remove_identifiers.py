from uuid import uuid4

from sqlalchemy import exists, select

from src.books.domain import Book, Identifier
from src.books.domain.events import BookAddedEvent
from src.books.repositories import SqlBookRepo
from src.books.repositories.models import IdentifierBookModel, IdentifierModel
from src.books.repositories.queries.selector import book_does_exists
from src.config.database import async_session_maker


class TestSQLBookRepo:
    async def test_remove_identifiers(self):
        book_uuid = uuid4()
        first_identifier = Identifier(
            key="fake 1",
            value="fake 1",
        )
        second_identifier = Identifier(
            key="fake 2",
            value="fake 2",
        )

        book = Book(
            uuid=book_uuid,
            title="fake book title",
            title_sort="fake title sort",
            isbn=None,
            published_at=None,
            authors=[],
            identifiers=[first_identifier, second_identifier],
            cover=None,
            files=[],
        )
        book.events.append(
            BookAddedEvent(
                book_uuid=book.uuid,
                library_uuid=uuid4(),
                title=book.title,
                title_sort=book.title_sort,
                published_at=book.published_at,
                stored_by_user_uuid=uuid4(),
            )
        )

        async with async_session_maker() as session:
            book_repo = SqlBookRepo(session_overwrite=session)

            await book_repo.add(book)
            await session.flush()
            first_identifier_model = (
                await session.execute(
                    select(IdentifierModel).where(
                        IdentifierModel.key == first_identifier.key,
                        IdentifierModel.value == first_identifier.value,
                    )
                )
            ).scalar_one_or_none()
            first_identifier_uuid = first_identifier_model.uuid
            second_identifier_model = (
                await session.execute(
                    select(IdentifierModel).where(
                        IdentifierModel.key == second_identifier.key,
                        IdentifierModel.value == second_identifier.value,
                    )
                )
            ).scalar_one_or_none()
            second_identifier_uuid = second_identifier_model.uuid

            assert await book_does_exists(session, book_uuid) is True
            assert await session.scalar(exists().where(IdentifierModel.uuid == first_identifier_uuid).select()) is True
            assert await session.scalar(exists().where(IdentifierModel.uuid == second_identifier_uuid).select()) is True
            assert await session.scalar(exists().where(IdentifierBookModel.book_uuid == book_uuid).select()) is True
            await book_repo.remove(book)
            await session.flush()

            assert await book_does_exists(session, book_uuid) is False
            assert await session.scalar(exists().where(IdentifierModel.uuid == first_identifier_uuid).select()) is False
            assert (
                await session.scalar(exists().where(IdentifierModel.uuid == second_identifier_uuid).select()) is False
            )
            assert await session.scalar(exists().where(IdentifierBookModel.book_uuid == book_uuid).select()) is False
