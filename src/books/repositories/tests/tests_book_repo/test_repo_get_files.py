from uuid import uuid4

from src.books.domain import Book, BookFile, BookMimeType
from src.books.domain.events import BookAddedEvent
from src.books.repositories import InMemoryBookRepo, SqlBookRepo
from src.config.database import async_session_maker
from src.file_management.repositories.models import FileRecord


class TestInMemoryBookRepo:
    async def test_get_files(self):
        book_uuid = uuid4()
        file_uuid = uuid4()

        book = Book(
            uuid=book_uuid,
            title="fake book title",
            title_sort="fake title sort",
            isbn=None,
            published_at=None,
            authors=[],
            identifiers=[],
            cover=None,
            files=[
                BookFile(
                    uuid=file_uuid,
                    href="/test/epub",
                    mime_type=BookMimeType.EPUB,
                )
            ],
        )
        book.events.append(
            BookAddedEvent(
                book_uuid=book.uuid,
                library_uuid=uuid4(),
                title=book.title,
                title_sort=book.title_sort,
                published_at=book.published_at,
                stored_by_user_uuid=uuid4(),
            )
        )
        book_repo = InMemoryBookRepo([])

        await book_repo.add(book)
        book_found = await book_repo.get(book_uuid)

        assert isinstance(book_found, Book)
        assert isinstance(book_found.files, list)
        assert book_found.files == [
            BookFile(
                uuid=file_uuid,
                href="/test/epub",
                mime_type=BookMimeType.EPUB,
            )
        ]


class TestSQLBookRepo:
    async def fake_file(self, session):
        file_record = FileRecord(
            uuid=uuid4(),
            path="fakepath",
            size_bytes=1,
            extension="fake",
            content_type="fake",
            storage_service_name="test",
            entity_class_name="File",
        )
        session.add(file_record)
        await session.flush()
        return file_record

    async def test_get_files(self):
        book_uuid = uuid4()

        async with async_session_maker() as session:
            fake_file = await self.fake_file(session)

            book = Book(
                uuid=book_uuid,
                title="fake book title",
                title_sort="fake title sort",
                isbn=None,
                published_at=None,
                authors=[],
                identifiers=[],
                cover=None,
                files=[
                    BookFile(
                        uuid=fake_file.uuid,
                        href="/unique/epub",
                        mime_type=BookMimeType.EPUB,
                    )
                ],
            )
            book.events.append(
                BookAddedEvent(
                    book_uuid=book.uuid,
                    library_uuid=uuid4(),
                    title=book.title,
                    title_sort=book.title_sort,
                    published_at=book.published_at,
                    stored_by_user_uuid=uuid4(),
                )
            )
            book_repo = SqlBookRepo(session_overwrite=session)

            await book_repo.add(book)
            await session.flush()
            book_found = await book_repo.get(book_uuid)

            assert isinstance(book_found, Book)
            assert isinstance(book_found.files, list)
            assert book_found.files == [
                BookFile(
                    uuid=fake_file.uuid,
                    href="/unique/epub",
                    mime_type=BookMimeType.EPUB,
                )
            ]
            await session.rollback()
