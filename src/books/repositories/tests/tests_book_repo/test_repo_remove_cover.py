from uuid import uuid4

from sqlalchemy import exists

from src.books.domain import Book, Cover, CoverPath
from src.books.domain.events import BookAddedEvent
from src.books.repositories import SqlBookRepo
from src.books.repositories.models import CoverModel
from src.books.repositories.queries.selector import book_does_exists
from src.config.database import async_session_maker
from src.file_management.repositories.models import FileRecord, ImageRecord


class TestSQLBookRepo:
    async def cover_image(self, session):
        file_record = FileRecord(
            uuid=uuid4(),
            path="fakepath",
            size_bytes=1,
            extension="fake",
            content_type="fake",
            storage_service_name="test",
            entity_class_name="ImageFile",
        )
        image_record = ImageRecord(
            uuid=uuid4(),
            file_record_uuid=file_record.uuid,
            extracted_from_file_record_uuid=None,
            pixel_height=1,
            pixel_width=1,
        )
        session.add_all([file_record, image_record])
        await session.flush()
        return image_record

    async def test_remove_cover(
        self,
    ):
        book_uuid = uuid4()
        cover_uuid = uuid4()

        async with async_session_maker() as session:
            cover_image_model = await self.cover_image(session)
            cover = Cover(
                uuid=cover_uuid,
                cover_path=CoverPath("fake/path"),
                image_uuid=cover_image_model.uuid,
            )

            book = Book(
                uuid=book_uuid,
                title="fake book title",
                title_sort="fake title sort",
                isbn=None,
                published_at=None,
                authors=[],
                identifiers=[],
                cover=cover,
                files=[],
            )
            book.events.append(
                BookAddedEvent(
                    book_uuid=book.uuid,
                    library_uuid=uuid4(),
                    title=book.title,
                    title_sort=book.title_sort,
                    published_at=book.published_at,
                    stored_by_user_uuid=uuid4(),
                )
            )
            book_repo = SqlBookRepo(session_overwrite=session)

            await book_repo.add(book)

            assert await book_does_exists(session, book_uuid) is True
            assert await session.scalar(exists().where(CoverModel.uuid == cover_uuid).select()) is True
            await book_repo.remove(book)
            await session.flush()

            assert await book_does_exists(session, book_uuid) is False
            assert await session.scalar(exists().where(CoverModel.uuid == cover_uuid).select()) is False
            await session.rollback()
