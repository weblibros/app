from uuid import uuid4

from src.books.domain import Book, Cover, CoverPath
from src.books.domain.events import BookAddedEvent
from src.books.repositories import InMemoryBookRepo, SqlBookRepo
from src.config.database import async_session_maker
from src.file_management.repositories.models import FileRecord, ImageRecord


class TestInMemoryBookRepo:
    async def test_get_cover(self):
        book_uuid = uuid4()
        cover_uuid = uuid4()

        cover = Cover(
            uuid=cover_uuid,
            cover_path=CoverPath("fake/path"),
            image_uuid=uuid4(),
        )

        book = Book(
            uuid=book_uuid,
            title="fake book title",
            title_sort="fake title sort",
            isbn=None,
            published_at=None,
            authors=[],
            identifiers=[],
            cover=cover,
            files=[],
        )
        book.events.append(
            BookAddedEvent(
                book_uuid=book.uuid,
                library_uuid=uuid4(),
                title=book.title,
                title_sort=book.title_sort,
                published_at=book.published_at,
                stored_by_user_uuid=uuid4(),
            )
        )
        book_repo = InMemoryBookRepo([])

        await book_repo.add(book)
        book_found = await book_repo.get(book_uuid)
        assert isinstance(book_found, Book)
        assert isinstance(book_found.cover, Cover)
        assert book_found.cover == cover


class TestSQLBookRepo:
    async def cover_image(self, session):
        file_record = FileRecord(
            uuid=uuid4(),
            path="fakepath",
            size_bytes=1,
            extension="fake",
            content_type="fake",
            storage_service_name="test",
            entity_class_name="ImageFile",
        )
        image_record = ImageRecord(
            uuid=uuid4(),
            file_record_uuid=file_record.uuid,
            extracted_from_file_record_uuid=None,
            pixel_height=1,
            pixel_width=1,
        )
        session.add_all([file_record, image_record])
        await session.flush()
        return image_record

    async def test_get_cover(self):
        book_uuid = uuid4()
        cover_uuid = uuid4()

        async with async_session_maker() as session:
            cover_image_model = await self.cover_image(session)
            cover = Cover(
                uuid=cover_uuid,
                cover_path=CoverPath("fake/path"),
                image_uuid=cover_image_model.uuid,
            )

            book = Book(
                uuid=book_uuid,
                title="fake book title",
                title_sort="fake title sort",
                isbn=None,
                published_at=None,
                authors=[],
                identifiers=[],
                cover=cover,
                files=[],
            )
            book.events.append(
                BookAddedEvent(
                    book_uuid=book.uuid,
                    library_uuid=uuid4(),
                    title=book.title,
                    title_sort=book.title_sort,
                    published_at=book.published_at,
                    stored_by_user_uuid=uuid4(),
                )
            )
            book_repo = SqlBookRepo(session_overwrite=session)

            await book_repo.add(book)
            await session.flush()
            book_found = await book_repo.get(book_uuid)
            assert isinstance(book_found, Book)
            assert isinstance(book_found.cover, Cover)
            assert book_found.cover == cover

            await session.rollback()
