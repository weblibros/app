from uuid import uuid4

import pytest

from src.books.domain import Book, Identifier
from src.books.domain.events import BookAddedEvent
from src.books.repositories import InMemoryBookRepo, SqlBookRepo
from src.config.database import async_session_maker


@pytest.fixture
def book():
    first_identifier = Identifier(
        key="fake 1",
        value="fake 1",
    )
    second_identifier = Identifier(
        key="fake 2",
        value="fake 2",
    )
    book = Book(
        uuid=uuid4(),
        title="fake plain title",
        title_sort="fake title sort",
        isbn=None,
        published_at=None,
        authors=[],
        identifiers=[first_identifier, second_identifier],
        cover=None,
        files=[],
    )
    book.events.append(
        BookAddedEvent(
            book_uuid=book.uuid,
            library_uuid=uuid4(),
            title=book.title,
            title_sort=book.title_sort,
            published_at=book.published_at,
            stored_by_user_uuid=uuid4(),
        )
    )
    return book


class TestInMemoryBookRepo:
    async def test_get_identifiers(self, book):
        [first_identifier, second_identifier] = book.identifiers
        book_repo = InMemoryBookRepo([])

        await book_repo.add(book)
        book_found = await book_repo.get(book.uuid)

        assert isinstance(book_found, Book)
        assert isinstance(book_found.identifiers, list)
        assert len(book_found.identifiers) == 2
        assert first_identifier in book_found.identifiers
        assert second_identifier in book_found.identifiers


class TestSQLBookRepo:
    async def test_get_identifiers(self, book):
        [first_identifier, second_identifier] = book.identifiers

        async with async_session_maker() as session:
            book_repo = SqlBookRepo(session_overwrite=session)

            await book_repo.add(book)
            await session.flush()
            book_found = await book_repo.get(book.uuid)

            assert isinstance(book_found, Book)
            assert isinstance(book_found.identifiers, list)
            assert len(book_found.identifiers) == 2
            assert first_identifier in book_found.identifiers
            assert second_identifier in book_found.identifiers

            await session.rollback()
