from uuid import uuid4

import pytest

from src.books.domain import Book
from src.books.domain.events import BookAddedEvent
from src.books.repositories import InMemoryBookRepo, SqlBookRepo
from src.books.repositories.queries.selector import book_does_exists
from src.config.database import async_session_maker


@pytest.fixture
def book():
    book = Book(
        uuid=uuid4(),
        title="fake plain title",
        title_sort="fake title sort",
        isbn=None,
        published_at=None,
        authors=[],
        identifiers=[],
        cover=None,
        files=[],
    )
    book.events.append(
        BookAddedEvent(
            book_uuid=book.uuid,
            library_uuid=uuid4(),
            title=book.title,
            title_sort=book.title_sort,
            published_at=book.published_at,
            stored_by_user_uuid=uuid4(),
        )
    )
    return book


class TestInMemoryBookRepo:
    async def test_remove_plain_book(self, book):
        in_memory_books = []
        book_repo = InMemoryBookRepo(in_memory_books)

        await book_repo.add(book)
        assert len(in_memory_books) == 1

        await book_repo.remove(book)
        assert len(in_memory_books) == 0


class TestSQLBookRepo:
    async def test_remove_plain_book(self, book):
        async with async_session_maker() as session:
            book_repo = SqlBookRepo(session_overwrite=session)

            await book_repo.add(book)
            await session.flush()

            await book_repo.remove(book)
            assert await book_does_exists(session, book.uuid) is False
