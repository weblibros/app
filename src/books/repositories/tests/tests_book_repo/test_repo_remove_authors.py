from uuid import uuid4

import pytest
from sqlalchemy import exists, select

from src.books.domain import Author, Book
from src.books.domain.events import BookAddedEvent
from src.books.repositories import SqlBookRepo
from src.books.repositories.models import AuthorBookModel, AuthorModel
from src.books.repositories.queries.selector import book_does_exists
from src.config.database import async_session_maker


@pytest.fixture
def book():
    first_author = Author(
        first_name="fake 1",
        last_name="fake 1",
    )
    second_author = Author(
        first_name="fake 2",
        last_name="fake 2",
    )
    book = Book(
        uuid=uuid4(),
        title="fake plain title",
        title_sort="fake title sort",
        isbn=None,
        published_at=None,
        authors=[first_author, second_author],
        identifiers=[],
        cover=None,
        files=[],
    )
    book.events.append(
        BookAddedEvent(
            book_uuid=book.uuid,
            library_uuid=uuid4(),
            title=book.title,
            title_sort=book.title_sort,
            published_at=book.published_at,
            stored_by_user_uuid=uuid4(),
        )
    )
    return book


class TestSQLBookRepo:
    async def test_remove_authors(self, book):
        [first_author, second_author] = book.authors
        async with async_session_maker() as session:
            book_repo = SqlBookRepo(session_overwrite=session)

            await book_repo.add(book)
            await session.flush()
            first_author_model = (
                await session.execute(
                    select(AuthorModel).where(
                        AuthorModel.first_name == first_author.first_name,
                        AuthorModel.last_name == first_author.last_name,
                    )
                )
            ).scalar_one_or_none()
            first_author_uuid = first_author_model.uuid
            second_author_model = (
                await session.execute(
                    select(AuthorModel).where(
                        AuthorModel.first_name == second_author.first_name,
                        AuthorModel.last_name == second_author.last_name,
                    )
                )
            ).scalar_one_or_none()
            second_author_uuid = second_author_model.uuid

            assert await book_does_exists(session, book.uuid) is True
            assert await session.scalar(exists().where(AuthorModel.uuid == first_author_uuid).select()) is True
            assert await session.scalar(exists().where(AuthorModel.uuid == second_author_uuid).select()) is True
            assert await session.scalar(exists().where(AuthorBookModel.book_uuid == book.uuid).select()) is True

            await book_repo.remove(book)
            await session.flush()

            assert await book_does_exists(session, book.uuid) is False
            assert await session.scalar(exists().where(AuthorModel.uuid == first_author_uuid).select()) is False
            assert await session.scalar(exists().where(AuthorModel.uuid == second_author_uuid).select()) is False
            assert await session.scalar(exists().where(AuthorBookModel.book_uuid == book.uuid).select()) is False

            await session.rollback()
