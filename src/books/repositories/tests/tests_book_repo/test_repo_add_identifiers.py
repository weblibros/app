from uuid import uuid4

import pytest
from sqlalchemy import select

from src.books.domain import Book, Identifier
from src.books.domain.events import BookAddedEvent
from src.books.repositories import InMemoryBookRepo, SqlBookRepo
from src.books.repositories.models import (
    BookModel,
    IdentifierBookModel,
    IdentifierModel,
)
from src.config.database import async_session_maker


@pytest.fixture
def book():
    first_identifier = Identifier(
        key="fake 1",
        value="fake 1",
    )
    second_identifier = Identifier(
        key="fake 2",
        value="fake 2",
    )
    book = Book(
        uuid=uuid4(),
        title="fake plain title",
        title_sort="fake title sort",
        isbn=None,
        published_at=None,
        authors=[],
        identifiers=[first_identifier, second_identifier],
        cover=None,
        files=[],
    )
    book.events.append(
        BookAddedEvent(
            book_uuid=book.uuid,
            library_uuid=uuid4(),
            title=book.title,
            title_sort=book.title_sort,
            published_at=book.published_at,
            stored_by_user_uuid=uuid4(),
        )
    )
    return book


class TestInMemoryBookRepo:
    async def test_add_identifiers(self, book):
        [first_identifier, second_identifier] = book.identifiers

        in_memory_books = []
        book_repo = InMemoryBookRepo(in_memory_books)

        await book_repo.add(book)
        [book_model] = in_memory_books
        [first_identifier_model, second_identifier_model] = book_model.identifiers

        assert book_model.uuid == book.uuid
        assert first_identifier_model.key == first_identifier.key
        assert first_identifier_model.value == first_identifier.value
        assert second_identifier_model.key == second_identifier.key
        assert second_identifier_model.value == second_identifier.value


class TestSQLBookRepo:
    async def test_add_identifiers(self, book):
        [first_identifier, second_identifier] = book.identifiers

        async with async_session_maker() as session:
            book_repo = SqlBookRepo(session_overwrite=session)

            await book_repo.add(book)
            await session.flush()
            book_model = (
                await session.execute(select(BookModel).where(BookModel.uuid == book.uuid))
            ).scalar_one_or_none()
            first_identifier_model = (
                await session.execute(
                    select(IdentifierModel).where(
                        IdentifierModel.key == first_identifier.key,
                        IdentifierModel.value == first_identifier.value,
                    )
                )
            ).scalar_one_or_none()
            first_identifier_uuid = first_identifier_model.uuid
            second_identifier_model = (
                await session.execute(
                    select(IdentifierModel).where(
                        IdentifierModel.key == second_identifier.key,
                        IdentifierModel.value == second_identifier.value,
                    )
                )
            ).scalar_one_or_none()
            second_identifier_uuid = second_identifier_model.uuid
            identifier_book_link_models = await session.scalars(
                select(IdentifierBookModel).where(IdentifierBookModel.book_uuid == book.uuid)
            )

            assert book_model.uuid == book.uuid
            assert first_identifier_model.key == first_identifier.key
            assert first_identifier_model.value == first_identifier.value
            assert second_identifier_model.key == second_identifier.key
            assert second_identifier_model.value == second_identifier.value
            identifier_book_link_identifier_uuids = [link.identifier_uuid for link in identifier_book_link_models]
            assert len(identifier_book_link_identifier_uuids) == 2
            assert first_identifier_uuid in identifier_book_link_identifier_uuids
            assert second_identifier_uuid in identifier_book_link_identifier_uuids

            await session.rollback()
