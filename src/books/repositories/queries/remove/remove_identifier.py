from typing import Protocol
from uuid import UUID

from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

from src.books.domain import Identifier
from src.books.repositories.models import IdentifierBookModel, IdentifierModel


class BookProtocol(Protocol):
    uuid: UUID


async def remove_identifier_book_link(
    session: AsyncSession,
    book: BookProtocol,
):
    query = delete(IdentifierBookModel).filter_by(book_uuid=book.uuid)
    await session.execute(query)


async def remove_identifier(
    session: AsyncSession,
    identifier: Identifier,
):
    query = delete(IdentifierModel).filter_by(
        key=identifier.key,
        value=identifier.value,
    )
    await session.execute(query)
