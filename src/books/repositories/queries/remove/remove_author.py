from typing import Protocol
from uuid import UUID

from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

from src.books.domain import Author

from ...models import AuthorBookModel, AuthorModel


class BookProtocol(Protocol):
    uuid: UUID


async def remove_author_book_link(
    session: AsyncSession,
    book: BookProtocol,
):
    query = delete(AuthorBookModel).filter_by(book_uuid=book.uuid)
    await session.execute(query)


async def remove_author(
    session: AsyncSession,
    author: Author,
):
    query = delete(AuthorModel).filter_by(
        first_name=author.first_name,
        last_name=author.last_name,
    )
    await session.execute(query)
