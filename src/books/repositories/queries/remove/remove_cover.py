from typing import Protocol
from uuid import UUID

from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

from ...models import CoverModel


class BookProtocol(Protocol):
    uuid: UUID


async def remove_covers_from_book(session: AsyncSession, book: BookProtocol):
    query = delete(CoverModel).filter_by(book_uuid=book.uuid)
    await session.execute(query)
