from uuid import UUID

from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

from ...models import BookFileRecords, BookModel


async def remove_book(
    session: AsyncSession,
    book_uuid: UUID,
):
    query_remove = delete(BookModel).filter_by(uuid=book_uuid)
    await session.execute(query_remove)


async def remove_book_file_records(
    session: AsyncSession,
    book_uuid: UUID,
):
    query_remove = delete(BookFileRecords).filter_by(book_uuid=book_uuid)
    await session.execute(query_remove)
