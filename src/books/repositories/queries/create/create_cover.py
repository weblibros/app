from typing import TypedDict
from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession

from ...models import CoverModel


class CoverInputDict(TypedDict):
    uuid: UUID
    book_uuid: UUID
    image_uuid: UUID
    static_file_path: str


async def create_cover(session: AsyncSession, cover_input: CoverInputDict) -> CoverModel:
    cover = CoverModel(**cover_input)  # type: ignore
    try:
        session.add(cover)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise e

    return cover
