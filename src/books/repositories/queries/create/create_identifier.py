from typing import Tuple
from uuid import UUID, uuid4

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from src.books.repositories.models import IdentifierBookModel, IdentifierModel


async def get_or_create_identifier(session: AsyncSession, key: str, value: str) -> Tuple[IdentifierModel, bool]:
    identifier = await session.scalar(
        select(IdentifierModel).where(IdentifierModel.value == value, IdentifierModel.key == key)
    )
    created = False
    if identifier is None:
        identifier = await create_identifier(session, key=key, value=value)
        created = True
    return identifier, created


async def create_identifier(session: AsyncSession, key: str, value: str) -> IdentifierModel:
    identifier_data = {
        "uuid": uuid4(),
        "key": key,
        "value": value,
    }
    identifier = IdentifierModel(**identifier_data)  # type: ignore
    try:
        session.add(identifier)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)

    return identifier


async def create_link_identifiers_to_book(
    session: AsyncSession,
    book_uuid: UUID,
    identifier_uuid_list: list[UUID],
):
    book_identifiers = [
        IdentifierBookModel(book_uuid=book_uuid, identifier_uuid=identifier_uuid)  # type: ignore
        for identifier_uuid in identifier_uuid_list
    ]
    try:
        session.add_all(book_identifiers)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)
