from datetime import datetime
from typing import TYPE_CHECKING, Optional, Sequence, TypedDict
from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession

from ...models import BookFileRecords, BookModel

if TYPE_CHECKING:
    from src.books.domain import Book, BookFile
    from src.modules.database.base_model import Base as SqlModel


class BookDict(TypedDict):
    uuid: UUID
    title: str
    title_sort: Optional[str]
    isbn: Optional[int]
    published_at: Optional[datetime]


class DataToLink(TypedDict):
    attribute_name: str
    data_models: list["SqlModel"]


async def create_book(
    session: AsyncSession,
    book_data: BookDict,
    attributes_to_link: list[DataToLink] = [],
) -> BookModel:
    book = BookModel(**book_data)  # type: ignore
    try:
        for data_to_link in attributes_to_link:
            link_attributes_to_book(book, data_to_link)
        session.add(book)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)

    return book


async def create_book_model(
    session: AsyncSession,
    book: "Book",
) -> BookModel:
    book_data = {
        "uuid": book.uuid,
        "title": book.title,
        "title_sort": book.title_sort,
        "isbn": book.isbn,
        "published_at": book.published_at,
    }
    book_model = BookModel(**book_data)  # type: ignore
    try:
        session.add(book_model)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)

    return book_model


def link_attributes_to_book(book: BookModel, data_to_link: DataToLink):
    attribute = getattr(book, data_to_link["attribute_name"], None)

    if attribute is None:
        raise Exception(f"class {book.__class__.__name__} does not containt attribute {data_to_link['attribute_name']}")
    attribute.extend(data_to_link["data_models"])


async def create_book_files(session: AsyncSession, book_uuid: UUID, book_files: Sequence["BookFile"]):
    book_file_records = [
        BookFileRecords(
            book_uuid=book_uuid,
            file_record_uuid=book_file.uuid,
            href=book_file.href,
            mime_type=book_file.mime_type.value,
        )  # type: ignore
        for book_file in book_files
    ]

    try:
        session.add_all(book_file_records)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)
