from typing import Tuple
from uuid import UUID, uuid4

from sqlalchemy import func, select
from sqlalchemy.ext.asyncio import AsyncSession

from ...models import AuthorBookModel, AuthorModel


async def get_or_create_author(session: AsyncSession, first_name: str, last_name: str) -> Tuple[AuthorModel, bool]:
    author = await session.scalar(
        select(AuthorModel).where(
            func.lower(AuthorModel.first_name) == first_name.lower(),
            func.lower(AuthorModel.last_name) == last_name.lower(),
        )
    )
    created = False
    if author is None:
        author = await create_author(session, first_name=first_name, last_name=last_name)
        created = True
    return author, created


async def create_author(session: AsyncSession, first_name: str, last_name: str) -> AuthorModel:
    author_data = {
        "uuid": uuid4(),
        "first_name": first_name,
        "last_name": last_name,
    }
    author = AuthorModel(**author_data)  # type: ignore
    try:
        session.add(author)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)

    return author


async def create_link_authors_to_book(
    session: AsyncSession,
    book_uuid: UUID,
    author_uuid_list: list[UUID],
):
    book_authors = [
        AuthorBookModel(book_uuid=book_uuid, author_uuid=author_uuid)  # type: ignore
        for author_uuid in author_uuid_list
    ]
    try:
        session.add_all(book_authors)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)
