from typing import Sequence
from uuid import UUID

from sqlalchemy import exists, select
from sqlalchemy.ext.asyncio import AsyncSession

from src.books.domain import Identifier
from src.books.repositories.models import IdentifierBookModel, IdentifierModel


async def identifier_by_book(session: AsyncSession, book_uuid: UUID) -> Sequence[IdentifierModel]:
    query = (
        select(IdentifierModel)
        .join_from(
            IdentifierBookModel,
            IdentifierModel,
            IdentifierBookModel.identifier_uuid == IdentifierModel.uuid,
        )
        .where(
            IdentifierBookModel.book_uuid == book_uuid,
        )
    )
    return (await session.scalars(query)).all()


async def identifier_used_in_other_books(session: AsyncSession, identifier: Identifier, book_uuid: UUID) -> bool:
    query = (
        select(IdentifierBookModel.uuid)
        .join_from(
            IdentifierBookModel,
            IdentifierModel,
            IdentifierBookModel.identifier_uuid == IdentifierModel.uuid,
        )
        .where(
            IdentifierModel.key == identifier.key,
            IdentifierModel.value == identifier.value,
            IdentifierBookModel.book_uuid != book_uuid,
        )
    )
    return await session.scalar(exists(query).select()) is True
