from uuid import UUID

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ...models import CoverModel


async def get_cover_by_book_uuid(session: AsyncSession, book_uuid: UUID) -> CoverModel | None:
    query_book = select(CoverModel).where(CoverModel.book_uuid == book_uuid)
    return (await session.execute(query_book)).scalar_one_or_none()
