from uuid import UUID

from sqlalchemy import exists, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import load_only

from src.books.domain import BookFile, BookMimeType
from src.libraries.repositories.models import LibraryBook

from ...models import BookFileRecords, BookModel


async def select_book_detail(session: AsyncSession, uuid: UUID) -> BookModel | None:
    query = select(BookModel).filter_by(uuid=uuid)
    result = await session.execute(query)
    book = result.scalar_one_or_none()
    return book


async def select_book_by_file_record(session: AsyncSession, file_record_uuid) -> BookModel | None:
    query = select(BookModel).join(BookFileRecords).where(BookFileRecords.file_record_uuid == file_record_uuid)
    return (await session.execute(query)).scalar_one_or_none()


async def book_does_exists(session: AsyncSession, book_uuid: UUID) -> bool:
    query_book_exists = exists().where(BookModel.uuid == book_uuid).select()
    return await session.scalar(query_book_exists) is True


async def book_exists_in_libraries(session: AsyncSession, book_uuid: UUID, library_uuids: list[UUID]) -> bool:
    query_book_exists = (
        exists()
        .where(
            LibraryBook.book_uuid == book_uuid,
            LibraryBook.library_uuid.in_(library_uuids),
        )
        .select()
    )
    return await session.scalar(query_book_exists) is True


async def select_book_files_by_book_uuid(session: AsyncSession, book_uuid: UUID) -> list[BookFile]:
    query = (
        select(BookFileRecords)
        .where(BookFileRecords.book_uuid == book_uuid)
        .options(
            load_only(
                BookFileRecords.file_record_uuid,
                BookFileRecords.href,
                BookFileRecords.mime_type,
            )
        )
    )
    result = (await session.scalars(query)).all()

    return [
        BookFile(uuid=item.file_record_uuid, href=item.href, mime_type=BookMimeType(item.mime_type)) for item in result
    ]
