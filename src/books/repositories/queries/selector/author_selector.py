from typing import Sequence
from uuid import UUID

from sqlalchemy import exists, select
from sqlalchemy.ext.asyncio import AsyncSession

from src.books.domain import Author

from ...models import AuthorBookModel, AuthorModel


async def authors_by_book(session: AsyncSession, book_uuid: UUID) -> Sequence[AuthorModel]:
    query = (
        select(AuthorModel)
        .join_from(
            AuthorBookModel,
            AuthorModel,
            AuthorBookModel.author_uuid == AuthorModel.uuid,
        )
        .where(
            AuthorBookModel.book_uuid == book_uuid,
        )
    )
    return (await session.scalars(query)).all()


async def author_used_in_other_books(session: AsyncSession, author: Author, book_uuid: UUID) -> bool:
    query = (
        select(AuthorBookModel.uuid)
        .join_from(
            AuthorBookModel,
            AuthorModel,
            AuthorBookModel.author_uuid == AuthorModel.uuid,
        )
        .where(
            AuthorModel.first_name == author.first_name,
            AuthorModel.last_name == author.last_name,
            AuthorBookModel.book_uuid != book_uuid,
        )
    )
    return await session.scalar(exists(query).select()) is True
