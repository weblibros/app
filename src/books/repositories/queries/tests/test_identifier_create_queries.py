from sqlalchemy import exists

from src.config.database import async_session_maker

from ...models import IdentifierModel
from ..create import get_or_create_identifier


class TestGetOrCreateIdentifierQuery:
    async def test_create_in_get_or_create(self):
        key = "test_create_in_get_or_create"
        value = "unique value for test ljhgdgeb"
        async with async_session_maker() as session:
            identifier, created = await get_or_create_identifier(session, key=key, value=value)

            identifier_exists_query = exists(IdentifierModel).where(IdentifierModel.uuid == identifier.uuid).select()

            identifier_exists = await session.scalar(identifier_exists_query)

            await session.rollback()

        assert identifier_exists is True
        assert created is True
        assert isinstance(identifier, IdentifierModel)
        assert identifier.key == key
        assert identifier.value == value

    async def test_get_in_get_or_create(self):
        key = "test_get_in_get_or_create"
        value = "unique value for test plkjg"
        async with async_session_maker() as session:
            session.add(IdentifierModel(key=key, value=value))
            await session.flush()
            identifier, created = await get_or_create_identifier(session, key=key, value=value)

            identifier_exists_query = exists(IdentifierModel).where(IdentifierModel.uuid == identifier.uuid).select()

            identifier_exists = await session.scalar(identifier_exists_query)
            identifier_key = identifier.key
            identifier_value = identifier.value

            await session.rollback()

        assert identifier_exists is True
        assert created is False
        assert isinstance(identifier, IdentifierModel)
        assert key == identifier_key
        assert value == identifier_value
