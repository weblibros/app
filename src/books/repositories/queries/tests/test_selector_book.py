from uuid import uuid4

import pytest

from src.config.database import async_session_maker
from src.file_management.repositories.models import FileRecord

from ...models import BookModel
from ..selector import select_book_by_file_record, select_book_detail


@pytest.fixture()
async def session_book_detail_selector():
    async with async_session_maker() as session:
        book = BookModel(title="book very detailed in selectors")
        session.add(book)
        await session.flush()
        yield session, book
        await session.rollback()


class TestBooksDetailSelector:
    async def test_get_book(self, session_book_detail_selector):
        session, book_detail_selector = session_book_detail_selector
        book = await select_book_detail(session, book_detail_selector.uuid)
        assert isinstance(book, BookModel)
        assert book.uuid == book_detail_selector.uuid

    async def test_get_no_book(self):
        async with async_session_maker() as session:
            book = await select_book_detail(session, uuid4())
            assert book is None


@pytest.fixture()
async def session_book_with_file_record():
    async with async_session_maker() as session:
        book = BookModel(title="book very detailed with file record in selectors")
        file_record = FileRecord(
            path="file_record_test_book_factory.epub",
            size_bytes=23,
            extension="epub",
            content_type="application/epub+zip",
            storage_service_name="FakeService",
            entity_class_name="EbookFile",
        )
        book.file_records.extend([file_record])
        session.add(book)
        await session.flush()
        yield session, book, file_record
        await session.rollback()


class TestSelectBookFromFileRecord:
    async def test_select_book(self, session_book_with_file_record):
        session, book, file_record = session_book_with_file_record
        found_book = await select_book_by_file_record(session, file_record.uuid)

        assert found_book is not None
        assert found_book.uuid == book.uuid

    async def test_select_book_no_filerecord(self):
        async with async_session_maker() as session:
            found_book = await select_book_by_file_record(session, uuid4())

        assert found_book is None
