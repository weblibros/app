import pytest
from sqlalchemy import select

from src.config.database import async_session_maker
from src.file_management.repositories.models import FileRecord

from ...models import BookFileRecords, BookModel
from ..create import create_book


async def test_create_book(session):
    book = await create_book(session, book_data={"title": "fake title"})
    assert isinstance(book, BookModel)
    await session.delete(book)
    await session.rollback()


async def test_wrong_input(session):
    with pytest.raises(TypeError) as e:
        await create_book(session, book_data={"wrong_title": "fake title"})
    assert "invalid" in str(e)


async def test_create_book_with_file_record(
    session,
):
    file_record = FileRecord(
        path="test_create_book_file_record.epub",
        size_bytes=23,
        extension="epub",
        content_type="application/epub+zip",
        storage_service_name="FakeService",
        entity_class_name="EbookFile",
    )
    async with async_session_maker() as session:
        session.add(file_record)
        book = await create_book(
            session,
            book_data={"title": "fake title"},
            attributes_to_link=[
                {
                    "attribute_name": "file_records",
                    "data_models": [file_record],
                }
            ],
        )
        assert isinstance(book, BookModel)
        query_filerecords = select(FileRecord).join(BookFileRecords).where(BookFileRecords.book_uuid == book.uuid)
        results_file_records = (await session.scalars(query_filerecords)).all()

        assert len(results_file_records) == 1
        assert isinstance(results_file_records[0], FileRecord)
        assert results_file_records[0].uuid == file_record.uuid

        await session.rollback()
