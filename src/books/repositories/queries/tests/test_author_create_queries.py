from sqlalchemy import exists

from src.books.repositories.models import AuthorModel
from src.books.repositories.queries.create import get_or_create_author
from src.config.database import async_session_maker


class TestGetOrCreateAuthorQuery:
    async def test_create_in_get_or_create(self):
        first_name = "test_create_in_get_or_create"
        last_name = "unique last_name for test ljhgdgeb"
        async with async_session_maker() as session:
            author, created = await get_or_create_author(session, first_name=first_name, last_name=last_name)

            author_exists_query = exists(AuthorModel).where(AuthorModel.uuid == author.uuid).select()

            author_exists = await session.scalar(author_exists_query)

            await session.rollback()

        assert author_exists is True
        assert created is True
        assert isinstance(author, AuthorModel)
        assert author.first_name == first_name
        assert author.last_name == last_name

    async def test_get_in_get_or_create(self):
        first_name = "test_get_in_get_or_create"
        last_name = "unique last_name for test plkjg"
        async with async_session_maker() as session:
            session.add(AuthorModel(first_name=first_name, last_name=last_name))
            await session.flush()
            author, created = await get_or_create_author(session, first_name=first_name, last_name=last_name)

            author_exists_query = exists(AuthorModel).where(AuthorModel.uuid == author.uuid).select()

            author_exists = await session.scalar(author_exists_query)
            author_first_name = author.first_name
            author_last_name = author.last_name

            await session.rollback()

        assert author_exists is True
        assert created is False
        assert isinstance(author, AuthorModel)
        assert first_name == author_first_name
        assert last_name == author_last_name
