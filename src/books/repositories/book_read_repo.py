import abc
from functools import partial
from typing import Optional
from uuid import UUID

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from src.books.dto import BookDetailDTO
from src.books.repositories.models import BookModel, CoverModel
from src.config.database import async_session_manager
from src.libraries.repositories.models import LibraryBook

from .queries import selector


class BookReadRepo(abc.ABC):
    @abc.abstractmethod
    async def book_exists_in_libraries(self, book_uuid: UUID, library_uuids: list[UUID]) -> bool:
        raise NotImplementedError

    @abc.abstractmethod
    async def get_book_detail(self, book_uuid: UUID) -> BookDetailDTO:
        raise NotImplementedError


class BookNotFoundException(Exception):
    def __init__(self, book_uuid: UUID):
        super().__init__(f"book :{book_uuid} was not found")


class SqlBookReadRepo(BookReadRepo):
    async_session_manager = partial(async_session_manager)

    def __init__(self, session_overwrite: Optional[AsyncSession] = None) -> None:
        self.session_overwrite = session_overwrite

    async def book_exists_in_libraries(self, book_uuid: UUID, library_uuids: list[UUID]) -> bool:
        async with self.async_session_manager() as session:
            book_exists = await selector.book_exists_in_libraries(session, book_uuid, library_uuids)
        return book_exists

    async def get_book_detail(self, book_uuid: UUID) -> BookDetailDTO:
        async with self.async_session_manager() as session:
            book_model = await selector.select_book_detail(session, book_uuid)
            if book_model:
                book_detail = BookDetailDTO.from_model(book_model)
            else:
                raise BookNotFoundException(book_uuid)
        return book_detail

    def books_by_library_query(self, libraries: list[UUID]):
        sub_query_book_list = select(LibraryBook.book_uuid.label("book_uuid")).where(
            LibraryBook.library_uuid.in_(libraries)
        )

        query_book = (
            select(
                BookModel.uuid.label("uuid"),
                BookModel.title.label("title"),
                CoverModel.static_file_path.label("static_file_path"),
            )
            .join(CoverModel, isouter=True)
            .where(BookModel.uuid.in_(sub_query_book_list))
        )
        return query_book
