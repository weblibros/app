import abc
from functools import partial
from typing import Optional
from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession

from src.books.domain import ISBN, Author, Book, Cover, CoverPath, Identifier
from src.books.domain.events import BookAddedEvent, BookCoverAddedEvent
from src.books.repositories.queries.create import create_cover
from src.books.repositories.queries.remove import remove_covers_from_book
from src.books.repositories.queries.selector import get_cover_by_book_uuid
from src.config.database import async_session_manager
from src.events import Event

from .queries import selector
from .queries.create import (
    create_book_files,
    create_book_model,
    create_link_authors_to_book,
    create_link_identifiers_to_book,
    get_or_create_author,
    get_or_create_identifier,
)
from .queries.remove import (
    remove_author,
    remove_author_book_link,
    remove_book,
    remove_book_file_records,
    remove_identifier,
    remove_identifier_book_link,
)
from .queries.selector import (
    author_used_in_other_books,
    authors_by_book,
    identifier_by_book,
    identifier_used_in_other_books,
    select_book_files_by_book_uuid,
)


class AbstractBookRepo(abc.ABC):
    seen: set[Book]

    @abc.abstractmethod
    async def get(self, book_uuid: UUID) -> Book | None:
        raise NotImplementedError

    @abc.abstractmethod
    async def add(self, book: Book):
        raise NotImplementedError

    @abc.abstractmethod
    async def remove(self, book: Book):
        raise NotImplementedError


class InMemoryBookRepo(AbstractBookRepo):
    _books_in_memory: list[Book]

    def __init__(self, books_in_memory: list[Book] = []):
        self._books_in_memory = books_in_memory
        self.seen = set()

    async def get(self, book_uuid: UUID) -> Book | None:
        found_books = [book for book in self._books_in_memory if book.uuid == book_uuid]
        if len(found_books) == 1:
            book = found_books[0]
            self.seen.add(book)
            return book
        else:
            return None

    async def add(self, book: Book):
        events = book.events.copy()
        for event in events:
            await self._handle(book, event)
        self.seen.add(book)

    async def _handle(self, book: Book, event: Event):
        if isinstance(event, BookAddedEvent):
            await self._handle_book_added_event(book)
        elif isinstance(event, BookCoverAddedEvent):
            await self._handle_cover_added_event(book)
        else:
            raise NotImplementedError(f"event {event} cannot be handled")

    async def _handle_book_added_event(self, book: Book):
        self._books_in_memory.append(book)

    async def _handle_cover_added_event(self, book: Book):
        found_book = await self.get(book.uuid)
        if found_book:
            found_book.cover = book.cover
        else:
            raise Exception(f"book {book.uuid} not found")

    async def remove(self, book: Book):
        self._books_in_memory.remove(book)
        self.seen.add(book)


class SqlBookRepo(AbstractBookRepo):
    async_session_manager = partial(async_session_manager)

    def __init__(self, session_overwrite: Optional[AsyncSession] = None) -> None:
        self.session_overwrite = session_overwrite
        self.seen = set()

    async def get(self, book_uuid: UUID) -> Book | None:
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            book_model = await selector.select_book_detail(session, book_uuid)
            if book_model:
                author_models = await authors_by_book(session, book_uuid)
                identifier_models = await identifier_by_book(session, book_uuid)
                cover_model = await get_cover_by_book_uuid(session, book_uuid)
                book_files = await select_book_files_by_book_uuid(session, book_uuid)

                title_sort = book_model.title_sort
                if title_sort is None:
                    raise Exception("Oops title_sort should not be None")

                book = Book(
                    uuid=book_model.uuid,
                    title=book_model.title,
                    title_sort=title_sort,
                    isbn=ISBN(str(book_model.isbn)) if book_model.isbn else None,
                    published_at=book_model.published_at,
                    cover=(
                        Cover(
                            uuid=cover_model.uuid,
                            image_uuid=cover_model.image_uuid,
                            cover_path=CoverPath(cover_model.static_file_path),
                        )
                        if cover_model
                        else None
                    ),
                    authors=[
                        Author(
                            first_name=author_model.first_name,
                            last_name=author_model.last_name,
                        )
                        for author_model in author_models
                    ],
                    identifiers=[
                        Identifier(
                            key=identifier_model.key,
                            value=identifier_model.value,
                        )
                        for identifier_model in identifier_models
                    ],
                    files=book_files,
                )
                self.seen.add(book)
                return book
            else:
                return None

    async def add(self, book: Book):
        events = book.events.copy()
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            for event in events:
                await self._handle(session, book, event)
        self.seen.add(book)

    async def _handle(
        self,
        async_session: AsyncSession,
        book: Book,
        event: Event,
    ):
        if isinstance(event, BookAddedEvent):
            await self._handle_book_added_event(async_session, book)
        elif isinstance(event, BookCoverAddedEvent):
            await self._handle_cover_added_event(async_session, book)
        else:
            raise NotImplementedError(f"event {event} cannot be handled")

    async def _handle_book_added_event(self, session: AsyncSession, book: Book):
        await create_book_model(session, book)
        await self._create_cover(session, book)
        await self._append_book_files_to_book(session, book)
        await self._append_identifiers_to_book_model(session, book)
        await self._append_authors_to_book_model(session, book)

    async def _handle_cover_added_event(self, session: AsyncSession, book: Book):
        await self._create_cover(session, book)

    async def _create_cover(self, session: AsyncSession, book: Book):
        if book.cover:
            cover_input = {
                "uuid": book.cover.uuid,
                "book_uuid": book.uuid,
                "image_uuid": book.cover.image_uuid,
                "static_file_path": book.cover.cover_path.value,
            }

            await create_cover(session, cover_input)  # type: ignore

    async def _append_book_files_to_book(self, session: AsyncSession, book: Book):
        current_book_files = await select_book_files_by_book_uuid(session, book.uuid)
        current_book_files_uuids = [file.uuid for file in current_book_files]
        book_files_to_add = [file for file in book.files if file.uuid not in current_book_files_uuids]
        await create_book_files(session, book.uuid, book_files_to_add)

    async def _append_identifiers_to_book_model(self, session: AsyncSession, book: Book):
        identifier_uuid_list = []
        for identifier in book.identifiers:
            identifier_model, _ = await get_or_create_identifier(session, key=identifier.key, value=identifier.value)
            identifier_uuid_list.append(identifier_model.uuid)
        await create_link_identifiers_to_book(
            session,
            book.uuid,
            identifier_uuid_list,
        )

    async def _append_authors_to_book_model(self, session: AsyncSession, book: Book):
        author_uuid_list = []
        for author in book.authors:
            author_model, _ = await get_or_create_author(
                session, first_name=author.first_name, last_name=author.last_name
            )
            author_uuid_list.append(author_model.uuid)
        await create_link_authors_to_book(
            session,
            book.uuid,
            author_uuid_list,
        )

    async def remove(self, book: Book):
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            await self._remove_filerecords(session, book)
            await self._remove_identifiers(session, book)
            await self._remove_authors(session, book)
            await self._remove_cover(session, book)
            await self._remove_book(session, book)
        self.seen.add(book)

    async def _remove_filerecords(self, session: AsyncSession, book: Book):
        await remove_book_file_records(session, book.uuid)

    async def _remove_identifiers(
        self,
        session: AsyncSession,
        book: Book,
    ):
        await remove_identifier_book_link(session, book)
        for identifier in book.identifiers:
            if not (await identifier_used_in_other_books(session, identifier, book.uuid)):
                await remove_identifier(session, identifier)

    async def _remove_authors(
        self,
        session: AsyncSession,
        book: Book,
    ):
        await remove_author_book_link(session, book)
        for author in book.authors:
            if not (await author_used_in_other_books(session, author, book.uuid)):
                await remove_author(session, author)

    async def _remove_cover(
        self,
        session: AsyncSession,
        book: Book,
    ):
        await remove_covers_from_book(session, book)

    async def _remove_book(self, session: AsyncSession, book: Book):
        await remove_book(session, book.uuid)
