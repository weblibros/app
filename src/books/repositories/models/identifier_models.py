from typing import TYPE_CHECKING
from uuid import UUID

import sqlalchemy as sa
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.config import table_names
from src.modules.database.base_model import Base, TimeStamp

if TYPE_CHECKING:
    from src.books.repositories.models import BookModel


class IdentifierModel(Base, TimeStamp):
    __tablename__ = table_names.IDENTIFIER_TABLE_NAME

    key: Mapped[str] = mapped_column(nullable=False)
    value: Mapped[str] = mapped_column(nullable=False)

    books: Mapped[list["BookModel"]] = relationship(
        "BookModel",
        secondary=table_names.IDENTIFIER_BOOK_TABLE_NAME,
        back_populates="identifiers",
        passive_deletes=True,
    )

    __table_args__ = (sa.UniqueConstraint("key", "value", name="_key_value_uc"),)


class IdentifierBookModel(Base, TimeStamp):
    __tablename__ = table_names.IDENTIFIER_BOOK_TABLE_NAME

    identifier_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.IDENTIFIER_TABLE_NAME}.uuid",
            ondelete="CASCADE",
        ),
    )
    book_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.BOOK_TABLE_NAME}.uuid",
            ondelete="CASCADE",
        ),
    )
