from datetime import datetime
from typing import TYPE_CHECKING, List, Optional
from uuid import UUID

import sqlalchemy as sa
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.config import table_names
from src.modules.database.base_model import Base, TimeStamp

if TYPE_CHECKING:
    from src.file_management.repositories.models import FileRecord

    from ..models import AuthorModel, CoverModel, IdentifierModel


class BookModel(Base, TimeStamp):
    __tablename__ = table_names.BOOK_TABLE_NAME

    title: Mapped[str] = mapped_column(sa.String, nullable=False)
    title_sort: Mapped[Optional[str]] = mapped_column(sa.String, nullable=True)
    isbn: Mapped[Optional[int]] = mapped_column(sa.Integer, nullable=True)
    published_at: Mapped[Optional[datetime]] = mapped_column(sa.DateTime, nullable=True)

    file_records: Mapped[List["FileRecord"]] = relationship(
        "FileRecord",
        secondary=table_names.BOOK_FILERECORDS_TABLE_NAME,
        back_populates="book",
        cascade="all, delete",
        passive_deletes=False,
    )
    authors: Mapped[List["AuthorModel"]] = relationship(
        "AuthorModel",
        secondary=table_names.AUTHOR_BOOK_TABLE_NAMES,
        back_populates="books",
        passive_deletes=True,
    )
    identifiers: Mapped[List["IdentifierModel"]] = relationship(
        "IdentifierModel",
        secondary=table_names.IDENTIFIER_BOOK_TABLE_NAME,
        back_populates="books",
        passive_deletes=True,
    )
    cover: Mapped[Optional["CoverModel"]] = relationship(
        "CoverModel",
        back_populates="book",
        cascade="all, delete",
        passive_deletes=False,
    )


class BookFileRecords(Base, TimeStamp):
    __tablename__ = table_names.BOOK_FILERECORDS_TABLE_NAME

    book_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(f"{table_names.BOOK_TABLE_NAME}.uuid"),
    )

    file_record_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.FILERECORD_TABLE_NAME}.uuid",
            ondelete="CASCADE",
        ),
    )
    href: Mapped[str] = mapped_column(sa.String, nullable=True)
    mime_type: Mapped[str] = mapped_column(sa.String, nullable=True)
