from typing import TYPE_CHECKING
from uuid import UUID

import sqlalchemy as sa
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.config import table_names
from src.modules.database.base_model import Base, TimeStamp

if TYPE_CHECKING:
    from src.books.repositories.models import BookModel


class AuthorModel(Base, TimeStamp):
    __tablename__ = table_names.AUTHOR_TABLE_NAMES

    first_name: Mapped[str] = mapped_column(nullable=False)
    last_name: Mapped[str] = mapped_column(nullable=False)

    books: Mapped[list["BookModel"]] = relationship(
        "BookModel",
        secondary=table_names.AUTHOR_BOOK_TABLE_NAMES,
        back_populates="authors",
        passive_deletes=True,
    )


class AuthorBookModel(Base, TimeStamp):
    __tablename__ = table_names.AUTHOR_BOOK_TABLE_NAMES

    author_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.AUTHOR_TABLE_NAMES}.uuid",
            ondelete="CASCADE",
        ),
    )
    book_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.BOOK_TABLE_NAME}.uuid",
            ondelete="CASCADE",
        ),
    )
