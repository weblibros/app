from typing import TYPE_CHECKING
from uuid import UUID

import sqlalchemy as sa
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.config import table_names
from src.modules.database.base_model import Base, TimeStamp

if TYPE_CHECKING:
    from src.books.repositories.models import BookModel
    from src.file_management.repositories.models import ImageRecord


class CoverModel(Base, TimeStamp):
    __tablename__ = table_names.BOOK_COVER_TABLE_NAME

    book_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.BOOK_TABLE_NAME}.uuid",
        ),
        nullable=False,
    )
    image_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.IMAGE_FILERECORD_TABLE_NAME}.uuid",
            ondelete="CASCADE",
        ),
        nullable=False,
    )

    static_file_path: Mapped[str] = mapped_column(sa.String(512), nullable=False)

    book: Mapped["BookModel"] = relationship(
        "BookModel",
        back_populates="cover",
    )

    image: Mapped["ImageRecord"] = relationship(
        "ImageRecord",
        back_populates="cover",
        cascade="all, delete",
        passive_deletes=False,
    )

    __table_args__ = (
        sa.UniqueConstraint("book_uuid", name="_book_uuid_uc"),
        sa.UniqueConstraint("image_uuid", name="_image_uuid_uc"),
    )
