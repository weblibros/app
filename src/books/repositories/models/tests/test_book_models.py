from unittest import mock
from uuid import uuid4

import pytest
from sqlalchemy import exists, func, select

from src.config.database import async_session_maker
from src.file_management.domain.events import FileManagmentEvents
from src.file_management.repositories.models import FileRecord

from ..book_models import BookFileRecords, BookModel


class TestLinkedBookAndFileRecordsDeleteBehaviour:
    BOOK_UUID = uuid4()
    FIRST_FILE_RECORD_UUID = uuid4()
    SECOND_FILE_RECORD_UUID = uuid4()

    @pytest.fixture(scope="function")
    async def session_book_with_files(self):
        file_data = {
            "size_bytes": 23,
            "extension": "epub",
            "content_type": "application/epub+zip",
            "storage_service_name": "FakeService",
            "entity_class_name": "EbookFile",
        }
        book = BookModel(uuid=self.BOOK_UUID, title="book to link")
        first_file_record = FileRecord(
            uuid=self.FIRST_FILE_RECORD_UUID,
            path="first_file_record_delete_single_file.epub",
            **file_data,
        )
        second_file_record = FileRecord(
            uuid=self.SECOND_FILE_RECORD_UUID,
            path="second_file_record_delete_single_file.epub",
            **file_data,
        )
        book.file_records.extend([first_file_record, second_file_record])
        async with async_session_maker() as session:
            session.add(book)
            await session.flush()
            yield session
            await session.rollback()

    async def test_cascade_deletion_of_single_filerecord(self, session_book_with_files):
        session = session_book_with_files

        count_query = (
            select(func.count()).select_from(BookFileRecords).where(BookFileRecords.book_uuid == self.BOOK_UUID)
        )
        count = (await session.execute(count_query)).scalar()
        assert count == 2

        second_file_record = await session.get(FileRecord, self.SECOND_FILE_RECORD_UUID)
        await session.delete(second_file_record)
        await session.flush()

        count_after_deletion = (await session.execute(count_query)).scalar()
        assert count_after_deletion == 1

        query_bookfilerecords = select(BookFileRecords).where(BookFileRecords.book_uuid == self.BOOK_UUID)
        result_bookfilerecords = (await session.scalars(query_bookfilerecords)).all()
        assert len(result_bookfilerecords) == 1
        book_filerecord = result_bookfilerecords[0]
        assert book_filerecord.file_record_uuid == self.FIRST_FILE_RECORD_UUID

        query_filerecords = select(FileRecord).join(BookFileRecords).where(BookFileRecords.book_uuid == self.BOOK_UUID)
        result_filerecords = (await session.scalars(query_filerecords)).all()
        assert len(result_filerecords) == 1
        file_record = result_filerecords[0]
        assert file_record.uuid == self.FIRST_FILE_RECORD_UUID

    async def test_book_exits_after_delete_filerecord(self, session_book_with_files):
        session = session_book_with_files

        query_book_exists = exists().where(BookModel.uuid == self.BOOK_UUID).select()

        exits_before = await session.scalar(query_book_exists)
        assert exits_before is True
        second_file_record = await session.get(FileRecord, self.SECOND_FILE_RECORD_UUID)
        await session.delete(second_file_record)
        await session.flush()
        exits_after = await session.scalar(query_book_exists)
        assert exits_after is True

    async def test_delete_book_cascade_delete_filerecords(self, session_book_with_files):
        session = session_book_with_files

        query_book_exists = exists().where(BookModel.uuid == self.BOOK_UUID).select()
        count_query_book_file_records = (
            select(func.count()).select_from(BookFileRecords).where(BookFileRecords.book_uuid == self.BOOK_UUID)
        )
        count_query_filerecords = (
            select(func.count())
            .select_from(FileRecord)
            .join(BookFileRecords)
            .where(BookFileRecords.book_uuid == self.BOOK_UUID)
        )

        exits_before = await session.scalar(query_book_exists)
        assert exits_before is True

        before_count_book_filerecords = (await session.execute(count_query_book_file_records)).scalar()
        assert before_count_book_filerecords == 2

        before_count_filerecords = (await session.execute(count_query_filerecords)).scalar()
        assert before_count_filerecords == 2

        book = await session.get(BookModel, self.BOOK_UUID)
        await session.delete(book)
        await session.flush()

        exits_after = await session.scalar(query_book_exists)
        assert exits_after is False

        after_count_book_filerecords = (await session.execute(count_query_book_file_records)).scalar()
        assert after_count_book_filerecords == 0

        after_count_filerecords = (await session.execute(count_query_filerecords)).scalar()
        assert after_count_filerecords == 0

    async def test_cascasde_deletion_when_filerecords_not_present_in_session(self, session_book_with_files):
        session = session_book_with_files

        query_book_exists = exists().where(BookModel.uuid == self.BOOK_UUID).select()
        count_query_book_file_records = (
            select(func.count()).select_from(BookFileRecords).where(BookFileRecords.book_uuid == self.BOOK_UUID)
        )
        count_query_filerecords = (
            select(func.count())
            .select_from(FileRecord)
            .join(BookFileRecords)
            .where(BookFileRecords.book_uuid == self.BOOK_UUID)
        )

        exits_before = await session.scalar(query_book_exists)
        assert exits_before is True

        before_count_book_filerecords = (await session.execute(count_query_book_file_records)).scalar()
        assert before_count_book_filerecords == 2

        before_count_filerecords = (await session.execute(count_query_filerecords)).scalar()
        assert before_count_filerecords == 2

        session.expire_all()
        book = await session.get(BookModel, self.BOOK_UUID)
        await session.delete(book)
        await session.flush()

        exits_after = await session.scalar(query_book_exists)
        assert exits_after is False

        after_count_book_filerecords = (await session.execute(count_query_book_file_records)).scalar()
        assert after_count_book_filerecords == 0

        after_count_filerecords = (await session.execute(count_query_filerecords)).scalar()
        assert after_count_filerecords == 0

    async def test_file_record_removed_event_called_after_book_deletion(self, session_book_with_files):
        session = session_book_with_files
        with mock.patch("src.file_management.repositories.models.file_record_models.dispatch") as mocked_dispatch:
            book = await session.get(BookModel, self.BOOK_UUID)
            await session.delete(book)
            await session.flush()

        mocked_dispatch.assert_called()

        assert mocked_dispatch.call_count == 2
        mocked_call_args = [call.args[0] for call in mocked_dispatch.call_args_list]

        assert mocked_call_args == [
            FileManagmentEvents.FILE_RECORD_REMOVED,
            FileManagmentEvents.FILE_RECORD_REMOVED,
        ]
