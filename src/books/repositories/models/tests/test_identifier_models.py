import pytest
from sqlalchemy import select
from sqlalchemy.exc import IntegrityError

from src.books.repositories.models import (
    BookModel,
    IdentifierBookModel,
    IdentifierModel,
)
from src.config.database import async_session_maker


class TestIdentifiersUniqueContrains:
    async def test_create_twice_same_key_value(self):
        key = "not_unique"
        value = "also_not_unique"
        first_identifier = IdentifierModel(key=key, value=value)
        second_identifier = IdentifierModel(key=key, value=value)

        async with async_session_maker() as session:
            session.add(first_identifier)
            session.add(second_identifier)
            with pytest.raises(IntegrityError) as e:
                await session.flush()

        assert "unique constraint" in str(e.value).lower()

    async def test_create_twice_same_value(self):
        first_key = "first_unique"
        second_key = "second_unique"
        value = "also_not_unique"
        first_identifier = IdentifierModel(key=first_key, value=value)
        second_identifier = IdentifierModel(key=second_key, value=value)

        async with async_session_maker() as session:
            session.add(first_identifier)
            session.add(second_identifier)
            await session.flush()

            query = select(IdentifierModel).where(IdentifierModel.value == value)
            results = (await session.scalars(query)).all()
            await session.rollback()

        results_uuids = [obj.uuid for obj in results]

        assert len(results_uuids) == 2
        assert first_identifier.uuid in results_uuids
        assert second_identifier.uuid in results_uuids

    async def test_create_twice_same_key(self):
        key = "not_unique"
        first_value = "first_identifier_unique"
        second_value = "second_identifier_unique"
        first_identifier = IdentifierModel(key=key, value=first_value)
        second_identifier = IdentifierModel(key=key, value=second_value)

        async with async_session_maker() as session:
            session.add(first_identifier)
            session.add(second_identifier)
            await session.flush()

            query = select(IdentifierModel).where(IdentifierModel.key == key)
            results = (await session.scalars(query)).all()
            await session.rollback()

        results_uuids = [obj.uuid for obj in results]

        assert len(results_uuids) == 2
        assert first_identifier.uuid in results_uuids
        assert second_identifier.uuid in results_uuids


class TestManytoManyBehaviourIdentifiersBooks:
    async def test_append_identifiers_to_book(self):
        first_identifier = IdentifierModel(key="first", value="first_value")
        second_identifier = IdentifierModel(key="second", value="second_value")
        book = BookModel(title="test append identifiers")
        book.identifiers.extend([first_identifier, second_identifier])
        async with async_session_maker() as session:
            session.add(book)
            await session.flush()
            query = select(IdentifierModel).join(IdentifierBookModel).where(IdentifierBookModel.book_uuid == book.uuid)
            results = (await session.scalars(query)).all()
            await session.rollback()

        results_uuids = [obj.uuid for obj in results]

        assert len(results_uuids) == 2
        assert first_identifier.uuid in results_uuids
        assert second_identifier.uuid in results_uuids

    async def test_append_books_to_identifier(self):
        identifier = IdentifierModel(key="test books to identifier", value="value")
        first_book = BookModel(title="first book to append to identifier")
        second_book = BookModel(title="second book to append to identifier")
        identifier.books.extend([first_book, second_book])
        async with async_session_maker() as session:
            session.add(identifier)
            await session.flush()
            query = (
                select(BookModel)
                .join(IdentifierBookModel)
                .where(IdentifierBookModel.identifier_uuid == identifier.uuid)
            )
            results = (await session.scalars(query)).all()
            await session.rollback()

        results_uuids = [obj.uuid for obj in results]

        assert len(results_uuids) == 2
        assert first_book.uuid in results_uuids
        assert second_book.uuid in results_uuids


class TestLinkedbehaviourCascadeDeleteIdentifiersBooks:
    async def test_cascade_delete_identifiers(self):
        first_identifier = IdentifierModel(key="first_cascade_delete", value="first_value")
        second_identifier = IdentifierModel(key="second_cascade_delete", value="second_value")
        book = BookModel(title="test cascade delete identifiers")
        book.identifiers.extend([first_identifier, second_identifier])
        async with async_session_maker() as session:
            session.add(book)
            await session.flush()
            await session.delete(first_identifier)
            await session.flush()
            query_identifiers = (
                select(IdentifierModel.uuid).join(IdentifierBookModel).where(IdentifierBookModel.book_uuid == book.uuid)
            )
            query_identifiers_book = select(IdentifierBookModel.identifier_uuid).where(
                IdentifierBookModel.book_uuid == book.uuid
            )
            query_books = (
                select(BookModel.uuid).join(IdentifierBookModel).where(IdentifierBookModel.book_uuid == book.uuid)
            )
            results_identifiers = (await session.scalars(query_identifiers)).all()
            results_identifiers_book = (await session.scalars(query_identifiers_book)).all()
            results_book = (await session.scalars(query_books)).all()
            await session.rollback()

        assert len(results_identifiers) == 1
        assert second_identifier.uuid in results_identifiers

        assert len(results_identifiers_book) == 1
        assert second_identifier.uuid in results_identifiers_book

        assert len(results_book) == 1
        assert book.uuid in results_book

    async def test_cascade_delete_books(self):
        identifier = IdentifierModel(key="test cascade delete books", value="value")
        first_book = BookModel(title="first book to test cascade delete identifier")
        second_book = BookModel(title="second book to test cascade delete identifier")
        identifier.books.extend([first_book, second_book])
        async with async_session_maker() as session:
            session.add(identifier)
            await session.flush()
            await session.delete(first_book)
            await session.flush()
            query_identifiers = (
                select(IdentifierModel.uuid)
                .join(IdentifierBookModel)
                .where(IdentifierBookModel.identifier_uuid == identifier.uuid)
            )
            query_identifiers_book = select(IdentifierBookModel.book_uuid).where(
                IdentifierBookModel.identifier_uuid == identifier.uuid
            )
            query_books = (
                select(BookModel.uuid)
                .join(IdentifierBookModel)
                .where(IdentifierBookModel.identifier_uuid == identifier.uuid)
            )
            results_identifiers = (await session.scalars(query_identifiers)).all()
            results_identifiers_book = (await session.scalars(query_identifiers_book)).all()
            results_book = (await session.scalars(query_books)).all()
            await session.rollback()

        assert len(results_identifiers) == 1
        assert identifier.uuid in results_identifiers

        assert len(results_identifiers_book) == 1
        assert second_book.uuid in results_identifiers_book

        assert len(results_book) == 1
        assert second_book.uuid in results_book
