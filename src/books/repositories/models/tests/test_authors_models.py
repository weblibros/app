from sqlalchemy import select

from src.books.repositories.models import AuthorBookModel, AuthorModel, BookModel
from src.config.database import async_session_maker


class TestManytoManyBehaviourAuthorsBooks:
    async def test_append_authors_to_book(self):
        first_author = AuthorModel(first_name="first jack", last_name="ripper")
        second_author = AuthorModel(first_name="second jack", last_name="ripper")
        book = BookModel(title="test_append_authors_to_books")
        book.authors.extend([first_author, second_author])

        async with async_session_maker() as session:
            session.add(book)
            await session.flush()
            query = select(AuthorModel).join(AuthorBookModel).where(AuthorBookModel.book_uuid == book.uuid)
            results = (await session.scalars(query)).all()
            await session.rollback()

        results_uuids = [obj.uuid for obj in results]

        assert len(results_uuids) == 2
        assert first_author.uuid in results_uuids
        assert second_author.uuid in results_uuids

    async def test_append_books_to_author(self):
        author = AuthorModel(first_name="to book jack", last_name="ripper")
        first_book = BookModel(title="first book to append to authors")
        second_book = BookModel(title="second book to append to authors")
        author.books.extend([first_book, second_book])

        async with async_session_maker() as session:
            session.add(author)
            await session.flush()
            query = select(BookModel).join(AuthorBookModel).where(AuthorBookModel.author_uuid == author.uuid)
            results = (await session.scalars(query)).all()
            await session.rollback()

        results_uuids = [obj.uuid for obj in results]

        assert len(results_uuids) == 2
        assert first_book.uuid in results_uuids
        assert second_book.uuid in results_uuids


class TestLinkedbehaviourCascadeDeleteAuthorsBooks:
    async def test_cascade_delete_authors_book(self):
        first_author = AuthorModel(first_name="first jack cascade delete", last_name="ripper")
        second_author = AuthorModel(first_name="second jack cascade delete", last_name="ripper")
        book = BookModel(title="test cascade delte authors")
        book.authors.extend([first_author, second_author])
        async with async_session_maker() as session:
            session.add(book)
            await session.flush()
            await session.delete(first_author)
            await session.flush()
            query_authors = select(AuthorModel.uuid).join(AuthorBookModel).where(AuthorBookModel.book_uuid == book.uuid)
            query_authors_book = select(AuthorBookModel.author_uuid).where(AuthorBookModel.book_uuid == book.uuid)
            query_books = select(BookModel.uuid).join(AuthorBookModel).where(AuthorBookModel.book_uuid == book.uuid)
            results_authors = (await session.scalars(query_authors)).all()
            results_authors_book = (await session.scalars(query_authors_book)).all()
            results_book = (await session.scalars(query_books)).all()
            await session.rollback()

        assert len(results_authors) == 1
        assert second_author.uuid in results_authors

        assert len(results_authors_book) == 1
        assert second_author.uuid in results_authors_book

        assert len(results_book) == 1
        assert book.uuid in results_book

    async def test_cascade_delete_books_author(self):
        author = AuthorModel(first_name="jack", last_name="ripper")
        first_book = BookModel(title="first book to delete")
        second_book = BookModel(title="second book to keep")
        author.books.extend([first_book, second_book])

        async with async_session_maker() as session:
            session.add(author)
            await session.flush()
            await session.delete(first_book)
            await session.flush()
            query_authors = (
                select(AuthorModel.uuid).join(AuthorBookModel).where(AuthorBookModel.author_uuid == author.uuid)
            )
            query_authors_book = select(AuthorBookModel.book_uuid).where(AuthorBookModel.author_uuid == author.uuid)
            query_books = select(BookModel.uuid).join(AuthorBookModel).where(AuthorBookModel.author_uuid == author.uuid)
            results_authors = (await session.scalars(query_authors)).all()
            results_authors_book = (await session.scalars(query_authors_book)).all()
            results_book = (await session.scalars(query_books)).all()
            await session.rollback()

        assert len(results_authors) == 1
        assert author.uuid in results_authors

        assert len(results_authors_book) == 1
        assert second_book.uuid in results_authors_book

        assert len(results_book) == 1
        assert second_book.uuid in results_book
