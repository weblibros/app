from uuid import uuid4

import pytest
from sqlalchemy import exists

from src.books.repositories.models import BookModel, CoverModel
from src.config.database import async_session_maker
from src.file_management.repositories.models import FileRecord, ImageRecord


class TestDeleteBehaviourCoverFileRecord:
    IMAGE_UUID = uuid4()
    FILE_RECORD_EBOOK = uuid4()
    FILE_RECORD_COVER = uuid4()
    BOOK_UUID = uuid4()
    COVER_UUID = uuid4()

    @pytest.fixture(scope="function")
    async def session_cover_book_and_files(self):
        file_data = {
            "size_bytes": 23,
            "extension": "epub",
            "content_type": "application/epub+zip",
            "storage_service_name": "FakeService",
            "entity_class_name": "File",
        }
        book = BookModel(uuid=self.BOOK_UUID, title="book test delete cover")
        file_record_book = FileRecord(
            uuid=self.FILE_RECORD_EBOOK,
            path="ebook_file_record_delete_file.epub",
            **file_data,
        )
        file_record_cover = FileRecord(
            uuid=self.FILE_RECORD_COVER,
            path="cover_file_record_delete.epub",
            **file_data,
        )
        book.file_records.extend([file_record_book])
        image = ImageRecord(
            uuid=self.IMAGE_UUID,
            file_record_uuid=file_record_cover.uuid,
            extracted_from_file_record_uuid=file_record_book.uuid,
            pixel_height=100,
            pixel_width=150,
        )
        cover = CoverModel(
            uuid=self.COVER_UUID,
            book_uuid=book.uuid,
            image_uuid=image.uuid,
            static_file_path="fake/link",
        )
        async with async_session_maker() as session:
            session.add_all([file_record_book, file_record_cover, cover, book, image])
            await session.flush()
            session.expire_all()
            yield session
            await session.rollback()

    async def test_cascase_delete_book(self, session_cover_book_and_files):
        session = session_cover_book_and_files
        assert await session.scalar(self.query_book_exists()) is True
        assert await session.scalar(self.query_cover_exists()) is True
        assert await session.scalar(self.query_image_exists()) is True
        assert await session.scalar(self.query_file_record_book_exists()) is True
        assert await session.scalar(self.query_file_record_cover_exists()) is True

        book = await session.get(BookModel, self.BOOK_UUID)
        await session.delete(book)
        await session.flush()
        assert await session.scalar(self.query_book_exists()) is False
        assert await session.scalar(self.query_cover_exists()) is False
        assert await session.scalar(self.query_image_exists()) is False
        assert await session.scalar(self.query_file_record_book_exists()) is False
        assert await session.scalar(self.query_file_record_cover_exists()) is False

    async def test_cascase_delete_cover(self, session_cover_book_and_files):
        session = session_cover_book_and_files
        assert await session.scalar(self.query_book_exists()) is True
        assert await session.scalar(self.query_cover_exists()) is True
        assert await session.scalar(self.query_image_exists()) is True
        assert await session.scalar(self.query_file_record_book_exists()) is True
        assert await session.scalar(self.query_file_record_cover_exists()) is True

        cover = await session.get(CoverModel, self.COVER_UUID)
        await session.delete(cover)
        await session.flush()
        assert await session.scalar(self.query_book_exists()) is True
        assert await session.scalar(self.query_cover_exists()) is False
        assert await session.scalar(self.query_image_exists()) is False
        assert await session.scalar(self.query_file_record_book_exists()) is True
        assert await session.scalar(self.query_file_record_cover_exists()) is False

    async def test_cascase_delete_image(self, session_cover_book_and_files):
        session = session_cover_book_and_files
        assert await session.scalar(self.query_book_exists()) is True
        assert await session.scalar(self.query_cover_exists()) is True
        assert await session.scalar(self.query_image_exists()) is True
        assert await session.scalar(self.query_file_record_book_exists()) is True
        assert await session.scalar(self.query_file_record_cover_exists()) is True

        image = await session.get(ImageRecord, self.IMAGE_UUID)
        await session.delete(image)
        await session.flush()
        assert await session.scalar(self.query_book_exists()) is True
        assert await session.scalar(self.query_cover_exists()) is False
        assert await session.scalar(self.query_image_exists()) is False
        assert await session.scalar(self.query_file_record_book_exists()) is True
        assert await session.scalar(self.query_file_record_cover_exists()) is False

    async def test_cascase_delete_image_file(self, session_cover_book_and_files):
        session = session_cover_book_and_files
        assert await session.scalar(self.query_book_exists()) is True
        assert await session.scalar(self.query_cover_exists()) is True
        assert await session.scalar(self.query_image_exists()) is True
        assert await session.scalar(self.query_file_record_book_exists()) is True
        assert await session.scalar(self.query_file_record_cover_exists()) is True

        image_file = await session.get(FileRecord, self.FILE_RECORD_COVER)
        await session.delete(image_file)
        await session.flush()
        assert await session.scalar(self.query_book_exists()) is True
        assert await session.scalar(self.query_cover_exists()) is False
        assert await session.scalar(self.query_image_exists()) is False
        assert await session.scalar(self.query_file_record_book_exists()) is True
        assert await session.scalar(self.query_file_record_cover_exists()) is False

    async def test_cascase_delete_book_file(self, session_cover_book_and_files):
        session = session_cover_book_and_files
        assert await session.scalar(self.query_book_exists()) is True
        assert await session.scalar(self.query_cover_exists()) is True
        assert await session.scalar(self.query_image_exists()) is True
        assert await session.scalar(self.query_file_record_book_exists()) is True
        assert await session.scalar(self.query_file_record_cover_exists()) is True

        book_file = await session.get(FileRecord, self.FILE_RECORD_EBOOK)
        await session.delete(book_file)
        await session.flush()
        assert await session.scalar(self.query_book_exists()) is True
        assert await session.scalar(self.query_cover_exists()) is True
        assert await session.scalar(self.query_image_exists()) is True
        assert await session.scalar(self.query_file_record_book_exists()) is False
        assert await session.scalar(self.query_file_record_cover_exists()) is True

    def query_book_exists(self):
        return exists().where(BookModel.uuid == self.BOOK_UUID).select()

    def query_cover_exists(self):
        return exists().where(CoverModel.uuid == self.COVER_UUID).select()

    def query_image_exists(self):
        return exists().where(ImageRecord.uuid == self.IMAGE_UUID).select()

    def query_file_record_book_exists(self):
        return exists().where(FileRecord.uuid == self.FILE_RECORD_EBOOK).select()

    def query_file_record_cover_exists(self):
        return exists().where(FileRecord.uuid == self.FILE_RECORD_COVER).select()


class TestUniqueOneToOneRelationship:
    async def test_create_cover_use_twice_with_same_book(self):
        file_data = {
            "size_bytes": 23,
            "extension": "epub",
            "content_type": "application/epub+zip",
            "storage_service_name": "FakeService",
            "entity_class_name": "File",
        }
        book = BookModel(uuid=uuid4(), title="book test delete cover")

        first_file_record_cover = FileRecord(
            uuid=uuid4(),
            path="cover_file_record_delete.epub",
            **file_data,
        )
        second_file_record_cover = FileRecord(
            uuid=uuid4(),
            path="cover_file_record_delete.epub",
            **file_data,
        )
        first_image = ImageRecord(
            uuid=uuid4(),
            file_record_uuid=first_file_record_cover.uuid,
            pixel_height=100,
            pixel_width=150,
        )
        second_image = ImageRecord(
            uuid=uuid4(),
            file_record_uuid=second_file_record_cover.uuid,
            pixel_height=100,
            pixel_width=150,
        )
        first_cover = CoverModel(
            uuid=uuid4(),
            book_uuid=book.uuid,
            image_uuid=first_image.uuid,
            static_file_path="fake/link",
        )
        second_cover = CoverModel(
            uuid=uuid4(),
            book_uuid=book.uuid,
            image_uuid=second_image.uuid,
            static_file_path="fake/link",
        )
        async with async_session_maker() as session:
            session.add_all([first_file_record_cover, first_cover, book, first_image])
            await session.flush()
            session.expire_all()
            with pytest.raises(Exception) as e:
                session.add(second_cover)
                await session.flush()
            assert "unique" in str(e.value).lower()
            assert "constraint" in str(e.value)
            await session.rollback()

    async def test_create_cover_twice_with_same_image(self):
        file_data = {
            "size_bytes": 23,
            "extension": "epub",
            "content_type": "application/epub+zip",
            "storage_service_name": "FakeService",
            "entity_class_name": "File",
        }
        first_book = BookModel(uuid=uuid4(), title="book test delete cover")
        second_book = BookModel(uuid=uuid4(), title="book test delete cover")

        file_record_cover = FileRecord(
            uuid=uuid4(),
            path="cover_file_record_delete.epub",
            **file_data,
        )
        image = ImageRecord(
            uuid=uuid4(),
            file_record_uuid=file_record_cover.uuid,
            pixel_height=100,
            pixel_width=150,
        )
        first_cover = CoverModel(
            uuid=uuid4(),
            book_uuid=first_book.uuid,
            image_uuid=image.uuid,
            static_file_path="fake/link",
        )
        second_cover = CoverModel(
            uuid=uuid4(),
            book_uuid=second_book.uuid,
            image_uuid=image.uuid,
            static_file_path="fake/link",
        )
        async with async_session_maker() as session:
            session.add_all([file_record_cover, first_cover, first_book, image])
            await session.flush()
            session.expire_all()
            with pytest.raises(Exception) as e:
                session.add(second_cover)
                await session.flush()
            assert "unique" in str(e.value).lower()
            assert "constraint" in str(e.value)
            await session.rollback()
