MY_BOOKS_URL = "/api/v1/books"
DETAIL_BOOK_URL = "/api/v1/books/{book_uuid}"
REMOVE_BOOK_URL = "/api/v1/books/{book_uuid}"
DOWNLOAD_BOOK_URL = "/api/v1/books/{book_uuid}/download"
