from fastapi import APIRouter

from src.books.features.book_detail import book_detail_router
from src.books.features.download_books.router import book_download_router
from src.books.features.my_books.router import book_list_router
from src.books.features.remove_book.router import book_remove_router

books_router = APIRouter()
books_router.include_router(book_detail_router)
books_router.include_router(book_list_router)
books_router.include_router(book_remove_router)
books_router.include_router(book_download_router)
