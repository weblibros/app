import abc
from typing import TYPE_CHECKING, Callable, Self

from fastapi_events.dispatcher import dispatch
from sqlalchemy.ext.asyncio import AsyncSession

from src.config.database import async_session_maker

from .repositories import AbstractBookRepo, InMemoryBookRepo, SqlBookRepo

if TYPE_CHECKING:
    from src.books.domain import Book
    from src.events import Event


class AbstractUnitOfWork(abc.ABC):
    books: AbstractBookRepo
    _dispatch: Callable[["Event"], None]

    async def __aenter__(self) -> Self:
        return self

    async def __aexit__(self, *args):
        await self.rollback()

    async def commit(self):
        await self._commit()
        self.publish_events()

    @abc.abstractmethod
    async def _commit(self):
        raise NotImplementedError

    @abc.abstractmethod
    async def rollback(self):
        raise NotImplementedError

    def publish_events(self):
        for books in self.books.seen:
            while books.events:
                event = books.events.pop(0)
                self._dispatch(event)


def empty_dispatch(event: "Event"):
    pass


class InMemoryUnitOfWork(AbstractUnitOfWork):
    books: InMemoryBookRepo

    def __init__(self, books_in_memory: list["Book"] = [], dispatch=empty_dispatch):
        self._books_in_memory = books_in_memory
        self.committed = False
        self._dispatch = dispatch

    async def __aenter__(self) -> Self:
        self.books = InMemoryBookRepo(self._books_in_memory)
        return await super().__aenter__()

    async def _commit(self):
        self.committed = True

    async def rollback(self):
        pass


class SqlAlchemyUnitOfWork(AbstractUnitOfWork):
    async_session: AsyncSession

    def __init__(self, async_sessionmaker=async_session_maker, dispatch=dispatch):
        self.async_sessionmaker = async_sessionmaker
        self._dispatch = dispatch

    async def __aenter__(self) -> Self:
        self.async_session = self.async_sessionmaker()
        self.books = SqlBookRepo(self.async_session)
        return await super().__aenter__()

    async def __aexit__(self, *args):
        await super().__aexit__(*args)
        await self.async_session.close()

    async def _commit(self):
        await self.async_session.commit()

    async def rollback(self):
        await self.async_session.rollback()
