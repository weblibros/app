ARTICLES_ENG = ["the", "a", "an"]
ARTICLES_NL = ["de", "het", "een"]
ARTICLES_ES = [
    "el",
    "la",
    "lo",
    "un",
    "una",
    "los",
    "las",
    "unos",
    "unas",
]
DEFAULT_ARTICLE_LANGUAGE = "eng"


def get_articles_by_languages(language) -> list["str"]:
    language_map = {
        "english": "eng",
        "eng": "eng",
        "dutch": "nl",
        "nederland": "nl",
        "nl": "nl",
        "spanish": "es",
        "spa": "es",
        "espanol": "es",
        "español": "es",
        "es": "es",
    }
    language_short = language_map.get(language, DEFAULT_ARTICLE_LANGUAGE)
    article_language_map = {
        "eng": ARTICLES_ENG,
        "nl": ARTICLES_NL,
        "es": ARTICLES_ES,
    }
    return article_language_map[language_short]
