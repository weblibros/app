from typing import TYPE_CHECKING, Optional
from uuid import UUID

from src.books.repositories import BookReadRepo, SqlBookReadRepo
from src.libraries.membership_roles import MembershipRole

if TYPE_CHECKING:
    from src.accounts.users import User


class BookPermissions:
    _read_repo: BookReadRepo

    def __init__(self, read_repo: Optional[BookReadRepo] = None):
        self._read_repo = read_repo or SqlBookReadRepo()

    async def user_can_read_book(self, user: "User", book_uuid: UUID) -> bool:
        libraries_uuid_list = user.get_libraries()
        return await self._read_repo.book_exists_in_libraries(book_uuid, libraries_uuid_list)

    async def user_can_remove_book(self, user: "User", book_uuid: UUID) -> bool:
        libraries_uuid_list = user.get_libraries(membership_roles=[MembershipRole.OWNER, MembershipRole.ADMIN])
        can_remove_book = await self._read_repo.book_exists_in_libraries(book_uuid, libraries_uuid_list)
        return can_remove_book

    async def user_can_download_book(self, user: "User", book_uuid: UUID) -> bool:
        libraries_uuid_list = user.get_libraries(
            membership_roles=[
                MembershipRole.OWNER,
                MembershipRole.ADMIN,
                MembershipRole.CONTRIBUTOR,
                MembershipRole.VIEWER,
            ]
        )
        can_download_book = await self._read_repo.book_exists_in_libraries(book_uuid, libraries_uuid_list)
        return can_download_book
