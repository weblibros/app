from datetime import datetime
from enum import StrEnum
from uuid import UUID

from fastapi_events.registry.payload_schema import registry as payload_schema
from pydantic import BaseModel

from src.events import Command, Event


class BookEvents(StrEnum):
    BOOK_STORED = "BOOK_STORED"
    BOOK_COVER_STORED = "BOOK_COVER_STORED"
    BOOK_REMOVED = "BOOK_REMOVED"


class DownloadLinkBookCmd(BaseModel, Command):
    book_uuid: UUID
    requested_by_user_uuid: UUID


class RemoveBookCmd(BaseModel, Command):
    book_uuid: UUID
    requested_by_user_uuid: UUID


@payload_schema.register
class BookAddedEvent(BaseModel, Event):
    __event_name__ = str(BookEvents.BOOK_STORED)

    book_uuid: UUID
    library_uuid: UUID
    title: str
    title_sort: str | None = None
    published_at: datetime | None = None
    stored_by_user_uuid: UUID


@payload_schema.register
class BookCoverAddedEvent(BaseModel, Event):
    __event_name__ = str(BookEvents.BOOK_COVER_STORED)

    book_uuid: UUID
    cover_uuid: UUID
    image_uuid: UUID


@payload_schema.register
class BookRemovedEvent(BaseModel, Event):
    __event_name__ = str(BookEvents.BOOK_REMOVED)

    book_uuid: UUID
    removed_by_user_uuid: UUID
    image_uuid_list: list[UUID]
    file_uuid_list: list[UUID]
