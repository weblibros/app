import pytest

from src.books.domain.aggregate import BookMimeType


def test_extract_mime_type_enum_from_str():
    value = "application/pdf"
    mime_type = BookMimeType.from_str(value)
    assert mime_type == BookMimeType.PDF


def test_extract_mime_type_enum_from_str_with_unknown_value_raises_an_error():
    value = "application/unknown"
    with pytest.raises(ValueError):
        BookMimeType.from_str(value)
