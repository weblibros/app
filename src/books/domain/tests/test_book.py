from datetime import datetime
from uuid import uuid4

from src.books.domain import Author, Book, BookFile, BookMimeType, Identifier


def fake_book():
    book = Book(
        uuid=uuid4(),
        title="The Hitchhiker's Guide to the Galaxy",
        title_sort="Hitchhiker's Guide to the Galaxy",
        isbn=None,
        published_at=datetime(2019, 1, 1),
        authors=[
            Author(
                first_name="Douglas",
                last_name="Adams",
            )
        ],
        identifiers=[
            Identifier(
                key="key",
                value="value",
            )
        ],
    )
    return book


def test_book_can_add_file():
    book = fake_book()
    assert len(book.files) == 0
    book.add_file(
        BookFile(
            uuid=uuid4(),
            href="/todo/epub",
            mime_type=BookMimeType.EPUB,
        )
    )
    assert len(book.files) == 1


def test_get_file_returns_first_file():
    book = fake_book()
    assert len(book.files) == 0
    first_book_file = BookFile(
        uuid=uuid4(),
        href="/todo/epub",
        mime_type=BookMimeType.EPUB,
    )
    book.add_file(first_book_file)
    book.add_file(
        BookFile(
            uuid=uuid4(),
            href="/todo/epub",
            mime_type=BookMimeType.EPUB,
        )
    )
    book.add_file(
        BookFile(
            uuid=uuid4(),
            href="/todo/epub",
            mime_type=BookMimeType.EPUB,
        )
    )
    assert book.get_file() == first_book_file
    assert len(book.files) == 3


def test_get_file_returns_none_if_no_files():
    book = fake_book()
    assert book.get_file() is None
