from dataclasses import dataclass


@dataclass
class Identifier:
    key: str
    value: str
