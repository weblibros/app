from dataclasses import dataclass
from uuid import UUID


@dataclass
class CoverPath:
    value: str


@dataclass
class Cover:
    uuid: UUID
    image_uuid: UUID
    cover_path: CoverPath

    @property
    def image_uuids(self) -> list[UUID]:
        return [self.image_uuid]
