from dataclasses import dataclass, field
from datetime import datetime
from enum import StrEnum
from typing import Optional, Self
from uuid import UUID

from src.books.domain.events import BookCoverAddedEvent
from src.events import Event

from .author import Author
from .cover import Cover
from .identifier import Identifier


@dataclass
class ISBN:
    value: str


class CoverIsAlreadySetException(Exception):
    pass


class BookMimeType(StrEnum):
    PDF = "application/pdf"
    EPUB = "application/epub+zip"
    MOBI = "application/x-mobipocket-ebook"

    @classmethod
    def from_str(cls, value: str) -> Self:
        return cls(value)


@dataclass
class BookFile:
    uuid: UUID
    href: str
    mime_type: BookMimeType


@dataclass
class Book:
    uuid: UUID
    title: str
    title_sort: str
    isbn: Optional[ISBN]
    published_at: Optional[datetime]
    authors: list[Author]
    identifiers: list[Identifier]
    cover: Optional[Cover] = None
    files: list[BookFile] = field(default_factory=lambda: [])
    events: list[Event] = field(default_factory=lambda: [])

    def add_cover(self, cover: Cover):
        if self.cover is None:
            self.cover = cover
            self.events.append(
                BookCoverAddedEvent(
                    book_uuid=self.uuid,
                    cover_uuid=cover.uuid,
                    image_uuid=cover.image_uuid,
                )
            )
        else:
            raise CoverIsAlreadySetException()

    def add_file(self, file: BookFile):
        self.files.append(file)

    @property
    def image_uuids(self) -> list[UUID]:
        return self.cover.image_uuids if self.cover else []

    @property
    def file_uuids(self) -> list[UUID]:
        return [item.uuid for item in self.files]

    def __hash__(self):
        return hash(self.uuid)

    def get_file(self) -> Optional[BookFile]:
        # TODO: support book with multiple files
        return self.files[0] if self.files else None
