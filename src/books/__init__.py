# import all linked child tables for books
# to build sqlachemy mapped relationships
from src.books.repositories.models import AuthorBookModel as AuthorBookModel
from src.books.repositories.models import CoverModel as CoverModel
from src.books.repositories.models import IdentifierBookModel as IdentifierBookModel
