from dataclasses import dataclass
from typing import TYPE_CHECKING, Self
from uuid import UUID

from src.libraries.membership_roles import MembershipRole

if TYPE_CHECKING:
    from src.libraries.repositories.models import Library, LibraryBook, LibraryMember


@dataclass(frozen=True)
class LibraryDTO:
    uuid: UUID
    name: str

    @classmethod
    def from_model(cls, library: "Library") -> Self:
        return cls(uuid=library.uuid, name=library.name)


@dataclass(frozen=True)
class MemberShipDTO:
    library_uuid: UUID
    membership_role: MembershipRole

    @classmethod
    def from_model(cls, library_member: "LibraryMember") -> Self:
        return cls(
            library_uuid=library_member.library_uuid,
            membership_role=MembershipRole(library_member.membership),
        )


@dataclass(frozen=True)
class LibraryBookDTO:
    book_uuid: UUID
    library_uuid: UUID

    @classmethod
    def from_model(cls, library_book: "LibraryBook") -> Self:
        return cls(
            book_uuid=library_book.book_uuid,
            library_uuid=library_book.library_uuid,
        )
