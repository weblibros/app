from typing import Sequence
from uuid import UUID

from src.libraries.dto import LibraryDTO
from src.libraries.repositories import LibraryReadRepo, LibraryWriteRepo


class BookAlreadyInLibrary(Exception):
    def __init__(self, book_uuid: UUID, libraries: Sequence[LibraryDTO]):
        self.message = f"book: {book_uuid} is already attached to the following libraries : {libraries}"

    def __str__(self):
        return self.message


class BookDoesNotExits(Exception):
    def __init__(self, book_uuid: UUID):
        self.message = f"book: {book_uuid} does not exist"

    def __str__(self):
        return self.message


class LibraryDoesNotExits(Exception):
    def __init__(self, library_uuid: UUID):
        self.message = f"library: {library_uuid} does not exist"

    def __str__(self):
        return self.message


async def add_book_to_library(library_uuid: UUID, book_uuid: UUID):
    library_read_repo = LibraryReadRepo()
    libraries_by_book = await library_read_repo.find_libraries_by_book(book_uuid)
    if not await library_read_repo.library_exists(library_uuid):
        raise LibraryDoesNotExits(library_uuid)
    elif not await library_read_repo.book_exists(book_uuid):
        raise BookDoesNotExits(book_uuid)
    elif libraries_by_book:
        raise BookAlreadyInLibrary(book_uuid, libraries_by_book)
    else:
        return await LibraryWriteRepo().add_book_to_library(library_uuid, book_uuid)
