from uuid import UUID

from src.accounts.repositories import UserRepo
from src.libraries.dto import LibraryDTO, MemberShipDTO
from src.libraries.membership_roles import MembershipRole
from src.libraries.repositories import LibraryReadRepo, LibraryWriteRepo


class UserIsAlreadyLibraryOwnerException(Exception):
    def __init__(self, user_uuid: UUID, user_memberships: list[MemberShipDTO]):
        owner_at = [
            str(member.library_uuid) for member in user_memberships if MembershipRole.OWNER == member.membership_role
        ]
        super().__init__(
            f"failed assign library as owner {user_uuid} user has already ownership at libraries {owner_at}"
        )


async def user_has_library_as_owner(user_uuid: UUID, membership_role: MembershipRole) -> bool:
    return await LibraryReadRepo().user_has_membership(user_uuid, membership_role=membership_role)


async def create_personal_library(user_uuid: UUID) -> LibraryDTO:
    membership_role = MembershipRole.OWNER
    if await user_has_library_as_owner(user_uuid, membership_role):
        user_memberships = await LibraryReadRepo().get_user_memberships(
            user_uuid=user_uuid, membership_role=membership_role
        )
        raise UserIsAlreadyLibraryOwnerException(user_uuid, user_memberships)

    library_name = await generate_personal_library_name(user_uuid)

    return await LibraryWriteRepo().create_library_with_member(library_name, user_uuid, membership_role)


async def generate_personal_library_name(user_uuid: UUID) -> str:
    user_email = await UserRepo().retrieve_user_email(user_uuid)
    if user_email:
        return f"{user_email}'s Library'"
    else:
        return "Personal Library"
