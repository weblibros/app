from typing import Sequence
from uuid import UUID

from src.libraries.dto import LibraryBookDTO
from src.libraries.repositories import LibraryReadRepo, LibraryWriteRepo


async def pop_book_off_library(book_uuid: UUID) -> Sequence[LibraryBookDTO]:
    library_books = await LibraryReadRepo().get_librarybooks_by_book(book_uuid)
    await LibraryWriteRepo().pop_book_out_library_book_collection(
        [library_book.book_uuid for library_book in library_books]
    )
    return library_books
