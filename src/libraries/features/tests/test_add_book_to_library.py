from uuid import uuid4

import pytest
from sqlalchemy import select

from src.books.repositories.models import BookModel
from src.config.database import async_session_maker
from src.libraries.features.add_book_to_library import (
    BookAlreadyInLibrary,
    BookDoesNotExits,
    LibraryDoesNotExits,
    add_book_to_library,
)
from src.libraries.repositories.models import Library, LibraryBook


class TestCreateLibraryBook:
    BOOK_UUID = uuid4()
    LIBRARY_UUID = uuid4()

    @pytest.fixture()
    async def fake_book(self):
        async with async_session_maker() as session:
            book = BookModel(uuid=self.BOOK_UUID, title="fake book")
            session.add(book)
            await session.commit()
        yield book
        async with async_session_maker() as session:
            await session.delete(book)
            await session.commit()

    @pytest.fixture()
    async def fake_library(self):
        async with async_session_maker() as session:
            library = Library(uuid=self.LIBRARY_UUID, name="fake library")
            session.add(library)
            await session.commit()
        yield library
        async with async_session_maker() as session:
            await session.delete(library)
            await session.commit()

    @pytest.fixture()
    async def fake_library_book(self, fake_library, fake_book):
        async with async_session_maker() as session:
            library_book = LibraryBook(book_uuid=fake_book.uuid, library_uuid=fake_library.uuid)
            session.add(library_book)
            await session.commit()
        yield library_book
        async with async_session_maker() as session:
            await session.delete(library_book)
            await session.commit()

    async def test_add_book_to_library(self, fake_library, fake_book):
        await add_book_to_library(book_uuid=fake_book.uuid, library_uuid=fake_library.uuid)
        async with async_session_maker() as session:
            query = select(LibraryBook).where(LibraryBook.library_uuid == self.LIBRARY_UUID)

            result = (await session.execute(query)).scalar_one_or_none()
            assert result is not None
            assert result.book_uuid == self.BOOK_UUID

    async def test_add_book_to_library_correct_results(self, fake_library, fake_book):
        book_library = await add_book_to_library(book_uuid=fake_book.uuid, library_uuid=fake_library.uuid)
        assert book_library.book_uuid == self.BOOK_UUID
        assert book_library.library_uuid == self.LIBRARY_UUID

    async def test_add_book_to_library_no_library(self, fake_book):
        library_uuid = uuid4()
        with pytest.raises(LibraryDoesNotExits) as e:
            await add_book_to_library(book_uuid=fake_book.uuid, library_uuid=library_uuid)
        assert str(library_uuid) in str(e.value)

    async def test_add_book_to_library_no_book(self, fake_library):
        book_uuid = uuid4()
        with pytest.raises(BookDoesNotExits) as e:
            await add_book_to_library(book_uuid=book_uuid, library_uuid=fake_library.uuid)
        assert str(book_uuid) in str(e.value)

    @pytest.mark.usefixtures("fake_library_book")
    async def test_add_book_to_library_already_attached(self, fake_library_book):
        book_uuid = fake_library_book.book_uuid
        library_uuid = fake_library_book.library_uuid
        with pytest.raises(BookAlreadyInLibrary) as e:
            await add_book_to_library(library_uuid=library_uuid, book_uuid=book_uuid)
        assert str(book_uuid) in str(e.value)
        assert str(library_uuid) in str(e.value)
