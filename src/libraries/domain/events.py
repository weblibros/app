from enum import StrEnum
from uuid import UUID

from fastapi_events.registry.payload_schema import registry as payload_schema
from pydantic import BaseModel


class LibraryEvents(StrEnum):
    LIBRARY_CREATED = "LIBRARY_CREATED"
    LIBRARY_APPEND_BOOK = "LIBRARY_APPEND_BOOK"
    LIBRARY_POP_BOOK = "LIBRARY_POP_BOOK"


@payload_schema.register(event_name=LibraryEvents.LIBRARY_CREATED)
class LibraryCreatedEvent(BaseModel):
    library_uuid: UUID
    user_uuid: UUID
    name: str


@payload_schema.register(event_name=LibraryEvents.LIBRARY_APPEND_BOOK)
class BookAddedToLibrary(BaseModel):
    library_uuid: UUID
    book_uuid: UUID


class LibraryBook(BaseModel):
    library_uuid: UUID
    book_uuid: UUID


@payload_schema.register(event_name=LibraryEvents.LIBRARY_POP_BOOK)
class BookPopOffLibrary(BaseModel):
    library_books: list[LibraryBook]
