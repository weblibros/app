from enum import StrEnum


class MembershipRole(StrEnum):
    OWNER = "Owner"
    ADMIN = "Administrator"
    CONTRIBUTOR = "Contributor"
    VIEWER = "Viewer"
