from typing import TYPE_CHECKING

import pytest
from faker import Faker
from sqlalchemy import func, select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from src.accounts.repositories.queries.create import create_user
from src.config.database import async_session_maker
from src.libraries.repositories.models import Library, LibraryMember
from src.libraries.repositories.queries.create import create_library

if TYPE_CHECKING:
    from src.accounts.repositories.models import UserModel


fake = Faker()


class TestCreateLibraryMember:
    async def member_user_factory(self, session: AsyncSession) -> "UserModel":
        user = await create_user(
            session,
            fake.email(),
            password="supersecret",
        )
        return user

    async def library_factory(self, session: AsyncSession) -> Library:
        return await create_library(session, fake.name())

    async def test_create_library_member(self):
        async with async_session_maker() as session:
            user = await self.member_user_factory(session)
            library = await self.library_factory(session)
            library_member = LibraryMember(
                user_uuid=user.uuid,
                library_uuid=library.uuid,
                membership="whatever",
            )
            session.add(library_member)
            await session.flush()
            library_member_uuid = library_member.uuid

            found_library_member = (
                await session.execute(select(LibraryMember).filter_by(uuid=library_member_uuid))
            ).scalar_one_or_none()
            assert found_library_member is not None
            await session.rollback()

    async def test_create_library_member_without_membership(self):
        async with async_session_maker() as session:
            user = await self.member_user_factory(session)
            library = await self.library_factory(session)
            library_member = LibraryMember(
                user_uuid=user.uuid,
                library_uuid=library.uuid,
            )
            session.add(library_member)
            with pytest.raises(IntegrityError) as e:
                await session.flush()

            assert "null" in str(e.value).lower()

    async def test_create_library_member_duplicate_user_and_library(self):
        async with async_session_maker() as session:
            user = await self.member_user_factory(session)
            library = await self.library_factory(session)
            library_member = LibraryMember(
                user_uuid=user.uuid,
                library_uuid=library.uuid,
                membership="whatever",
            )
            session.add(library_member)
            await session.flush()

            second_library_member = LibraryMember(
                user_uuid=user.uuid,
                library_uuid=library.uuid,
                membership="whatever",
            )
            session.add(second_library_member)
            with pytest.raises(IntegrityError) as e:
                await session.flush()

            assert "unique" in str(e.value).lower()

    async def test_create_multiple_memberships_for_same_user(self):
        async with async_session_maker() as session:
            user = await self.member_user_factory(session)
            first_library = await self.library_factory(session)
            second_library = await self.library_factory(session)
            first_library_member = LibraryMember(
                user_uuid=user.uuid,
                library_uuid=first_library.uuid,
                membership="whatever",
            )
            session.add(first_library_member)
            await session.flush()

            second_library_member = LibraryMember(
                user_uuid=user.uuid,
                library_uuid=second_library.uuid,
                membership="whatever",
            )
            session.add(second_library_member)
            await session.flush()
            count_library_member_query = select(func.count()).select_from(LibraryMember).filter_by(user_uuid=user.uuid)
            count_library_member_query = (await session.execute(count_library_member_query)).scalar()
            assert count_library_member_query == 2

    async def test_create_multiple_memberships_for_same_library(self):
        async with async_session_maker() as session:
            first_user = await self.member_user_factory(session)
            second_user = await self.member_user_factory(session)
            library = await self.library_factory(session)
            first_library_member = LibraryMember(
                user_uuid=first_user.uuid,
                library_uuid=library.uuid,
                membership="whatever",
            )
            session.add(first_library_member)
            await session.flush()

            second_library_member = LibraryMember(
                user_uuid=second_user.uuid,
                library_uuid=library.uuid,
                membership="whatever",
            )
            session.add(second_library_member)
            await session.flush()
            count_library_member_query = (
                select(func.count()).select_from(LibraryMember).filter_by(library_uuid=library.uuid)
            )
            count_library_member_query = (await session.execute(count_library_member_query)).scalar()
            assert count_library_member_query == 2
