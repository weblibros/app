from uuid import uuid4

import pytest
from faker import Faker
from sqlalchemy import exists, select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from src.books.repositories.models import BookModel
from src.books.repositories.queries.create import create_book
from src.config.database import async_session_maker
from src.libraries.repositories.models import Library, LibraryBook
from src.libraries.repositories.queries.create import create_library

fake = Faker()


class TestCreateLibraryBook:
    async def create_fake_book(self, session: AsyncSession) -> "BookModel":
        return await create_book(
            session,
            {
                "uuid": uuid4(),
                "title": fake.paragraph(nb_sentences=5),
                "title_sort": None,
                "isbn": None,
                "published_at": None,
            },
        )

    async def create_fake_library(self, session: AsyncSession) -> Library:
        return await create_library(session, f"{fake.first_name()}'s library")

    async def test_create_single_book_library(self):
        async with async_session_maker() as session:
            book = await self.create_fake_book(session)
            library = await self.create_fake_library(session)

            session.add_all([book, library])
            await session.flush()
            library_book = LibraryBook(book_uuid=book.uuid, library_uuid=library.uuid)  # type: ignore
            session.add(library_book)
            await session.flush()

            found_result = (
                await session.execute(select(LibraryBook).filter_by(book_uuid=book.uuid, library_uuid=library.uuid))
            ).scalar_one_or_none()

            assert found_result is not None
            assert found_result.book_uuid == book.uuid
            assert found_result.library_uuid == library.uuid
            await session.rollback()

    async def test_add_multiple_books_to_library(self):
        async with async_session_maker() as session:
            first_book = await self.create_fake_book(session)
            second_book = await self.create_fake_book(session)
            library = await self.create_fake_library(session)

            session.add_all([first_book, second_book, library])
            await session.flush()
            first_library_book = LibraryBook(book_uuid=first_book.uuid, library_uuid=library.uuid)  # type: ignore
            second_library_book = LibraryBook(book_uuid=second_book.uuid, library_uuid=library.uuid)  # type: ignore
            session.add_all([first_library_book, second_library_book])
            await session.flush()

            found_result = (await session.execute(select(LibraryBook).filter_by(library_uuid=library.uuid))).scalars()
            book_uuid_list = [obj.book_uuid for obj in found_result]
            assert found_result is not None
            assert len(book_uuid_list) == 2
            assert first_book.uuid in book_uuid_list
            assert second_book.uuid in book_uuid_list
            await session.rollback()

    async def test_add_book_to_multiple_libraries(self):
        async with async_session_maker() as session:
            book = await self.create_fake_book(session)
            first_library = await self.create_fake_library(session)
            second_library = await self.create_fake_library(session)

            session.add_all([book, first_library, second_library])
            await session.flush()
            first_library_book = LibraryBook(book_uuid=book.uuid, library_uuid=first_library.uuid)  # type: ignore
            session.add(first_library_book)
            await session.flush()
            with pytest.raises(IntegrityError) as e:
                second_library_book = LibraryBook(book_uuid=book.uuid, library_uuid=second_library.uuid)
                session.add(second_library_book)
                await session.flush()

            assert "duplicate key value violates unique constraint" or "UNIQUE constraint failed" in str(e.value)
            await session.rollback()


class TestDeleteLibraryBook:
    async def create_fake_book(self, session: AsyncSession) -> "BookModel":
        return await create_book(
            session,
            {
                "uuid": uuid4(),
                "title": fake.paragraph(nb_sentences=5),
                "title_sort": None,
                "isbn": None,
                "published_at": None,
            },
        )

    async def create_fake_library(self, session: AsyncSession) -> Library:
        return await create_library(session, f"{fake.first_name()}'s library")

    async def create_library_book(self, session: AsyncSession, library: Library, book: "BookModel") -> LibraryBook:
        library_book = LibraryBook(book_uuid=book.uuid, library_uuid=library.uuid)  # type: ignore

        session.add(library_book)
        await session.flush()
        return library_book

    def query_book_exists(self, book_uuid):
        return exists().where(BookModel.uuid == book_uuid).select()

    def query_library_exists(self, library_uuid):
        return exists().where(Library.uuid == library_uuid).select()

    def query_library_book_exists(self, library_book_uuid):
        return exists().where(LibraryBook.uuid == library_book_uuid).select()

    async def test_remove_book_behaviour(self):
        async with async_session_maker() as session:
            book = await self.create_fake_book(session)
            library = await self.create_fake_library(session)
            library_book = await self.create_library_book(session, library, book)
            book_uuid = book.uuid
            library_uuid = library.uuid
            library_book_uuid = library_book.uuid

            await session.delete(book)
            await session.flush()

            assert await session.scalar(self.query_book_exists(book_uuid)) is False
            assert await session.scalar(self.query_library_exists(library_uuid)) is True
            assert await session.scalar(self.query_library_book_exists(library_book_uuid)) is False

    async def test_remove_library_behaviour(self):
        async with async_session_maker() as session:
            book = await self.create_fake_book(session)
            library = await self.create_fake_library(session)
            library_book = await self.create_library_book(session, library, book)
            book_uuid = book.uuid
            library_uuid = library.uuid
            library_book_uuid = library_book.uuid

            await session.delete(library)
            await session.flush()

            assert await session.scalar(self.query_book_exists(book_uuid)) is True
            assert await session.scalar(self.query_library_exists(library_uuid)) is False
            assert await session.scalar(self.query_library_book_exists(library_book_uuid)) is False

    async def test_remove_library_book_behaviour(self):
        async with async_session_maker() as session:
            book = await self.create_fake_book(session)
            library = await self.create_fake_library(session)
            library_book = await self.create_library_book(session, library, book)
            book_uuid = book.uuid
            library_uuid = library.uuid
            library_book_uuid = library_book.uuid

            await session.delete(library_book)
            await session.flush()

            assert await session.scalar(self.query_book_exists(book_uuid)) is True
            assert await session.scalar(self.query_library_exists(library_uuid)) is True
            assert await session.scalar(self.query_library_book_exists(library_book_uuid)) is False
