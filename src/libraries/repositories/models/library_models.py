from uuid import UUID

import sqlalchemy as sa
from sqlalchemy.orm import Mapped, mapped_column

from src.config import table_names
from src.modules.database.base_model import Base, TimeStamp


class Library(Base, TimeStamp):
    __tablename__ = table_names.LIBRARY_TABLE_NAME

    name: Mapped[str] = mapped_column(sa.String, nullable=False)


class LibraryMember(Base, TimeStamp):
    __tablename__ = table_names.LIBRARY_MEMBER_TABLE_NAME

    library_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.LIBRARY_TABLE_NAME}.uuid",
            ondelete="CASCADE",
        )
    )

    user_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.USER_TABLE_NAME}.uuid",
            ondelete="CASCADE",
        )
    )

    membership: Mapped[str] = mapped_column(sa.String(20))

    __table_args__ = (sa.UniqueConstraint("library_uuid", "user_uuid", name="_library_user_uc"),)


class LibraryBook(Base, TimeStamp):
    __tablename__ = table_names.LIBRARY_BOOK_TABLE_NAME

    library_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.LIBRARY_TABLE_NAME}.uuid",
            ondelete="CASCADE",
        )
    )
    book_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.BOOK_TABLE_NAME}.uuid",
            ondelete="CASCADE",
        ),
    )
    __table_args__ = (sa.UniqueConstraint("book_uuid", name="_book_uc"),)
