from typing import Optional, Sequence
from uuid import UUID

from sqlalchemy import exists, select
from sqlalchemy.ext.asyncio import AsyncSession

from src.libraries.membership_roles import MembershipRole
from src.libraries.repositories.models import Library, LibraryBook, LibraryMember


async def get_user_memberships(
    session: AsyncSession,
    user_uuid: UUID,
    membership_role: Optional[MembershipRole] = None,
) -> Sequence[LibraryMember]:
    filter_kwargs: dict = {"user_uuid": user_uuid}
    if membership_role:
        filter_kwargs["membership"] = str(membership_role)
    query = select(LibraryMember).filter_by(**filter_kwargs)
    result = await session.execute(query)
    return result.scalars().all()


async def user_has_membership(
    session: AsyncSession,
    user_uuid: UUID,
    library_uuid: Optional[UUID] = None,
    membership_role: Optional[MembershipRole] = None,
) -> bool:
    where_args = [LibraryMember.user_uuid == user_uuid]
    if membership_role:
        where_args.append(LibraryMember.membership == str(membership_role))

    if library_uuid:
        where_args.append(LibraryMember.library_uuid == library_uuid)

    return await session.scalar(exists().where(*where_args).select()) is True


async def find_libraries_by_book_uuid(
    session: AsyncSession,
    book_uuid: UUID,
) -> Sequence[Library]:
    query = (
        select(Library)
        .join(LibraryBook, LibraryBook.library_uuid == Library.uuid)
        .where(LibraryBook.book_uuid == book_uuid)
    )

    return (await session.execute(query)).scalars().all()


async def find_librarybooks_by_book_uuid(
    session: AsyncSession,
    book_uuid: UUID,
) -> Sequence[LibraryBook]:
    query = select(LibraryBook).where(LibraryBook.book_uuid == book_uuid)

    return (await session.execute(query)).scalars().all()


async def library_does_exists(session: AsyncSession, library_uuid: UUID) -> bool:
    query_library_exists = exists().where(Library.uuid == library_uuid).select()
    return await session.scalar(query_library_exists) is True
