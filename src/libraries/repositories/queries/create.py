from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession

from src.libraries.membership_roles import MembershipRole
from src.libraries.repositories.models import Library, LibraryBook, LibraryMember


async def create_library(session: AsyncSession, name: str) -> Library:
    library = Library(name=name)  # type: ignore[call-arg]
    try:
        session.add(library)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)

    return library


async def create_library_member(
    session: AsyncSession,
    library_uuid: UUID,
    user_uuid: UUID,
    membership_role: MembershipRole,
) -> LibraryMember:
    library_member = LibraryMember(library_uuid=library_uuid, user_uuid=user_uuid, membership=membership_role.value)  # type: ignore[call-arg]
    try:
        session.add(library_member)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)

    return library_member


async def create_library_book(
    session: AsyncSession,
    library_uuid: UUID,
    book_uuid: UUID,
) -> LibraryBook:
    library_book = LibraryBook(library_uuid=library_uuid, book_uuid=book_uuid)  # type: ignore[call-arg]
    try:
        session.add(library_book)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)

    return library_book
