from sqlalchemy import select

from src.config.database import async_session_maker
from src.libraries.membership_roles import MembershipRole
from src.libraries.repositories.models import Library, LibraryMember
from src.libraries.repositories.queries import create


class TestCreateLibrary:
    async def test_create_library(self):
        async with async_session_maker() as session:
            library_name = "test_library"
            library = await create.create_library(session, library_name)

            assert isinstance(library, Library)
            assert library.name == library_name

    async def test_create_duplicated_library(self):
        async with async_session_maker() as session:
            library_name = "test_library"
            library_1 = await create.create_library(session, library_name)
            library_2 = await create.create_library(session, library_name)

            assert isinstance(library_1, Library)
            assert isinstance(library_2, Library)
            assert library_1.name == library_name
            assert library_2.name == library_name


class TestCreateLibraryMembership:
    async def test_create_library_member(self, user):
        async with async_session_maker() as session:
            library_name = "test_library"
            library = await create.create_library(session, library_name)
            library_member = await create.create_library_member(
                session,
                library_uuid=library.uuid,
                user_uuid=user.uuid,
                membership_role=MembershipRole.OWNER,
            )
            found_library_member = (
                await session.execute(select(LibraryMember).filter_by(uuid=library_member.uuid))
            ).scalar_one_or_none()
            assert found_library_member.library_uuid == library.uuid
            assert found_library_member.user_uuid == user.uuid
            assert isinstance(found_library_member.membership, str)
            assert found_library_member.membership == "Owner"
