from uuid import uuid4

from src.accounts.repositories.queries.create import create_user
from src.books.repositories.models import BookModel
from src.config.database import async_session_maker
from src.libraries.membership_roles import MembershipRole
from src.libraries.repositories.models import Library, LibraryBook
from src.libraries.repositories.queries import create, selector


class TestSelectorLibraryMembership:
    async def test_get_library_memberships_off_user(self):
        async with async_session_maker() as session:
            user = await create_user(session, "user_get_memberships@test.com", "fakepass")
            first_library = await create.create_library(session, "test_first_library")
            first_library_member = await create.create_library_member(
                session,
                library_uuid=first_library.uuid,
                user_uuid=user.uuid,
                membership_role=MembershipRole.OWNER,
            )
            second_library = await create.create_library(session, "test_second_library")
            second_library_member = await create.create_library_member(
                session,
                library_uuid=second_library.uuid,
                user_uuid=user.uuid,
                membership_role=MembershipRole.CONTRIBUTOR,
            )

            result = await selector.get_user_memberships(session, user.uuid)

            assert first_library_member in result
            assert second_library_member in result
            assert len(result) == 2


class TestUserHasMembership:
    async def test_new_user_has_no_membership(self):
        async with async_session_maker() as session:
            user = await create_user(session, "user_without_library@test.com", "fakepass")
            assert await selector.user_has_membership(session, user_uuid=user.uuid) is False

    async def test_has_membership(self):
        async with async_session_maker() as session:
            user = await create_user(session, "user_with_library@test.com", "fakepass")
            library = await create.create_library(session, "test_user_with_library")
            await create.create_library_member(
                session,
                library_uuid=library.uuid,
                user_uuid=user.uuid,
                membership_role=MembershipRole.CONTRIBUTOR,
            )
            assert await selector.user_has_membership(session, user_uuid=user.uuid) is True

    async def test_has_membership_in_library(self):
        async with async_session_maker() as session:
            user = await create_user(session, "user_with_library@test.com", "fakepass")
            library = await create.create_library(session, "test_user_with_library")
            await create.create_library_member(
                session,
                library_uuid=library.uuid,
                user_uuid=user.uuid,
                membership_role=MembershipRole.CONTRIBUTOR,
            )
            assert await selector.user_has_membership(session, user_uuid=user.uuid, library_uuid=library.uuid) is True

    async def test_has_membership_in_other_library(self):
        async with async_session_maker() as session:
            user = await create_user(session, "user_with_library@test.com", "fakepass")
            library = await create.create_library(session, "test_user_with_library")
            other_library = await create.create_library(session, "test_other_library")
            await create.create_library_member(
                session,
                library_uuid=library.uuid,
                user_uuid=user.uuid,
                membership_role=MembershipRole.CONTRIBUTOR,
            )
            assert (
                await selector.user_has_membership(session, user_uuid=user.uuid, library_uuid=other_library.uuid)
                is False
            )

    async def test_has_membership_specific_membership(self):
        async with async_session_maker() as session:
            user = await create_user(session, "user_with_library@test.com", "fakepass")
            library = await create.create_library(session, "test_user_with_library")
            await create.create_library_member(
                session,
                library_uuid=library.uuid,
                user_uuid=user.uuid,
                membership_role=MembershipRole.CONTRIBUTOR,
            )
            assert (
                await selector.user_has_membership(
                    session,
                    user_uuid=user.uuid,
                    membership_role=MembershipRole.CONTRIBUTOR,
                )
                is True
            )

    async def test_has_membership_specific_other_membership(self):
        async with async_session_maker() as session:
            user = await create_user(session, "user_with_library@test.com", "fakepass")
            library = await create.create_library(session, "test_user_with_library")
            await create.create_library_member(
                session,
                library_uuid=library.uuid,
                user_uuid=user.uuid,
                membership_role=MembershipRole.CONTRIBUTOR,
            )
            assert (
                await selector.user_has_membership(
                    session,
                    user_uuid=user.uuid,
                    membership_role=MembershipRole.VIEWER,
                )
                is False
            )


class TestFindLibraryByBook:
    BOOK_UUID = uuid4()
    LIBRARY_UUID = uuid4()

    async def test_find_library(self):
        async with async_session_maker() as session:
            book = BookModel(uuid=self.BOOK_UUID, title="fake book")
            library = Library(uuid=self.LIBRARY_UUID, name="fake library")
            library_book = LibraryBook(book_uuid=self.BOOK_UUID, library_uuid=self.LIBRARY_UUID)
            session.add(book)
            session.add(library)
            session.add(library_book)

            await session.flush()

            result = await selector.find_libraries_by_book_uuid(session, self.BOOK_UUID)

            assert isinstance(result, list)
            assert len(result) == 1
            assert result[0].uuid == self.LIBRARY_UUID

    async def test_find_library_no_book(self):
        async with async_session_maker() as session:
            result = await selector.find_libraries_by_book_uuid(session, self.BOOK_UUID)

            assert isinstance(result, list)
            assert len(result) == 0
