from uuid import uuid4

from sqlalchemy import select

from src.books.repositories.models import BookModel
from src.config.database import async_session_maker
from src.libraries.repositories.models import Library, LibraryBook
from src.libraries.repositories.queries import create


class TestCreateLibraryBook:
    BOOK_UUID = uuid4()
    LIBRARY_UUID = uuid4()

    async def test_add_book_to_library(self):
        async with async_session_maker() as session:
            book = BookModel(uuid=self.BOOK_UUID, title="fake book")
            library = Library(uuid=self.LIBRARY_UUID, name="fake library")
            session.add(book)
            session.add(library)

            await session.flush()

            await create.create_library_book(session, self.LIBRARY_UUID, self.BOOK_UUID)

            query = select(LibraryBook).where(LibraryBook.library_uuid == self.LIBRARY_UUID)

            result = (await session.execute(query)).scalar_one_or_none()
            assert result is not None
            assert result.book_uuid == self.BOOK_UUID
