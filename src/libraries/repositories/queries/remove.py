from uuid import UUID

from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

from src.libraries.repositories.models import Library, LibraryBook, LibraryMember


async def remove_library(session: AsyncSession, library_uuid: UUID):
    query_remove_members = delete(LibraryMember).filter_by(library_uuid=library_uuid)
    query_remove_library = delete(Library).filter_by(uuid=library_uuid)
    await session.execute(query_remove_members)
    await session.execute(query_remove_library)


async def remove_library_book(session: AsyncSession, book_uuids: list[UUID]):
    query_remove_library_book = delete(LibraryBook).where(LibraryBook.book_uuid.in_(book_uuids))
    await session.execute(query_remove_library_book)
