from functools import partial
from typing import Optional
from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession

from src.config.database import async_session_manager
from src.libraries.dto import LibraryBookDTO, LibraryDTO
from src.libraries.membership_roles import MembershipRole

from .queries import create, remove


class LibraryWriteRepo:
    async_session_manager = partial(async_session_manager)

    def __init__(self, session_overwrite: Optional[AsyncSession] = None) -> None:
        self.session_overwrite = session_overwrite

    async def create_library_with_member(
        self, name: str, user_uuid: UUID, membership_role: MembershipRole
    ) -> LibraryDTO:
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            library = await create.create_library(session, name)
            await create.create_library_member(
                session,
                library.uuid,
                user_uuid,
                membership_role,
            )
        return LibraryDTO.from_model(library)

    async def delete_library(self, library: LibraryDTO):
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            await remove.remove_library(session, library.uuid)

    async def add_book_to_library(self, library_uuid: UUID, book_uuid: UUID) -> LibraryBookDTO:
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            library_book_model = await create.create_library_book(
                session,
                library_uuid=library_uuid,
                book_uuid=book_uuid,
            )
        return LibraryBookDTO.from_model(library_book_model)

    async def pop_book_out_library_book_collection(self, book_uuids: list[UUID]):
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            await remove.remove_library_book(session, book_uuids)
