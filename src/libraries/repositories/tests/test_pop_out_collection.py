from uuid import uuid4

from sqlalchemy import select

from src.books.repositories.models import BookModel
from src.config.database import async_session_maker
from src.libraries.repositories import LibraryWriteRepo
from src.libraries.repositories.models import LibraryBook
from src.libraries.repositories.queries import create


class TestPopBookLibraryCollection:
    async def test_pop_single_book(self):
        book_uuid = uuid4()
        async with async_session_maker() as session:
            book = BookModel(uuid=book_uuid, title="fake book")
            session.add(book)
            await session.flush()
            library_name = "test_library"
            library = await create.create_library(session, library_name)
            library_uuid = library.uuid
            await create.create_library_book(session, library_uuid, book_uuid)

            query = select(LibraryBook).where(LibraryBook.library_uuid == library_uuid)
            result = (await session.execute(query)).scalar_one_or_none()
            assert result is not None
            assert result.book_uuid == book_uuid

            write_repo = LibraryWriteRepo(session)

            await write_repo.pop_book_out_library_book_collection([book_uuid])
            result = (await session.execute(query)).scalar_one_or_none()
            assert result is None

    async def test_pop_multiple_books(self):
        book_1_uuid = uuid4()
        book_2_uuid = uuid4()
        book_3_uuid = uuid4()
        async with async_session_maker() as session:
            book_1 = BookModel(uuid=book_1_uuid, title="fake book 1")
            book_2 = BookModel(uuid=book_2_uuid, title="fake book 2")
            book_3 = BookModel(uuid=book_3_uuid, title="fake book 3")
            session.add_all([book_1, book_2, book_3])
            await session.flush()

            library_name = "test_library"
            library = await create.create_library(session, library_name)
            library_uuid = library.uuid
            await create.create_library_book(session, library_uuid, book_1_uuid)
            await create.create_library_book(session, library_uuid, book_2_uuid)
            await create.create_library_book(session, library_uuid, book_3_uuid)

            query = select(LibraryBook).where(LibraryBook.library_uuid == library_uuid)
            before_results = (await session.scalars(query)).all()
            assert len(before_results) == 3
            before_book_uuids = [result.book_uuid for result in before_results]
            assert book_1_uuid in before_book_uuids
            assert book_2_uuid in before_book_uuids
            assert book_3_uuid in before_book_uuids

            write_repo = LibraryWriteRepo(session)

            await write_repo.pop_book_out_library_book_collection([book_1_uuid, book_3_uuid])
            after_results = (await session.scalars(query)).all()
            assert len(after_results) == 1
            after_book_uuids = [result.book_uuid for result in after_results]
            assert book_1_uuid not in after_book_uuids
            assert book_2_uuid in after_book_uuids
            assert book_3_uuid not in after_book_uuids
