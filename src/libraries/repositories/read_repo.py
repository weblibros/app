from functools import partial
from typing import Optional, Sequence
from uuid import UUID

from src.books.repositories.queries import selector as book_selector
from src.config.database import async_session_manager
from src.libraries.dto import LibraryBookDTO, LibraryDTO, MemberShipDTO
from src.libraries.membership_roles import MembershipRole

from .queries import selector


class LibraryReadRepo:
    async_session_manager = partial(async_session_manager)

    async def get_user_memberships(
        self, user_uuid: UUID, membership_role: Optional[MembershipRole]
    ) -> list[MemberShipDTO]:
        async with self.async_session_manager() as session:
            memberships = await selector.get_user_memberships(
                session,
                user_uuid=user_uuid,
                membership_role=membership_role,
            )
            return [MemberShipDTO.from_model(membership) for membership in memberships]

    async def user_has_membership(
        self,
        user_uuid: UUID,
        library_uuid: Optional[UUID] = None,
        membership_role: Optional[MembershipRole] = None,
    ) -> bool:
        async with self.async_session_manager() as session:
            return await selector.user_has_membership(session, user_uuid, library_uuid, membership_role)

    async def find_libraries_by_book(self, book_uuid: UUID) -> Sequence[LibraryDTO]:
        async with self.async_session_manager() as session:
            return [
                LibraryDTO.from_model(library_model)
                for library_model in await selector.find_libraries_by_book_uuid(session, book_uuid)
            ]

    async def library_exists(self, library_uuid: UUID) -> bool:
        async with self.async_session_manager() as session:
            return await selector.library_does_exists(session, library_uuid)

    async def book_exists(self, book_uuid: UUID) -> bool:
        async with self.async_session_manager() as session:
            return await book_selector.book_does_exists(session, book_uuid)

    async def get_librarybooks_by_book(self, book_uuid: UUID) -> Sequence[LibraryBookDTO]:
        async with self.async_session_manager() as session:
            library_book_models = await selector.find_librarybooks_by_book_uuid(session, book_uuid)
            library_book_dtos = [
                LibraryBookDTO.from_model(library_book_model) for library_book_model in library_book_models
            ]
        return library_book_dtos
