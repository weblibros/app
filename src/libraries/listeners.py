from fastapi_events.dispatcher import dispatch
from fastapi_events.handlers.local import local_handler
from fastapi_events.typing import Event

from src.accounts.domain.events import UserEvents
from src.accounts.users.dto import UserCreatedEventDTO
from src.books.domain.events import BookEvents
from src.libraries.domain.events import LibraryEvents
from src.libraries.features.add_book_to_library import add_book_to_library
from src.libraries.features.create_personal_library import create_personal_library
from src.libraries.features.pop_book_off_library import pop_book_off_library


@local_handler.register(event_name=UserEvents.USER_CREATED)
async def email_validation_sent_on_user_creation(
    event: Event,
):
    _, payload = event
    user_created_event_dto = UserCreatedEventDTO.from_dict(payload)
    library = await create_personal_library(user_created_event_dto.user_uuid)
    if library:
        dispatch(
            LibraryEvents.LIBRARY_CREATED,
            payload={
                "user_uuid": user_created_event_dto.user_uuid,
                "library_uuid": library.uuid,
                "name": library.name,
            },
        )


@local_handler.register(event_name=BookEvents.BOOK_STORED)
async def add_book_to_library_event(
    event: Event,
):
    _, payload = event
    book_uuid = payload["book_uuid"]
    library_uuid = payload["library_uuid"]

    book_library = await add_book_to_library(book_uuid=book_uuid, library_uuid=library_uuid)
    if book_library:
        dispatch(
            LibraryEvents.LIBRARY_APPEND_BOOK,
            payload={
                "library_uuid": library_uuid,
                "book_uuid": book_uuid,
            },
        )


@local_handler.register(event_name=BookEvents.BOOK_REMOVED)
async def pop_book_off_library_event(event: Event):
    _, payload = event
    book_uuid = payload["book_uuid"]

    library_books = await pop_book_off_library(book_uuid)
    if library_books:
        dispatch(
            LibraryEvents.LIBRARY_POP_BOOK,
            payload={
                "library_books": [
                    {
                        "library_uuid": library_book.library_uuid,
                        "book_uuid": library_book.book_uuid,
                    }
                    for library_book in library_books
                ],
            },
        )
