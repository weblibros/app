from collections.abc import AsyncGenerator
from datetime import datetime
from typing import TYPE_CHECKING
from uuid import uuid4

import pytest

from src.accounts.repositories import UserRepo
from src.accounts.repositories.queries.create import create_user
from src.config.database import async_session_maker
from src.libraries.features.create_personal_library import (
    UserIsAlreadyLibraryOwnerException,
    create_personal_library,
)
from src.libraries.membership_roles import MembershipRole
from src.libraries.repositories.queries import create, remove, selector

if TYPE_CHECKING:
    from src.accounts.users import User


@pytest.fixture()
async def user() -> AsyncGenerator["User", None]:
    async with async_session_maker() as session:
        user_model = await create_user(
            session,
            email=f"user_test_personal_lib_{uuid4().hex}@fake.com",
            password="supersecret",
            is_active=True,
            email_verified_on=datetime(2000, 1, 1, 1, 00, 00, 0),
        )
        await session.commit()
    user = await UserRepo().get_by_uuid(user_model.uuid)
    yield user
    async with async_session_maker() as session:
        await session.delete(user_model)
        await session.commit()


class TestCreatePersonalLibrary:
    async def test_create_personal_library(self, user):
        library = await create_personal_library(user.uuid)

        user_email = await UserRepo().retrieve_user_email(user.uuid)
        assert user_email in library.name

        async with async_session_maker() as session:
            result = await selector.get_user_memberships(session, user.uuid)
            await remove.remove_library(session, library.uuid)
            await session.commit()
            assert len(result) == 1
            assert result[0].user_uuid == user.uuid

    async def test_create_duplicated_personal_library(self, user):
        async with async_session_maker() as session:
            first_library = await create.create_library(session, "test_first_library")
            await create.create_library_member(
                session,
                library_uuid=first_library.uuid,
                user_uuid=user.uuid,
                membership_role=MembershipRole.OWNER,
            )
            await session.commit()
        with pytest.raises(UserIsAlreadyLibraryOwnerException) as e:
            await create_personal_library(user.uuid)

        async with async_session_maker() as session:
            await remove.remove_library(session, first_library.uuid)

        assert str(user.uuid) in str(e.value)
        assert str(first_library.uuid) in str(e.value)
        assert "DTO" not in str(e.value)
