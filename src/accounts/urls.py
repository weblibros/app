USER_ME_URL = "/api/v1/user/me"
USER_SIGNUP_URL = "/api/v1/user/signup"
USER_EMAIL_VALIDATION_URL = "/api/v1/user/email/validate"
USER_RESEND_EMAIL_VALIDATION_URL = "/api/v1/user/email/validate/re-request"
USER_PASSWORD_RESET_TOKEN_URL = "/api/v1/user/reset-password/token"
USER_PASSWORD_RESET_EMAIL_URL = "/api/v1/user/reset-password/email-request"

REFRESH_TOKEN_URL = "/api/v1/auth/token/refresh"

PASSWORD_LOGIN_USER_URL = "/api/v1/auth/login"
PASSWORD_RESET_URL = "/api/v1/auth/reset-password"
