from functools import partial
from typing import Optional
from uuid import UUID

from pydantic import EmailStr

from src.accounts.repositories.models import UserModel
from src.accounts.repositories.queries import create, remove, selector, update
from src.accounts.users import User
from src.config.database import async_session_manager


class UserNotFoundException(Exception):
    """User not found"""


class UserRepo:
    async_session_manager = partial(async_session_manager)

    async def signup_user_with_email_password(self, email: EmailStr, password: str, **kwargs) -> tuple[UserModel, bool]:
        async with self.async_session_manager() as session:
            found_user = await selector.find_user_by_email(session, email)
            if found_user:
                return found_user, False
            else:
                user = await create.create_user(session, email, password, **kwargs)
                return user, True

    async def update_user_password(self, user_uuid: UUID, password: str) -> UserModel | None:
        async with self.async_session_manager() as session:
            user = await selector.find_user_by_uuid(session, user_uuid)
            if user is not None:
                user = await update.update_user_password(session, user, password)
            return user

    async def verify_email_and_activate_user(self, user_uuid: UUID) -> UserModel | None:
        async with self.async_session_manager() as session:
            user = await selector.find_user_by_uuid(session, user_uuid)
            if user is not None:
                user = await update.verify_email_and_activate_user(session, user)
            return user

    async def remove_user_by_uuid(self, user_uuid: UUID):
        async with self.async_session_manager() as session:
            user = await selector.find_user_by_uuid(session, user_uuid)
            if user is not None:
                await remove.remove_user(session, user)
                return user_uuid
            else:
                return None

    async def remove_user(self, user: UserModel):
        async with self.async_session_manager() as session:
            await remove.remove_user(session, user)

    async def find_by_uuid(self, user_uuid: UUID) -> Optional[User]:
        async with self.async_session_manager() as session:
            user_model = await selector.find_user_by_uuid(session, user_uuid)
            membership_models = await selector.find_user_memberships(session, user_uuid)

            if user_model:
                return User.from_model(
                    user_model=user_model,
                    membership_models=membership_models,
                )
            else:
                return None

    async def get_by_uuid(self, user_uuid: UUID) -> User:
        user = await self.find_by_uuid(user_uuid)
        if user:
            return user
        else:
            raise UserNotFoundException()

    async def find_by_email(self, user_email: str) -> Optional[User]:
        async with self.async_session_manager() as session:
            user_model = await selector.find_user_by_email(session, user_email)

            if user_model:
                membership_models = await selector.find_user_memberships(session, user_model.uuid)
                return User.from_model(
                    user_model=user_model,
                    membership_models=membership_models,
                )
            else:
                return None

    async def get_by_email(self, user_email: str) -> User:
        user = await self.find_by_email(user_email)
        if user:
            return user
        else:
            raise UserNotFoundException()

    async def retrieve_user_uuid(self, email: EmailStr) -> Optional[UUID]:
        async with self.async_session_manager() as session:
            user = await selector.find_user_by_email(session, email)
            if user:
                return user.uuid
            else:
                return None

    async def retrieve_user_email(self, user_uuid: UUID) -> Optional[str]:
        async with self.async_session_manager() as session:
            user = await selector.find_user_by_uuid(session, user_uuid)
            if user:
                return user.email
            else:
                return None
