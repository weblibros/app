from datetime import datetime
from typing import Optional

import sqlalchemy as sa
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy_utils import EmailType

from src.config import table_names
from src.modules.database.base_model import Base, TimeStamp


class UserModel(Base, TimeStamp):
    __tablename__ = table_names.USER_TABLE_NAME

    email: Mapped[str] = mapped_column(EmailType, unique=True, nullable=False)
    email_verified_on: Mapped[Optional[datetime]] = mapped_column(sa.DateTime, default=None, nullable=True)
    encrypted_password: Mapped[Optional[str]] = mapped_column(sa.String, nullable=True)
    is_active: Mapped[bool] = mapped_column(sa.Boolean, default=False, nullable=False)
