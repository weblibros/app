from src.accounts.repositories.models import UserModel


def test_create_model():
    email = "test@fake.com"
    password = "fakepass"
    user = UserModel(email=email, encrypted_password=password)
    assert isinstance(user, UserModel)
    assert isinstance(user.email, str)
    assert isinstance(user.encrypted_password, str)
    assert user.email_verified_on is None
