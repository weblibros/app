from sqlalchemy.ext.asyncio import AsyncSession

from src.accounts import password_hasher
from src.accounts.repositories.models import UserModel


async def create_user(session: AsyncSession, email: str, password: str, **kwargs) -> UserModel:
    hashed_password = password_hasher.hash_password(password)

    user = UserModel(email=email, encrypted_password=hashed_password, **kwargs)  # type: ignore[call-arg]
    try:
        session.add(user)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)

    return user
