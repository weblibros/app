from typing import Sequence
from uuid import UUID

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from src.accounts.repositories.models import UserModel
from src.libraries.repositories.models import LibraryMember


async def find_user_by_kwargs(session: AsyncSession, **kwargs) -> UserModel | None:
    query = select(UserModel).filter_by(**kwargs)
    result = await session.execute(query)
    user = result.scalar_one_or_none()
    return user


async def find_user_by_email(session: AsyncSession, email: str) -> UserModel | None:
    return await find_user_by_kwargs(session, email=email)


async def find_user_by_uuid(session: AsyncSession, uuid: UUID) -> UserModel | None:
    return await find_user_by_kwargs(session, uuid=uuid)


async def find_active_user_by_uuid(session: AsyncSession, uuid: UUID) -> UserModel | None:
    return await find_user_by_kwargs(session, uuid=uuid, is_active=True)


async def find_user_memberships(session: AsyncSession, user_uuid: UUID) -> Sequence[LibraryMember]:
    query = select(LibraryMember).filter_by(user_uuid=user_uuid)
    result = await session.execute(query)
    return result.scalars().all()
