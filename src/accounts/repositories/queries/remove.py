from uuid import UUID

from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

from src.accounts.repositories.models import UserModel
from src.accounts.repositories.queries.selector import find_user_by_uuid


async def remove_user(session: AsyncSession, user: UserModel):
    found_user = await find_user_by_uuid(session, user.uuid)
    if found_user is None:
        raise Exception(f"user {user.email} you try to delete does not exits")

    await session.delete(user)
    await session.flush()


async def remove_user_by_uuid(session: AsyncSession, user_uuid: UUID):
    query = delete(UserModel).filter_by(uuid=user_uuid)
    await session.execute(query)


async def remove_user_by_email(session: AsyncSession, email: str):
    query = delete(UserModel).filter_by(email=email)
    await session.execute(query)
