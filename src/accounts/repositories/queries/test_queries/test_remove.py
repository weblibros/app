from uuid import uuid4

import pytest

from src.accounts.repositories.models import UserModel
from src.accounts.repositories.queries.create import create_user
from src.accounts.repositories.queries.remove import (
    remove_user,
    remove_user_by_email,
    remove_user_by_uuid,
)
from src.accounts.repositories.queries.selector import (
    find_user_by_email,
    find_user_by_uuid,
)
from src.config.database import async_session_maker


async def test_remove_user():
    async with async_session_maker() as session:
        email = "test_remove_user@fake.com"
        password = "secretpass"
        user = await create_user(
            session,
            email=email,
            password=password,
        )
        user_uuid = user.uuid
        await remove_user(session, user)
        user = await find_user_by_uuid(session, user_uuid)
        assert user is None

        await session.rollback()


async def test_remove_user_not_saved():
    async with async_session_maker() as session:
        email = "test_remove_user@fake.com"
        password = "secretpass"
        user = UserModel(email=email, encrypted_password=password)
        user_uuid = user.uuid
        found_user = await find_user_by_uuid(session, user_uuid)
        assert found_user is None
        with pytest.raises(Exception) as e:
            await remove_user(session, user)
        assert "duplicate key value violates unique constraint" or "UNIQUE constraint failed" in str(e.value)

        await session.rollback()


async def test_remove_user_by_uuid():
    async with async_session_maker() as session:
        email = "test_remove_user_by_uuid@fake.com"
        password = "secretpass"
        user = UserModel(email=email, encrypted_password=password)
        session.add(user)
        await session.flush()
        user_uuid = user.uuid

        await remove_user_by_uuid(session, user_uuid)
        found_user = await find_user_by_uuid(session, user_uuid)
        assert found_user is None
        await session.rollback()


async def test_remove_user_by_uuid_no_user_created():
    async with async_session_maker() as session:
        user_uuid = uuid4()

        found_user = await find_user_by_uuid(session, user_uuid)
        assert found_user is None
        await remove_user_by_uuid(session, user_uuid)
        found_user = await find_user_by_uuid(session, user_uuid)
        assert found_user is None
        await session.rollback()


async def test_remove_user_by_email():
    async with async_session_maker() as session:
        email = "test_remove_user_by_uuid@fake.com"
        password = "secretpass"
        user = UserModel(email=email, encrypted_password=password)
        session.add(user)
        await session.flush()
        user_uuid = user.uuid

        await remove_user_by_email(session, email)
        found_user = await find_user_by_uuid(session, user_uuid)
        assert found_user is None
        await session.rollback()


async def test_remove_user_by_email_no_user_created():
    async with async_session_maker() as session:
        email = "test_remove_user_by_email@fake.com"

        await remove_user_by_email(session, email)
        found_user = await find_user_by_email(session, email)
        assert found_user is None
        await session.rollback()
