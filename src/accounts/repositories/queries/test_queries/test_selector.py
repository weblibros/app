from uuid import uuid4

import pytest

from src.accounts.repositories.models import UserModel
from src.accounts.repositories.queries.selector import (
    find_user_by_email,
    find_user_by_uuid,
)
from src.config.database import async_session_maker


@pytest.fixture(scope="class")
async def one_user():
    async with async_session_maker() as session:
        user = UserModel(
            email="test@fake.com",
            encrypted_password="superpassword",
        )
        session.add(user)
        await session.commit()
    yield user
    async with async_session_maker() as session:
        await session.delete(user)
        await session.commit()


@pytest.mark.usefixtures("one_user")
class TestUserModelSelector:
    async def test_get_user_by_email(self, one_user):
        async with async_session_maker() as session:
            user = await find_user_by_email(session, one_user.email)
            assert isinstance(user, UserModel)
            assert user.uuid == one_user.uuid

    async def test_get_no_user_by_email(self):
        async with async_session_maker() as session:
            user = await find_user_by_email(session, "doesnotexits@fake.com")
            assert user is None

    async def test_get_user_by_uuid(self, one_user):
        async with async_session_maker() as session:
            user = await find_user_by_uuid(session, one_user.uuid)
            assert isinstance(user, UserModel)
            assert user.uuid == one_user.uuid

    async def test_get_no_user_by_uuid(self):
        async with async_session_maker() as session:
            user = await find_user_by_uuid(session, uuid4())
            assert user is None
