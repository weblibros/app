import pytest

from src.accounts import password_hasher
from src.accounts.repositories.models import UserModel
from src.accounts.repositories.queries.create import create_user
from src.accounts.repositories.queries.selector import (
    find_user_by_email,
    find_user_by_uuid,
)
from src.config.database import async_session_maker


async def test_create_user():
    async with async_session_maker() as session:
        email = "test_create_user@fake.com"
        password = "secretpass"
        user = await create_user(
            session,
            email=email,
            password=password,
        )
        assert isinstance(user, UserModel)
        user = await find_user_by_uuid(session, user.uuid)
        assert user is not None
        assert user.email == email
        assert user.encrypted_password != password
        valid_password = password_hasher.verify_password(
            raw_password=password,
            hashed_password=str(user.encrypted_password),
        )
        assert valid_password

        await session.rollback()


async def test_create_user_exits():
    async with async_session_maker() as session:
        email = "user_already_exists@fake.com"
        password = "secretpass"
        user = UserModel(email=email, encrypted_password="fake password")
        session.add(user)
        await session.flush()
        user_found = await find_user_by_email(session, email)
        assert user_found is not None

        with pytest.raises(Exception) as e:
            await create_user(
                session,
                email=email,
                password=password,
            )
        assert "duplicate key value violates unique constraint" or "UNIQUE constraint failed" in str(e.value)
        await session.rollback()
