import datetime

import pytest

from src.accounts import password_hasher
from src.accounts.repositories.queries import selector
from src.accounts.repositories.queries.create import create_user
from src.accounts.repositories.queries.update import (
    update_user_password,
    verify_email_and_activate_user,
)
from src.config.database import async_session_maker


@pytest.fixture(scope="function")
async def unactive_unverified_user():
    async with async_session_maker() as session:
        user = await create_user(
            session,
            email="test_update_verify_user_email@fake.com",
            password="superpassword",
            is_active=False,
            email_verified_on=None,
        )
        session.add(user)
        await session.commit()
    yield user
    async with async_session_maker() as session:
        await session.delete(user)
        await session.commit()


@pytest.fixture(scope="function")
async def active_verified_user():
    async with async_session_maker() as session:
        user = await create_user(
            session,
            email="test_update_verify_user_email@fake.com",
            password="superpassword",
            is_active=True,
            email_verified_on=datetime.datetime.now(),
        )
        session.add(user)
        await session.commit()
    yield user
    async with async_session_maker() as session:
        await session.delete(user)
        await session.commit()


@pytest.fixture(scope="function")
async def active_unverified_user():
    async with async_session_maker() as session:
        user = await create_user(
            session,
            email="test_active_unverify_user_email@fake.com",
            password="superpassword",
            is_active=True,
            email_verified_on=None,
        )
        session.add(user)
        await session.commit()
    yield user
    async with async_session_maker() as session:
        await session.delete(user)
        await session.commit()


@pytest.fixture(scope="function")
async def update_password_user(session):
    user = await create_user(
        session,
        email="test_updatetor_password@fake.com",
        password="superpassword",
    )
    session.add(user)
    await session.commit()
    yield user
    await session.delete(user)
    await session.commit()


class TestPasswordUpdate:
    async def test_update_password(self, session, update_password_user):
        original_password_hash = update_password_user.encrypted_password
        new_password = "newsecret_password"
        await update_user_password(session, update_password_user, new_password)

        await session.refresh(update_password_user)
        assert update_password_user.encrypted_password != original_password_hash
        assert password_hasher.verify_password(
            raw_password=new_password,
            hashed_password=update_password_user.encrypted_password,
        )


class TestverifyUser:
    async def test_verify_unactive_unverifeid_user(self, unactive_unverified_user):
        async with async_session_maker() as session:
            await verify_email_and_activate_user(session, unactive_unverified_user)
            user = await selector.find_user_by_uuid(session, unactive_unverified_user.uuid)
        assert user is not None
        assert user.is_active is True
        assert user.email_verified_on is not None
        assert isinstance(user.email_verified_on, datetime.datetime)

    async def test_verify_active_verifeid_user(self, active_verified_user):
        old_verified_date = active_verified_user.email_verified_on
        async with async_session_maker() as session:
            await verify_email_and_activate_user(session, active_verified_user)
            user = await selector.find_user_by_uuid(session, active_verified_user.uuid)
        assert user is not None
        assert user.is_active is True
        assert user.email_verified_on is not None
        assert isinstance(user.email_verified_on, datetime.datetime)
        assert user.email_verified_on != old_verified_date
        assert user.email_verified_on.timestamp() > old_verified_date.timestamp()

    async def test_verify_active_unverifeid_user(self, session, active_unverified_user):
        async with async_session_maker() as session:
            await verify_email_and_activate_user(session, active_unverified_user)
            user = await selector.find_user_by_uuid(session, active_unverified_user.uuid)
        assert user is not None
        assert user.is_active is True
        assert user.email_verified_on is not None
        assert isinstance(user.email_verified_on, datetime.datetime)
