from datetime import datetime

from sqlalchemy.ext.asyncio import AsyncSession

from src.accounts import password_hasher
from src.accounts.repositories.models import UserModel


async def update_user_password(
    session: AsyncSession,
    user: UserModel,
    password: str,
) -> UserModel:
    hashed_password = password_hasher.hash_password(password)

    try:
        user.encrypted_password = hashed_password
        session.add(user)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)

    return user


async def verify_email_and_activate_user(
    session: AsyncSession,
    user: UserModel,
) -> UserModel:
    try:
        user.is_active = True
        user.email_verified_on = datetime.now()
        session.add(user)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise Exception(e)

    return user
