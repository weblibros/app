from uuid import uuid4

import pytest

from src.accounts import password_hasher, urls
from src.accounts.repositories.models import UserModel
from src.accounts.repositories.queries import selector
from src.accounts.repositories.queries.create import create_user
from src.accounts.users import User
from src.config.database import async_session_maker
from src.services.jwt import JWTPasswordResetToken, JWTUserRefreshToken


@pytest.fixture(scope="function")
async def reset_user():
    async with async_session_maker() as session:
        user = await create_user(
            session,
            email="test_reset_password_by_token@fake.com",
            password="superpassword",
        )
        await session.commit()
    yield user
    async with async_session_maker() as session:
        await session.delete(user)
        await session.commit()


class TestPasswordUpdate:
    async def test_reset_password(self, client, reset_user):
        reset_user_uuid = reset_user.uuid
        orginal_password_hash = reset_user.encrypted_password
        updated_password = "newpassword"
        body = {
            "password": updated_password,
            "reset_token": str(JWTPasswordResetToken.encode_user(User.from_model(reset_user))),
        }
        response = await client.post(
            urls.USER_PASSWORD_RESET_TOKEN_URL,
            json=body,
        )
        assert response.status_code == 200
        result = response.json()
        assert "user_uuid" in result.keys()
        assert len(result.keys()) == 1
        assert result.get("user_uuid") == str(reset_user_uuid)

        async with async_session_maker() as session:
            refreshed_user = await selector.find_user_by_uuid(session, reset_user.uuid)

        assert refreshed_user is not None
        new_password_hash = refreshed_user.encrypted_password
        assert new_password_hash is not None
        assert new_password_hash != updated_password
        assert new_password_hash != orginal_password_hash
        assert (
            password_hasher.verify_password(
                raw_password="superpassword",
                hashed_password=new_password_hash,
            )
            is False
        )
        assert password_hasher.verify_password(
            raw_password=updated_password,
            hashed_password=new_password_hash,
        )

    async def test_wrong_token_type(self, client, reset_user):
        old_password_hash = reset_user.encrypted_password
        updated_password = "newpassword"
        body = {
            "password": updated_password,
            "reset_token": str(JWTUserRefreshToken.encode_user(User.from_model(reset_user))),
        }
        response = await client.post(
            urls.USER_PASSWORD_RESET_TOKEN_URL,
            json=body,
        )
        assert response.status_code == 422
        result = response.json()
        assert result["detail"][0]["msg"] == "Token is not of type password-reset"

        async with async_session_maker() as session:
            refreshed_user = await selector.find_user_by_uuid(session, reset_user.uuid)

        assert refreshed_user is not None
        new_password_hash = refreshed_user.encrypted_password
        assert new_password_hash is not None
        assert new_password_hash != updated_password
        assert old_password_hash == new_password_hash
        assert (
            password_hasher.verify_password(
                raw_password=updated_password,
                hashed_password=new_password_hash,
            )
            is False
        )

    async def test_no_user_found(self, client):
        updated_password = "newpassword"
        non_saved_user = UserModel(
            uuid=uuid4(),
            email="non_saved_user@fake.com",
            encrypted_password="superpassword",
        )
        body = {
            "password": updated_password,
            "reset_token": str(JWTPasswordResetToken.encode_user(User.from_model(non_saved_user))),
        }
        response = await client.post(
            urls.USER_PASSWORD_RESET_TOKEN_URL,
            json=body,
        )
        assert response.status_code == 404
        result = response.json()
        assert result == {"detail": "user is not found"}
