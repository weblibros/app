from src.accounts import urls


class TestUserMe:
    async def test_user_me(self, client, auth_header_user, user):
        response = await client.get(
            urls.USER_ME_URL,
            headers=auth_header_user,
        )
        assert response.status_code == 200
        result = response.json()
        assert "password" not in result.keys()
        assert "uuid" in result.keys()
        assert "email" in result.keys()
        assert len(result.keys()) == 2
        assert result.get("uuid") == str(user.uuid)

    async def test_unactive_user_raise_exception(self, client, auth_header_unactive_user):
        response = await client.get(
            urls.USER_ME_URL,
            headers=auth_header_unactive_user,
        )
        assert response.status_code == 401
        result = response.json()
        assert "detail" in result
        assert result["detail"] == "User is not activated"
