from unittest import mock

import pytest

from src.accounts import urls
from src.accounts.domain.events import UserCreationMethods, UserEvents
from src.accounts.repositories.queries import selector

mock_path = "src.accounts.features.signup_account.dispatch"


class TestSignupUserWithEmailPassword:
    async def test_signup_user(self, client, session):
        email = "user-created-with-signup@test.com"
        password = "special-password"
        with mock.patch(mock_path) as mocked_dispatch:
            response = await client.post(
                urls.USER_SIGNUP_URL,
                json={
                    "email": email,
                    "password": password,
                },
            )
        user = await selector.find_user_by_email(session, email)
        assert user is not None
        assert user.is_active is False
        user_uuid = user.uuid
        await session.delete(user)
        await session.commit()
        assert response.status_code == 201
        response_dict = response.json()
        assert "uuid" in response_dict
        assert "email" in response_dict
        assert response_dict["email"] == email
        assert response_dict["uuid"] == str(user_uuid)
        mocked_dispatch.assert_called_once()
        assert mocked_dispatch.call_args.args[0] == UserEvents.USER_CREATED
        mocked_kwargs = mocked_dispatch.call_args.kwargs
        assert len(mocked_kwargs) == 1
        assert "payload" in mocked_kwargs
        assert isinstance(mocked_kwargs["payload"], dict)
        assert "user_uuid" in mocked_kwargs["payload"]
        assert mocked_kwargs["payload"]["user_uuid"] == user_uuid
        assert "creation_method" in mocked_kwargs["payload"]
        assert mocked_kwargs["payload"]["creation_method"] == UserCreationMethods.SIGNUP.value
        assert mocked_kwargs["payload"]["creation_method"] == "Signup"

    @pytest.mark.usefixtures("user")
    async def test_user_already_exists(self, client):
        email = "user@fake.com"
        password = "special-password"
        with mock.patch(mock_path) as mocked_dispatch:
            response = await client.post(
                urls.USER_SIGNUP_URL,
                json={
                    "email": email,
                    "password": password,
                },
            )
        assert response.status_code == 403
        response_dict = response.json()
        assert "detail" in response_dict
        assert response_dict["detail"] == "User already exists"
        mocked_dispatch.assert_not_called()
