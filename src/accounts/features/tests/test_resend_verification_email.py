from collections.abc import AsyncGenerator
from unittest import mock
from uuid import uuid4

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from src.accounts import urls
from src.accounts.repositories.models import UserModel
from src.accounts.repositories.queries import selector
from src.accounts.repositories.queries.create import create_user
from src.accounts.users import User
from src.config.database import async_session_maker
from src.services.jwt import JWTUserAccesToken, JWTUserActivateToken
from src.services.mail.events import MailEvents
from src.services.mail.fastmail import fast_mail_service as mail_service

mock_path = "src.accounts.features.resend_verification_email.dispatch"


@pytest.fixture()
async def active_unverified_user(session) -> AsyncGenerator[UserModel, None]:
    async with async_session_maker() as session:
        user = await create_user(
            session,
            email="active_unverified_user@test.com",
            password="supersecret",
            is_active=True,
            email_verified_on=None,
        )
        await session.commit()
    yield user
    async with async_session_maker() as session:
        await session.delete(user)
        await session.commit()


class TestRequestResendValidationEmail:
    async def test_correct_response(self, client, unactive_user):
        access_token = JWTUserAccesToken.encode_user(unactive_user)
        with mock.patch(mock_path) as mocked_dispatch:
            response = await client.post(
                urls.USER_RESEND_EMAIL_VALIDATION_URL,
                json={
                    "access_token": str(access_token),
                },
            )
        assert response.status_code == 200
        result = response.json()

        assert result["message"] == "Activation email send"
        mocked_dispatch.assert_called_once()

    async def test_email_send(self, client, unactive_user):
        access_token = JWTUserAccesToken.encode_user(unactive_user)
        with mock.patch(mock_path) as mocked_dispatch:
            response = await client.post(
                urls.USER_RESEND_EMAIL_VALIDATION_URL,
                json={
                    "access_token": str(access_token),
                },
            )
        assert response.status_code == 200

        mocked_dispatch.assert_called_once()
        assert mocked_dispatch.call_args.args[0] == MailEvents.EMAIL_VALIDATION
        mocked_kwargs = mocked_dispatch.call_args.kwargs
        assert len(mocked_kwargs) == 1
        assert "payload" in mocked_kwargs
        assert isinstance(mocked_kwargs["payload"], dict)
        assert "user_uuid" in mocked_kwargs["payload"]
        assert mocked_kwargs["payload"]["user_uuid"] == unactive_user.uuid

    async def test_unchanged_user(self, client, session: AsyncSession, unactive_user: UserModel):
        access_token = JWTUserAccesToken.encode_user(User.from_model(unactive_user))

        mail_service.config.SUPPRESS_SEND = 1  # type: ignore[attr-defined]

        with mock.patch(mock_path) as mocked_dispatch:
            response = await client.post(
                urls.USER_RESEND_EMAIL_VALIDATION_URL,
                json={
                    "access_token": str(access_token),
                },
            )
        assert response.status_code == 200
        user = await selector.find_user_by_uuid(session, unactive_user.uuid)

        assert user is not None
        assert user.is_active is False
        assert user.email_verified_on is None
        mocked_dispatch.assert_called_once()

    async def test_no_token_input(self, client):
        with mock.patch(mock_path) as mocked_dispatch:
            response = await client.post(
                urls.USER_RESEND_EMAIL_VALIDATION_URL,
                json={
                    "access_token": "no.token.input",
                },
            )
        assert response.status_code == 422

        result = response.json()

        assert result["detail"][0]["msg"] == "Invalid token"
        mocked_dispatch.assert_not_called()

    async def test_wrong_token_type(self, client, unactive_user: UserModel):
        wrong_access_token = JWTUserActivateToken.encode_user(User.from_model(unactive_user))

        with mock.patch(mock_path) as mocked_dispatch:
            response = await client.post(
                urls.USER_RESEND_EMAIL_VALIDATION_URL,
                json={
                    "access_token": str(wrong_access_token),
                },
            )
        assert response.status_code == 422

        result = response.json()

        assert result["detail"][0]["msg"] == "Token is not of type access"
        mocked_dispatch.assert_not_called()

    async def test_user_does_not_exists(self, client):
        unsaved_user = UserModel(
            uuid=uuid4(),
            email="unsaved_user@test.com",
            encrypted_password="notsaved",
            is_active=False,
            email_verified_on=None,
        )
        wrong_access_token = JWTUserAccesToken.encode_user(User.from_model(unsaved_user))
        mail_service.config.SUPPRESS_SEND = 1  # type: ignore[attr-defined]

        with mock.patch(mock_path) as mocked_dispatch:
            response = await client.post(
                urls.USER_RESEND_EMAIL_VALIDATION_URL,
                json={
                    "access_token": str(wrong_access_token),
                },
            )
        assert response.status_code == 404

        result = response.json()

        assert result["detail"] == "User does not exists"
        mocked_dispatch.assert_not_called()

    async def test_user_email_already_verified(self, client, access_token_user):
        with mock.patch(mock_path) as mocked_dispatch:
            response = await client.post(
                urls.USER_RESEND_EMAIL_VALIDATION_URL,
                json={
                    "access_token": str(access_token_user),
                },
            )
        assert response.status_code == 409

        result = response.json()

        assert result["detail"] == "User email is already verified"

        mocked_dispatch.assert_not_called()

    async def test_send_email_active_unverified_user(self, client, active_unverified_user):
        access_token = JWTUserAccesToken.encode_user(User.from_model(active_unverified_user))
        with mock.patch(mock_path) as mocked_dispatch:
            response = await client.post(
                urls.USER_RESEND_EMAIL_VALIDATION_URL,
                json={
                    "access_token": str(access_token),
                },
            )
        assert response.status_code == 200

        mocked_dispatch.assert_called_once()
        assert mocked_dispatch.call_args.args[0] == MailEvents.EMAIL_VALIDATION
        mocked_kwargs = mocked_dispatch.call_args.kwargs
        assert len(mocked_kwargs) == 1
        assert "payload" in mocked_kwargs
        assert isinstance(mocked_kwargs["payload"], dict)
        assert "user_uuid" in mocked_kwargs["payload"]
        assert mocked_kwargs["payload"]["user_uuid"] == active_unverified_user.uuid
