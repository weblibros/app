import datetime
from datetime import timedelta

import pytest

from src.accounts import urls
from src.accounts.repositories.queries import selector
from src.accounts.repositories.queries.create import create_user
from src.accounts.users import User
from src.config.database import async_session_maker
from src.services.jwt import JWTToken, JWTUserAccesToken, JWTUserActivateToken


@pytest.fixture(scope="function")
async def unactive_unverified_user():
    async with async_session_maker() as session:
        user = await create_user(
            session,
            email="test_update_verify_user_email@fake.com",
            password="superpassword",
            is_active=False,
            email_verified_on=None,
        )
        session.add(user)
        await session.commit()
    yield user
    async with async_session_maker() as session:
        await session.delete(user)
        await session.commit()


@pytest.fixture(scope="function")
async def active_verified_user():
    async with async_session_maker() as session:
        user = await create_user(
            session,
            email="test_update_verify_user_email@fake.com",
            password="superpassword",
            is_active=True,
            email_verified_on=datetime.datetime.now(),
        )
        session.add(user)
        await session.commit()
    yield user
    async with async_session_maker() as session:
        await session.delete(user)
        await session.commit()


@pytest.fixture(scope="function")
async def active_unverified_user():
    async with async_session_maker() as session:
        user = await create_user(
            session,
            email="test_active_unverify_user_email@fake.com",
            password="superpassword",
            is_active=True,
            email_verified_on=None,
        )
        session.add(user)
        await session.commit()
    yield user
    async with async_session_maker() as session:
        await session.delete(user)
        await session.commit()


class TestActivateUser:
    async def test_activate_unactive_unverifeid_user(self, client, unactive_unverified_user):
        token = JWTUserActivateToken.encode_user(User.from_model(unactive_unverified_user))
        response = await client.post(
            urls.USER_EMAIL_VALIDATION_URL,
            json={
                "activation_token": str(token),
            },
        )
        assert response.status_code == 200
        result = response.json()
        assert "message" in result.keys()
        assert "User is activated" == result["message"]

        async with async_session_maker() as session:
            user = await selector.find_user_by_uuid(session, unactive_unverified_user.uuid)

        assert user is not None
        assert user.is_active is True
        assert user.email_verified_on is not None
        assert isinstance(user.email_verified_on, datetime.datetime)

    async def test_activate_active_verifeid_user(self, client, active_verified_user):
        old_verified_date = active_verified_user.email_verified_on
        token = JWTUserActivateToken.encode_user(User.from_model(active_verified_user))
        response = await client.post(
            urls.USER_EMAIL_VALIDATION_URL,
            json={
                "activation_token": str(token),
            },
        )
        assert response.status_code == 200
        result = response.json()
        assert "message" in result.keys()
        assert "User is activated" == result["message"]
        async with async_session_maker() as session:
            user = await selector.find_user_by_uuid(session, active_verified_user.uuid)

        assert user is not None
        assert user.is_active is True
        assert user.email_verified_on is not None
        assert isinstance(user.email_verified_on, datetime.datetime)
        assert user.email_verified_on != old_verified_date
        assert user.email_verified_on.timestamp() > old_verified_date.timestamp()

    async def test_activate_active_unverifeid_user(self, client, active_unverified_user):
        token = JWTUserActivateToken.encode_user(User.from_model(active_unverified_user))
        response = await client.post(
            urls.USER_EMAIL_VALIDATION_URL,
            json={
                "activation_token": str(token),
            },
        )
        assert response.status_code == 200
        result = response.json()
        assert "message" in result.keys()
        assert "User is activated" == result["message"]
        async with async_session_maker() as session:
            user = await selector.find_user_by_uuid(session, active_unverified_user.uuid)

        assert user is not None
        assert user.is_active is True
        assert user.email_verified_on is not None
        assert isinstance(user.email_verified_on, datetime.datetime)

    async def test_wrong_jwt_token_send(self, client, unactive_unverified_user):
        token = JWTUserAccesToken.encode_user(User.from_model(unactive_unverified_user))
        response = await client.post(
            urls.USER_EMAIL_VALIDATION_URL,
            json={
                "activation_token": str(token),
            },
        )
        assert response.status_code == 422
        result = response.json()
        assert result["detail"][0]["msg"] == "Token is not of type email-activate"

    async def test_string_instead_of_token(
        self,
        client,
    ):
        response = await client.post(urls.USER_EMAIL_VALIDATION_URL, json={"activation_token": "not.a.token"})
        assert response.status_code == 422
        result = response.json()
        assert result["detail"][0]["msg"] == "Invalid token"

    async def test_expired_activation_token(self, client, unactive_unverified_user):
        token = JWTToken.encode(
            {
                "user": str(unactive_unverified_user.uuid),
                "type": "email-activate",
                "exp": str(datetime.datetime.utcnow() - timedelta(seconds=3600)),
            }
        )
        response = await client.post(urls.USER_EMAIL_VALIDATION_URL, json={"activation_token": str(token)})
        assert response.status_code == 422
        result = response.json()
        assert result["detail"][0]["msg"] == "Token is expired"
