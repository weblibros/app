from datetime import datetime, timedelta

from jose import jwt

from src.accounts import urls
from src.services.jwt import JWTToken, JWTUserAccesToken, JWTUserRefreshToken


class TestRefreshToken:
    async def test_correct_token(self, client, user):
        refresh_token_raw = str(JWTUserRefreshToken.encode_user(user))
        assert isinstance(refresh_token_raw, str)
        response = await client.post(
            urls.REFRESH_TOKEN_URL,
            json={"refresh_token": refresh_token_raw},
        )
        assert response.status_code == 200
        result = response.json()
        assert "assert_token" not in result.keys()
        assert "refresh_token" in result.keys()
        assert "token_type" in result.keys()
        assert len(result.keys()) == 3
        access_token_response = result.get("access_token")
        refresh_token_response = result.get("refresh_token")
        access_token_response_claims = jwt.get_unverified_claims(access_token_response)
        refresh_token_response_claims = jwt.get_unverified_claims(refresh_token_response)
        assert refresh_token_response != refresh_token_raw
        assert access_token_response_claims.get("user") == str(user.uuid)
        assert refresh_token_response_claims.get("user") == str(user.uuid)

    async def test_pass_valid_access_token(self, client, user):
        access_token_raw = str(JWTUserAccesToken.encode_user(user))
        response = await client.post(
            urls.REFRESH_TOKEN_URL,
            json={"refresh_token": access_token_raw},
        )
        assert response.status_code == 422
        result = response.json()
        assert result["detail"][0]["msg"] == "Token is not of type refresh"

    async def test_pass_random_string(self, client):
        response = await client.post(
            urls.REFRESH_TOKEN_URL,
            json={"refresh_token": "random.random.random"},
        )
        assert response.status_code == 422

    async def test_pass_expired_refresh_token(self, client, user):
        payload = {
            "exp": datetime.utcnow() - timedelta(seconds=3600),
            "iat": datetime.utcnow(),
            "user": str(user.uuid),
            "type": "refresh",
        }
        expired_refresh_token = JWTToken.encode(payload)
        response = await client.post(
            urls.REFRESH_TOKEN_URL,
            json={"refresh_token": str(expired_refresh_token)},
        )
        assert response.status_code == 422
