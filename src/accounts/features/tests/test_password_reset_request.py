from unittest import mock

from src.accounts import urls
from src.services.mail.events import MailEvents

mock_path = "src.accounts.features.password_reset_request.dispatch"


class TestPasswordResetEndpoint:
    async def test_password_reset_endpoint(self, client, user):
        email_to_reset = "user@fake.com"
        with mock.patch(mock_path) as mocked_reset:
            response = await client.post(urls.USER_PASSWORD_RESET_EMAIL_URL, json={"email": email_to_reset})
        assert response.status_code == 200
        response_dict = response.json()

        assert "message" in response_dict
        assert response_dict["message"] == "password reset request received"
        mocked_reset.assert_called_once()
        assert mocked_reset.call_args.args[0] == MailEvents.RESET_PASSWORD
        mocked_kwargs = mocked_reset.call_args.kwargs
        assert len(mocked_kwargs) == 1
        assert "payload" in mocked_kwargs
        assert isinstance(mocked_kwargs["payload"], dict)
        assert "user_uuid" in mocked_kwargs["payload"]
        assert mocked_kwargs["payload"]["user_uuid"] == user.uuid

    async def test_password_reset_endpoint_incorrect_body(self, client):
        with mock.patch(mock_path) as mocked_reset:
            response = await client.post(urls.USER_PASSWORD_RESET_EMAIL_URL, json={"email": "noemail"})
        mocked_reset.assert_not_called()
        assert response.status_code == 422
        response_dict = response.json()

        assert "value is not a valid email address" in response_dict.get("detail")[0]["msg"]
