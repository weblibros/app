from uuid import uuid4

from httpx import Headers
from sqlalchemy import exists

from src.accounts import urls
from src.accounts.repositories import UserRepo
from src.accounts.repositories.models import UserModel
from src.accounts.repositories.queries.create import create_user
from src.config.database import async_session_maker
from src.services.jwt import JWTUserAccesToken


class TestRemoveMe:
    def _user_auth_header(self, user):
        return Headers({"Authorization": f"Bearer {JWTUserAccesToken.encode_user(user)}"})

    async def _create_user(self, email):
        async with async_session_maker() as session:
            user_model = await create_user(
                session,
                email=email,
                password="superpassword",
                is_active=True,
                email_verified_on=None,
                uuid=uuid4(),
            )
            session.add(user_model)
            await session.commit()
        user = await UserRepo().get_by_uuid(user_model.uuid)
        return user

    async def test_remove_me(self, client):
        email = "test_remove_me@example.com"
        user = await self._create_user(email)
        response = await client.delete(urls.USER_ME_URL, headers=self._user_auth_header(user))
        assert response.status_code == 202
        async with async_session_maker() as session:
            query = exists(UserModel).where(UserModel.email == email).select()
            result = await session.scalar(query)
        assert result is False

    async def test_calling_remove_me_twice_returns_403(self, client):
        email = "test_remove_me_twice@example.com"
        user = await self._create_user(email)
        response = await client.delete(urls.USER_ME_URL, headers=self._user_auth_header(user))
        assert response.status_code == 202
        response = await client.delete(urls.USER_ME_URL, headers=self._user_auth_header(user))
        assert response.status_code == 403
        async with async_session_maker() as session:
            query = exists(UserModel).where(UserModel.email == email).select()
            result = await session.scalar(query)
        assert result is False
