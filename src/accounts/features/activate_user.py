from fastapi import APIRouter, HTTPException, status
from pydantic import BaseModel

from src.accounts import urls
from src.accounts.jwt.fields import JWTUserActivateTokenType
from src.accounts.repositories import UserRepo
from src.services.jwt import JWTUserActivateToken
from src.tags import ApiTags

activate_user_router = APIRouter()


class ResponseMessage(BaseModel):
    message: str


class ActivateUserBody(BaseModel):
    activation_token: JWTUserActivateTokenType


@activate_user_router.post(urls.USER_EMAIL_VALIDATION_URL, tags=[ApiTags.TAG_ACCOUNTS])
async def activate_user(
    request_body: ActivateUserBody,
):
    user_repo = UserRepo()
    activate_token = JWTUserActivateToken(str(request_body.activation_token))

    user_uuid = activate_token.payload.user

    user = await user_repo.verify_email_and_activate_user(user_uuid)

    if user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User does not exists",
        )

    return ResponseMessage(message="User is activated")
