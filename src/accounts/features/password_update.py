from uuid import UUID

from fastapi import APIRouter, HTTPException
from pydantic import BaseModel

from src.accounts import urls
from src.accounts.jwt.fields import JWTPasswordResetTokenType
from src.accounts.repositories import UserRepo
from src.services.jwt import JWTPasswordResetToken
from src.tags import ApiTags

password_update_router = APIRouter()


class ResetPasswordByToken(BaseModel):
    reset_token: JWTPasswordResetTokenType
    password: str


class ResetPasswordByTokenResponse(BaseModel):
    user_uuid: UUID


@password_update_router.post(urls.USER_PASSWORD_RESET_TOKEN_URL, tags=[ApiTags.TAG_ACCOUNTS])
async def reset_user_password_by_token(
    request_body: ResetPasswordByToken,
) -> ResetPasswordByTokenResponse:
    reset_token = JWTPasswordResetToken(token_string=str(request_body.reset_token))

    user_uuid = reset_token.payload.user
    new_raw_password = request_body.password
    user = await UserRepo().update_user_password(user_uuid, new_raw_password)
    if user is None:
        raise HTTPException(
            status_code=404,
            detail="user is not found",
        )

    return ResetPasswordByTokenResponse(user_uuid=user_uuid)
