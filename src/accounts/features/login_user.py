from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.ext.asyncio import AsyncSession

from src.accounts import password_hasher, urls
from src.accounts.jwt.schema import JwtKeyPair
from src.accounts.jwt.token_factory import user_jwt_tokens_factory
from src.accounts.repositories import UserRepo
from src.accounts.repositories.queries.selector import find_user_by_email
from src.config.database import get_async_session
from src.tags import ApiTags

login_router = APIRouter()


@login_router.post(urls.PASSWORD_LOGIN_USER_URL, tags=[ApiTags.TAG_AUTH])
async def auth_user_by_password(
    session: AsyncSession = Depends(get_async_session),
    form_data: OAuth2PasswordRequestForm = Depends(),
) -> JwtKeyPair:
    exception = HTTPException(
        status_code=403,
        detail="incorrect login",
    )

    user_model = await find_user_by_email(session, form_data.username)

    if user_model is None:
        raise exception

    valid_password = password_hasher.verify_password(
        raw_password=form_data.password,
        hashed_password=user_model.encrypted_password,
    )
    if not valid_password:
        raise exception

    user = await UserRepo().get_by_uuid(user_model.uuid)
    return JwtKeyPair.model_validate(user_jwt_tokens_factory(user), from_attributes=True)
