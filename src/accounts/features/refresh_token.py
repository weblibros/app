from fastapi import APIRouter, HTTPException
from pydantic import BaseModel

from src.accounts import urls
from src.accounts.jwt.fields import JWTRefreshTokenType
from src.accounts.jwt.schema import JwtKeyPair
from src.accounts.jwt.token_factory import user_jwt_tokens_factory
from src.accounts.repositories import UserRepo
from src.services.jwt import JWTUserRefreshToken
from src.tags import ApiTags

token_refresh_router = APIRouter()


class JwtRefresh(BaseModel):
    refresh_token: JWTRefreshTokenType


@token_refresh_router.post(urls.REFRESH_TOKEN_URL, tags=[ApiTags.TAG_AUTH])
async def refresh_token(
    jwt_token: JwtRefresh,
) -> JwtKeyPair:
    exception = HTTPException(
        status_code=403,
        detail="incorrect refresh token",
    )

    token = JWTUserRefreshToken(str(jwt_token.refresh_token))

    user_uuid = token.payload.user

    user = await UserRepo().find_by_uuid(user_uuid)

    if user:
        return JwtKeyPair.model_validate(user_jwt_tokens_factory(user), from_attributes=True)
    else:
        raise exception
