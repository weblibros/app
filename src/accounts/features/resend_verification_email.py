from fastapi import APIRouter, Depends, HTTPException, Request, status
from fastapi_events.dispatcher import dispatch
from pydantic import BaseModel
from sqlalchemy.ext.asyncio import AsyncSession

from src.accounts import urls
from src.accounts.jwt.fields import JWTAccesTokenType
from src.accounts.repositories.queries import selector
from src.config.database import get_async_session
from src.services.jwt import JWTUserAccesToken
from src.services.mail.events import MailEvents
from src.tags import ApiTags

resend_verification_email_router = APIRouter()


class ResponseMessage(BaseModel):
    message: str


class RequestActivationBody(BaseModel):
    access_token: JWTAccesTokenType


@resend_verification_email_router.post(urls.USER_RESEND_EMAIL_VALIDATION_URL, tags=[ApiTags.TAG_ACCOUNTS])
async def request_resend_verification_email(
    request: Request,
    request_body: RequestActivationBody,
    session: AsyncSession = Depends(get_async_session),
):
    access_token = JWTUserAccesToken(str(request_body.access_token))

    user_uuid = access_token.payload.user

    user = await selector.find_user_by_uuid(session, user_uuid)

    if user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User does not exists",
        )

    if user.email_verified_on is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="User email is already verified",
        )

    dispatch(
        MailEvents.EMAIL_VALIDATION,
        payload={
            "user_uuid": user.uuid,
        },
    )

    return ResponseMessage(message="Activation email send")
