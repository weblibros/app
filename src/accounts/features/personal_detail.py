from uuid import UUID

from fastapi import APIRouter, Depends
from pydantic import BaseModel, ConfigDict, EmailStr
from sqlalchemy.ext.asyncio import AsyncSession

from src.accounts import urls
from src.accounts.repositories.queries import selector
from src.config.database import get_async_session
from src.services.authentication import request_user_uuid
from src.tags import ApiTags

personal_detail_router = APIRouter()


class UserDetailSchema(BaseModel):
    uuid: UUID
    email: EmailStr
    model_config = ConfigDict(from_attributes=True)


@personal_detail_router.get(urls.USER_ME_URL, response_model=UserDetailSchema, tags=[ApiTags.TAG_ACCOUNTS])
async def user_me(
    session: AsyncSession = Depends(get_async_session),
    user_uuid: UUID = Depends(request_user_uuid),
):
    user = await selector.find_user_by_uuid(session, user_uuid)
    return user
