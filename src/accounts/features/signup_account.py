from uuid import UUID

from fastapi import APIRouter, HTTPException, Request, status
from fastapi_events.dispatcher import dispatch
from pydantic import BaseModel, EmailStr

from src.accounts import urls
from src.accounts.domain.events import UserCreationMethods, UserEvents
from src.accounts.repositories import UserRepo
from src.config import settings
from src.tags import ApiTags

signup_account_router = APIRouter()


class SignupUserEmailPassword(BaseModel):
    email: EmailStr
    password: str


class UserDetailSchema(BaseModel):
    uuid: UUID
    email: EmailStr


@signup_account_router.post(
    urls.USER_SIGNUP_URL,
    status_code=status.HTTP_201_CREATED,
    tags=[ApiTags.TAG_ACCOUNTS],
)
async def signup_user_with_email_password(
    request: Request,
    signup_user: SignupUserEmailPassword,
) -> UserDetailSchema:
    user_needs_to_activate_with_email = settings.EMAIL_ENABLED

    user, created = await UserRepo().signup_user_with_email_password(
        email=signup_user.email,
        password=signup_user.password,
        is_active=not user_needs_to_activate_with_email,
    )
    if created:
        dispatch(
            UserEvents.USER_CREATED,
            payload={
                "user_uuid": user.uuid,
                "creation_method": UserCreationMethods.SIGNUP.value,
            },
        )
    else:
        raise HTTPException(
            status_code=403,
            detail="User already exists",
        )

    return UserDetailSchema.model_validate(user, from_attributes=True)
