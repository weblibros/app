import logging

from fastapi import APIRouter, Request
from fastapi_events.dispatcher import dispatch
from pydantic import BaseModel, EmailStr

from src.accounts import urls
from src.accounts.repositories import UserRepo
from src.services.mail.events import MailEvents
from src.tags import ApiTags

logger = logging.getLogger("default")

password_reset_request_router = APIRouter()


class RequestResetPassword(BaseModel):
    email: EmailStr


class ResponseMessage(BaseModel):
    message: str


@password_reset_request_router.post(urls.USER_PASSWORD_RESET_EMAIL_URL, tags=[ApiTags.TAG_ACCOUNTS])
async def reset_password_email_request(
    request: Request,
    request_body: RequestResetPassword,
) -> ResponseMessage:
    logging.info(f"request for password reset {request_body.email} received")

    user_uuid = await UserRepo().retrieve_user_uuid(request_body.email)
    if user_uuid:
        dispatch(
            MailEvents.RESET_PASSWORD,
            payload={
                "user_uuid": user_uuid,
            },
        )

    return ResponseMessage(message="password reset request received")
