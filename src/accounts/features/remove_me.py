from typing import TYPE_CHECKING
from uuid import UUID

from fastapi import APIRouter, Depends, Response, status
from pydantic import BaseModel, ConfigDict, EmailStr

from src.accounts import urls
from src.accounts.repositories import UserRepo
from src.services.authentication import active_user
from src.tags import ApiTags

remove_me_router = APIRouter()

if TYPE_CHECKING:
    from src.accounts.users import User


class UserDetailSchema(BaseModel):
    uuid: UUID
    email: EmailStr
    model_config = ConfigDict(from_attributes=True)


@remove_me_router.delete(urls.USER_ME_URL, status_code=status.HTTP_202_ACCEPTED, tags=[ApiTags.TAG_ACCOUNTS])
async def remove_me(
    user: "User" = Depends(active_user),
):
    removed_user_uuid = await UserRepo().remove_user_by_uuid(user.uuid)
    if removed_user_uuid:
        response = Response(status_code=status.HTTP_202_ACCEPTED)
        response.delete_cookie("webLibrosJwtAccessToken")
        response.delete_cookie("webLibrosJwtRefreshToken")

    else:
        return Response(status_code=status.HTTP_403_FORBIDDEN)
