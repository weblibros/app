from pydantic import BaseModel

from src.accounts.jwt.fields import JWTAccesTokenType, JWTRefreshTokenType


class JwtKeyPair(BaseModel):
    token_type: str = "bearer"
    access_token: JWTAccesTokenType
    refresh_token: JWTRefreshTokenType
