from dataclasses import dataclass
from typing import TYPE_CHECKING

from src.services.jwt import JWTUserAccesToken, JWTUserRefreshToken

if TYPE_CHECKING:
    from src.accounts.users import User


@dataclass(frozen=True)
class JwtKeyPair:
    access_token: JWTUserAccesToken
    refresh_token: JWTUserRefreshToken


def user_jwt_tokens_factory(
    user: "User",
) -> JwtKeyPair:
    return JwtKeyPair(
        access_token=JWTUserAccesToken.encode_user(user),
        refresh_token=JWTUserRefreshToken.encode_user(user),
    )
