from uuid import uuid4

from src.accounts.jwt.token_factory import user_jwt_tokens_factory
from src.accounts.users import User


def test_user_jwt_token_factory():
    user = User(
        uuid=uuid4(),
        is_active=True,
        memberships=[],
    )
    user_tokens = user_jwt_tokens_factory(user)

    acces_token = user_tokens.access_token
    refresh_token = user_tokens.refresh_token
    assert acces_token is not None
    assert refresh_token is not None

    assert acces_token.payload.user == user.uuid
    assert refresh_token.payload.user == user.uuid

    assert acces_token.payload.token_type.value == "access"
    assert refresh_token.payload.token_type.value == "refresh"
