from typing import Any, Callable

from pydantic import GetJsonSchemaHandler
from pydantic.json_schema import JsonSchemaValue
from pydantic_core import CoreSchema, PydanticCustomError, core_schema

from src.services.jwt import (
    JWTPasswordResetToken,
    JWTToken,
    JWTUserAccesToken,
    JWTUserActivateToken,
    JWTUserRefreshToken,
)


class JWTTokenType:
    token_class = JWTToken

    @classmethod
    def __get_pydantic_json_schema__(
        cls, core_schema: core_schema.CoreSchema, handler: GetJsonSchemaHandler
    ) -> JsonSchemaValue:
        field_schema: dict[str, Any] = {}
        field_schema.update(type="string")
        return field_schema

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source: type[Any], handler: Callable[[Any], CoreSchema]
    ) -> core_schema.CoreSchema:
        return core_schema.with_info_plain_validator_function(
            cls.validate, serialization=core_schema.to_string_ser_schema()
        )

    @classmethod
    def validate(cls, v, _info) -> str:
        type_error = PydanticCustomError("jwt_error", "Invalid token")
        type_error_unvalid_token_type = PydanticCustomError(
            "jwt_error", f"Token is not of type {str(cls.token_class.token_type)}"
        )
        type_error_expired = PydanticCustomError("jwt_error", "Token is expired")
        try:
            v = cls.token_class(str(v))
        except:  # noqa: E722
            raise type_error

        if not v.is_valid_token_type():
            raise type_error_unvalid_token_type
        elif not v.is_valid():
            raise type_error_expired
        return str(v)


class JWTAccesTokenType(JWTTokenType):
    token_class = JWTUserAccesToken


class JWTRefreshTokenType(JWTTokenType):
    token_class = JWTUserRefreshToken


class JWTPasswordResetTokenType(JWTTokenType):
    token_class = JWTPasswordResetToken


class JWTUserActivateTokenType(JWTTokenType):
    token_class = JWTUserActivateToken
