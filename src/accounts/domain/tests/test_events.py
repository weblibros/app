from uuid import uuid4

from ..events import UserCreatedEventSchema, UserCreationMethods


def test_to_values():
    assert UserCreationMethods.to_values() == ["Signup"]


def test_create_schema_with_plain_strings_for_creation_methods():
    schema = UserCreatedEventSchema(
        user_uuid=uuid4(),
        creation_method="Signup",
        email="fake@email.com",
    )
    assert schema.creation_method == UserCreationMethods.SIGNUP


def test_create_schema_with_enum_for_creation_methods():
    schema = UserCreatedEventSchema(
        user_uuid=uuid4(),
        creation_method=UserCreationMethods.SIGNUP,
        email="fake@email.com",
    )
    assert schema.creation_method == "Signup"
