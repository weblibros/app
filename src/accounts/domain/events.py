from enum import StrEnum
from uuid import UUID

from fastapi_events.registry.payload_schema import registry as payload_schema
from pydantic import BaseModel


class UserCreationMethods(StrEnum):
    SIGNUP = "Signup"

    @classmethod
    def to_values(cls) -> list[str]:
        return [item.value for item in cls]


class UserEvents(StrEnum):
    USER_CREATED = "USER_CREATED"


@payload_schema.register(event_name=UserEvents.USER_CREATED)
class UserCreatedEventSchema(BaseModel):
    user_uuid: UUID
    creation_method: UserCreationMethods
