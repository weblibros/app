from dataclasses import dataclass
from typing import TYPE_CHECKING, Optional, Self, Sequence
from uuid import UUID

from src.libraries.membership_roles import MembershipRole

if TYPE_CHECKING:
    from src.accounts.repositories.models import UserModel
    from src.libraries.repositories.models import LibraryMember
    from src.services.jwt import JWTUserAccesToken, JwtUserMembership


class UserDoesNotHaveAPersonalLibraryException(Exception):
    def __init__(self, user: "User"):
        self.message = f"user {user.uuid} does not contain a personal library: {user.memberships}"

    def __str__(self):
        return str(self.message)


class UserHasMultiplePersonalLibraryException(Exception):
    def __init__(self, user: "User"):
        self.message = f"user {user.uuid} has multiple personal libraries: {user.memberships}"

    def __str__(self):
        return str(self.message)


@dataclass
class Membership:
    library_uuid: UUID
    membership_role: MembershipRole

    @classmethod
    def from_model(cls, membership_model: "LibraryMember") -> Self:
        return cls(
            library_uuid=membership_model.library_uuid,
            membership_role=MembershipRole(membership_model.membership),
        )

    @classmethod
    def from_jwt_membership(cls, member: "JwtUserMembership") -> Self:
        return cls(
            library_uuid=member.library_uuid,
            membership_role=member.membership_role,
        )

    @property
    def is_owner(self) -> bool:
        return self.membership_role == MembershipRole.OWNER


@dataclass
class User:
    uuid: UUID
    memberships: list[Membership]
    is_active: Optional[bool]

    @classmethod
    def from_model(
        cls,
        user_model: "UserModel",
        membership_models: Sequence["LibraryMember"] = [],
    ) -> Self:
        return cls(
            uuid=user_model.uuid,
            is_active=user_model.is_active,
            memberships=[Membership.from_model(membership_model) for membership_model in membership_models],
        )

    @classmethod
    def from_access_token(cls, token: "JWTUserAccesToken") -> Self:
        return cls(
            uuid=token.payload.user,
            is_active=token.payload.is_active,
            memberships=[Membership.from_jwt_membership(member) for member in token.payload.memberships],
        )

    @property
    def personal_library_uuid(self) -> UUID:
        def is_personal_library(membership: Membership) -> bool:
            return membership.is_owner

        owners = list(filter(is_personal_library, self.memberships))

        if len(owners) == 1:
            return owners[0].library_uuid
        elif len(owners) == 0:
            raise UserDoesNotHaveAPersonalLibraryException(self)
        else:
            raise UserHasMultiplePersonalLibraryException(self)

    def get_libraries(self, membership_roles: Optional[list[MembershipRole]] = None) -> list[UUID]:
        def filter_on_membership_roles(item_membership: Membership):
            if membership_roles is None:
                return True
            else:
                return item_membership.membership_role in membership_roles

        filtered_members = filter(filter_on_membership_roles, self.memberships)
        return [membership.library_uuid for membership in filtered_members]
