import logging
from uuid import UUID

from src.accounts.repositories import UserRepo
from src.config import settings
from src.services.jwt import JWTPasswordResetToken, JWTUserActivateToken
from src.services.mail.service import EmailService

logger = logging.getLogger("default")


def get_hostname():
    if settings.ENVIRONMENT == "local":
        return "http://localhost:8500"
    else:
        return f"https://{settings.DOMAIN}"


def reset_password_link(token) -> str:
    return f"{get_hostname()}/reset-password?token={token}"


def activation_email_link(token) -> str:
    return f"{get_hostname()}/user/activate?token={token}"


async def reset_password_by_email(user_uuid: UUID):
    user_repo = UserRepo()
    user = await user_repo.find_by_uuid(user_uuid)
    email = await user_repo.retrieve_user_email(user_uuid)

    if user and email:
        jwt_token = JWTPasswordResetToken.encode_user(user)
        link = reset_password_link(token=jwt_token)
        await EmailService(receiver_email=email).send_password_reset_email(link)
    else:
        logger.info(f"password reset request {user_uuid} user not found")


async def send_activation_email(user_uuid: UUID):
    user_repo = UserRepo()
    user = await user_repo.find_by_uuid(user_uuid)
    email = await user_repo.retrieve_user_email(user_uuid)
    if user and email:
        token = JWTUserActivateToken.encode_user(user)
        activation_link = activation_email_link(token=token)
        await EmailService(receiver_email=email).send_user_activation_email(activation_link)
    else:
        logger.info(f"activation email request {user_uuid} user not found")
