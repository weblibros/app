from dataclasses import dataclass
from typing import Self
from uuid import UUID


@dataclass(frozen=True)
class UserCreatedEventDTO:
    user_uuid: UUID
    creation_method: str

    @classmethod
    def from_dict(cls, raw_dict) -> Self:
        return cls(
            user_uuid=raw_dict["user_uuid"],
            creation_method=raw_dict["creation_method"],
        )
