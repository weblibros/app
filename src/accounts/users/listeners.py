from fastapi_events.dispatcher import dispatch
from fastapi_events.handlers.local import local_handler
from fastapi_events.typing import Event

from src.accounts.domain.events import UserCreationMethods, UserEvents
from src.accounts.users.dto import UserCreatedEventDTO
from src.config import settings
from src.services.mail.events import MailEvents


@local_handler.register(event_name=UserEvents.USER_CREATED.value)
async def email_validation_sent_on_user_creation(
    event: Event,
):
    user_needs_to_activate_with_email = settings.EMAIL_ENABLED
    _, payload = event
    user_created_event_dto = UserCreatedEventDTO.from_dict(payload)
    send_email_on_user_creation_methods_list = [UserCreationMethods.SIGNUP.value]

    if (
        user_needs_to_activate_with_email
        and user_created_event_dto.creation_method in send_email_on_user_creation_methods_list
    ):
        dispatch(
            MailEvents.EMAIL_VALIDATION,
            payload={
                "user_uuid": user_created_event_dto.user_uuid,
            },
        )
