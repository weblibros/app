from uuid import uuid4

import pytest

from src.accounts.users.user import (
    Membership,
    User,
    UserDoesNotHaveAPersonalLibraryException,
    UserHasMultiplePersonalLibraryException,
)
from src.libraries.membership_roles import MembershipRole


class TestGetPernalLibrary:
    LIBRARY_UUID = uuid4()

    @pytest.fixture
    def user_with_one_personal_library(self) -> User:
        return User(
            uuid=uuid4(),
            is_active=True,
            memberships=[
                Membership(
                    library_uuid=self.LIBRARY_UUID,
                    membership_role=MembershipRole.OWNER,
                ),
                Membership(
                    library_uuid=uuid4(),
                    membership_role=MembershipRole.ADMIN,
                ),
                Membership(
                    library_uuid=uuid4(),
                    membership_role=MembershipRole.CONTRIBUTOR,
                ),
                Membership(
                    library_uuid=uuid4(),
                    membership_role=MembershipRole.VIEWER,
                ),
            ],
        )

    @pytest.fixture
    def user_with_no_memberships(self) -> User:
        return User(
            uuid=uuid4(),
            is_active=True,
            memberships=[],
        )

    @pytest.fixture
    def user_with_multiple_personal_library(self) -> User:
        return User(
            uuid=uuid4(),
            is_active=True,
            memberships=[
                Membership(
                    library_uuid=self.LIBRARY_UUID,
                    membership_role=MembershipRole.OWNER,
                ),
                Membership(
                    library_uuid=uuid4(),
                    membership_role=MembershipRole.OWNER,
                ),
            ],
        )

    def test_user_has_personal_library(self, user_with_one_personal_library):
        user = user_with_one_personal_library
        assert user.personal_library_uuid == self.LIBRARY_UUID

    def test_user_has_personal_library_raises_none_founded(self, user_with_no_memberships):
        user = user_with_no_memberships
        with pytest.raises(UserDoesNotHaveAPersonalLibraryException) as e:
            user.personal_library_uuid

        assert str(user.uuid) in str(e.value)

    def test_user_has_personal_library_raises_multi_founded(self, user_with_multiple_personal_library):
        user = user_with_multiple_personal_library
        with pytest.raises(UserHasMultiplePersonalLibraryException) as e:
            user.personal_library_uuid

        assert str(user.uuid) in str(e.value)
