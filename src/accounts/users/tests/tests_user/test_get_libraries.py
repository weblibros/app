from uuid import uuid4

import pytest

from src.accounts.users.user import Membership, User
from src.libraries.membership_roles import MembershipRole


class TestGetLibraries:
    OWNER_LIBRARY_UUID = uuid4()
    ADMIN_LIBRARY_UUID = uuid4()

    @pytest.fixture
    def user(self) -> User:
        return User(
            uuid=uuid4(),
            is_active=True,
            memberships=[
                Membership(
                    library_uuid=self.OWNER_LIBRARY_UUID,
                    membership_role=MembershipRole.OWNER,
                ),
                Membership(
                    library_uuid=self.ADMIN_LIBRARY_UUID,
                    membership_role=MembershipRole.ADMIN,
                ),
            ],
        )

    def test_user_get_all_libraries(self, user):
        libraries = user.get_libraries()
        assert isinstance(libraries, list)
        assert len(libraries) == 2
        assert self.OWNER_LIBRARY_UUID in libraries
        assert self.ADMIN_LIBRARY_UUID in libraries

    def test_user_get_filterd_libraries(self, user):
        libraries = user.get_libraries([MembershipRole.OWNER])
        assert isinstance(libraries, list)
        assert len(libraries) == 1
        assert self.OWNER_LIBRARY_UUID in libraries
        assert self.ADMIN_LIBRARY_UUID not in libraries

    def test_user_get_filterd_libraries_look_for_owner_and_viewer(self, user):
        libraries = user.get_libraries([MembershipRole.OWNER, MembershipRole.VIEWER])
        assert isinstance(libraries, list)
        assert len(libraries) == 1
        assert self.OWNER_LIBRARY_UUID in libraries
        assert self.ADMIN_LIBRARY_UUID not in libraries

    def test_user_get_filterd_libraries_look_for_owner_and_admin(self, user):
        libraries = user.get_libraries([MembershipRole.OWNER, MembershipRole.ADMIN])
        assert isinstance(libraries, list)
        assert len(libraries) == 2
        assert self.OWNER_LIBRARY_UUID in libraries
        assert self.ADMIN_LIBRARY_UUID in libraries
