import base64

from src.accounts.users.service import send_activation_email
from src.services.mail.fastmail import fast_mail_service


class TestServiceActivateEmail:
    async def test_send_activation_email(self, user):
        fast_mail_service.config.SUPPRESS_SEND = 1
        with fast_mail_service.record_messages() as outbox:
            await send_activation_email(user_uuid=user.uuid)
        assert len(outbox) == 1
        assert outbox[0]["from"] == "info@mail.web-libros.com"
        assert outbox[0]["To"] == "user@fake.com"
        assert outbox[0]["Subject"] == "Activate user WebLibros"

        mime_text = outbox[0].get_payload()[0]
        content = base64.b64decode(mime_text.get_payload()).decode("utf-8")
        assert "http" in content
        assert "<a href=" in content
        assert "/user/activate?token=" in content
