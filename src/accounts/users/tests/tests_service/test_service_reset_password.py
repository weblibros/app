from src.accounts.users.service import reset_password_by_email
from src.services.mail.fastmail import fast_mail_service


class TestServicePasswordReset:
    async def test_reset_password(self, user):
        fast_mail_service.config.SUPPRESS_SEND = 1
        with fast_mail_service.record_messages() as outbox:
            await reset_password_by_email(user_uuid=user.uuid)
        assert len(outbox) == 1
        assert outbox[0]["from"] == "info@mail.web-libros.com"
        assert outbox[0]["To"] == "user@fake.com"
        assert outbox[0]["Subject"] == "Reset User Password WebLibros"
