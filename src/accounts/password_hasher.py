from passlib.hash import pbkdf2_sha512


def hash_password(raw_password: str) -> str:
    hashed_password = pbkdf2_sha512.hash(raw_password)
    return hashed_password


def verify_password(raw_password: str, hashed_password: str | None) -> bool:
    if hashed_password:
        return pbkdf2_sha512.verify(raw_password, hashed_password)
    else:
        return False
