class FileNotFoundException(Exception):
    pass


class ImageFileNotFoundException(Exception):
    pass
