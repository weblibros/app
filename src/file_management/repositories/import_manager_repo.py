import abc
from typing import TYPE_CHECKING

from .models import ImportManager as ImportManagerORMModel
from .models import ImportStep as ImportStepORMModel

if TYPE_CHECKING:
    from sqlalchemy.ext.asyncio import AsyncSession

    from src.file_management.domain import ImportManager


class AbstractImportManagerRepo(abc.ABC):
    _seen: set["ImportManager"]

    async def add(self, import_manager: "ImportManager"):
        if not self._is_seen(import_manager):
            await self._store_model_to_persistence(import_manager)
            self.seen.add(import_manager)

    @abc.abstractmethod
    async def _store_model_to_persistence(self, import_manager: "ImportManager"):
        pass

    def _is_seen(self, import_manager: "ImportManager") -> bool:
        return import_manager in self._seen

    @property
    def seen(self) -> set["ImportManager"]:
        return self._seen


class InMemoryImportManagerRepo(AbstractImportManagerRepo):
    _managers_in_memory: list["ImportManager"]

    def __init__(self, memory: list["ImportManager"]):
        self._seen = set()
        self._managers_in_memory = memory

    async def _store_model_to_persistence(self, import_manager: "ImportManager"):
        self._managers_in_memory.append(import_manager)


class SqlImportManagerRepo(AbstractImportManagerRepo):
    def __init__(self, session: "AsyncSession") -> None:
        self.session = session
        self._seen = set()

    async def _store_model_to_persistence(self, import_manager: "ImportManager"):
        import_manager_kwargs = {
            "uuid": import_manager.uuid,
            "temp_file_uuid": import_manager.original_file.uuid,
            "book_uuid": import_manager.book_uuid,
            "library_uuid": import_manager.library_uuid,
            "uploaded_by_user_uuid": import_manager.uploaded_by_user_uuid,
            "started_at": import_manager.started_at,
            "finished_at": import_manager.finished_at,
            "imported_from": import_manager.imported_from.value,
        }
        import_steps_kwargs = [
            {
                "uuid": step.uuid,
                "step_name": step.step_name,
                "step_version": step._version,
                "import_manager_uuid": import_manager.uuid,
                "generated_files_uuids": [str(generated_file.uuid) for generated_file in step.generated_files],
                "result": step.processed_result.result.to_raw(),
                "completed": step.successfully_completed,
                "error_message": step.error_message,
                "started_at": step.started_at,
                "finished_at": step.finished_at,
            }
            for step in import_manager.steps
        ]
        import_manager = ImportManagerORMModel(**import_manager_kwargs)  # type: ignore
        import_manager_steps = [ImportStepORMModel(**step) for step in import_steps_kwargs]  # type: ignore
        try:
            self.session.add_all([import_manager, *import_manager_steps])
            await self.session.flush()
        except Exception as e:
            await self.session.rollback()
            raise e
