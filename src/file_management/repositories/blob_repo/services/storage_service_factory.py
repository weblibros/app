from typing import Optional, Type

from .enums import EnumStorageServices
from .storage_service import LocalStorageService, StorageServiceProtocol

storage_service_list: list[Type[StorageServiceProtocol]] = [
    LocalStorageService,
]


def get_default_storage_service_name() -> EnumStorageServices:
    return EnumStorageServices.LOCAL_STORAGE_SERVICE


def storage_service_factory(
    storage_service_name: Optional[EnumStorageServices | str] = None,
) -> StorageServiceProtocol:
    if storage_service_name is None:
        storage_service_name = get_default_storage_service_name()
    for storage_service_class in storage_service_list:
        if str(storage_service_class.name) == str(storage_service_name):
            storage_service = storage_service_class()
            return storage_service
    raise Exception(f"No storage service with name '{storage_service_name}' found")
