from enum import StrEnum


class EnumStorageServices(StrEnum):
    LOCAL_STORAGE_SERVICE = "local_storage_service"

    def __str__(self):
        return str(self.value)
