import os
from pathlib import Path
from typing import TYPE_CHECKING, Any, AsyncIterable, ClassVar, Generator, Protocol
from uuid import uuid4

import aiofiles

from src.config import settings
from src.file_management.domain import FileBlobLocal

from .enums import EnumStorageServices

if TYPE_CHECKING:
    from src.file_management.domain import AbstractFileBlob


class StorageServiceProtocol(Protocol):
    name: ClassVar[EnumStorageServices]

    async def store_blob(
        self,
        file_blob: "AbstractFileBlob",
        path: str,
    ): ...

    async def remove_blob(self, path: str): ...

    def stream_blob(self, path: str) -> Generator[bytes, Any, Any]: ...

    def file_iterator(self, path: str) -> AsyncIterable[bytes]: ...

    def create_file_path(self, file_name: str) -> str: ...

    def create_blob(self, path: str | Path) -> "AbstractFileBlob": ...


class AsyncFileIterable:
    def __init__(self, path, chunk_size=1024):
        self.path = path
        self.chunk_size = chunk_size

    async def __aiter__(self):
        async with aiofiles.open(self.path, mode="rb") as file:
            while True:
                chunk = await file.read(self.chunk_size)
                if not chunk:
                    break
                yield chunk


class LocalStorageService(StorageServiceProtocol):
    name = EnumStorageServices.LOCAL_STORAGE_SERVICE

    def __init__(self):
        self._check_file_dir_exists()

    async def store_blob(
        self,
        file_blob: "AbstractFileBlob",
        path: str | Path,
    ):
        if isinstance(path, str):
            path = Path(path)
        os.makedirs(os.path.dirname(path), exist_ok=True)
        async with aiofiles.open(path, "wb") as local_file:
            content = await file_blob.content()
            await local_file.write(content)

    async def remove_blob(self, path: str):
        if os.path.isfile(path):
            os.remove(path)

    def stream_blob(self, path: str) -> Generator[bytes, Any, Any]:
        with open(path, "rb") as f:
            yield from f

    def file_iterator(self, path: str) -> AsyncIterable[bytes]:
        return AsyncFileIterable(path)

    def _check_file_dir_exists(self):
        directory = settings.STORAGE_BASE_DIR
        if not os.path.isdir(directory):
            raise Exception(f"directory {directory} does not exists")

    def create_file_path(self, file_name):
        unique_string = uuid4().hex[:10]
        return f"{settings.STORAGE_BASE_DIR}/{unique_string}-{file_name}"

    def create_blob(self, path: str | Path) -> "AbstractFileBlob":
        return FileBlobLocal(path)
