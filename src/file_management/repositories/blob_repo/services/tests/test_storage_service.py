import os

import aiofiles

from src.file_management.domain import FileBlobUpload
from src.file_management.tests.fakers import fake_small_upload_file

from ..storage_service import AsyncFileIterable, LocalStorageService


class TestLocalStorageService:
    local_storage_service_class = LocalStorageService

    def test_service_name(self):
        local_storage_service = self.local_storage_service_class()
        assert str(local_storage_service.name) == "local_storage_service"

    async def test_upload_file(self):
        path = "/tmp/test-fake-file-upload.epub"
        upload_file = fake_small_upload_file()
        local_storage_service = self.local_storage_service_class()
        await local_storage_service.store_blob(FileBlobUpload(upload_file), path)
        assert os.path.isfile(path)
        os.remove(path)

    async def test_remove_blob(self):
        temp_path = "/tmp/fake-file-to-delete.txt"
        async with aiofiles.open(temp_path, "w+") as local_file:
            await local_file.write("for testing")

        assert os.path.isfile(temp_path)
        local_storage_service = self.local_storage_service_class()
        await local_storage_service.remove_blob(temp_path)
        assert not os.path.isfile(temp_path)

    async def test_file_service_can_return_a_file_iterator(self):
        local_storage_service = self.local_storage_service_class()
        temp_path = "/tmp/fake-file-to-delete.txt"
        async with aiofiles.open(temp_path, "w+") as local_file:
            await local_file.write("for testing")
        assert os.path.isfile(temp_path)
        file_iterator = local_storage_service.file_iterator(temp_path)
        assert isinstance(file_iterator, AsyncFileIterable)
        await local_storage_service.remove_blob(temp_path)
        assert not os.path.isfile(temp_path)
