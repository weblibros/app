import pytest

from ..enums import EnumStorageServices
from ..storage_service import LocalStorageService
from ..storage_service_factory import storage_service_factory


def test_storage_service_factory():
    storage_service = storage_service_factory()

    assert isinstance(storage_service, LocalStorageService)


def test_storage_service_factory_pass_service_name_as_string():
    storage_service = storage_service_factory("local_storage_service")

    assert isinstance(storage_service, LocalStorageService)


def test_storage_service_factory_pass_service_name_as_enum():
    storage_service = storage_service_factory(EnumStorageServices.LOCAL_STORAGE_SERVICE)

    assert isinstance(storage_service, LocalStorageService)


def test_storage_service_factory_service_does_not_exists():
    wrong_storage_service = "does_not_exists"
    with pytest.raises(Exception) as e:
        storage_service_factory(wrong_storage_service)

    assert wrong_storage_service in str(e.value)
