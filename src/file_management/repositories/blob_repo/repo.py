import contextlib
from pathlib import Path
from typing import TYPE_CHECKING, Optional

from src.file_management.domain import File

from .services import StorageServiceProtocol, storage_service_factory

if TYPE_CHECKING:
    from src.file_management.domain import AbstractFileBlob

    from .services.enums import EnumStorageServices


class FileBlobRepo:
    blob_storage_service: StorageServiceProtocol

    def __init__(self, storage_service_name: Optional["str | EnumStorageServices"] = None):
        self.blob_storage_service = storage_service_factory(storage_service_name)

    @contextlib.asynccontextmanager
    async def add_file_blob(self, file: "File"):
        await self.blob_storage_service.store_blob(file.blob, str(file.path))
        try:
            yield
        except Exception as e:
            await self._rollback_add_file_blob(file)
            raise e

    async def _rollback_add_file_blob(self, file: "File"):
        await self.blob_storage_service.remove_blob(str(file.path))

    def storage_service_name(self):
        return str(self.blob_storage_service.name)

    async def get_file_blob(self, path: Path | str) -> "AbstractFileBlob":
        return self.blob_storage_service.create_blob(path)

    async def remove_blob(self, path: Path | str):
        await self.blob_storage_service.remove_blob(str(path))
