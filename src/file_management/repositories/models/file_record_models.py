from typing import TYPE_CHECKING, Optional
from uuid import UUID

import sqlalchemy as sa
from fastapi_events.dispatcher import dispatch
from sqlalchemy import event
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.config import table_names
from src.file_management.domain.events import FileManagmentEvents, FileRecordEventSchema
from src.modules.database.base_model import Base, TimeStamp

if TYPE_CHECKING:
    from src.books.repositories.models import BookModel, CoverModel


class FileRecord(Base, TimeStamp):
    __tablename__ = table_names.FILERECORD_TABLE_NAME

    path: Mapped[str] = mapped_column(sa.String, nullable=False)
    size_bytes: Mapped[int] = mapped_column(sa.BigInteger, nullable=False)
    extension: Mapped[str] = mapped_column(sa.String(15), nullable=False)
    content_type: Mapped[str] = mapped_column(sa.String(50), nullable=False)
    storage_service_name: Mapped[str] = mapped_column(sa.String(25), nullable=False)
    entity_class_name: Mapped[str] = mapped_column(sa.String(50), nullable=False)

    book: Mapped[Optional["BookModel"]] = relationship(
        "BookModel",
        secondary=table_names.BOOK_FILERECORDS_TABLE_NAME,
        back_populates="file_records",
    )

    image: Mapped[Optional["ImageRecord"]] = relationship(
        "ImageRecord",
        back_populates="file_record",
        foreign_keys="[ImageRecord.file_record_uuid]",
        passive_deletes=True,
    )

    __table_args__ = (sa.UniqueConstraint("path", name="_path_uc"),)


class ImageRecord(Base, TimeStamp):
    __tablename__ = table_names.IMAGE_FILERECORD_TABLE_NAME

    file_record_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.FILERECORD_TABLE_NAME}.uuid",
            ondelete="CASCADE",
        ),
        nullable=False,
    )
    extracted_from_file_record_uuid: Mapped[Optional[UUID]] = mapped_column(
        sa.ForeignKey(
            f"{table_names.FILERECORD_TABLE_NAME}.uuid",
            ondelete="SET NULL",
        ),
        nullable=True,
    )

    pixel_height: Mapped[int] = mapped_column(sa.SmallInteger, nullable=False)
    pixel_width: Mapped[int] = mapped_column(sa.SmallInteger, nullable=False)

    file_record: Mapped[FileRecord] = relationship(
        "FileRecord",
        back_populates="image",
        foreign_keys=[file_record_uuid],
        cascade="all, delete",
        passive_deletes=False,
    )
    cover: Mapped["CoverModel"] = relationship(
        "CoverModel",
        back_populates="image",
        cascade="all, delete",
        passive_deletes=False,
    )


@event.listens_for(FileRecord, "after_delete")
def event_after_delete_file_record(_mapper, _connection, target: FileRecord):
    dispatch(
        FileManagmentEvents.FILE_RECORD_REMOVED,
        payload=dict(FileRecordEventSchema.model_validate(target, from_attributes=True)),
    )
