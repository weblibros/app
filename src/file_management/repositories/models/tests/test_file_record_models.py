from uuid import uuid4

import pytest
from sqlalchemy import exists, select

from src.config.database import async_session_maker
from src.file_management.repositories.models import FileRecord, ImageRecord


class TestDeleteBehaviourImageFileRecord:
    IMAGE_UUID = uuid4()
    FILE_RECORD = uuid4()
    FILE_RECORD_EXTRACTED_FORM = uuid4()

    @pytest.fixture(scope="function")
    async def session_image_and_files(self):
        file_data = {
            "size_bytes": 23,
            "extension": "epub",
            "content_type": "application/epub+zip",
            "storage_service_name": "FakeService",
            "entity_class_name": "File",
        }
        file_record = FileRecord(
            uuid=self.FILE_RECORD,
            path="first_file_record_delete_file.epub",
            **file_data,
        )
        file_record_extracted_form = FileRecord(
            uuid=self.FILE_RECORD_EXTRACTED_FORM,
            path="second_file_record_delete_extracted_from.epub",
            **file_data,
        )
        image = ImageRecord(
            uuid=self.IMAGE_UUID,
            file_record_uuid=file_record.uuid,
            extracted_from_file_record_uuid=file_record_extracted_form.uuid,
            pixel_height=100,
            pixel_width=150,
        )
        async with async_session_maker() as session:
            session.add_all([file_record, file_record_extracted_form, image])
            await session.flush()
            session.expire_all()
            yield session
            await session.rollback()

    async def test_cascase_delete_image(self, session_image_and_files):
        session = session_image_and_files
        assert await session.scalar(self.query_image_exists()) is True
        assert await session.scalar(self.query_file_record_exists()) is True
        assert await session.scalar(self.query_file_record_extraxted_from_exists()) is True

        image = await session.get(ImageRecord, self.IMAGE_UUID)
        await session.delete(image)
        await session.flush()
        assert await session.scalar(self.query_image_exists()) is False
        assert await session.scalar(self.query_file_record_exists()) is False
        assert await session.scalar(self.query_file_record_extraxted_from_exists()) is True

    async def test_cascase_delete_filerecord(self, session_image_and_files):
        session = session_image_and_files
        assert await session.scalar(self.query_image_exists()) is True
        assert await session.scalar(self.query_file_record_exists()) is True
        assert await session.scalar(self.query_file_record_extraxted_from_exists()) is True

        file_record = await session.get(FileRecord, self.FILE_RECORD)
        await session.delete(file_record)
        await session.flush()
        assert await session.scalar(self.query_image_exists()) is False
        assert await session.scalar(self.query_file_record_exists()) is False
        assert await session.scalar(self.query_file_record_extraxted_from_exists()) is True

    async def test_delete_filerecord_extracted_from(self, session_image_and_files):
        session = session_image_and_files
        assert await session.scalar(self.query_image_exists()) is True
        assert await session.scalar(self.query_file_record_exists()) is True
        assert await session.scalar(self.query_file_record_extraxted_from_exists()) is True

        file_record = await session.get(FileRecord, self.FILE_RECORD_EXTRACTED_FORM)
        await session.delete(file_record)
        await session.flush()
        assert await session.scalar(self.query_image_exists()) is True
        assert await session.scalar(self.query_file_record_exists()) is True
        assert await session.scalar(self.query_file_record_extraxted_from_exists()) is False
        image = await session.scalar(select(ImageRecord).where(ImageRecord.uuid == self.IMAGE_UUID))
        assert image.file_record_uuid == self.FILE_RECORD

    def query_image_exists(self):
        return exists().where(ImageRecord.uuid == self.IMAGE_UUID).select()

    def query_file_record_exists(self):
        return exists().where(FileRecord.uuid == self.FILE_RECORD).select()

    def query_file_record_extraxted_from_exists(self):
        return exists().where(FileRecord.uuid == self.FILE_RECORD_EXTRACTED_FORM).select()
