from datetime import datetime
from uuid import UUID

import sqlalchemy as sa
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy_utils import UUIDType

from src.config import table_names
from src.modules.database.base_model import Base


class ImportManager(Base):
    __tablename__ = table_names.IMPORT_MANAGER_TABLE_NAME

    temp_file_uuid: Mapped[UUID] = mapped_column(UUIDType(binary=False), nullable=False)
    book_uuid: Mapped[UUID] = mapped_column(UUIDType(binary=False), nullable=False)
    library_uuid: Mapped[UUID] = mapped_column(UUIDType(binary=False), nullable=False)
    uploaded_by_user_uuid: Mapped[UUID] = mapped_column(UUIDType(binary=False), nullable=False)
    imported_from: Mapped[str] = mapped_column(sa.String(50), nullable=True)
    started_at: Mapped[datetime] = mapped_column(
        sa.DateTime,
        nullable=False,
    )
    finished_at: Mapped[datetime] = mapped_column(
        sa.DateTime,
        nullable=False,
    )


class ImportStep(Base):
    __tablename__ = table_names.IMPORT_STEP_TABLE_NAME

    import_manager_uuid: Mapped[UUID] = mapped_column(
        sa.ForeignKey(
            f"{table_names.IMPORT_MANAGER_TABLE_NAME}.uuid",
            ondelete="CASCADE",
        ),
        nullable=False,
    )
    step_name: Mapped[str] = mapped_column(sa.String(50), nullable=False)
    step_version: Mapped[str] = mapped_column(sa.String(10), nullable=False)
    generated_files_uuids: Mapped[dict] = mapped_column(sa.JSON, nullable=False)
    result: Mapped[dict] = mapped_column(sa.JSON, nullable=False)
    completed: Mapped[bool] = mapped_column(sa.Boolean, nullable=False)
    error_message: Mapped[str] = mapped_column(sa.Text, nullable=False)
    started_at: Mapped[datetime] = mapped_column(
        sa.DateTime,
        nullable=False,
    )
    finished_at: Mapped[datetime] = mapped_column(
        sa.DateTime,
        nullable=False,
    )
