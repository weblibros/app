from uuid import uuid4

import pytest

from src.config.database import async_session_maker
from src.file_management.domain import EbookFile, File, ImageFile, TempFile
from src.file_management.repositories import FileRepo
from src.file_management.repositories.models import FileRecord, ImageRecord


class TestGetEbookFileRepo:
    repo_class = FileRepo
    file_path = "src/file_management/tests/files/dummy-ebook.epub"

    @pytest.fixture()
    async def file_record_with_session(self):
        async with async_session_maker() as session:
            file_record = FileRecord(
                path=self.file_path,
                size_bytes=23,
                extension="epub",
                content_type="application/epub+zip",
                storage_service_name="local_storage_service",
                entity_class_name="EbookFile",
            )
            session.add(file_record)
            await session.flush()
            file_record_uuid = file_record.uuid
            session.expire_all()
            yield file_record_uuid, session
            await session.rollback()

    async def test_get_file(self, file_record_with_session):
        file_record_uuid, session = file_record_with_session
        repo = self.repo_class(session)
        file = await repo.get(file_record_uuid)

        assert file is not None
        assert file.blob is not None
        assert isinstance(file, File)
        assert isinstance(file, EbookFile)
        assert hasattr(file.blob, "content")
        assert str(file.path) == self.file_path


class TestGetTempFileRepo:
    repo_class = FileRepo
    file_path = "src/file_management/tests/files/dummy-ebook.epub"

    @pytest.fixture()
    async def file_record_with_session(self):
        async with async_session_maker() as session:
            file_record = FileRecord(
                path=self.file_path,
                size_bytes=23,
                extension="epub",
                content_type="application/epub+zip",
                storage_service_name="local_storage_service",
                entity_class_name="TempFile",
            )
            session.add(file_record)
            await session.flush()
            file_record_uuid = file_record.uuid
            session.expire_all()
            yield file_record_uuid, session
            await session.rollback()

    async def test_get_file(self, file_record_with_session):
        file_record_uuid, session = file_record_with_session
        repo = self.repo_class(session)
        file = await repo.get(file_record_uuid)

        assert file is not None
        assert file.blob is not None
        assert isinstance(file, File)
        assert isinstance(file, TempFile)
        assert hasattr(file.blob, "content")
        assert str(file.path) == self.file_path


class TestGetImageFileRepo:
    repo_class = FileRepo
    file_path = "src/file_management/tests/files/random_image.jpg"

    @pytest.fixture()
    async def image_record_with_session(self):
        async with async_session_maker() as session:
            file_record = FileRecord(
                uuid=uuid4(),
                path=self.file_path,
                size_bytes=23,
                extension="jpeg",
                content_type="image/jpeg",
                storage_service_name="local_storage_service",
                entity_class_name="ImageFile",
            )
            image_record = ImageRecord(
                uuid=uuid4(),
                file_record_uuid=file_record.uuid,
                pixel_height=100,
                pixel_width=100,
                extracted_from_file_record_uuid=None,
            )
            session.add_all([file_record, image_record])
            await session.flush()
            file_record_uuid = file_record.uuid
            image_record_uuid = image_record.uuid
            session.expire_all()
            yield image_record_uuid, file_record_uuid, session
            await session.rollback()

    async def test_get_file(self, image_record_with_session):
        image_record_uuid, file_record_uuid, session = image_record_with_session
        repo = self.repo_class(session)
        file = await repo.get(file_record_uuid)

        assert file is not None
        assert file.blob is not None
        assert isinstance(file, File)
        assert isinstance(file, ImageFile)
        assert hasattr(file.blob, "content")
        assert str(file.path) == self.file_path
        assert file.uuid == file_record_uuid
        assert file.image_uuid == image_record_uuid

    async def test_get_file_by_image_uuid(self, image_record_with_session):
        image_record_uuid, file_record_uuid, session = image_record_with_session
        repo = self.repo_class(session)
        file = await repo.get_by_image_uuid(image_record_uuid)

        assert file is not None
        assert file.blob is not None
        assert isinstance(file, File)
        assert hasattr(file.blob, "content")
        assert str(file.path) == self.file_path
        assert file.uuid == file_record_uuid
        assert file.image_uuid == image_record_uuid
