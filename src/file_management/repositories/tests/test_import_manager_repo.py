from uuid import UUID, uuid4

import pytest
from sqlalchemy import select

from src.config.database import async_session_maker
from src.file_management.domain import ImportManager
from src.file_management.domain.events import ProcessedImportStep, ProcessedImportStepResult
from src.file_management.domain.import_manager import ImportStep
from src.file_management.domain.tests.fake import fake_temp_file_factory
from src.file_management.repositories import InMemoryImportManagerRepo, SqlImportManagerRepo
from src.file_management.repositories.models import ImportManager as ImportManagerORMModel
from src.file_management.repositories.models import ImportStep as ImportStepORMModel


class FakeProcessedImportStepResult(ProcessedImportStepResult):
    uuid: UUID = uuid4()
    foo: str = "bar"


class FakeProcessedImportStep(ProcessedImportStep):
    result: FakeProcessedImportStepResult


class FakeSuccessImportStep(ImportStep):
    process_event_class = FakeProcessedImportStep

    async def _process(self):
        return FakeProcessedImportStepResult()


class FakeFailingImportStep(ImportStep):
    process_event_class = FakeProcessedImportStep

    async def _process(self):
        raise Exception("Something went wrong")
        return FakeProcessedImportStepResult()


class FakeImportManager(ImportManager):
    _step_classes = [
        FakeSuccessImportStep,
        FakeSuccessImportStep,
    ]


class FakeFailedImportManager(ImportManager):
    _step_classes = [
        FakeSuccessImportStep,
        FakeFailingImportStep,
        FakeSuccessImportStep,
    ]


@pytest.fixture
async def import_manager():
    manager = FakeImportManager(
        temp_file=fake_temp_file_factory(),
        uploaded_by_user_uuid=uuid4(),
        library_uuid=uuid4(),
    )
    await manager.process()
    return manager


@pytest.fixture
async def failed_import_manager():
    manager = FakeFailedImportManager(
        temp_file=fake_temp_file_factory(),
        uploaded_by_user_uuid=uuid4(),
        library_uuid=uuid4(),
    )
    await manager.process()
    return manager


class TestInMemoryImportManagerRepo:
    repo_class = InMemoryImportManagerRepo

    async def test_repo_can_save_manager(self, import_manager):
        memory = []
        repo = self.repo_class(memory)
        await repo.add(import_manager)
        assert len(memory) == 1
        assert memory[0] == import_manager

    async def test_repo_does_not_save_manager_twice(self, import_manager):
        memory = []
        repo = self.repo_class(memory)
        await repo.add(import_manager)
        assert len(memory) == 1
        await repo.add(import_manager)
        assert len(memory) == 1
        assert memory[0] == import_manager


class TestSqlImportManagerRepo:
    repo_class = SqlImportManagerRepo

    async def test_repo_can_save_manager(self, import_manager):
        async with async_session_maker() as session:
            repo = self.repo_class(session)
            await repo.add(import_manager)
            assert len(repo.seen) == 1
            assert next(iter(repo.seen)) == import_manager
            query = select(ImportManagerORMModel).where(ImportManagerORMModel.uuid == import_manager.uuid)
            result = await session.execute(query)
            assert result.scalar().uuid == import_manager.uuid
            query_step = select(ImportStepORMModel).where(ImportStepORMModel.import_manager_uuid == import_manager.uuid)
            result_steps = (await session.execute(query_step)).scalars()
            result_steps_uuids = [step.uuid for step in result_steps]
            assert result_steps_uuids == [step.uuid for step in import_manager.steps]
            assert len(result_steps_uuids) == 2
            await session.rollback()

    async def test_repo_does_not_save_manager_twice(self, import_manager):
        async with async_session_maker() as session:
            repo = self.repo_class(session)
            await repo.add(import_manager)
            assert len(repo.seen) == 1
            assert next(iter(repo.seen)) == import_manager
            await repo.add(import_manager)
            assert len(repo.seen) == 1
            await session.rollback()

    async def test_repo_can_save_failed_manager(self, failed_import_manager):
        import_manager = failed_import_manager
        assert len(import_manager.steps) == 3
        async with async_session_maker() as session:
            repo = self.repo_class(session)
            await repo.add(import_manager)
            assert len(repo.seen) == 1
            assert next(iter(repo.seen)) == import_manager
            query = select(ImportManagerORMModel).where(ImportManagerORMModel.uuid == import_manager.uuid)
            result = await session.execute(query)
            assert result.scalar().uuid == import_manager.uuid
            query_step = select(ImportStepORMModel).where(ImportStepORMModel.import_manager_uuid == import_manager.uuid)
            result_steps = [step for step in (await session.execute(query_step)).scalars()]
            result_steps_uuids = [step.uuid for step in result_steps]
            assert result_steps_uuids == [step.uuid for step in import_manager.steps]
            assert len(result_steps_uuids) == 2
            assert [True, False] == [step.completed for step in result_steps]
            await session.rollback()
