from pathlib import Path
from uuid import uuid4

import pytest
from sqlalchemy import delete, select

from src.config.database import async_session_maker
from src.file_management.domain import FileSize, FileType, ImageFile
from src.file_management.domain.tests.fake import FakeFileBlobUpload
from src.file_management.repositories import FileRepo
from src.file_management.repositories.models import FileRecord, ImageRecord
from src.file_management.tests.cleanup import remove_files_in_dir

storage_dir = "/tmp/test_image_repo/"


class TestImageRepository:
    image_file = ImageFile(
        uuid=uuid4(),
        file_type=FileType(mime_type="image/png", extension=".png"),
        file_size=FileSize(101),
        blob=FakeFileBlobUpload(),
        path=Path(storage_dir).joinpath("cover_image.png"),
        pixel_width=14,
        pixel_height=12,
        extracted_from=None,
    )

    @pytest.fixture()
    async def cleanup(self):
        yield
        remove_files_in_dir(storage_dir)
        async with async_session_maker() as session:
            delete_query = delete(FileRecord).where(FileRecord.path.contains(storage_dir))
            await session.execute(delete_query)
            await session.commit()

    @pytest.mark.usefixtures("cleanup")
    async def test_add_repo(self):
        await FileRepo().add(self.image_file)

        async with async_session_maker() as session:
            file_record = await self.select_file_record(session, self.image_file.uuid)
            image_record = await self.select_image_record(session, self.image_file.uuid)

        assert isinstance(file_record, FileRecord)
        assert file_record.uuid == self.image_file.uuid
        assert isinstance(image_record, ImageRecord)
        assert image_record.file_record_uuid == self.image_file.uuid
        assert image_record.file_record_uuid == file_record.uuid

    async def select_image_record(self, session, file_record_uuid) -> ImageRecord:
        query = select(ImageRecord).where(ImageRecord.file_record_uuid == file_record_uuid)
        result = (await session.execute(query)).scalar()
        return result

    async def select_file_record(self, session, file_record_uuid) -> FileRecord:
        query = select(FileRecord).where(FileRecord.uuid == file_record_uuid)
        result = (await session.execute(query)).scalar()
        return result
