import os
import shutil
from uuid import uuid4

import pytest
from sqlalchemy import exists

from src.config.database import async_session_maker
from src.file_management.repositories import FileRepo
from src.file_management.repositories.models import FileRecord, ImageRecord


class TestRemoveFileRepoForEbooks:
    repo_class = FileRepo
    file_path = "src/file_management/tests/files/dummy-ebook.epub"
    tmp_file_path = "/tmp/dummy-ebook.epub"

    @pytest.fixture()
    def create_tmp_file_path(self):
        shutil.copyfile(self.file_path, self.tmp_file_path)
        yield self.tmp_file_path

        if os.path.isfile(self.tmp_file_path):
            os.remove(self.tmp_file_path)

    @pytest.fixture()
    async def file_record_with_session(self, create_tmp_file_path):
        async with async_session_maker() as session:
            file_record = FileRecord(
                path=self.tmp_file_path,
                size_bytes=23,
                extension="epub",
                content_type="application/epub+zip",
                storage_service_name="local_storage_service",
                entity_class_name="EbookFile",
            )
            session.add(file_record)
            await session.flush()
            file_record_uuid = file_record.uuid
            yield file_record_uuid, session
            await session.rollback()

    async def test_remove_file(self, file_record_with_session):
        file_record_uuid, session = file_record_with_session
        assert (await session.execute(self.query_exits_file_record(file_record_uuid))).scalar() is True
        repo = self.repo_class(session)

        file = await repo.get(file_record_uuid)
        assert file.uuid == file_record_uuid
        assert (await session.execute(self.query_exits_file_record(file_record_uuid))).scalar() is True
        await repo.remove(file)

        assert os.path.isfile(self.tmp_file_path) is False
        assert (await session.execute(self.query_exits_file_record(file.uuid))).scalar() is False

    def query_exits_file_record(self, file_record_uuid):
        return exists().where(FileRecord.uuid == file_record_uuid).select()


class TestRemoveImageFileRepo:
    repo_class = FileRepo
    file_path = "src/file_management/tests/files/random_image.jpg"
    tmp_file_path = "/tmp/random_image.jpeg"

    @pytest.fixture()
    def create_tmp_file_path(self):
        shutil.copyfile(self.file_path, self.tmp_file_path)
        yield self.tmp_file_path

        if os.path.isfile(self.tmp_file_path):
            os.remove(self.tmp_file_path)

    @pytest.fixture()
    async def image_record_with_session(self, create_tmp_file_path):
        async with async_session_maker() as session:
            file_record = FileRecord(
                uuid=uuid4(),
                path=self.tmp_file_path,
                size_bytes=23,
                extension="jpeg",
                content_type="image/jpeg",
                storage_service_name="local_storage_service",
                entity_class_name="ImageFile",
            )
            image_record = ImageRecord(
                uuid=uuid4(),
                file_record_uuid=file_record.uuid,
                pixel_height=100,
                pixel_width=100,
                extracted_from_file_record_uuid=None,
            )
            session.add_all([file_record, image_record])
            await session.flush()
            file_record_uuid = file_record.uuid
            image_record_uuid = image_record.uuid
            yield image_record_uuid, file_record_uuid, session
            await session.rollback()

    async def test_remove_image_file(self, image_record_with_session):
        image_record_uuid, file_record_uuid, session = image_record_with_session
        assert (await session.execute(self.query_exits_file_record(file_record_uuid))).scalar() is True
        assert (await session.execute(self.query_exits_image_record(image_record_uuid))).scalar() is True
        repo = self.repo_class(session)

        file = await repo.get_by_image_uuid(image_record_uuid)
        assert file.uuid == file_record_uuid
        assert (await session.execute(self.query_exits_file_record(file_record_uuid))).scalar() is True
        await repo.remove(file)

        assert os.path.isfile(self.tmp_file_path) is False
        assert (await session.execute(self.query_exits_file_record(file.uuid))).scalar() is False
        assert (await session.execute(self.query_exits_image_record(file.image_uuid))).scalar() is False

    def query_exits_file_record(self, file_record_uuid):
        return exists().where(FileRecord.uuid == file_record_uuid).select()

    def query_exits_image_record(self, image_record_uuid):
        return exists().where(ImageRecord.uuid == image_record_uuid).select()
