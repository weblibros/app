from pathlib import Path
from uuid import uuid4

import pytest
from sqlalchemy import delete, select

from src.config.database import async_session_maker
from src.file_management.domain import EbookFile, FileSize, FileType, TempFile
from src.file_management.domain.tests.fake import FakeFileBlobUpload
from src.file_management.repositories import FileRepo
from src.file_management.repositories.models import FileRecord
from src.file_management.tests.cleanup import remove_files_in_dir

storage_dir = "/tmp/test_file_repo/"


class TestFileRepositoryWithEbook:
    file_object = EbookFile(
        uuid=uuid4(),
        file_type=FileType(mime_type="application/epub+zip", extension=".epub"),
        file_size=FileSize(101),
        blob=FakeFileBlobUpload(),
        path=Path(storage_dir).joinpath("test_ebook.epub"),
    )

    @pytest.fixture()
    async def cleanup(self):
        yield
        remove_files_in_dir(storage_dir)
        async with async_session_maker() as session:
            delete_query = delete(FileRecord).where(FileRecord.path.contains(storage_dir))
            await session.execute(delete_query)
            await session.commit()

    @pytest.mark.usefixtures("cleanup")
    async def test_add_repo(self):
        await FileRepo().add(self.file_object)

        async with async_session_maker() as session:
            file_record = await self.select_file_record(session, self.file_object.uuid)

        assert isinstance(file_record, FileRecord)
        assert file_record.uuid == self.file_object.uuid

    async def select_file_record(self, session, file_record_uuid) -> FileRecord:
        query = select(FileRecord).where(FileRecord.uuid == file_record_uuid)
        result = (await session.execute(query)).scalar()
        return result


class TestFileRepositoryWithTempFile:
    file_object = TempFile(
        uuid=uuid4(),
        file_type=FileType(mime_type="application/epub+zip", extension=".epub"),
        file_size=FileSize(101),
        blob=FakeFileBlobUpload(),
        path=Path(storage_dir).joinpath("test_ebook.epub"),
    )

    @pytest.fixture()
    async def cleanup(self):
        yield
        remove_files_in_dir(storage_dir)
        async with async_session_maker() as session:
            delete_query = delete(FileRecord).where(FileRecord.path.contains(storage_dir))
            await session.execute(delete_query)
            await session.commit()

    @pytest.mark.usefixtures("cleanup")
    async def test_add_repo(self):
        await FileRepo().add(self.file_object)

        async with async_session_maker() as session:
            file_record = await self.select_file_record(session, self.file_object.uuid)

        assert isinstance(file_record, FileRecord)
        assert file_record.uuid == self.file_object.uuid

    async def select_file_record(self, session, file_record_uuid) -> FileRecord:
        query = select(FileRecord).where(FileRecord.uuid == file_record_uuid)
        result = (await session.execute(query)).scalar()
        return result
