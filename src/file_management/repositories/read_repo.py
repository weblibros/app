from functools import partial
from typing import TYPE_CHECKING, Any, Generator
from uuid import UUID

from src.config.database import async_session_manager
from src.file_management.repositories.queries import selector

from .blob_repo.services import storage_service_factory

if TYPE_CHECKING:
    from .dto import PathServiceNameDTO


class ImageFileReadRepo:
    storage_service_factory = partial(storage_service_factory)
    async_session_manager = partial(async_session_manager)

    async def image_is_cover_in_libraries(self, image_uuid: UUID, libraries: list[UUID]) -> bool:
        async with self.async_session_manager() as session:
            is_cover = await selector.image_is_cover_in_libraries(session, image_uuid, libraries)
        return is_cover

    async def stream_blob(self, image_uuid: UUID) -> Generator[bytes, Any, Any]:
        path_service_name_dto = await self._get_path_and_name_storage_service(image_uuid)
        if path_service_name_dto is None:
            raise Exception("Image record not found")
        storage_service = self.storage_service_factory(path_service_name_dto.service_name)
        return storage_service.stream_blob(path_service_name_dto.path)

    async def _get_path_and_name_storage_service(self, image_uuid) -> "PathServiceNameDTO | None":
        async with self.async_session_manager() as session:
            path_service_name_dto = await selector.get_path_and_service_name_by_image_uuid(session, image_uuid)
        return path_service_name_dto
