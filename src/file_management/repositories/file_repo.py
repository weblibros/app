import abc
from functools import partial, singledispatchmethod
from pathlib import Path
from typing import TYPE_CHECKING, Optional, Type
from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession

from src.config.database import async_session_manager
from src.file_management.domain import EbookFile, File, FileSize, FileType, ImageFile, TempFile
from src.file_management.repositories.queries import remove, selector
from src.file_management.repositories.queries.create import create_file_record_v2, create_image_record_v2

from .blob_repo import FileBlobRepo
from .exceptions import FileNotFoundException, ImageFileNotFoundException

if TYPE_CHECKING:
    from src.file_management.repositories.dto import ImageFileRecordDTO
    from src.file_management.repositories.models import FileRecord, ImageRecord


class AbstractFileRepo(abc.ABC):
    seen: set["File"] = set()

    @abc.abstractmethod
    async def add(self, file: "File"):
        raise NotImplementedError

    @abc.abstractmethod
    async def get(self, uuid: UUID) -> "File":
        raise NotImplementedError

    @abc.abstractmethod
    async def remove(self, file: "File"):
        raise NotImplementedError


class InMemoryFileRepo(AbstractFileRepo):
    _files_in_memory: list[File]

    def __init__(self, _files_in_memory: list[File] = []):
        self._files_in_memory = _files_in_memory
        self.seen = set()

    async def add(self, file: "File"):
        self._files_in_memory.append(file)
        self.seen.add(file)

    async def get(self, uuid: UUID) -> "File":
        found_files = [file for file in self._files_in_memory if file.uuid == uuid]
        if len(found_files) == 1:
            file = found_files[0]
            self.seen.add(file)
            return file
        else:
            raise FileNotFoundException()

    async def remove(self, file: "File"):
        self._files_in_memory.remove(file)
        self.seen.add(file)


class FileRecordRepo:
    async_session_manager = partial(async_session_manager)

    def __init__(self, session_overwrite: Optional[AsyncSession] = None) -> None:
        self.session_overwrite = session_overwrite

    async def add_file_record(self, file: "File", storage_service_name: str):
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            await create_file_record_v2(session, file, storage_service_name)

    async def add_image_file_record(self, image_file: "ImageFile", storage_service_name: str):
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            await create_image_record_v2(session, image_file, storage_service_name)

    async def get_file_record(self, uuid: UUID) -> "FileRecord":
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            file_record_model = await selector.find_file_record_by_uuid(session, uuid)
        if file_record_model:
            return file_record_model
        else:
            raise FileNotFoundException()

    async def get_image_record_by_file_uuid(self, file_record_uuid: UUID) -> "ImageRecord":
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            image_record_model = await selector.find_image_record_by_file_uuid(session, file_record_uuid)
        if image_record_model:
            return image_record_model
        else:
            raise FileNotFoundException()

    async def get_image_info_by_image_record_uuid(self, image_record_uuid: UUID) -> "ImageFileRecordDTO":
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            image_info = await selector.find_image_info_by_image_record_uuid(session, image_record_uuid)
        if image_info:
            return image_info
        else:
            raise ImageFileNotFoundException()

    async def remove_file_record(self, uuid: UUID):
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            await remove.remove_file_record(session, uuid)

    async def remove_image_record(self, file_uuid: UUID):
        async with self.async_session_manager(session_overwrite=self.session_overwrite) as session:
            await remove.remove_image_record(session, file_uuid)


class FileRepo(AbstractFileRepo):
    file_record_repo: FileRecordRepo
    file_blob_repo_class: Type[FileBlobRepo]

    def __init__(self, session_overwrite: Optional[AsyncSession] = None) -> None:
        self.session_overwrite = session_overwrite
        self.file_record_repo = FileRecordRepo(session_overwrite=session_overwrite)
        self.file_blob_repo_class = FileBlobRepo

    async def add(self, file: "File"):
        return await self._add(file)

    @singledispatchmethod
    async def _add(self, file: "File"):
        raise NotImplementedError("file type cannot be added")

    @_add.register
    async def _add_ebookfile(self, file: EbookFile):
        file_blob_repo = self.file_blob_repo_class()
        async with file_blob_repo.add_file_blob(file):
            await self.file_record_repo.add_file_record(file, file_blob_repo.storage_service_name())
        self.seen.add(file)

    @_add.register
    async def _add_tempfile(self, file: TempFile):
        file_blob_repo = self.file_blob_repo_class()
        async with file_blob_repo.add_file_blob(file):
            await self.file_record_repo.add_file_record(file, file_blob_repo.storage_service_name())
        self.seen.add(file)

    @_add.register
    async def _add_image_file(self, file: ImageFile):
        file_blob_repo = self.file_blob_repo_class()
        async with file_blob_repo.add_file_blob(file):
            await self.file_record_repo.add_image_file_record(file, file_blob_repo.storage_service_name())
        self.seen.add(file)

    async def get(self, uuid: UUID) -> File:
        file_record_model = await self.file_record_repo.get_file_record(uuid)
        file_type = FileType(file_record_model.content_type, file_record_model.extension)
        file_blob_repo = self.file_blob_repo_class(file_record_model.storage_service_name)
        blob = await file_blob_repo.get_file_blob(file_record_model.path)
        if file_record_model.entity_class_name == EbookFile.__name__:
            ebook_file = EbookFile(
                uuid=file_record_model.uuid,
                file_type=file_type,
                file_size=FileSize(file_record_model.size_bytes),
                blob=blob,
                path=Path(file_record_model.path),
            )
            self.seen.add(ebook_file)
            return ebook_file
        elif file_record_model.entity_class_name == TempFile.__name__:
            temp_file = TempFile(
                uuid=file_record_model.uuid,
                file_type=file_type,
                file_size=FileSize(file_record_model.size_bytes),
                blob=blob,
                path=Path(file_record_model.path),
            )
            self.seen.add(temp_file)
            return temp_file
        elif file_record_model.entity_class_name == File.__name__:
            _file = File(
                uuid=file_record_model.uuid,
                file_type=file_type,
                file_size=FileSize(file_record_model.size_bytes),
                blob=blob,
                path=Path(file_record_model.path),
            )
            self.seen.add(_file)
            return _file
        elif file_record_model.entity_class_name == ImageFile.__name__:
            image_record = await self.file_record_repo.get_image_record_by_file_uuid(uuid)
            image_file = ImageFile(
                uuid=file_record_model.uuid,
                image_uuid=image_record.uuid,
                file_type=file_type,
                file_size=FileSize(file_record_model.size_bytes),
                blob=blob,
                path=Path(file_record_model.path),
                pixel_width=image_record.pixel_width,
                pixel_height=image_record.pixel_height,
                extracted_from=image_record.extracted_from_file_record_uuid,
            )
            self.seen.add(image_file)
            return image_file
        else:
            raise Exception("Unknown file type")

    async def get_by_image_uuid(self, image_record_uuid: UUID) -> "ImageFile":
        image_info = await self.file_record_repo.get_image_info_by_image_record_uuid(image_record_uuid)
        file_blob_repo = self.file_blob_repo_class(image_info.storage_service_name)
        blob = await file_blob_repo.get_file_blob(image_info.path)
        return ImageFile(
            uuid=image_info.file_record_uuid,
            image_uuid=image_info.image_record_uuid,
            file_type=FileType(image_info.content_type, image_info.extension),
            file_size=FileSize(image_info.size_bytes),
            blob=blob,
            path=Path(image_info.path),
            pixel_width=image_info.pixel_width,
            pixel_height=image_info.pixel_height,
            extracted_from=image_info.extracted_from_file_record_uuid,
        )

    async def remove(self, file: "File"):
        return await self._remove(file)

    @singledispatchmethod
    async def _remove(self, file: "File"):
        raise NotImplementedError("file type cannot be removed")

    @_remove.register
    async def _remove_ebookfile(self, file: EbookFile):
        file_record_model = await self.file_record_repo.get_file_record(file.uuid)
        file_blob_repo = self.file_blob_repo_class(file_record_model.storage_service_name)
        await self.file_record_repo.remove_file_record(file.uuid)
        await file_blob_repo.remove_blob(file_record_model.path)
        self.seen.add(file)

    @_remove.register
    async def _remove_tempfile(self, file: TempFile):
        file_record_model = await self.file_record_repo.get_file_record(file.uuid)
        file_blob_repo = self.file_blob_repo_class(file_record_model.storage_service_name)
        await self.file_record_repo.remove_file_record(file.uuid)
        await file_blob_repo.remove_blob(file_record_model.path)
        self.seen.add(file)

    @_remove.register
    async def _remove_imagefile(self, file: ImageFile):
        file_record_model = await self.file_record_repo.get_file_record(file.uuid)
        file_blob_repo = self.file_blob_repo_class(file_record_model.storage_service_name)
        await self.file_record_repo.remove_image_record(file.uuid)
        await file_blob_repo.remove_blob(file_record_model.path)
