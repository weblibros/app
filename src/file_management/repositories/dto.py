from dataclasses import dataclass
from pathlib import Path
from typing import Optional
from uuid import UUID


@dataclass
class PathServiceNameDTO:
    path: str
    service_name: str


@dataclass
class FileServiceDTO:
    path: str
    service_name: str
    media_type: str


@dataclass
class ImageFileRecordDTO:
    file_record_uuid: UUID
    image_record_uuid: UUID
    path: Path
    size_bytes: int
    extension: str
    content_type: str
    storage_service_name: str
    pixel_height: int
    pixel_width: int
    extracted_from_file_record_uuid: Optional[UUID]
