from uuid import UUID

from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

from src.file_management.repositories.models import FileRecord, ImageRecord


async def remove_file_record(
    session: AsyncSession,
    file_record_uuid: UUID,
):
    query_remove = delete(FileRecord).filter_by(uuid=file_record_uuid)
    await session.execute(query_remove)


async def remove_image_record(
    session: AsyncSession,
    file_record_uuid: UUID,
):
    query_remove_file_record = delete(FileRecord).filter_by(uuid=file_record_uuid)
    query_remove_image_record = delete(ImageRecord).filter_by(file_record_uuid=file_record_uuid)

    await session.execute(query_remove_image_record)
    await session.execute(query_remove_file_record)
