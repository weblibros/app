from typing import TYPE_CHECKING

from sqlalchemy.ext.asyncio import AsyncSession

from src.file_management.repositories.models import FileRecord, ImageRecord

if TYPE_CHECKING:
    from src.file_management.domain import File, ImageFile


async def create_file_record_v2(session: AsyncSession, file: "File", storage_service_name: str) -> FileRecord:
    file_record_kwargs = {
        "uuid": file.uuid,
        "path": str(file.path),
        "size_bytes": file.file_size.size_bytes,
        "extension": file.file_type.extension,
        "content_type": file.content_type,
        "entity_class_name": file._entity_class_name,
        "storage_service_name": storage_service_name,
    }
    file_record = FileRecord(**file_record_kwargs)  # type: ignore
    try:
        session.add(file_record)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise e

    return file_record


async def create_image_record_v2(
    session: AsyncSession, image_file: "ImageFile", storage_service_name: str
) -> ImageRecord:
    file_record_kwargs = {
        "uuid": image_file.uuid,
        "path": str(image_file.path),
        "size_bytes": image_file.file_size.size_bytes,
        "extension": image_file.file_type.extension,
        "content_type": image_file.content_type,
        "entity_class_name": image_file._entity_class_name,
        "storage_service_name": storage_service_name,
    }
    image_record_kwargs = {
        "uuid": image_file.image_uuid,
        "extracted_from_file_record_uuid": image_file.extracted_from,
        "pixel_height": image_file.pixel_height,
        "pixel_width": image_file.pixel_width,
    }
    file_record = FileRecord(**file_record_kwargs)  # type: ignore
    image_record = ImageRecord(**image_record_kwargs)  # type: ignore
    image_record.file_record = file_record
    try:
        session.add(image_record)
        await session.flush()
    except Exception as e:
        await session.rollback()
        raise e

    return image_record
