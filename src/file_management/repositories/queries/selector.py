from pathlib import Path
from uuid import UUID

from sqlalchemy import exists, select
from sqlalchemy.ext.asyncio import AsyncSession

from src.books.repositories.models import CoverModel
from src.file_management.repositories.dto import FileServiceDTO, ImageFileRecordDTO, PathServiceNameDTO
from src.file_management.repositories.models import FileRecord, ImageRecord
from src.libraries.repositories.models import LibraryBook


async def find_file_record_by_kwargs(session: AsyncSession, **kwargs) -> FileRecord | None:
    query = select(FileRecord).filter_by(**kwargs)
    result = await session.execute(query)
    file_record = result.scalar_one_or_none()
    return file_record


async def find_file_record_by_uuid(session: AsyncSession, uuid: UUID) -> FileRecord | None:
    return await find_file_record_by_kwargs(session, uuid=uuid)


async def find_file_record_by_image_record(session: AsyncSession, image_record_uuid: UUID) -> FileRecord | None:
    query = (
        select(FileRecord)
        .join(ImageRecord, ImageRecord.file_record_uuid == FileRecord.uuid)
        .where(ImageRecord.uuid == image_record_uuid)
    )
    result = await session.execute(query)
    file_record = result.scalar_one_or_none()
    return file_record


async def find_image_record_by_file_uuid(session: AsyncSession, file_record_uuid: UUID) -> ImageRecord | None:
    query = select(ImageRecord).filter_by(file_record_uuid=file_record_uuid)
    result = await session.execute(query)
    image_record = result.scalar_one_or_none()
    return image_record


async def find_image_info_by_image_record_uuid(
    session: AsyncSession, image_record_uuid: UUID
) -> ImageFileRecordDTO | None:
    query = (
        select(
            FileRecord.uuid.label("file_record_uuid"),
            ImageRecord.uuid.label("image_record_uuid"),
            FileRecord.path,
            FileRecord.size_bytes,
            FileRecord.extension,
            FileRecord.content_type,
            FileRecord.storage_service_name,
            ImageRecord.pixel_height,
            ImageRecord.pixel_width,
            ImageRecord.extracted_from_file_record_uuid,
        )
        .join(ImageRecord, ImageRecord.file_record_uuid == FileRecord.uuid)
        .where(ImageRecord.uuid == image_record_uuid)
    )
    result = (await session.execute(query)).all()
    if len(result) == 1:
        return ImageFileRecordDTO(
            file_record_uuid=result[0][0],
            image_record_uuid=result[0][1],
            path=Path(result[0][2]),
            size_bytes=result[0][3],
            extension=result[0][4],
            content_type=result[0][5],
            storage_service_name=result[0][6],
            pixel_height=result[0][7],
            pixel_width=result[0][8],
            extracted_from_file_record_uuid=result[0][9],
        )
    else:
        return None


async def get_path_and_service_name_by_image_uuid(
    session: AsyncSession, image_record_uuid
) -> PathServiceNameDTO | None:
    query = (
        select(
            FileRecord.path,
            FileRecord.storage_service_name,
        )
        .join(ImageRecord, ImageRecord.file_record_uuid == FileRecord.uuid)
        .where(ImageRecord.uuid == image_record_uuid)
    )
    result = (await session.execute(query)).all()
    if len(result) == 1:
        return PathServiceNameDTO(path=result[0][0], service_name=result[0][1])
    else:
        return None


async def get_service_dto_by_file_uuid(session: AsyncSession, file_record_uuid: UUID) -> FileServiceDTO | None:
    query = select(
        FileRecord.path,
        FileRecord.storage_service_name,
        FileRecord.content_type,
    ).where(FileRecord.uuid == file_record_uuid)
    result = (await session.execute(query)).all()
    if len(result) == 1:
        return FileServiceDTO(path=result[0][0], service_name=result[0][1], media_type=result[0][2])
    else:
        return None


async def image_is_cover_in_libraries(session: AsyncSession, image_record_uuid: UUID, libraries: list[UUID]) -> bool:
    query_cover_exists_in_libraries = (
        select(CoverModel.uuid)
        .join_from(
            LibraryBook,
            CoverModel,
            LibraryBook.book_uuid == CoverModel.book_uuid,
        )
        .where(
            LibraryBook.library_uuid.in_(libraries),
            CoverModel.image_uuid == image_record_uuid,
        )
    )
    return await session.scalar(exists(query_cover_exists_in_libraries).select()) is True
