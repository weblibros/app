from uuid import uuid4

import pytest

from src.config.database import async_session_maker
from src.file_management.repositories.models import FileRecord, ImageRecord
from src.file_management.repositories.queries.selector import find_file_record_by_image_record, find_file_record_by_uuid


@pytest.fixture()
async def one_file_record_with_session():
    async with async_session_maker() as session:
        file_record = FileRecord(
            path="fake/dummy.epub",
            size_bytes=23,
            extension="epub",
            content_type="application/epub+zip",
            storage_service_name="FakeService",
            entity_class_name="File",
        )
        session.add(file_record)
        await session.flush()
        file_record_uuid = file_record.uuid
        session.expire_all()
        yield file_record_uuid, session
        await session.rollback()


class TestFileRecordSelector:
    async def test_get_file_record_by_uuid(self, one_file_record_with_session):
        one_file_record_uuid, session = one_file_record_with_session
        file_record = await find_file_record_by_uuid(session, one_file_record_uuid)
        assert isinstance(file_record, FileRecord)
        assert file_record.uuid == one_file_record_uuid

    async def test_get_no_file_record_by_uuid(self):
        async with async_session_maker() as session:
            file_record = await find_file_record_by_uuid(session, uuid4())
        assert file_record is None


@pytest.fixture()
async def one_image_record_with_session():
    async with async_session_maker() as session:
        file_record = FileRecord(
            uuid=uuid4(),
            path="fake/dummy.epub",
            size_bytes=23,
            extension="epub",
            content_type="application/epub+zip",
            storage_service_name="FakeService",
            entity_class_name="ImageFile",
        )
        image = ImageRecord(
            uuid=uuid4(),
            file_record=file_record,
            pixel_height=200,
            pixel_width=200,
        )
        session.add_all([file_record, image])
        await session.flush()
        image_record_uuid = image.uuid
        file_record_uuid = file_record.uuid
        session.expire_all()
        yield file_record_uuid, image_record_uuid, session
        await session.rollback()


class TestFileRecordByImageSelector:
    async def test_get_file_record_by_image_uuid(self, one_image_record_with_session):
        file_record_uuid, image_record_uuid, session = one_image_record_with_session
        file_record = await find_file_record_by_image_record(session, image_record_uuid)
        assert isinstance(file_record, FileRecord)
        assert file_record.uuid == file_record_uuid

    async def test_get_no_file_record_by_uuid(self):
        async with async_session_maker() as session:
            file_record = await find_file_record_by_image_record(session, uuid4())
        assert file_record is None
