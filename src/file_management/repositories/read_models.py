from functools import partial
from typing import TYPE_CHECKING, AsyncIterable, Optional
from uuid import UUID

from src.config.database import async_session_manager
from src.file_management.repositories.queries import selector

from .blob_repo.services import storage_service_factory

if TYPE_CHECKING:
    from .dto import FileServiceDTO


class FileReadModel:
    storage_service_factory = partial(storage_service_factory)
    async_session_manager = partial(async_session_manager)

    def __init__(self):
        self.__service_dto: Optional["FileServiceDTO"] = None

    async def get_file_iterator(self, file_uuid: UUID) -> AsyncIterable[bytes]:
        path_service_name_dto = await self._get_service_dto(file_uuid)
        storage_service = self.storage_service_factory(path_service_name_dto.service_name)
        return storage_service.file_iterator(path_service_name_dto.path)

    async def get_media_type(self, file_uuid: UUID) -> str:
        service_dto = await self._get_service_dto(file_uuid)
        return service_dto.media_type

    async def _get_service_dto(self, file_uuid) -> "FileServiceDTO":
        if self.__service_dto:
            return self.__service_dto
        else:
            async with self.async_session_manager() as session:
                service_dto = await selector.get_service_dto_by_file_uuid(session, file_uuid)
            if service_dto:
                self.__service_dto = service_dto
                return service_dto
            else:
                raise Exception("File record not found")
