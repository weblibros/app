import os
from dataclasses import dataclass
from pathlib import Path
from typing import TYPE_CHECKING, Self

if TYPE_CHECKING:
    from fastapi import UploadFile


class FileSizeExtractionException(Exception):
    message = "Could not extract byte size from upload_file"


def _extract_byte_size(upload_file: "UploadFile") -> int:
    size_bytes = upload_file.size
    if size_bytes is None:
        raise FileSizeExtractionException()

    return size_bytes


@dataclass
class FileSize:
    size_bytes: int

    @classmethod
    async def from_upload_file(cls, upload_file: "UploadFile") -> Self:
        size_bytes = _extract_byte_size(upload_file)
        return cls(size_bytes)

    @classmethod
    async def from_path(cls, path: Path) -> Self:
        return cls(os.path.getsize(path))
