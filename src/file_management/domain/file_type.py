import subprocess
from pathlib import Path
from typing import TYPE_CHECKING, Self

from src.file_management import constants

if TYPE_CHECKING:
    from fastapi import UploadFile


class FileTypeAnalyser:
    def from_path(self, file_path: str | Path) -> str:
        file_path = str(file_path)
        exception_message = f"could not read filetype of {file_path}"
        command_list = ["file", file_path, "--mime-type"]
        process = subprocess.run(command_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        if process.returncode != 0:
            raise Exception(exception_message)
        result = process.stdout
        if "cannot open" in result:
            raise Exception(exception_message)

        return self.process_result(result)

    def process_result(self, result: str) -> str:
        return result.split(":", maxsplit=1)[1].strip()

    async def from_fileupload(self, upload_file: "UploadFile") -> str:
        command_list = ["file", "-", "--mime-type"]
        input_bytes = await self._bytes_from_upload_file(upload_file)
        process = subprocess.run(
            command_list,
            input=input_bytes,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=False,
        )
        if process.returncode != 0:
            raise Exception("error at reading filetype of binary")
        result = process.stdout.decode("utf-8")

        return self.process_result(result)

    async def _bytes_from_upload_file(self, upload_file: "UploadFile") -> bytes:
        await upload_file.seek(0)
        return await upload_file.read()


class FileType:
    mime_type: str
    extension: str

    def __init__(self, mime_type: str, extension: str):
        self.mime_type = mime_type
        self.extension = extension

    @classmethod
    async def from_upload_file(cls, upload_file: "UploadFile") -> Self:
        if upload_file.filename is None:
            raise Exception("upload_file is missing the filename")
        else:
            mime_type = await FileTypeAnalyser().from_fileupload(upload_file)
            extension = Path(upload_file.filename).suffix
            return cls(mime_type, extension)

    @classmethod
    async def from_path(cls, path: str | Path) -> Self:
        if isinstance(path, str):
            path = Path(path)
        mime_type = FileTypeAnalyser().from_path(path)
        extension = path.suffix
        return cls(mime_type, extension)

    @property
    def is_supported(self) -> bool:
        return self.mime_type in constants.VALID_FILE_TYPES

    @property
    def is_ebook(self) -> bool:
        return self.mime_type in constants.VALID_EBOOK_FILE_TYPES

    @property
    def is_image(self) -> bool:
        return self.mime_type in constants.VALID_IMAGE_FILE_TYPES

    @property
    def is_valid(self):
        return constants.EXTENSION_MIME_TYPE_MAP.get(self.extension, None) == self.mime_type
