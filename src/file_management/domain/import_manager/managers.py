from datetime import datetime
from enum import StrEnum, auto
from pathlib import Path
from typing import TYPE_CHECKING, Optional, Sequence, Type
from uuid import UUID, uuid4

from src.config import settings

from ..events import EbookFileImported, ImportManagerFailedToImport, ProcessedFailedImportStep, ProcessedImportStep
from .steps import (
    ExtractCoverFromEbookStep,
    FileIsEbookStep,
    LoadFileContextIntoMemoryStep,
    RetrieveEbookMetaDataStep,
    TransformTempToEbookStep,
)

if TYPE_CHECKING:
    from src.events import Event
    from src.file_management.domain import File

    from .steps import ImportStep


class ImportFrom(StrEnum):
    UNKNOWN = auto()
    WEB_INTERFACE = auto()
    CLI = auto()


class ImportManager:
    _step_classes: Sequence[Type["ImportStep"]]

    class ImportStepIterator:
        def __init__(self, steps: Sequence["ImportStep"]):
            self.__steps = steps
            self.__iter_position = -1

        def __len__(self):
            return len(self.__steps)

        def __iter__(self):
            self.__iter_position = -1
            return self

        def __next__(self):
            if self.__iter_position == -1 and len(self.__steps) > 0:
                self.__iter_position += 1
                return self.__steps[self.__iter_position]
            elif self.__iter_position == len(self.__steps) - 1:
                raise StopIteration()
            else:
                prev_step = self.__steps[self.__iter_position]
                if self.__step_processed_successfully(prev_step):
                    self.__iter_position += 1
                    return self.__steps[self.__iter_position]
                else:
                    raise StopIteration()

        def __step_processed_successfully(self, step: "ImportStep") -> bool:
            return step.successfully_completed

    def __init__(
        self,
        temp_file: "File",
        library_uuid: UUID,
        uploaded_by_user_uuid: UUID,
        imported_from: Optional[ImportFrom] = None,
    ):
        self.uuid = uuid4()
        self._temp_file = temp_file
        self._book_uuid = uuid4()
        self._library_uuid = library_uuid
        self._uploaded_by_user_uuid = uploaded_by_user_uuid
        self._base_dir = self._create_ebook_dir(
            library_uuid=self._library_uuid,
            book_uuid=self._book_uuid,
        )
        self._steps = self.ImportStepIterator(
            [
                step_class(
                    uuid=uuid4(),
                    library_uuid=self._library_uuid,
                    book_uuid=self._book_uuid,
                    base_dir=self._base_dir,
                    temp_file=self._temp_file,
                )
                for step_class in self._step_classes
            ]
        )
        self.events: Sequence["Event"] = []
        self._processed_steps: list[ProcessedImportStep] = []
        self._started_at: Optional[datetime] = None
        self._finished_at: Optional[datetime] = None
        self.successfully_completed = False
        self._generated_files: list["File"] = []
        self._imported_from: ImportFrom = imported_from or ImportFrom.UNKNOWN

    @property
    def steps(self) -> ImportStepIterator:
        return self._steps

    async def process(self):
        started_at = datetime.utcnow()
        for import_step in self.steps:
            processed_step = await import_step.process()
            self._generated_files += import_step.generated_files
            self._processed_steps.append(processed_step)
        finished_at = datetime.utcnow()

        self._started_at = started_at
        self._finished_at = finished_at

        if all([not isinstance(step, ProcessedFailedImportStep) for step in self._processed_steps]):
            self.events.append(
                EbookFileImported(
                    user_uuid=self._uploaded_by_user_uuid,
                    library_uuid=self._library_uuid,
                    book_uuid=self._book_uuid,
                    steps_processed=self._processed_steps,
                    started_at=started_at,
                    finished_at=finished_at,
                )
            )
            self.successfully_completed = True
        else:
            self.events.append(
                ImportManagerFailedToImport(
                    error_message=next(
                        filter(lambda x: isinstance(x, ProcessedFailedImportStep), self._processed_steps)
                    ).result.error_message,
                    user_uuid=self._uploaded_by_user_uuid,
                    library_uuid=self._library_uuid,
                    book_uuid=self._book_uuid,
                    steps_processed=self._processed_steps,
                    started_at=self._started_at,
                    finished_at=self._finished_at,
                )
            )

    @property
    def original_file(self) -> "File":
        return self._temp_file

    @property
    def book_uuid(self) -> UUID:
        return self._book_uuid

    @property
    def library_uuid(self) -> UUID:
        return self._library_uuid

    @property
    def uploaded_by_user_uuid(self) -> UUID:
        return self._uploaded_by_user_uuid

    @property
    def started_at(self) -> datetime:
        if self._started_at:
            return self._started_at
        else:
            raise ValueError("Import manager has not been processed yet")

    @property
    def finished_at(self) -> datetime:
        if self._finished_at:
            return self._finished_at
        else:
            raise ValueError("Import manager has not been processed yet")

    @property
    def generated_files(self) -> list["File"]:
        return self._generated_files

    @property
    def imported_from(self) -> ImportFrom:
        return self._imported_from

    def _create_ebook_dir(self, library_uuid: UUID, book_uuid: UUID) -> Path:
        return Path(settings.STORAGE_BASE_DIR).joinpath(f"lib-{library_uuid}", f"ebook-{book_uuid}")


class EbookImportManager(ImportManager):
    _step_classes = [
        LoadFileContextIntoMemoryStep,
        FileIsEbookStep,
        RetrieveEbookMetaDataStep,
        ExtractCoverFromEbookStep,
        TransformTempToEbookStep,
    ]
