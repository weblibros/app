from abc import abstractmethod
from datetime import datetime
from pathlib import Path
from typing import TYPE_CHECKING, Optional, Sequence
from uuid import UUID, uuid4

from src.file_management.services.extract_cover_from_ebook.service import CoverExtractorService
from src.file_management.services.extract_meta_data_ebook.service import MetaDataExtractorService
from src.file_management.services.file_is_ebook.service import IsEbookFileService
from src.file_management.services.transform_temp_file_to_ebook_file.service import TransformTempToEbookService

from ..events import (
    ExtractCoverFromEbookStepResult,
    FailedImportStepResult,
    FileContextLoadedInMemoryResult,
    FileIsEbookStepResult,
    ProcessedExtractCoverFromEbookStep,
    ProcessedFailedImportStep,
    ProcessedFileContextLoadedInMemoryStep,
    ProcessedFileIsEbookStep,
    ProcessedImportStep,
    ProcessedImportStepResult,
    ProcessedRetrieveEbookMetaDataStep,
    ProcessedTransformTempToEbookStep,
    RetrieveEbookMetaDataStepResult,
    TransformTempToEbookStepResult,
)

if TYPE_CHECKING:
    from src.file_management.domain import File


class ImportStep:
    process_event_class: type[ProcessedImportStep] = ProcessedImportStep
    _version: str = "v1"

    def __init__(
        self, book_uuid: UUID, library_uuid: UUID, base_dir: Path, temp_file: "File", uuid: Optional[UUID] = None
    ):
        self.uuid = uuid or uuid4()
        self._book_uuid = book_uuid
        self._library_uuid = library_uuid
        self._base_dir = base_dir
        self.temp_file = temp_file
        self._started_at: Optional[datetime] = None
        self._finished_at: Optional[datetime] = None
        self._error_message: str = ""
        self._events: list[ProcessedImportStep] = []
        self._generated_files: list["File"] = []
        self._processed_result: Optional[ProcessedImportStep] = None

    @property
    def step_name(self) -> str:
        return self.__class__.__name__

    async def process(self) -> ProcessedImportStep:
        if self._processed_result:
            return self._processed_result
        else:
            processed_import_step = await self._start_processing()
            self._events.append(processed_import_step)
            self._processed_result = processed_import_step
            return processed_import_step

    async def _start_processing(self) -> ProcessedImportStep:
        started_at = datetime.utcnow()
        try:
            import_result = await self._process()
        except Exception as e:
            self._error_message = str(e)
        finally:
            finished_at = datetime.utcnow()

            self._started_at = started_at
            self._finished_at = finished_at

        if not self._error_message:
            processed_import_step = self.process_event_class(
                uuid=self.uuid,
                import_step_name=self.step_name,
                started_at=started_at,
                finished_at=finished_at,
                result=import_result,
            )
        else:
            processed_import_step = ProcessedFailedImportStep(
                uuid=self.uuid,
                import_step_name=self.step_name,
                started_at=started_at,
                finished_at=finished_at,
                result=FailedImportStepResult(error_message=self._error_message),
            )
        return processed_import_step

    @abstractmethod
    async def _process(self) -> ProcessedImportStepResult: ...

    @property
    def successfully_completed(self) -> bool:
        if self._events:
            return all([not isinstance(event, ProcessedFailedImportStep) for event in self._events])
        else:
            return False

    @property
    def generated_files(self) -> list["File"]:
        return self._generated_files

    @property
    def error_message(self) -> str:
        return self._error_message

    @property
    def started_at(self) -> datetime:
        if self._started_at:
            return self._started_at
        else:
            raise ValueError("Import step has not been processed yet")

    @property
    def finished_at(self) -> datetime:
        if self._finished_at:
            return self._finished_at
        else:
            raise ValueError("Import step has not been processed yet")

    @property
    def events(self) -> Sequence[ProcessedImportStep]:
        return self._events

    @property
    def processed_result(self) -> ProcessedImportStep:
        if self._processed_result:
            return self._processed_result
        else:
            raise ValueError("Import step has not been processed yet")


class LoadFileContextIntoMemoryStep(ImportStep):
    process_event_class = ProcessedFileContextLoadedInMemoryStep

    async def _process(self) -> FileContextLoadedInMemoryResult:
        await self.temp_file.blob.load_into_memory()
        return FileContextLoadedInMemoryResult(
            file_uuid=self.temp_file.uuid,
            loaded=True,
        )


class FileIsEbookStep(ImportStep):
    process_event_class = ProcessedFileIsEbookStep
    service = IsEbookFileService()

    async def _process(self) -> FileIsEbookStepResult:
        is_ebook = await self.service.is_ebook(self.temp_file)
        if is_ebook:
            return FileIsEbookStepResult(
                file_uuid=self.temp_file.uuid,
                is_ebook=is_ebook,
            )
        else:
            raise Exception(f"file :{self.temp_file.uuid} is not an ebook")


class RetrieveEbookMetaDataStep(ImportStep):
    process_event_class = ProcessedRetrieveEbookMetaDataStep
    service = MetaDataExtractorService()

    async def _process(self) -> RetrieveEbookMetaDataStepResult:
        metadata = await self.service.extract(self.temp_file)
        return RetrieveEbookMetaDataStepResult(
            book_uuid=self._book_uuid,
            data=metadata,
        )


class ExtractCoverFromEbookStep(ImportStep):
    process_event_class = ProcessedExtractCoverFromEbookStep
    service = CoverExtractorService()

    async def _process(self) -> ExtractCoverFromEbookStepResult:
        image = await self.service.extract(self.temp_file, self._base_dir)
        self._generated_files.append(image)
        return ExtractCoverFromEbookStepResult(
            book_uuid=self._book_uuid,
            image_uuid=image.image_uuid,
            image_path=image.path,
            file_uuid=image.uuid,
        )


class TransformTempToEbookStep(ImportStep):
    process_event_class = ProcessedTransformTempToEbookStep
    service = TransformTempToEbookService()

    async def _process(self) -> TransformTempToEbookStepResult:
        ebook_file = await self.service.transform(
            temp_file=self.temp_file, book_uuid=self._book_uuid, base_dir=self._base_dir
        )
        self._generated_files.append(ebook_file)
        return TransformTempToEbookStepResult(
            temp_file_uuid=self.temp_file.uuid,
            book_file_uuid=ebook_file.uuid,
            book_uuid=self._book_uuid,
            href=ebook_file.href,
            mime_type=ebook_file.file_type.mime_type,
        )
