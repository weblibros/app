import io
from pathlib import Path

import pytest
from fastapi import UploadFile

from ..file_size import FileSize


class TestFileFileSize:
    pytest_parameters = (
        "file_path, expected_file_size",
        [
            (
                "src/file_management/tests/files/random_image.png",
                4771,
            ),
            (
                "src/file_management/tests/files/random_image.jpg",
                15610,
            ),
            (
                "src/file_management/tests/files/dummy-ebook.epub",
                30078,
            ),
        ],
    )

    @pytest.mark.parametrize(*pytest_parameters)
    async def test_file_size_from_path(self, file_path, expected_file_size):
        assert expected_file_size == (await FileSize.from_path(Path(file_path))).size_bytes

    @pytest.mark.parametrize(*pytest_parameters)
    async def test_file_size_from_upload_file(self, file_path, expected_file_size):
        with open(file_path, "rb") as file:
            file_io_bytes = io.BytesIO(file.read())
            file_upload = UploadFile(
                file=file_io_bytes,
                size=len(file_io_bytes.getvalue()),
                filename="Fake",
            )
        assert expected_file_size == (await FileSize.from_upload_file(file_upload)).size_bytes
