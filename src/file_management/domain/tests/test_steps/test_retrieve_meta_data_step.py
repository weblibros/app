from pathlib import Path
from uuid import uuid4

import pytest

from src.file_management.domain import FileBlobLocal, FileSize, FileType, TempFile
from src.file_management.domain.events import EbookMetaData
from src.file_management.domain.import_manager.steps import (
    ProcessedImportStep,
    RetrieveEbookMetaDataStep,
    RetrieveEbookMetaDataStepResult,
)


class TestRetrieveMetaDataBook:
    dummy_file_path = Path("src/file_management/tests/files/dummy-ebook.epub")
    step_class = RetrieveEbookMetaDataStep

    @pytest.fixture()
    async def temp_file(self):
        return TempFile(
            file_type=(await FileType.from_path(self.dummy_file_path)),
            file_size=(await FileSize.from_path(self.dummy_file_path)),
            blob=FileBlobLocal(self.dummy_file_path),
            path=Path("/tmp/dummy-ebook.epub"),
        )

    async def test_metadata_is_retrieved(self, temp_file):
        book_uuid = uuid4()

        step = self.step_class(
            book_uuid=book_uuid,
            library_uuid=uuid4(),
            base_dir=Path("/tmp/test-step-extract-metadata/"),
            temp_file=temp_file,
        )
        result = await step.process()
        assert isinstance(result, ProcessedImportStep)
        assert result.uuid == step.uuid
        assert isinstance(result.result, RetrieveEbookMetaDataStepResult)
        assert result.result.book_uuid == book_uuid
        assert isinstance(result.result.data, EbookMetaData)
