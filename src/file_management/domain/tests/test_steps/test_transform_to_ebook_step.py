import os
from pathlib import Path
from uuid import uuid4

import pytest

from src.file_management.domain import EbookFile, FileBlobLocal, FileSize, FileType, TempFile
from src.file_management.domain.import_manager.steps import (
    ProcessedImportStep,
    TransformTempToEbookStep,
    TransformTempToEbookStepResult,
)


class TestTransformTempToEbookStepStep:
    dummy_file_path = Path("src/file_management/tests/files/dummy-ebook.epub")
    step_class = TransformTempToEbookStep

    @pytest.fixture()
    async def temp_file(self):
        return TempFile(
            file_type=(await FileType.from_path(self.dummy_file_path)),
            file_size=(await FileSize.from_path(self.dummy_file_path)),
            blob=FileBlobLocal(self.dummy_file_path),
            path=Path("/tmp/dummy-ebook.epub"),
        )

    async def test_transform_to_ebook(self, temp_file):
        book_uuid = uuid4()
        base_dir = Path("/tmp/extract-cover/")

        step = self.step_class(
            book_uuid=book_uuid,
            library_uuid=uuid4(),
            base_dir=base_dir,
            temp_file=temp_file,
        )
        result = await step.process()
        assert isinstance(result, ProcessedImportStep)
        assert result.uuid == step.uuid
        assert isinstance(result.result, TransformTempToEbookStepResult)
        assert len(step.generated_files) == 1
        ebook_file = step.generated_files[0]
        assert isinstance(ebook_file, EbookFile)

        assert result.result.book_uuid == book_uuid
        assert result.result.book_file_uuid == ebook_file.uuid
        assert result.result.temp_file_uuid == temp_file.uuid
        assert result.result.href == ebook_file.href
        assert result.result.href != temp_file.href
        assert result.result.mime_type == ebook_file.file_type.mime_type

        assert str(base_dir) == os.path.dirname(ebook_file.path)
        assert ebook_file.uuid != temp_file.uuid
        assert ebook_file.blob != temp_file.blob
        assert ebook_file.path != temp_file.path
        assert ebook_file.file_type == temp_file.file_type
        assert ebook_file.file_size == temp_file.file_size
        assert await ebook_file.blob.content() == await temp_file.blob.content()
