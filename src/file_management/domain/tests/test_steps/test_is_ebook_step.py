from pathlib import Path
from uuid import uuid4

import pytest

from src.file_management.domain import FileBlobLocal, FileSize, FileType, TempFile
from src.file_management.domain.events import (
    FailedImportStepResult,
    FileIsEbookStepResult,
    ProcessedFailedImportStep,
    ProcessedFileIsEbookStep,
    ProcessedImportStep,
)
from src.file_management.domain.import_manager.steps import FileIsEbookStep


class TestIsEbookFileStep:
    dummy_file_path = Path("src/file_management/tests/files/dummy-ebook.epub")
    dummy_image_file_path = Path("src/file_management/tests/files/random_image.jpg")
    step_class = FileIsEbookStep

    @pytest.fixture()
    async def temp_ebook_file(self):
        return TempFile(
            file_type=(await FileType.from_path(self.dummy_file_path)),
            file_size=(await FileSize.from_path(self.dummy_file_path)),
            blob=FileBlobLocal(self.dummy_file_path),
            path=Path("/tmp/dummy-ebook.epub"),
        )

    @pytest.fixture()
    async def temp_image_file(self):
        return TempFile(
            file_type=(await FileType.from_path(self.dummy_image_file_path)),
            file_size=(await FileSize.from_path(self.dummy_image_file_path)),
            blob=FileBlobLocal(self.dummy_image_file_path),
            path=Path("/tmp/random_image.jpg"),
        )

    async def test_return_is_ebook_when_correct_temp_file_is_given(self, temp_ebook_file):
        book_uuid = uuid4()

        step = self.step_class(
            book_uuid=book_uuid,
            library_uuid=uuid4(),
            base_dir=Path("/tmp/test-step-is-book/"),
            temp_file=temp_ebook_file,
        )
        result = await step.process()
        assert isinstance(result, ProcessedImportStep)
        assert not isinstance(result, ProcessedFailedImportStep)
        assert isinstance(result, ProcessedFileIsEbookStep)
        assert result.uuid == step.uuid
        assert isinstance(result.result, FileIsEbookStepResult)
        assert result.result.is_ebook is True

    async def test_fails_when_incorrect_temp_file_is_given(self, temp_image_file):
        book_uuid = uuid4()

        step = self.step_class(
            book_uuid=book_uuid,
            library_uuid=uuid4(),
            base_dir=Path("/tmp/test-step-is-book/"),
            temp_file=temp_image_file,
        )
        result = await step.process()
        assert isinstance(result, ProcessedImportStep)
        assert isinstance(result, ProcessedFailedImportStep)
        assert result.uuid == step.uuid
        assert isinstance(result.result, FailedImportStepResult)
        assert result.result.error_message == f"file :{temp_image_file.uuid} is not an ebook"
