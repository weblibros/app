from pathlib import Path

from src.file_management.domain.aggregate import FileBlobLocal


class TestFileBlobLocalFile:
    file_path = "src/file_management/tests/files/dummy-ebook.epub"

    async def test_can_read_content(self):
        file_blob = FileBlobLocal(Path(self.file_path))

        with open(self.file_path, "rb") as local_file:
            content = local_file.read()

        assert content == (await file_blob.content())
