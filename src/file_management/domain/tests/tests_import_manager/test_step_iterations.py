from uuid import uuid4

import pytest

from src.file_management.domain.events import (
    FailedImportStepResult,
    ProcessedFailedImportStep,
    ProcessedImportStepResult,
)
from src.file_management.domain.import_manager import ImportManager, ImportStep
from src.file_management.domain.tests.fake import fake_temp_file_factory


class FakeProcessedImportStepResult(ProcessedImportStepResult): ...


class FakeSuccessImportStep(ImportStep):
    async def _process(self):
        return FakeProcessedImportStepResult()


class FakeFailImportStep(ImportStep):
    process_event_class = ProcessedFailedImportStep

    async def _process(self):
        self._error_message = "fake error"
        return FailedImportStepResult(self._error_message)


class TestImportManagerWithSuccess:
    class FakeImportManagerCompleteWithSucces(ImportManager):
        _step_classes = [
            FakeSuccessImportStep,
            FakeSuccessImportStep,
        ]

    @pytest.fixture()
    def import_manager(self):
        return self.FakeImportManagerCompleteWithSucces(
            temp_file=fake_temp_file_factory(),
            uploaded_by_user_uuid=uuid4(),
            library_uuid=uuid4(),
        )

    def test_import_managers_has_two_steps(self, import_manager):
        steps = import_manager.steps
        assert len(steps) == 2

    async def test_process_steps(self, import_manager):
        results = []
        for step in import_manager.steps:
            result = await step.process()
            results.append(result)

        assert all([not isinstance(result, ProcessedFailedImportStep) for result in results]) is True
        assert len(results) == 2

    async def test_calling_iterate_twice_sets_the_iterator_to_the_beginning(self, import_manager):
        results = []
        for step in import_manager.steps:
            result = await step.process()
            results.append(result)

        assert all([not isinstance(result, ProcessedFailedImportStep) for result in results]) is True
        assert len(results) == 2

        second_results = []
        for step in import_manager.steps:
            result = await step.process()
            second_results.append(result)

        assert all([not isinstance(result, ProcessedFailedImportStep) for result in second_results]) is True
        assert len(second_results) == 2


class TestImportManagerWithfailedStep:
    class FakeImportManagerCompleteWithStepFailure(ImportManager):
        _step_classes = [
            FakeSuccessImportStep,
            FakeFailImportStep,
            FakeSuccessImportStep,
        ]

    @pytest.fixture()
    def import_manager(self):
        return self.FakeImportManagerCompleteWithStepFailure(
            temp_file=fake_temp_file_factory(),
            uploaded_by_user_uuid=uuid4(),
            library_uuid=uuid4(),
        )

    def test_import_managers_has_two_steps(self, import_manager):
        steps = import_manager.steps
        assert len(steps) == 3

    async def test_process_steps(self, import_manager):
        results = []
        for step in import_manager.steps:
            result = await step.process()
            results.append(result)

        assert [isinstance(result, ProcessedFailedImportStep) for result in results] == [False, True]
        assert len(results) == 2

    async def test_calling_iterate_twice_sets_the_iterator_to_the_beginning(self, import_manager):
        results = []
        assert len(import_manager.steps) == 3
        for step in import_manager.steps:
            result = await step.process()
            results.append(result)

        assert [isinstance(result, ProcessedFailedImportStep) for result in results] == [False, True]
        assert len(results) == 2

        second_results = []
        assert len(import_manager.steps) == 3
        for step in import_manager.steps:
            result = await step.process()
            second_results.append(result)

        assert [isinstance(result, ProcessedFailedImportStep) for result in results] == [False, True]
        assert len(second_results) == 2
