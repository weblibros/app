from uuid import uuid4

from src.file_management.domain.events import (
    ProcessedImportStepResult,
)
from src.file_management.domain.import_manager import ImportManager, ImportStep
from src.file_management.domain.import_manager.managers import ImportFrom
from src.file_management.domain.tests.fake import fake_temp_file_factory


class FakeProcessedImportStepResult(ProcessedImportStepResult): ...


class FakeSuccessImportStep(ImportStep):
    async def _process(self):
        return FakeProcessedImportStepResult()


class FakeImportManagerCompleteWithSucces(ImportManager):
    _step_classes = [
        FakeSuccessImportStep,
        FakeSuccessImportStep,
    ]


class TestTrackImportedFrom:
    def test_managers_has_no_track_from_where_the_file_was_imported_on_init(self):
        fake_import_manager = FakeImportManagerCompleteWithSucces(
            temp_file=fake_temp_file_factory(),
            uploaded_by_user_uuid=uuid4(),
            library_uuid=uuid4(),
        )
        assert fake_import_manager.imported_from == ImportFrom.UNKNOWN

    def test_managers_process_imported_from_cli_by_passing_imported_from_cli(self):
        fake_import_manager = FakeImportManagerCompleteWithSucces(
            temp_file=fake_temp_file_factory(),
            uploaded_by_user_uuid=uuid4(),
            library_uuid=uuid4(),
            imported_from=ImportFrom.CLI,
        )

        assert fake_import_manager.imported_from != ImportFrom.UNKNOWN
        assert fake_import_manager.imported_from == ImportFrom.CLI

    def test_managers_process_imported_from_web_by_passing_imported_from_web(self):
        fake_import_manager = FakeImportManagerCompleteWithSucces(
            temp_file=fake_temp_file_factory(),
            uploaded_by_user_uuid=uuid4(),
            library_uuid=uuid4(),
            imported_from=ImportFrom.WEB_INTERFACE,
        )

        assert fake_import_manager.imported_from != ImportFrom.UNKNOWN
        assert fake_import_manager.imported_from == ImportFrom.WEB_INTERFACE
