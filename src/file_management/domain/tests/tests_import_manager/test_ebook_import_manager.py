from pathlib import Path
from uuid import uuid4

import pytest

from src.file_management.domain import FileBlobLocal, FileSize, FileType, TempFile
from src.file_management.domain.events import EbookFileImported

from ...import_manager import EbookImportManager


@pytest.fixture
async def temp_file():
    dummy_file_path = Path("src/file_management/tests/files/dummy-ebook.epub")
    return TempFile(
        file_type=(await FileType.from_path(dummy_file_path)),
        file_size=(await FileSize.from_path(dummy_file_path)),
        blob=FileBlobLocal(dummy_file_path),
        path=Path("/tmp/dummy-ebook.epub"),
    )


class TestEbookImportManager:
    manager_class = EbookImportManager

    async def test_process_completes_succesfully(self, temp_file):
        manager = self.manager_class(
            temp_file=temp_file,
            uploaded_by_user_uuid=uuid4(),
            library_uuid=uuid4(),
        )
        assert len(manager.events) == 0
        await manager.process()
        assert len(manager.events) == 1
        event = manager.events[0]
        assert isinstance(event, EbookFileImported)
