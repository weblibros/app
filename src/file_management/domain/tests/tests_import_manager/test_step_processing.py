import asyncio
from datetime import datetime
from pathlib import Path
from unittest.mock import MagicMock
from uuid import uuid4

from src.file_management.domain.events import ProcessedFailedImportStep, ProcessedImportStep, ProcessedImportStepResult

from ...import_manager import ImportStep
from ..fake import fake_temp_file_factory


class FakeProcessedImportStepResult(ProcessedImportStepResult): ...


class FakeImportStepRaiseError(ImportStep):
    async def _process(self):
        raise Exception("Fake error")
        return FakeProcessedImportStepResult()


class FakeImportStepWithWait(ImportStep):
    async def _process(self):
        await asyncio.sleep(0.01)
        return FakeProcessedImportStepResult()


class FakeImportStepWithMock(ImportStep):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mock = MagicMock()

    async def _process(self):
        self.mock._is_called()
        await asyncio.sleep(0.01)
        return FakeProcessedImportStepResult()


async def test_start_and_finish_time_logged_for_steps():
    import_step = FakeImportStepWithWait(
        uuid=uuid4(),
        library_uuid=uuid4(),
        temp_file=fake_temp_file_factory(),
        book_uuid=uuid4(),
        base_dir=Path("/tmp"),
    )

    assert import_step._started_at is None
    assert import_step._finished_at is None
    await import_step.process()
    assert import_step._started_at is not None
    assert import_step._finished_at is not None
    assert isinstance(import_step._started_at, datetime)
    assert isinstance(import_step._finished_at, datetime)
    assert import_step._finished_at > import_step._started_at


async def test_start_and_finish_time_logged_for_steps_when_error_is_raised():
    import_step = FakeImportStepRaiseError(
        uuid=uuid4(),
        library_uuid=uuid4(),
        temp_file=fake_temp_file_factory(),
        book_uuid=uuid4(),
        base_dir=Path("/tmp"),
    )

    assert import_step._started_at is None
    assert import_step._finished_at is None
    await import_step.process()
    assert import_step._started_at is not None
    assert import_step._finished_at is not None
    assert isinstance(import_step._started_at, datetime)
    assert isinstance(import_step._finished_at, datetime)
    assert import_step._finished_at > import_step._started_at


async def test_error_is_stored_and_not_raised():
    import_step = FakeImportStepRaiseError(
        uuid=uuid4(),
        library_uuid=uuid4(),
        temp_file=fake_temp_file_factory(),
        book_uuid=uuid4(),
        base_dir=Path("/tmp"),
    )

    assert import_step._error_message == ""
    await import_step.process()
    assert import_step._error_message == "Fake error"


async def test_event_saved_after_processing():
    import_step = FakeImportStepWithWait(
        uuid=uuid4(),
        library_uuid=uuid4(),
        temp_file=fake_temp_file_factory(),
        book_uuid=uuid4(),
        base_dir=Path("/tmp"),
    )

    await import_step.process()
    assert len(import_step._events) == 1
    event = import_step._events[0]
    assert isinstance(event, ProcessedImportStep)


async def test_event_saved_data_structure():
    import_step = FakeImportStepWithWait(
        uuid=uuid4(),
        library_uuid=uuid4(),
        temp_file=fake_temp_file_factory(),
        book_uuid=uuid4(),
        base_dir=Path("/tmp"),
    )

    await import_step.process()
    event = import_step._events[0]
    assert isinstance(event, ProcessedImportStep)
    assert not isinstance(event, ProcessedFailedImportStep)
    assert isinstance(event.result, ProcessedImportStepResult)

    assert event.uuid == import_step.uuid
    assert event.import_step_name == "FakeImportStepWithWait"
    assert event.started_at == import_step._started_at
    assert event.finished_at == import_step._finished_at


async def test_process_is_only_called_once():
    import_step = FakeImportStepWithMock(
        uuid=uuid4(),
        library_uuid=uuid4(),
        temp_file=fake_temp_file_factory(),
        book_uuid=uuid4(),
        base_dir=Path("/tmp"),
    )

    assert len(import_step._events) == 0
    await import_step.process()
    assert len(import_step._events) == 1
    event = import_step._events[0]
    assert isinstance(event, ProcessedImportStep)
    assert not isinstance(event, ProcessedFailedImportStep)
    assert isinstance(event.result, ProcessedImportStepResult)

    assert event.uuid == import_step.uuid
    import_step.mock._is_called.assert_called_once()

    await import_step.process()
    assert len(import_step._events) == 1
    import_step.mock._is_called.assert_called_once()
    event_v2 = import_step._events[0]
    assert event == event_v2
