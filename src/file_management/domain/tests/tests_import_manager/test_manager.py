from uuid import uuid4

import pytest

from src.file_management.domain.events import (
    EbookFileImported,
    FailedImportStepResult,
    ImportManagerFailedToImport,
    ProcessedImportStepResult,
)
from src.file_management.domain.import_manager import ImportManager, ImportStep
from src.file_management.domain.tests.fake import fake_temp_file_factory


class FakeProcessedImportStepResult(ProcessedImportStepResult): ...


class FakeSuccessImportStep(ImportStep):
    async def _process(self):
        return FakeProcessedImportStepResult()


class FakeFailImportStep(ImportStep):
    async def _process(self):
        self._error_message = "fake error"
        return FailedImportStepResult()


class FakeImportManagerCompleteWithSucces(ImportManager):
    _step_classes = [
        FakeSuccessImportStep,
        FakeSuccessImportStep,
    ]


class FakeImportManagerCompleteWithFailure(ImportManager):
    _step_classes = [
        FakeSuccessImportStep,
        FakeFailImportStep,
        FakeSuccessImportStep,
    ]


@pytest.fixture()
def import_manager_with_success():
    return FakeImportManagerCompleteWithSucces(
        temp_file=fake_temp_file_factory(),
        uploaded_by_user_uuid=uuid4(),
        library_uuid=uuid4(),
    )


@pytest.fixture()
def import_manager_with_failure():
    return FakeImportManagerCompleteWithFailure(
        temp_file=fake_temp_file_factory(),
        uploaded_by_user_uuid=uuid4(),
        library_uuid=uuid4(),
    )


async def test_event_stored_after_success(import_manager_with_success):
    import_manager = import_manager_with_success
    assert import_manager.events == []
    await import_manager.process()
    assert len(import_manager.events) == 1
    event = import_manager.events[0]
    assert isinstance(event, EbookFileImported)


async def test_event_data_after_success(import_manager_with_success):
    import_manager = import_manager_with_success
    await import_manager.process()
    event = import_manager.events[0]
    assert isinstance(event, EbookFileImported)

    assert event.user_uuid == import_manager._uploaded_by_user_uuid
    assert event.library_uuid == import_manager._library_uuid
    assert event.started_at == import_manager._started_at
    assert event.finished_at == import_manager._finished_at
    assert event.steps_processed == import_manager._processed_steps
    assert len(event.steps_processed) == 2


async def test_event_stored_after_failure(import_manager_with_failure):
    import_manager = import_manager_with_failure
    assert import_manager.events == []
    await import_manager.process()
    assert len(import_manager.events) == 1
    event = import_manager.events[0]
    assert isinstance(event, ImportManagerFailedToImport)


async def test_event_data_after_failure(import_manager_with_failure):
    import_manager = import_manager_with_failure
    await import_manager.process()
    event = import_manager.events[0]
    assert isinstance(event, ImportManagerFailedToImport)

    assert event.user_uuid == import_manager._uploaded_by_user_uuid
    assert event.library_uuid == import_manager._library_uuid
    assert event.started_at == import_manager._started_at
    assert event.finished_at == import_manager._finished_at
    assert event.steps_processed == import_manager._processed_steps
    assert len(event.steps_processed) == 2
