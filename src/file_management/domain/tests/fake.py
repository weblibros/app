from pathlib import Path
from uuid import uuid4

from src.file_management.domain import FileSize, FileType, TempFile

from ..aggregate import AbstractFileBlob


class FakeFileBlobUpload(AbstractFileBlob):
    async def content(self) -> bytes:
        return b"fake blob"

    async def load_into_memory(self):
        pass


storage_dir = "/tmp/test_file_repo/"


def fake_temp_file_factory():
    return TempFile(
        uuid=uuid4(),
        file_type=FileType(mime_type="application/epub+zip", extension=".epub"),
        file_size=FileSize(101),
        blob=FakeFileBlobUpload(),
        path=Path(storage_dir).joinpath("test_ebook.epub"),
    )
