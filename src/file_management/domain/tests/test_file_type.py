import io
from pathlib import Path

import pytest
from fastapi import UploadFile

from src.file_management.tests.fakers import fake_small_upload_file

from ..file_type import FileType, FileTypeAnalyser


class TestFileType:
    pytest_parameters = (
        "file_path, expected_file_type, expected_extension",
        [
            (
                "src/file_management/tests/files/random_image.png",
                "image/png",
                ".png",
            ),
            (
                "src/file_management/tests/files/random_image.jpg",
                "image/jpeg",
                ".jpg",
            ),
            (
                "src/file_management/tests/files/dummy-ebook.epub",
                "application/epub+zip",
                ".epub",
            ),
        ],
    )

    @pytest.mark.parametrize(*pytest_parameters)
    async def test_import_from_path(self, file_path, expected_file_type, expected_extension):
        file_type = await FileType.from_path(file_path)
        assert file_type.mime_type == expected_file_type
        assert file_type.extension == expected_extension

    @pytest.mark.parametrize(*pytest_parameters)
    async def test_import_from_upload_file(self, file_path, expected_file_type, expected_extension):
        with open(file_path, "rb") as file:
            file_io_bytes = io.BytesIO(file.read())
            file_upload = UploadFile(
                file=file_io_bytes,
                size=len(file_io_bytes.getvalue()),
                filename="Fake" + Path(file_path).suffix,
            )
        file_type = await FileType.from_upload_file(file_upload)
        assert file_type.mime_type == expected_file_type
        assert file_type.extension == expected_extension

    async def test_error_no_file(self):
        file_path = "Does not exist"
        with pytest.raises(Exception) as e:
            await FileType.from_path(file_path)

        assert "could not read filetype" in str(e.value)

    async def test_error_incorrect_mime_type_and_extension(self):
        assert FileType("image/png", ".jpeg").is_valid is False

    @pytest.mark.parametrize(
        "mime_type, extension, is_supported",
        [
            ("image/png", ".png", True),
            ("image/jpeg", ".jpeg", True),
            ("application/epub+zip", ".epub", True),
        ],
    )
    def test_is_supported(self, mime_type, extension, is_supported):
        assert FileType(mime_type, extension).is_supported is is_supported

    @pytest.mark.parametrize(
        "mime_type, extension, is_ebook",
        [
            ("image/png", ".png", False),
            ("image/jpeg", ".jpeg", False),
            ("application/epub+zip", ".epub", True),
        ],
    )
    def test_is_ebook(self, mime_type, extension, is_ebook):
        assert FileType(mime_type, extension).is_ebook is is_ebook


class TestFileTypeAnalizersFromPath:
    pytest_parameters = (
        "file_path, expected_file_type",
        [
            (
                "src/file_management/tests/files/random_image.png",
                "image/png",
            ),
            (
                "src/file_management/tests/files/random_image.jpg",
                "image/jpeg",
            ),
            (
                "src/file_management/tests/files/dummy-ebook.epub",
                "application/epub+zip",
            ),
        ],
    )

    @pytest.mark.parametrize(*pytest_parameters)
    def test_file_type_from_path(self, file_path, expected_file_type):
        file_type_analyzer = FileTypeAnalyser()
        assert expected_file_type == file_type_analyzer.from_path(file_path)

    def test_error_no_file(self):
        file_type_analyzer = FileTypeAnalyser()
        with pytest.raises(Exception) as e:
            file_type_analyzer.from_path("doesnotexists")

        assert "could not read filetype" in str(e.value)

    @pytest.mark.parametrize(*pytest_parameters)
    async def test_file_type_from_fileupload(self, file_path, expected_file_type):
        with open(file_path, "rb") as file:
            file_io_bytes = io.BytesIO(file.read())
            file_upload = UploadFile(
                file=file_io_bytes,
                size=len(file_io_bytes.getvalue()),
                filename="Fake",
            )
        file_type_analyzer = FileTypeAnalyser()
        assert expected_file_type == await file_type_analyzer.from_fileupload(file_upload)

    @pytest.mark.parametrize(*pytest_parameters)
    async def test_byte_size_does_not_change(self, file_path, expected_file_type):
        with open(file_path, "rb") as file:
            file_io_bytes = io.BytesIO(file.read())
            file_upload = UploadFile(
                file=file_io_bytes,
                size=len(file_io_bytes.getvalue()),
                filename="Fake",
            )
        original_byte_size = len(file_upload.file.getvalue())
        assert original_byte_size != 0
        await FileTypeAnalyser().from_fileupload(file_upload)
        after_byte_size = len(file_upload.file.getvalue())
        assert original_byte_size == after_byte_size

    async def test_error_fake_binary(self):
        fake_upload_file = fake_small_upload_file()
        file_type_analyzer = FileTypeAnalyser()
        assert await file_type_analyzer.from_fileupload(fake_upload_file) == "application/octet-stream"
