from dataclasses import dataclass
from pathlib import Path
from typing import TYPE_CHECKING
from uuid import UUID

from src.events import Command

if TYPE_CHECKING:
    from fastapi import UploadFile


@dataclass
class UploadEbookFileToTempStorageCommand(Command):
    upload_file: "UploadFile"
    user_uuid: UUID
    library_uuid: UUID


@dataclass
class ImportLocalEbookFileCommand(Command):
    file_path: Path
    user_uuid: UUID
    library_uuid: UUID
