from uuid import UUID

from fastapi_events.registry.payload_schema import registry as payload_schema
from pydantic import BaseModel

from .enum_events import FileManagmentEvents

# TODO: deprecate schema's for proper events


@payload_schema.register(event_name=FileManagmentEvents.FILE_RECORD_REMOVED)
class FileRecordEventSchema(BaseModel):
    uuid: UUID
    path: str
    size_bytes: int
    content_type: str
    storage_service_name: str


@payload_schema.register(event_name=FileManagmentEvents.BOOK_FILE_REMOVED)
class BookFileRemovedEventSchema(BaseModel):
    book_uuid: UUID
    file_uuid: UUID


@payload_schema.register(event_name=FileManagmentEvents.COVER_FILE_REMOVED)
class CoverFileRemovedEventSchema(BaseModel):
    book_uuid: UUID
    image_uuid: UUID
