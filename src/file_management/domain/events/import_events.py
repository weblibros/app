from datetime import datetime
from pathlib import Path
from typing import Optional, Sequence, Union
from uuid import UUID

from fastapi_events.registry.payload_schema import registry as payload_schema
from pydantic import BaseModel

from src.events import Event

from .data_extracted_events import EbookMetaData
from .enum_events import FileManagmentEvents


class ProcessedImportStepResult(BaseModel):
    def to_raw(self) -> str:
        return self.model_dump_json()


class ProcessedImportStep(BaseModel, Event):
    uuid: UUID
    import_step_name: str
    started_at: datetime
    finished_at: datetime

    result: ProcessedImportStepResult


class FailedImportStepResult(ProcessedImportStepResult):
    error_message: str


class ProcessedFailedImportStep(ProcessedImportStep):
    result: FailedImportStepResult


class FileContextLoadedInMemoryResult(ProcessedImportStepResult):
    file_uuid: UUID
    loaded: bool


class ProcessedFileContextLoadedInMemoryStep(ProcessedImportStep):
    result: FileContextLoadedInMemoryResult


class ExtractCoverFromEbookStepResult(ProcessedImportStepResult):
    book_uuid: UUID
    image_uuid: UUID
    file_uuid: UUID
    image_path: Path


class ProcessedExtractCoverFromEbookStep(ProcessedImportStep):
    result: ExtractCoverFromEbookStepResult


class TransformTempToEbookStepResult(ProcessedImportStepResult):
    temp_file_uuid: UUID
    book_file_uuid: UUID
    book_uuid: UUID
    href: str
    mime_type: str


class ProcessedTransformTempToEbookStep(ProcessedImportStep):
    result: TransformTempToEbookStepResult


class RetrieveEbookMetaDataStepResult(ProcessedImportStepResult):
    book_uuid: UUID
    data: EbookMetaData


class ProcessedRetrieveEbookMetaDataStep(ProcessedImportStep):
    result: RetrieveEbookMetaDataStepResult


class FileIsEbookStepResult(ProcessedImportStepResult):
    file_uuid: UUID
    is_ebook: bool


class ProcessedFileIsEbookStep(ProcessedImportStep):
    result: FileIsEbookStepResult


@payload_schema.register
class EbookFileImported(BaseModel, Event):
    __event_name__ = FileManagmentEvents.EBOOK_FILE_IMPORTED
    steps_processed: Sequence[
        Union[
            ProcessedImportStep,
            ProcessedFileContextLoadedInMemoryStep,
            ProcessedFileIsEbookStep,
            ProcessedExtractCoverFromEbookStep,
            ProcessedRetrieveEbookMetaDataStep,
            ProcessedTransformTempToEbookStep,
        ]
    ]
    user_uuid: UUID
    library_uuid: UUID
    book_uuid: UUID
    started_at: datetime
    finished_at: datetime


@payload_schema.register
class ImportManagerFailedToImport(BaseModel, Event):
    __event_name__ = FileManagmentEvents.EBOOK_FILE_FAILED_TO_IMPORT
    error_message: str
    steps_processed: Sequence[Union[ProcessedImportStep, ProcessedFailedImportStep]]
    user_uuid: UUID
    library_uuid: UUID

    started_at: Optional[datetime]
    finished_at: Optional[datetime]
