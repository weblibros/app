from dataclasses import dataclass
from typing import TYPE_CHECKING
from uuid import UUID

from fastapi_events.registry.payload_schema import registry as payload_schema
from pydantic import BaseModel

from src.events import Event

from .enum_events import FileManagmentEvents

if TYPE_CHECKING:
    from src.file_management.domain import TempFile


@payload_schema.register
class TempEbookFileUploadedEvent(BaseModel, Event):
    __event_name__ = FileManagmentEvents.TEMP_EBOOK_FILE_UPLOADED

    file_uuid: UUID
    uploaded_by_user_uuid: UUID
    destination_library_uuid: UUID


@dataclass
class TempEbookFileLocallyLoadedEvent(Event):
    uploaded_by_user_uuid: UUID
    destination_library_uuid: UUID
    temp_file: "TempFile"
