from datetime import datetime
from typing import Any, Dict

from pydantic import BaseModel, model_validator


class AuthorMetaData(BaseModel):
    first_name: str
    last_name: str


class IdentifierMetaData(BaseModel):
    key: str
    value: str


class EbookMetaData(BaseModel):
    title: str
    authors: list[AuthorMetaData]
    tags: list[str] | None = None
    title_sort: str | None = None
    rights: str | None = None
    identifiers: list[IdentifierMetaData] = []
    published: datetime | None = None
    language: str | None = None

    extra: Dict[str, Any]

    @model_validator(mode="before")
    @classmethod
    def build_extra(cls, values: Dict[str, Any]) -> Dict[str, Any]:
        all_required_field_names = {field for field in cls.model_fields.keys() if field != "extra"}  # to support alias

        extra: Dict[str, Any] = {}
        for field_name in list(values):
            if field_name not in all_required_field_names:
                extra[field_name] = values.pop(field_name)
        values["extra"] = extra
        return values
