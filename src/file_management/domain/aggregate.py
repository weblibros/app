from abc import ABC, abstractmethod
from pathlib import Path
from tempfile import SpooledTemporaryFile
from typing import TYPE_CHECKING, Optional, Self
from uuid import UUID, uuid4

import aiofiles

from src.events import Event
from src.file_management import urls

from .file_size import FileSize
from .file_type import FileType

if TYPE_CHECKING:
    from fastapi import UploadFile


class AbstractFileBlob(ABC):
    @abstractmethod
    async def content(self) -> bytes:
        pass

    @abstractmethod
    async def load_into_memory(self):
        # load content into memory when it's not local
        # to be used when content is in external system
        pass


class FileBlobUpload(AbstractFileBlob):
    def __init__(self, upload_file: "UploadFile"):
        self._upload_file = upload_file

    async def content(self) -> bytes:
        await self.upload_file.seek(0)
        return await self.upload_file.read()

    @property
    def upload_file(self) -> "UploadFile":
        return self._upload_file

    async def load_into_memory(self):
        pass


class FileBlobLocal(AbstractFileBlob):
    def __init__(self, local_path: Path | str):
        self._local_path = local_path

    async def content(self) -> bytes:
        async with aiofiles.open(self._local_path, "rb") as local_file:
            await local_file.seek(0)
            content = await local_file.read()
        return content

    async def load_into_memory(self):
        pass


class FileBlobSpoolTempFile(AbstractFileBlob):
    def __init__(self, spooled_temp_file: SpooledTemporaryFile):
        self._spooled_temp_file = spooled_temp_file

    async def content(self) -> bytes:
        self._spooled_temp_file.seek(0)
        content = self._spooled_temp_file.read()
        return content

    @classmethod
    async def from_upload_file(cls, upload_file: "UploadFile") -> Self:
        await upload_file.seek(0)
        content = await upload_file.read()
        temp_file = SpooledTemporaryFile()
        temp_file.write(content)
        return cls(temp_file)

    @classmethod
    async def from_path(cls, file_path: Path) -> Self:
        async with aiofiles.open(file_path, "rb") as local_file:
            await local_file.seek(0)
            content = await local_file.read()
            temp_file = SpooledTemporaryFile()
            temp_file.write(content)
        return cls(temp_file)

    @classmethod
    async def from_blob(cls, blob: AbstractFileBlob) -> Self:
        content = await blob.content()
        temp_file = SpooledTemporaryFile()
        temp_file.write(content)
        return cls(temp_file)

    async def load_into_memory(self):
        pass


class File:
    events: list[Event] = []

    def __init__(
        self,
        file_type: FileType,
        file_size: FileSize,
        blob: AbstractFileBlob,
        path: Path,
        uuid: Optional[UUID] = None,
    ):
        self.uuid = uuid or uuid4()
        self.file_size = file_size
        self.file_type = file_type
        self.blob = blob
        self.path = path

    @property
    def content_type(self) -> str:
        return self.file_type.mime_type

    @property
    def size_bytes(self) -> int:
        return self.file_size.size_bytes

    @property
    def file_name(self) -> str:
        return self.path.name

    @property
    def _entity_class_name(self) -> str:
        return self.__class__.__name__

    def __hash__(self):
        return hash(self.uuid)

    @property
    def href(self) -> str:
        return urls.DOWNLOAD_FILE_URL.format(file_uuid=self.uuid)


class EbookFile(File):
    def __init__(
        self,
        file_type: FileType,
        file_size: FileSize,
        blob: AbstractFileBlob,
        path: Path,
        uuid: Optional[UUID] = None,
    ):
        if file_type.is_ebook:
            super().__init__(
                file_type=file_type,
                file_size=file_size,
                blob=blob,
                path=path,
                uuid=uuid,
            )
        else:
            raise ValueError("file is not an ebook")


class ImageFile(File):
    def __init__(
        self,
        pixel_height: int,
        pixel_width: int,
        file_type: FileType,
        file_size: FileSize,
        blob: AbstractFileBlob,
        path: Path,
        uuid: Optional[UUID] = None,
        image_uuid: Optional[UUID] = None,
        extracted_from: Optional[UUID] = None,
    ):
        self.pixel_height = pixel_height
        self.pixel_width = pixel_width
        self.extracted_from = extracted_from
        if image_uuid:
            self.image_uuid = image_uuid
        else:
            self.image_uuid = uuid4()
        super().__init__(
            file_type=file_type,
            file_size=file_size,
            blob=blob,
            path=path,
            uuid=uuid,
        )

    @property
    def href(self) -> str:
        return urls.DOWNLOAD_COVER_IMAGE_URL.format(image_uuid=self.image_uuid)

    @property
    def static_file_path(self) -> str:
        return self.href


class TempFile(File):
    @classmethod
    async def from_path(cls, file_path: Path) -> Self:
        return cls(
            file_type=await FileType.from_path(file_path),
            file_size=await FileSize.from_path(file_path),
            blob=FileBlobLocal(file_path),
            path=file_path,
        )
