import logging

from fastapi_events.handlers.local import local_handler
from fastapi_events.typing import Event

from src.file_management.domain.events import FileManagmentEvents, FileRecordEventSchema
from src.file_management.repositories.blob_repo.services import storage_service_factory

logger = logging.getLogger("default")


@local_handler.register(event_name=str(FileManagmentEvents.FILE_RECORD_REMOVED))
async def cleanup_blob_linked_to_file_record(event: Event):
    _, payload = event
    file_record = FileRecordEventSchema(**payload)

    await _cleanup_blob_linked_to_file_record(file_record.path, file_record.storage_service_name)


async def _cleanup_blob_linked_to_file_record(path: str, storage_service_name: str):
    storage_service = storage_service_factory(storage_service_name)
    await storage_service.remove_blob(path)
