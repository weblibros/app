from src.config import settings

TESTED_EBOOK_FILE_TYPES = [
    "application/epub+zip",
]

UNTESTED_EBOOK_FILE_TYPES = [
    "application/x-mobipocket-ebook",
    "application/vnd.amazon.ebook",
    "application/pdf",
]
valid_ebook_file_types = TESTED_EBOOK_FILE_TYPES
if settings.ALLOW_EXPIRIMENTAL_FILETYPES:
    valid_ebook_file_types += UNTESTED_EBOOK_FILE_TYPES

VALID_IMAGE_FILE_TYPES = [
    "image/png",
    "image/jpeg",
]

VALID_EBOOK_FILE_TYPES = valid_ebook_file_types
VALID_FILE_TYPES = VALID_EBOOK_FILE_TYPES + VALID_IMAGE_FILE_TYPES

EXTENSION_MIME_TYPE_MAP = {
    ".png": "image/png",
    ".jpeg": "image/jpeg",
    ".jpg": "image/jpeg",
    ".pjpg": "image/jpeg",
    ".jpe": "image/jpeg",
    ".jfif": "image/jpeg",
    ".jfif-tbnl": "image/jpeg",
    ".jif": "image/jpeg",
    ".epub": "application/epub+zip",
    ".mobi": "application/x-mobipocket-ebook",
    ".prc": "application/x-mobipocket-ebook",
    ".azw": "application/vnd.amazon.ebook",
    ".pdf": "application/pdf",
}
