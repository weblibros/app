DOWNLOAD_FILE_URL = "/api/v1/file/{file_uuid}/download"
EBOOK_UPLOAD_URL = "/api/v1/file/upload/ebook"
DOWNLOAD_COVER_IMAGE_URL = "/api/v1/file/cover/{image_uuid}/image"
