from pathlib import Path
from uuid import UUID

from src.file_management.domain import EbookFile, File, FileBlobSpoolTempFile


class TransformTempToEbookService:
    async def transform(self, temp_file: File, book_uuid: UUID, base_dir: Path) -> EbookFile:
        file_size = temp_file.file_size
        file_type = temp_file.file_type
        path = base_dir.joinpath(f"orginal{file_type.extension}")
        blob = await FileBlobSpoolTempFile.from_blob(temp_file.blob)

        ebook = EbookFile(
            uuid=book_uuid,
            file_size=file_size,
            file_type=file_type,
            path=path,
            blob=blob,
        )

        return ebook
