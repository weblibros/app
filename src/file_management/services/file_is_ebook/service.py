from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from src.file_management.domain import File


class IsEbookFileService:
    async def is_ebook(self, temp_file: "File") -> bool:
        is_ebook = temp_file.file_type.is_ebook
        is_valid = temp_file.file_type.is_valid

        return is_ebook and is_valid
