from datetime import datetime

import pytest

from ..calibre_command import (
    ProccesAuthorsResults,
    ProccesIdentifiersResults,
    ProccesSingleLineResults,
    extract_key_word_out_of_line,
    process_results_ebook_meta,
)
from .calibre_command_results import (
    result_command_moby_dick,
    results_command_spanish_lobra_negra,
)


@pytest.mark.parametrize(
    "result_command, expected_result",
    [
        (
            result_command_moby_dick,
            {
                "title": "Moby Dick; Or, The Whale",
                "title_sort": "Moby Dick; Or, The Whale",
                "extra": {"Fake": "not in results"},
                "language": "eng",
                "authors": [
                    {
                        "first_name": "Herman",
                        "last_name": "Melville",
                    },
                ],
                "identifiers": [
                    {
                        "key": "uri",
                        "value": "http://www.gutenberg.org/pg2701",
                    },
                ],
                "published": datetime.fromisoformat("2001-07-01T00:00:00"),
                "rights": "Public domain in the USA.",
                "tags": [
                    "Whaling -- Fiction",
                    "Sea stories",
                    "Psychological fiction",
                    "Ship captains -- Fiction",
                    "Adventure stories",
                    "Mentally ill -- Fiction",
                    "Ahab",
                    "Captain (Fictitious character) -- Fiction",
                    "Whales -- Fiction",
                    "Whaling ships -- Fiction",
                ],
            },
        ),
        (
            results_command_spanish_lobra_negra,
            {
                "authors": [
                    {
                        "first_name": "Juan",
                        "last_name": "Gómez-Jurado",
                    },
                ],
                "identifiers": [
                    {
                        "key": "mobi-asin",
                        "value": "B07X6JD96W",
                    },
                    {
                        "key": "isbn",
                        "value": "9788466666619",
                    },
                    {
                        "key": "google",
                        "value": "wy2sDwAAQBAJ",
                    },
                    {
                        "key": "amazon",
                        "value": "B07X6JD96W",
                    },
                ],
                "language": "es",
                "published": datetime.fromisoformat("2019-10-23T22:00:00"),
                "rights": None,
                "tags": ["Fiction", "Mystery & Detective", "General"],
                "title": "Loba negra",
                "title_sort": "Loba negra",
                "extra": {
                    "Book Producer": "calibre (3.48.0) [https://calibre-ebook.com]",
                    "Comments": "<div><p><strong>El <em>thriller</em> que los 250.000 lectores de <em>Reina Roja</em> están esperando.</strong>",  # noqa: E501
                    "Publisher": "EDICIONES B",
                },
            },
        ),
    ],
)
def test_process_results_ebook_meta(result_command, expected_result):
    result = process_results_ebook_meta(result_command).model_dump()
    assert result == expected_result


@pytest.mark.parametrize(
    "result, line",
    [
        ("Title", "Title               : Moby Dick; Or, The Whale"),
        ("Author(s)", "Author(s)           : Herman Melville [Melville, Herman]"),
        (
            "Tags",
            "Tags                : Whaling -- Fiction, Sea stories, Psychological fiction, Ship captains -- Fiction, Adventure stories, Mentally ill -- Fiction, Ahab, Captain (Fictitious character) -- Fiction, Whales -- Fiction, Whaling ships -- Fiction",  # noqa: E501
        ),
        ("Languages", "Languages           : eng"),
        ("Identifiers", "Identifiers         : uri:http://www.gutenberg.org/pg2701"),
        ("Title sort", "Title sort          : Moby Dick; Or, The Whale"),
    ],
)
def test_find_keyword(result, line):
    assert extract_key_word_out_of_line(line) == result


@pytest.mark.parametrize(
    "expected_result, line",
    [
        ("Moby Dick; Or, The Whale", "Title               : Moby Dick; Or, The Whale"),
        (
            "Herman Melville [Melville, Herman]",
            "Author(s)           : Herman Melville [Melville, Herman]",
        ),
        ("eng", "Languages           : eng"),
        (
            "uri:http://www.gutenberg.org/pg2701",
            "Identifiers         : uri:http://www.gutenberg.org/pg2701",
        ),
    ],
)
def test_process_single_line(expected_result, line):
    key = "doesnotmatter"
    process = ProccesSingleLineResults(key)
    assert process.extract_results(line).get(key) == expected_result


@pytest.mark.parametrize(
    "expected_result, line",
    [
        (
            [
                {
                    "key": "mobi-asin",
                    "value": "B07X6JD96W",
                },
                {
                    "key": "isbn",
                    "value": "9788466666619",
                },
                {
                    "key": "google",
                    "value": "wy2sDwAAQBAJ",
                },
                {
                    "key": "amazon",
                    "value": "B07X6JD96W",
                },
            ],
            "Identifiers         : mobi-asin:B07X6JD96W, isbn:9788466666619, google:wy2sDwAAQBAJ, amazon:B07X6JD96W",
        ),
        (
            [
                {
                    "key": "uri",
                    "value": "http://www.gutenberg.org/pg2701",
                },
            ],
            "Identifiers         : uri:http://www.gutenberg.org/pg2701",
        ),
    ],
)
def test_process_identifiers(expected_result, line):
    key = "identifiers"
    process = ProccesIdentifiersResults(key)
    assert process.extract_results(line).get(key) == expected_result


@pytest.mark.parametrize(
    "expected_result, line",
    [
        (
            [
                {
                    "first_name": "Juan",
                    "last_name": "Gómez-jurado",
                },
            ],
            "author(s)           : juan gómez-jurado [gómez-jurado, juan]",
        ),
        (
            [
                {
                    "first_name": "Herman",
                    "last_name": "Melville",
                },
            ],
            "author(s)           : herman melville [melville, herman]",
        ),
        (
            [
                {
                    "first_name": "Erich",
                    "last_name": "Gamma",
                },
                {
                    "first_name": "Richard",
                    "last_name": "Helm",
                },
                {
                    "first_name": "Ralph",
                    "last_name": "Johnson",
                },
                {
                    "first_name": "John",
                    "last_name": "Vlissides",
                },
            ],
            "Author(s)           : Erich Gamma & Richard Helm & Ralph Johnson & John Vlissides",
        ),
        (
            [
                {
                    "first_name": "Jeff",
                    "last_name": "Sutherland",
                },
                {
                    "first_name": "Jeffrey Victor",
                    "last_name": "Sutherland",
                },
            ],
            "Author(s)           : Jeff Sutherland & Jeffrey Victor Sutherland [Sutherland, Jeff & Sutherland, Jeffrey Victor]",  # noqa: E501
        ),
        (
            [
                {
                    "first_name": "First_name_only",
                    "last_name": "",
                },
            ],
            "Author(s)           : first_name_only",
        ),
    ],
)
def test_process_author_line(expected_result, line):
    key = "doesnotmatter"
    process = ProccesAuthorsResults(key)
    assert process.extract_results(line).get(key) == expected_result


@pytest.mark.parametrize(
    "expected_result, line",
    [
        (
            "gómez-jurado, juan",
            "author(s)           : juan gómez-jurado [gómez-jurado, juan]",
        ),
        (
            "melville, herman",
            "author(s)           : herman melville [melville, herman]",
        ),
        (
            None,
            "Author(s)           : Erich Gamma & Richard Helm & Ralph Johnson & John Vlissides",
        ),
        (
            "Sutherland, Jeff & Sutherland, Jeffrey Victor",
            "Author(s)           : Jeff Sutherland & Jeffrey Victor Sutherland [Sutherland, Jeff & Sutherland, Jeffrey Victor]",  # noqa: E501
        ),
    ],
)
def test_process_author_line_value_between_chars(expected_result, line):
    key = "doesnotmatter"
    process = ProccesAuthorsResults(key)
    assert process._find_value_between(line, "[", "]") == expected_result
