import io

from fastapi import UploadFile
from fastapi.datastructures import Headers

dummy_file_path = "src/file_management/tests/files/dummy-ebook.epub"


def fake_ebook_as_upload_file(fake_file_name: str = "fake-ebook.epub") -> UploadFile:
    with open(dummy_file_path, "rb") as file:
        file_size = len(file.read())
        file_b = io.BytesIO(file.read())
        upload_file = UploadFile(
            file=file_b,
            size=file_size,
            filename=fake_file_name,
            headers=Headers(headers={"content-type": "application/epub+zip"}),
        )
    return upload_file
