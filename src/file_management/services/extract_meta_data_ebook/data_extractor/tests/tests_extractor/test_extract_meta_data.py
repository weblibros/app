import os
from pathlib import Path

from src.file_management.domain import File, FileBlobUpload, FileSize, FileType
from src.file_management.domain.events.data_extracted_events import EbookMetaData

from ...extractor import ExtractMetaDataEbook
from ..fake_ebook import fake_ebook_as_upload_file


async def test_extrator():
    expected_title = "expected-title.epub"
    upload_file = fake_ebook_as_upload_file()
    extractor = ExtractMetaDataEbook()
    ebook_file = File(
        file_type=FileType("application/epub+zip", ".epub"),
        file_size=(await FileSize.from_upload_file(upload_file)),
        blob=FileBlobUpload(upload_file),
        path=Path("/tmp/" + expected_title),
    )
    metadata = await extractor.extract_from_file(ebook_file)

    assert isinstance(metadata, EbookMetaData)
    assert metadata.title == expected_title.split(".")[0]


async def test_cleanup_file():
    upload_file = fake_ebook_as_upload_file()
    extractor = ExtractMetaDataEbook()
    ebook_file = File(
        file_type=FileType("application/epub+zip", ".epub"),
        file_size=(await FileSize.from_upload_file(upload_file)),
        blob=FileBlobUpload(upload_file),
        path=Path("/tmp/dummy-ebook.epub"),
    )
    metadata = await extractor.extract_from_file(ebook_file)

    assert isinstance(metadata, EbookMetaData)
    assert len(os.listdir(extractor.BASE_DIR)) == 0
