# ruff: noqa: E501
result_command_moby_dick = """
Title               : Moby Dick; Or, The Whale
Title sort          : Moby Dick; Or, The Whale
Author(s)           : Herman Melville [Melville, Herman]
Tags                : Whaling -- Fiction, Sea stories, Psychological fiction, Ship captains -- Fiction, Adventure stories, Mentally ill -- Fiction, Ahab, Captain (Fictitious character) -- Fiction, Whales -- Fiction, Whaling ships -- Fiction
Languages           : eng
Published           : 2001-07-01T00:00:00+00:00
Rights              : Public domain in the USA.
Identifiers         : uri:http://www.gutenberg.org/pg2701
Fake                : not in results
"""

results_command_spanish_lobra_negra = """
Title               : Loba negra
Title sort          : Loba negra
Author(s)           : Juan Gómez-Jurado [Gómez-Jurado, Juan]
Publisher           : EDICIONES B
Book Producer       : calibre (3.48.0) [https://calibre-ebook.com]
Tags                : Fiction, Mystery & Detective, General
Languages           : spa
Published           : 2019-10-23T22:00:00+00:00
Identifiers         : mobi-asin:B07X6JD96W, isbn:9788466666619, google:wy2sDwAAQBAJ, amazon:B07X6JD96W
Comments            : <div><p><strong>El <em>thriller</em> que los 250.000 lectores de <em>Reina Roja</em> están esperando.</strong>
<strong>SEGUIR VIVA</strong><br>
Antonia Scott no tiene miedo a nada. Solo a sí misma.
<strong>NUNCA FUE</strong><br>
Pero hay alguien más peligroso que ella. Alguien que podría vencerla.
<strong>TAN DIFÍCIL</strong><br>
La Loba negra está cada vez más cerca. Y Antonia, por primera vez, está asustada.
<strong>La crítica y los lectores opinan sobre Antonia Scott:</strong><br>
«Es literalmente imposible no engancharse. Antonia Scott es lo mejor que le ha ocurrido al <em>thriller</em> internacional en los últimos años.»<br>
<em>ABC</em>
«El mejor escritor de <em>thriller</em> de Europa es Juan Gómez-Jurado.»<br>
<em>Zenda</em>
«Gómez-Jurado, autor en auge internacional, ha hilado un personaje cautivador, el de Antonia Scott.»<br>
<em>La Vanguardia</em>
«Atrapa irremediablemente al lector.»<br>
<em>Booklist</em>
**</p><h3>About the Author</h3>
<p><strong>Juan Gómez-Jurado</strong>  (Madrid, 1977) es periodista y autor de varias novelas de gran éxito, traducidas a cuarenta lenguas. Su último  <em>thriller</em> ,  <em>Reina Roja</em>  (La Trama, 2018), se ha convertido en un gran fenómeno de ventas, con más de doscientos cincuenta mil ejemplares vendidos, y ha consagrado a su autor como uno de los máximos exponentes del género a nivel internacional. Actualmente es colaborador en varios medios y cocreador de los podcast  <em>Todopoderosos</em> y  <em>Aquí hay dragones</em>. </p></div>
"""

results_design_patterns = """
Title               : Design Patterns: Elements of Reusable Object-Oriented Software (Joanne Romanovich's Library)
Author(s)           : Erich Gamma & Richard Helm & Ralph Johnson & John Vlissides
Publisher           : Addison-Wesley
Languages           : eng
Published           : 1995-07-15T00:00:00+00:00
Rights              : All rights reserved.
Identifiers         : isbn:0321700694
"""

results_scrum = """
Title               : Scrum: The Art of Doing Twice the Work in Half the Time
Title sort          : Scrum: The Art of Doing Twice the Work in Half the Time
Author(s)           : Jeff Sutherland & Jeffrey Victor Sutherland [Sutherland, Jeff & Sutherland, Jeffrey Victor]
Publisher           : Crown Business
Book Producer       : calibre (5.15.0) [https://calibre-ebook.com]
Tags                : Time Management, Business, Leadership, Project Management, Business & Economics, Management, Self Help
Languages           : eng
Rating              : 4
Published           : 2014-04-15T07:32:08+00:00
Rights              : Copyright © 2014 by Jeff Sutherland and Scrum, Inc
Identifiers         : google:RoPZCwAAQBAJ, goodreads:19288230, isbn:9780385346450
Comments            : We live in a world that is broken. For those who believe that there must be a more efficient way for people to get things done, here from Scrum pioneer Jeff Sutherland is a brilliantly discursive, thought-provoking book about the management process that is changing the way we live.  In the future, historians may look back on human progress and draw a sharp line designating “before Scrum” and “after Scrum.” Scrum is that ground-breaking. It already drives most of the world's top technology companies. And now it's starting to spread to every domain where people wrestle with complex projects.  If you've ever been startled by how fast the world is changing, Scrum is one of the reasons why. Productivity gains of as much as 1200% have been recorded, and there's no more lucid – or compelling – explainer of Scrum and its bright promise than Jeff Sutherland, the man who put together the first Scrum team more than twenty years ago.  The thorny problem Jeff began tackling back then boils down to this: people are spectacularly bad at doing things quickly and efficiently. Best laid plans go up in smoke. Teams often work at cross purposes to each other. And when the pressure rises, unhappiness soars. Drawing on his experience as a West Point-educated fighter pilot, biometrics expert, early innovator of ATM technology, and V.P. of engineering or CTO at eleven different technology companies, Jeff began challenging those dysfunctional realities, looking for solutions that would have global impact.  In this book you'll journey to Scrum's front lines where Jeff's system of deep accountability, team interaction, and constant iterative improvement is, among other feats, bringing the FBI into the 21st century, perfecting the design of an affordable 140 mile per hour/100 mile per gallon car, helping NPR report fast-moving action in the Middle East, changing the way pharmacists interact with patients, reducing poverty in the Third World, and even helping people plan their weddings and accomplish weekend chores.   Woven with insights from martial arts, judicial decision making, advanced aerial combat, robotics, and many other disciplines, Scrum is consistently riveting. But the most important reason to read this book is that it may just help you achieve what others consider unachievable – whether it be inventing a trailblazing technology, devising a new system of education, pioneering a way to feed the hungry, or, closer to home, a building a foundation for your family to thrive and prosper.
GOODREADS           : 19288230
GOOGLE              : RoPZCwAAQBAJ
ISBN                : 9780385346450
"""
