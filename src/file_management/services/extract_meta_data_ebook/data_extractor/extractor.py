import os
import shutil
from typing import TYPE_CHECKING, Optional
from uuid import uuid4

import aiofiles

from src.file_management.domain.events.data_extracted_events import EbookMetaData

from .calibre_command import extract_meta_data

if TYPE_CHECKING:
    from src.file_management.domain import File


class ExtractMetaDataEbook:
    BASE_DIR = "/tmp/extract_meta_data"

    def __init__(self) -> None:
        self._temp_dir: Optional[str] = None

    async def extract_from_file(self, file: "File") -> EbookMetaData:
        file_name = file.file_name
        file_path = f"{self.temp_dir}/{file_name}"
        await self._write_file_to_path(file, file_path)
        result_extration = self.command_extract_meta_data(file_path)
        self._cleanup_temp_dir(self.temp_dir)
        return result_extration

    @property
    def temp_dir(self) -> str:
        if self._temp_dir:
            return self._temp_dir
        else:
            temp_dir = self._create_temp_file_dir()
            self._temp_dir = temp_dir
            return temp_dir

    def _create_temp_file_dir(self) -> str:
        dir = f"{self.BASE_DIR}/{uuid4()}"
        os.makedirs(dir)
        return dir

    async def _write_file_to_path(self, file: "File", file_path: str):
        async with aiofiles.open(file_path, "wb") as local_file:
            content = await file.blob.content()
            await local_file.write(content)

    def command_extract_meta_data(self, file_path: str) -> EbookMetaData:
        try:
            result = extract_meta_data(file_path)
        except Exception as e:
            self._cleanup_temp_dir(self.temp_dir)
            raise e
        return result

    def _cleanup_temp_dir(self, temp_dir: str):
        shutil.rmtree(temp_dir)
