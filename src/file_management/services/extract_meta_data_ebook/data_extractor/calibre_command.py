import subprocess
from datetime import datetime
from typing import Any, Protocol

from src.file_management.domain.events.data_extracted_events import EbookMetaData


def extract_key_word_out_of_line(line: str) -> str:
    return line.split(":")[0].strip()


class ProccesLineProtocol(Protocol):
    def __init__(self, result_keyword: str): ...

    def extract_results(self, line) -> dict[str, Any]: ...


class ProccesSingleLineResults(ProccesLineProtocol):
    def __init__(self, result_keyword: str):
        self.result_keyword = result_keyword

    def extract_results(self, line) -> dict[str, str]:
        result = line.split(":", maxsplit=1)[1].strip()
        return {self.result_keyword: result}


class ProccesMultiLineResults(ProccesLineProtocol):
    def __init__(self, result_keyword: str):
        self.result_keyword = result_keyword

    def extract_results(self, line) -> dict[str, list[str]]:
        results = line.split(":", maxsplit=1)[1].strip()
        tags = results.split(",")
        stripped_tags = [tag.strip() for tag in tags]
        return {self.result_keyword: stripped_tags}


class ProccesLanguageResults(ProccesLineProtocol):
    def __init__(self, result_keyword: str):
        self.result_keyword = result_keyword

    def extract_results(self, line) -> dict[str, str]:
        language_map = {
            "spa": "es",
            "dutch": "nl",
            "en": "eng",
        }
        result = line.split(":", maxsplit=1)[1].strip()
        corrected_result = language_map.get(result, result)
        return {self.result_keyword: corrected_result}


class ProccesDateTimeLineResults(ProccesLineProtocol):
    def __init__(self, result_keyword: str):
        self.result_keyword = result_keyword

    def extract_results(self, line) -> dict[str, datetime]:
        results = line.split(":", maxsplit=1)[1].strip()
        return {self.result_keyword: datetime.fromisoformat(results).replace(tzinfo=None)}


class ProccesAuthorsResults(ProccesLineProtocol):
    def __init__(self, result_keyword: str):
        self.result_keyword = result_keyword

    def extract_results(self, line) -> dict[str, list[dict]]:
        result = line.split(":", maxsplit=1)[1].strip()
        if "Unknown" == result:
            return {self.result_keyword: []}
        value_between_chars = self._find_value_between(result, "[", "]")
        if value_between_chars:
            authors = self._extract_authors_from_between_chars(value_between_chars)
        else:
            authors = self._extract_authors(result)
        return {self.result_keyword: authors}

    def _find_value_between(self, value: str, first_char: str, second_char: str) -> str | None:
        if first_char == second_char:
            raise Exception(f"values {first_char} and {second_char} in {self.__class__.__name__}")
        if first_char not in value and second_char not in value:
            return None
        index_first_char = value.find(first_char)
        index_second_char = value.find(second_char)
        if index_first_char > index_second_char:
            raise Exception(
                f"char {second_char} at i {index_second_char} come before the char {first_char} at i {index_first_char}"
            )
        return value[index_first_char + 1 : index_second_char]

    def _extract_authors(self, value: str) -> list[dict]:
        author_list: list[dict] = []
        for author in value.split("&"):
            author = author.strip()
            if " " in author:
                first_name, last_name = author.split(" ", maxsplit=1)
            else:
                first_name = author
                last_name = ""
            author_list.append(
                {
                    "first_name": self._first_char_to_upper(first_name.strip()),
                    "last_name": self._first_char_to_upper(last_name.strip()),
                }
            )
        return author_list

    def _extract_authors_from_between_chars(self, value: str) -> list[dict]:
        author_list: list[dict] = []
        for author in value.split("&"):
            last_name, first_name = author.strip().split(",", maxsplit=1)
            author_list.append(
                {
                    "first_name": self._first_char_to_upper(first_name.strip()),
                    "last_name": self._first_char_to_upper(last_name.strip()),
                }
            )
        return author_list

    def _first_char_to_upper(self, value_str: str):
        if len(value_str) > 0:
            return value_str.replace(value_str[0], value_str[0].upper(), 1)
        else:
            return value_str


class ProccesIdentifiersResults(ProccesLineProtocol):
    def __init__(self, result_keyword: str):
        self.result_keyword = result_keyword

    def extract_results(self, line) -> dict[str, list[dict[str, str]]]:
        results = line.split(":", maxsplit=1)[1].strip()
        identifiers = []
        for split_result in results.split(","):
            key_identifier, value_identifier = split_result.split(":", maxsplit=1)
            identifiers_kwargs = {
                "key": key_identifier.strip(),
                "value": value_identifier.strip(),
            }
            identifiers.append(identifiers_kwargs)
        return {self.result_keyword: identifiers}


class ProccesUnknownKeyword(ProccesLineProtocol):
    def __init__(self, _):
        pass

    def extract_results(self, line) -> dict[str, str]:
        if ":" not in line:
            return {}
        keyword, result = line.split(":", maxsplit=1)
        stripped_keyword = keyword.strip()
        keyword_does_not_start_with_capital = stripped_keyword[0].islower()
        keyword_has_xml_tags = "<" in stripped_keyword
        if keyword_does_not_start_with_capital or keyword_has_xml_tags:
            return {}
        return {stripped_keyword: result.strip()}


def find_result_processor(keyword: str) -> ProccesLineProtocol:
    proccessors = {
        "Title": ProccesSingleLineResults("title"),
        "Title sort": ProccesSingleLineResults("title_sort"),
        "Author(s)": ProccesAuthorsResults("authors"),
        "Languages": ProccesLanguageResults("language"),
        "Tags": ProccesMultiLineResults("tags"),
        "Published": ProccesDateTimeLineResults("published"),
        "Rights": ProccesSingleLineResults("rights"),
        "Identifiers": ProccesIdentifiersResults("identifiers"),
    }
    return proccessors.get(keyword, ProccesUnknownKeyword("no_results"))


def process_results_ebook_meta(results: str) -> EbookMetaData:
    meta_data = {}
    for line in results.split("\n"):
        key_word = extract_key_word_out_of_line(line)
        line_processor = find_result_processor(key_word)
        meta_data.update(line_processor.extract_results(line))
    return EbookMetaData(**meta_data)


def extract_meta_data(path: str) -> EbookMetaData:
    command = "ebook-meta"
    process = subprocess.run([command, path], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    if process.returncode != 0:
        raise Exception(f"could not extract meta data from {path}")
    result = process.stdout
    return process_results_ebook_meta(result)
