from typing import TYPE_CHECKING

from .data_extractor.extractor import ExtractMetaDataEbook

if TYPE_CHECKING:
    from src.file_management.domain import File
    from src.file_management.domain.events import EbookMetaData


class MetaDataExtractorService:
    def __init__(self, extractor_class=ExtractMetaDataEbook):
        self._extractor_class = extractor_class

    async def extract(self, ebookfile: "File") -> "EbookMetaData":
        return await self._extractor_class().extract_from_file(ebookfile)
