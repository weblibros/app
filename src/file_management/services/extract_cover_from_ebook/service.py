from dataclasses import dataclass
from pathlib import Path
from uuid import UUID

from src.file_management.domain import File, FileSize, FileType, ImageFile

from .cover_extractor.extractor import ExtractCoverEbook


@dataclass
class CoverExtractedResult:
    cover_file: ImageFile
    book_uuid: UUID
    user_uuid: UUID
    library_uuid: UUID


class CoverExtractorService:
    def __init__(self, extractor_class=ExtractCoverEbook):
        self._extractor_class = extractor_class

    async def extract(self, temp_file: File, base_dir: Path) -> ImageFile:
        cover_blob_file, image_info = await self._extractor_class().extract_from_file(temp_file)
        file_type = FileType(mime_type=image_info.mime_type, extension=image_info.file_extension)
        path = Path(base_dir).joinpath(f"cover{file_type.extension}")
        image_file = ImageFile(
            pixel_width=image_info.pixel_width,
            pixel_height=image_info.pixel_height,
            file_type=file_type,
            file_size=FileSize(image_info.byte_size),
            path=path,
            blob=cover_blob_file,
        )

        return image_file
