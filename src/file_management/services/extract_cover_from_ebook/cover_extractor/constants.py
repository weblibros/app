mime_type_file_extension_map = {
    "image/jpeg": ".jpg",
    "image/png": ".png",
    "application/epub+zip": ".epub",
}
