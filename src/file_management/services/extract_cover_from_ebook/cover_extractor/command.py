import subprocess


def extract_cover(path_book: str, tmp_cover_file: str) -> str:
    command_list = ["ebook-meta", path_book, "--get-cover", tmp_cover_file]
    process = subprocess.run(command_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    if process.returncode != 0:
        raise Exception(f"could not extract cover from file {path_book}")
    result = process.stdout
    return result
