import os
from pathlib import Path

import pytest

from src.file_management.domain import File, FileBlobSpoolTempFile, FileSize, FileType

from ..extractor import ExtractCoverEbook
from ..file_analyzers import ImageInfo


class TestExtractCoverEbook:
    dummy_file_path = "src/file_management/tests/files/dummy-ebook.epub"

    @pytest.fixture()
    async def file_ebook(self):
        file_path = Path(self.dummy_file_path)
        ebook = File(
            file_type=(await FileType.from_path(file_path)),
            file_size=(await FileSize.from_path(file_path)),
            blob=await FileBlobSpoolTempFile.from_path(file_path),
            path=Path("/tmp/dummy-ebook.epub"),
        )
        return ebook

    async def test_from_file_upload(self, file_ebook):
        extractor = ExtractCoverEbook()

        cover_blob_file, image_info = await extractor.extract_from_file(file_ebook)

        assert isinstance(cover_blob_file, FileBlobSpoolTempFile)
        assert isinstance(image_info, ImageInfo)

    async def test_from_file_upload_cleanup(self, file_ebook):
        extractor = ExtractCoverEbook()

        _, _ = await extractor.extract_from_file(file_ebook)

        assert len(os.listdir(extractor.BASE_DIR)) == 0
