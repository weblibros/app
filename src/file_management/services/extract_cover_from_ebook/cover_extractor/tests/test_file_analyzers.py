import os

import pytest

from ..file_analyzers import ImageAnalyzerFileType, ImageInfo

dummy_ebook_path = "src/file_management/tests/files/dummy-ebook.epub"
dummy_jpg_path = "src/file_management/tests/files/random_image.jpg"
dummy_png_path = "src/file_management/tests/files/random_image.png"


@pytest.mark.parametrize(
    "path, mime_type, file_extension, pixel_width, pixel_height, byte_size",
    [
        (dummy_png_path, "image/png", ".png", 200, 200, 4771),
        (dummy_jpg_path, "image/jpeg", ".jpg", 200, 200, 15610),
    ],
)
def test_correct_file_type(path, mime_type, file_extension, pixel_width, pixel_height, byte_size):
    analyzer = ImageAnalyzerFileType()
    result = analyzer.get_image_info_from_path(path)
    assert isinstance(result, ImageInfo)
    assert result.mime_type == mime_type
    assert result.file_extension == file_extension
    assert result.pixel_width == pixel_width
    assert result.pixel_height == pixel_height
    assert result.byte_size == byte_size
    assert result.byte_size == os.path.getsize(path)


def test_wrong_file_type():
    analyzer = ImageAnalyzerFileType()
    with pytest.raises(Exception) as e:
        analyzer.get_image_info_from_path(dummy_ebook_path)

    assert "could not process file" in str(e.value)
    assert dummy_ebook_path in str(e.value)
