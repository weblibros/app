import os
import shutil
from pathlib import Path
from typing import TYPE_CHECKING, Optional, Tuple
from uuid import uuid4

import aiofiles

from src.file_management.domain import FileBlobSpoolTempFile

from .command import extract_cover
from .file_analyzers import ImageAnalyzerFileType, ImageInfo

if TYPE_CHECKING:
    from src.file_management.domain import AbstractFileBlob, EbookFile


class ExtractCoverEbook:
    BASE_DIR = "/tmp/extract_cover_data"

    def __init__(self) -> None:
        self._temp_dir: Optional[str] = None

    async def extract_from_file(self, file: "EbookFile") -> Tuple["AbstractFileBlob", ImageInfo]:
        file_name = file.file_name
        base_file_name = file_name.rsplit(".", 1)[0]
        file_path = f"{self.temp_dir}/{file_name}"
        tmp_cover_path = f"{self.temp_dir}/{base_file_name}.tmp-cover"

        await self._write_file_to_path(file, file_path)
        self.command_extract_cover(file_path, tmp_cover_path)
        image_info = self._read_image_info_from_cover(tmp_cover_path)
        self._validate_file_type(image_info)

        cover_blob_file = await self._load_into_blob(
            tmp_cover_path,
        )
        self._cleanup_temp_dir(self.temp_dir)
        return cover_blob_file, image_info

    @property
    def temp_dir(self) -> str:
        if self._temp_dir:
            return self._temp_dir
        else:
            temp_dir = self._create_temp_file_dir()
            self._temp_dir = temp_dir
            return temp_dir

    def _create_temp_file_dir(self) -> str:
        dir = f"{self.BASE_DIR}/{uuid4()}"
        os.makedirs(dir)
        return dir

    async def _write_file_to_path(self, file: "EbookFile", file_path: str):
        async with aiofiles.open(file_path, "wb") as local_file:
            content = await file.blob.content()
            await local_file.write(content)

    def command_extract_cover(self, file_path: str, cover_path: str) -> str:
        try:
            result = extract_cover(file_path, cover_path)
        except Exception as e:
            self._cleanup_temp_dir(self.temp_dir)
            raise e
        return result

    def _read_image_info_from_cover(self, cover_path: str, file_analyser=ImageAnalyzerFileType()) -> ImageInfo:
        try:
            result = file_analyser.get_image_info_from_path(cover_path)
        except Exception as e:
            self._cleanup_temp_dir(self.temp_dir)
            raise e
        return result

    def _validate_file_type(self, image_info: ImageInfo, raise_error=True) -> bool:
        allowed_mime_types = ["image/jpeg", "image/png"]
        if image_info.mime_type in allowed_mime_types:
            return True
        elif raise_error is False:
            return False
        else:
            raise Exception(f"File type {image_info.mime_type} is not supported for a cover")

    async def _load_into_blob(self, file_path: str) -> "AbstractFileBlob":
        return await FileBlobSpoolTempFile.from_path(Path(file_path))

    def _cleanup_temp_dir(self, temp_dir: str):
        shutil.rmtree(temp_dir)
