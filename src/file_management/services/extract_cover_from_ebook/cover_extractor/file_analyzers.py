import subprocess
from dataclasses import dataclass
from typing import Protocol

from . import constants


class ProccesLineProtocol(Protocol):
    def extract_results(self, line) -> dict[str, int | str]: ...


class ProccessPixelSize(ProccesLineProtocol):
    def extract_results(self, line) -> dict[str, int | str]:
        result = line.split(":", maxsplit=1)[1].strip()
        numbers = result.split("+", maxsplit=1)[0].strip()
        width, height = numbers.split("x")
        return {
            "pixel_height": int(height.strip()),
            "pixel_width": int(width.strip()),
        }


class ProccessFileType(ProccesLineProtocol):
    def extract_results(self, line) -> dict[str, int | str]:
        result = line.split(":", maxsplit=1)[1].strip()
        return {
            "file_extension": constants.mime_type_file_extension_map[result],
            "mime_type": result,
        }


class ProccessByteSize(ProccesLineProtocol):
    def extract_results(self, line) -> dict[str, int | str]:
        result = line.split(":", maxsplit=1)[1].strip()
        byte_size = int(result.replace("B", ""))
        return {
            "byte_size": byte_size,
        }


class ProccesUnknownKeyword(ProccesLineProtocol):
    def extract_results(self, line) -> dict:
        return {}


@dataclass
class ImageInfo:
    pixel_height: int
    pixel_width: int
    file_extension: str
    mime_type: str
    byte_size: int

    @classmethod
    def create_from_dict(cls, raw_dict: dict) -> "ImageInfo":
        return cls(
            pixel_height=raw_dict["pixel_height"],
            pixel_width=raw_dict["pixel_width"],
            file_extension=raw_dict["file_extension"],
            mime_type=raw_dict["mime_type"],
            byte_size=raw_dict["byte_size"],
        )


class ImageAnalyzerFileType:
    def get_image_info_from_path(self, path) -> ImageInfo:
        result_command = self._file_command_wrapper_identify(path)
        return self._parse_results_identify_command(result_command)

    def _file_command_wrapper_identify(self, path: str) -> str:
        command_list = [
            "identify",
            "-verbose",
            path,
        ]
        process = subprocess.run(command_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        if process.returncode != 0:
            raise Exception(f"{self.__class__.__name__} could not process file {path}")
        result = process.stdout
        return result

    def _parse_results_identify_command(self, results: str) -> ImageInfo:
        meta_data = {}
        for line in results.split("\n"):
            key_word = self.extract_key_word_out_of_line(line)
            line_processor = self.find_result_processor(key_word)
            meta_data.update(line_processor.extract_results(line))
        return ImageInfo.create_from_dict(meta_data)

    def extract_key_word_out_of_line(self, line: str) -> str:
        return line.split(":")[0].strip()

    def find_result_processor(self, keyword: str) -> ProccesLineProtocol:
        proccessors = {
            "Geometry": ProccessPixelSize(),
            "Mime type": ProccessFileType(),
            "Filesize": ProccessByteSize(),
        }
        return proccessors.get(keyword, ProccesUnknownKeyword())
