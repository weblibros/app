from pathlib import Path

import pytest

from src.file_management.domain import EbookFile, FileBlobLocal, FileSize, FileType, ImageFile

from ..service import CoverExtractorService


class TestExtractCoverEbook:
    dummy_file_path = Path("src/file_management/tests/files/dummy-ebook.epub")

    @pytest.fixture()
    async def file_ebook(self):
        ebook = EbookFile(
            file_type=(await FileType.from_path(self.dummy_file_path)),
            file_size=(await FileSize.from_path(self.dummy_file_path)),
            blob=FileBlobLocal(self.dummy_file_path),
            path=Path("/tmp/dummy-ebook.epub"),
        )
        return ebook

    async def test_service(self, file_ebook):
        service = CoverExtractorService()
        result = await service.extract(file_ebook, Path("/tmp"))

        assert isinstance(result, ImageFile)

    async def test_service_saves_file_in_given_path(self, file_ebook):
        service = CoverExtractorService()
        result = await service.extract(file_ebook, Path("/tmp"))

        assert isinstance(result, ImageFile)
        assert result.path == Path("/tmp/cover.jpg")
