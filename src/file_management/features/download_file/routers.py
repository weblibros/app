from typing import Mapping, Optional
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.responses import StreamingResponse
from jose import ExpiredSignatureError, JWTError
from jose.exceptions import JWTClaimsError

from src.file_management import urls
from src.file_management.repositories import FileReadModel
from src.services.jwt import InvalidJwtTokenType, JWTUserDownloadBookToken
from src.tags import ApiTags

download_file_router = APIRouter()

cache_max_age = 60


class StreamingResponseFactory:
    file_read_model = FileReadModel()

    @classmethod
    async def from_file_uuid(
        cls,
        file_uuid: UUID,
        headers: Optional[Mapping[str, str]] = None,
    ) -> StreamingResponse:
        async_file_iterator = await cls.file_read_model.get_file_iterator(file_uuid)
        media_type = await cls.file_read_model.get_media_type(file_uuid) or "application/octet-stream"
        return StreamingResponse(async_file_iterator, headers=headers, media_type=media_type)


def jwt_download_token(token: Optional[str]) -> JWTUserDownloadBookToken:
    no_token_exception = HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="No token provided",
    )
    no_valid_token_exception = HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="Could not validate token",
    )
    expired_exception = HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="Credentials are expired",
    )
    if token is None:
        raise no_token_exception
    else:
        try:
            jwt_token = JWTUserDownloadBookToken(token)
            jwt_token.is_valid(raise_error=True)
        except ExpiredSignatureError:
            raise expired_exception
        except (JWTError, JWTClaimsError):
            raise no_valid_token_exception
        except InvalidJwtTokenType:
            raise no_valid_token_exception
        else:
            return jwt_token


class TitleTransformer:
    def transform(self, value: str) -> str:
        return value.lower().replace(" ", "_")


@download_file_router.get(
    urls.DOWNLOAD_FILE_URL,
    tags=[ApiTags.TAG_FILES],
)
async def download_file(file_uuid: UUID, token: JWTUserDownloadBookToken = Depends(jwt_download_token)):
    if token.payload.file == file_uuid:
        file_name = TitleTransformer().transform(token.payload.title)
        return await StreamingResponseFactory.from_file_uuid(
            file_uuid,
            headers={
                "Cache-Control": f"public, max-age={cache_max_age}",
                "Content-Disposition": f'attachment; filename="{file_name}"',
            },
        )
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Invalid token")
