from uuid import UUID, uuid4

import pytest
from httpx import ASGITransport, AsyncClient

from src.config.database import async_session_maker
from src.file_management import urls
from src.file_management.repositories.blob_repo.services.storage_service import LocalStorageService
from src.file_management.repositories.models import FileRecord
from src.main import app
from src.services.jwt import JWTUserDownloadBookToken


@pytest.fixture
def token_factory():
    def _token_factory(file_record: UUID):
        return JWTUserDownloadBookToken.encode(
            {
                "user": str(uuid4()),
                "book": str(uuid4()),
                "file": str(file_record),
                "title": "fake book title",
            }
        )

    return _token_factory


class TestDownloadFileEndpoint:
    EBOOK_FILE_PATH = "src/file_management/tests/files/dummy-ebook.epub"

    @pytest.fixture
    async def file_record(self):
        async with async_session_maker() as session:
            file_data = {
                "size_bytes": 4771,
                "extension": "epub",
                "content_type": "application/epub+zip",
                "storage_service_name": str(LocalStorageService.name),
                "path": self.EBOOK_FILE_PATH,
                "entity_class_name": "EbookFile",
            }
            file_record = FileRecord(
                uuid=uuid4(),
                **file_data,
            )
            session.add(file_record)

            await session.commit()

        yield file_record
        async with async_session_maker() as session:
            await session.delete(file_record)
            await session.commit()

    async def test_download_cover_endpoint(self, file_record, token_factory):
        async with AsyncClient(transport=ASGITransport(app=app), base_url="http://0.0.0.0:8500") as client:
            response = await client.get(
                f"{urls.DOWNLOAD_FILE_URL.format(file_uuid=file_record.uuid)}?token={token_factory(file_record.uuid)}"
            )
        assert response.status_code == 200
        assert response.read() == open(self.EBOOK_FILE_PATH, "rb").read()

    async def test_download_cover_endpoint_failes_when_no_token_is_given(self, file_record):
        async with AsyncClient(transport=ASGITransport(app=app), base_url="http://0.0.0.0:8500") as client:
            response = await client.get(
                urls.DOWNLOAD_FILE_URL.format(file_uuid=file_record.uuid),
            )
        assert response.status_code == 422
        assert response.json() == {
            "detail": [
                {
                    "input": None,
                    "loc": [
                        "query",
                        "token",
                    ],
                    "msg": "Field required",
                    "type": "missing",
                },
            ],
        }

    async def test_download_cover_endpoint_failes_when_wrong_token_is_given(self, file_record):
        async with AsyncClient(transport=ASGITransport(app=app), base_url="http://0.0.0.0:8500") as client:
            response = await client.get(f"{urls.DOWNLOAD_FILE_URL.format(file_uuid=file_record.uuid)}?token=wrongtoken")
        assert response.status_code == 403
        assert response.json() == {"detail": "Could not validate token"}

    async def test_download_cover_endpoint_failes_when_token_for_other_file_is_given(self, file_record, token_factory):
        async with AsyncClient(transport=ASGITransport(app=app), base_url="http://0.0.0.0:8500") as client:
            response = await client.get(
                f"{urls.DOWNLOAD_FILE_URL.format(file_uuid=file_record.uuid)}?token={token_factory(uuid4())}"
            )
        assert response.status_code == 403
        assert response.json() == {"detail": "Invalid token"}
