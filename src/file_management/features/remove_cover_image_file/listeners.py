import logging
from dataclasses import dataclass
from typing import Self
from uuid import UUID

from fastapi_events.dispatcher import dispatch
from fastapi_events.handlers.local import local_handler
from fastapi_events.typing import Event

from src.books.domain.events import BookEvents
from src.file_management.domain.events import FileManagmentEvents
from src.file_management.repositories import FileRepo

logger = logging.getLogger("default")


@dataclass
class BookRemovedEvent:
    book_uuid: UUID
    image_uuid_list: list[UUID]
    file_uuid_list: list[UUID]

    @classmethod
    def from_event(cls, event: Event) -> Self:
        _, payload = event
        return cls(
            book_uuid=payload["book_uuid"],
            image_uuid_list=payload["image_uuid_list"],
            file_uuid_list=payload["file_uuid_list"],
        )


@local_handler.register(event_name=str(BookEvents.BOOK_REMOVED))
async def cleanup_cover_files(event: Event):
    book_removed_event = BookRemovedEvent.from_event(event)
    file_repo = FileRepo()

    for image_uuid in book_removed_event.image_uuid_list:
        try:
            cover_file = await file_repo.get_by_image_uuid(image_uuid)
            await file_repo.remove(cover_file)
        except Exception:
            logger.info(f"failed to remove image file: {cover_file}")

        else:
            dispatch(
                FileManagmentEvents.COVER_FILE_REMOVED,
                payload={
                    "book_uuid": book_removed_event.book_uuid,
                    "image_uuid": image_uuid,
                },
            )
