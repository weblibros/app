import logging
from dataclasses import dataclass
from typing import Self
from uuid import UUID

from fastapi_events.dispatcher import dispatch
from fastapi_events.handlers.local import local_handler
from fastapi_events.typing import Event

from src.books.domain.events import BookEvents
from src.file_management.domain.events import FileManagmentEvents, FileRecordEventSchema
from src.file_management.repositories import FileRepo
from src.file_management.repositories.blob_repo.services import storage_service_factory

logger = logging.getLogger("default")


@local_handler.register(event_name=str(FileManagmentEvents.FILE_RECORD_REMOVED))
async def cleanup_blob_linked_to_file_record(event: Event):
    _, payload = event
    file_record = FileRecordEventSchema(**payload)

    await _cleanup_blob_linked_to_file_record(file_record.path, file_record.storage_service_name)


async def _cleanup_blob_linked_to_file_record(path: str, storage_service_name: str):
    storage_service = storage_service_factory(storage_service_name)
    await storage_service.remove_blob(path)


@dataclass
class BookRemovedEvent:
    book_uuid: UUID
    image_uuid_list: list[UUID]
    file_uuid_list: list[UUID]

    @classmethod
    def from_event(cls, event: Event) -> Self:
        _, payload = event
        return cls(
            book_uuid=payload["book_uuid"],
            image_uuid_list=payload["image_uuid_list"],
            file_uuid_list=payload["file_uuid_list"],
        )


@local_handler.register(event_name=str(BookEvents.BOOK_REMOVED))
async def cleanup_book_files(event: Event):
    book_removed_event = BookRemovedEvent.from_event(event)
    file_repo = FileRepo()

    for file_uuid in book_removed_event.file_uuid_list:
        try:
            book_file = await file_repo.get(file_uuid)
            await file_repo.remove(book_file)
        except Exception:
            logger.info(f"failed to remove book file: {file_uuid}")

        else:
            dispatch(
                FileManagmentEvents.BOOK_FILE_REMOVED,
                payload={
                    "book_uuid": book_removed_event.book_uuid,
                    "file_uuid": file_uuid,
                },
            )
