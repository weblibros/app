from uuid import uuid4

import pytest

from src.file_management.domain.events import (
    EbookFileImported,
    FailedImportStepResult,
    ImportManagerFailedToImport,
    ProcessedImportStepResult,
    TempEbookFileUploadedEvent,
)
from src.file_management.domain.import_manager import ImportManager, ImportStep
from src.file_management.domain.tests.fake import fake_temp_file_factory
from src.file_management.features.import_ebook_file.handlers import ResultImportProcess
from src.file_management.unit_of_work import InMemoryImportManagerUnitOfWork

from ..handlers import ImportEbookHandler


class FakeProcessedImportStepResult(ProcessedImportStepResult): ...


class FakeSuccessImportStep(ImportStep):
    async def _process(self):
        return FakeProcessedImportStepResult()


class FakeFailImportStep(ImportStep):
    async def _process(self):
        self._error_message = "fake error"
        return FailedImportStepResult()


class FakeImportManagerCompleteWithSucces(ImportManager):
    _step_classes = [
        FakeSuccessImportStep,
        FakeSuccessImportStep,
    ]


class FakeImportManagerCompleteWithStepFailure(ImportManager):
    _step_classes = [
        FakeSuccessImportStep,
        FakeFailImportStep,
        FakeSuccessImportStep,
    ]


@pytest.fixture
def fake_event_and_temp_file():
    temp_file = fake_temp_file_factory()
    event = TempEbookFileUploadedEvent(
        file_uuid=temp_file.uuid,
        uploaded_by_user_uuid=uuid4(),
        destination_library_uuid=uuid4(),
    )
    return event, temp_file


class TestImportEbookHandlerWithSuccessFull:
    async def test_response_handler(self, fake_event_and_temp_file):
        event, temp_file = fake_event_and_temp_file

        handler = ImportEbookHandler(
            uow=InMemoryImportManagerUnitOfWork(files_in_memory=[temp_file]),
            import_manager_class=FakeImportManagerCompleteWithSucces,
        )

        result = await handler.handle(event)

        assert isinstance(result, ResultImportProcess)
        assert result.completed is True

    async def test_manager_is_saved(self, fake_event_and_temp_file):
        event, temp_file = fake_event_and_temp_file
        managers_in_memory = []

        handler = ImportEbookHandler(
            uow=InMemoryImportManagerUnitOfWork(files_in_memory=[temp_file], managers_in_memory=managers_in_memory),
            import_manager_class=FakeImportManagerCompleteWithSucces,
        )

        await handler.handle(event)

        assert len(managers_in_memory) == 1
        assert isinstance(managers_in_memory[0], ImportManager)

    async def test_if_process_has_run(self, fake_event_and_temp_file):
        event, temp_file = fake_event_and_temp_file
        managers_in_memory = []

        handler = ImportEbookHandler(
            uow=InMemoryImportManagerUnitOfWork(files_in_memory=[temp_file], managers_in_memory=managers_in_memory),
            import_manager_class=FakeImportManagerCompleteWithSucces,
        )

        await handler.handle(event)

        manager = managers_in_memory[0]
        assert manager._finished_at is not None

    async def test_if_imported_event_is_fired_off(self, fake_event_and_temp_file):
        event, temp_file = fake_event_and_temp_file

        events = []

        def save_events_dispatch(event):
            events.append(event)

        handler = ImportEbookHandler(
            uow=InMemoryImportManagerUnitOfWork(files_in_memory=[temp_file], dispatch=save_events_dispatch),
            import_manager_class=FakeImportManagerCompleteWithSucces,
        )

        await handler.handle(event)

        assert len(events) == 1
        assert isinstance(events[0], EbookFileImported)


class TestImportEbookHandlerWithStepFailure:
    async def test_response_handler(self, fake_event_and_temp_file):
        event, temp_file = fake_event_and_temp_file

        handler = ImportEbookHandler(
            uow=InMemoryImportManagerUnitOfWork(files_in_memory=[temp_file]),
            import_manager_class=FakeImportManagerCompleteWithStepFailure,
        )

        result = await handler.handle(event)

        assert isinstance(result, ResultImportProcess)
        assert result.completed is False

    async def test_manager_is_saved(self, fake_event_and_temp_file):
        event, temp_file = fake_event_and_temp_file
        managers_in_memory = []

        handler = ImportEbookHandler(
            uow=InMemoryImportManagerUnitOfWork(files_in_memory=[temp_file], managers_in_memory=managers_in_memory),
            import_manager_class=FakeImportManagerCompleteWithStepFailure,
        )

        await handler.handle(event)

        assert len(managers_in_memory) == 1
        assert isinstance(managers_in_memory[0], ImportManager)

    async def test_if_process_has_run(self, fake_event_and_temp_file):
        event, temp_file = fake_event_and_temp_file
        managers_in_memory = []

        handler = ImportEbookHandler(
            uow=InMemoryImportManagerUnitOfWork(files_in_memory=[temp_file], managers_in_memory=managers_in_memory),
            import_manager_class=FakeImportManagerCompleteWithStepFailure,
        )

        await handler.handle(event)

        manager = managers_in_memory[0]
        assert manager._finished_at is not None

    async def test_if_imported_event_is_fired_off(self, fake_event_and_temp_file):
        event, temp_file = fake_event_and_temp_file

        events = []

        def save_events_dispatch(event):
            events.append(event)

        handler = ImportEbookHandler(
            uow=InMemoryImportManagerUnitOfWork(files_in_memory=[temp_file], dispatch=save_events_dispatch),
            import_manager_class=FakeImportManagerCompleteWithStepFailure,
        )

        await handler.handle(event)

        assert len(events) == 1
        assert isinstance(events[0], ImportManagerFailedToImport)
