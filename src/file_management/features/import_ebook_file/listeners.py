from fastapi_events.handlers.local import local_handler
from fastapi_events.typing import Event

from src.file_management.domain.events import FileManagmentEvents, TempEbookFileUploadedEvent
from src.file_management.unit_of_work import SqlAlchemyImportManagerUnitOfWork

from .handlers import ImportEbookHandler


@local_handler.register(event_name=str(FileManagmentEvents.TEMP_EBOOK_FILE_UPLOADED))
async def import_ebook_from_uploaded_ebook_file(event: Event):
    _, payload = event
    ebook_uploaded_event = TempEbookFileUploadedEvent(**payload)
    handler = ImportEbookHandler(
        uow=SqlAlchemyImportManagerUnitOfWork(),
    )

    await handler.handle(ebook_uploaded_event)
