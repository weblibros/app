from dataclasses import dataclass
from functools import singledispatchmethod
from typing import Type

from src.file_management.domain import EbookImportManager, ImportFrom, ImportManager
from src.file_management.domain.events import TempEbookFileLocallyLoadedEvent, TempEbookFileUploadedEvent
from src.file_management.unit_of_work import AbstractImportManagerUnitOfWork


@dataclass
class ResultImportProcess:
    completed: bool


class ImportEbookHandler:
    def __init__(
        self,
        uow: AbstractImportManagerUnitOfWork,
        import_manager_class: Type[ImportManager] = EbookImportManager,
    ):
        self.uow = uow
        self._import_manager_class = import_manager_class

    async def __call__(self, event) -> ResultImportProcess:
        return await self.handle(event)

    @singledispatchmethod
    async def handle(self, event) -> ResultImportProcess:
        raise NotImplementedError("event cannot be handled")

    @handle.register
    async def _(self, event: TempEbookFileUploadedEvent) -> ResultImportProcess:
        async with self.uow as uow:
            temp_file = await uow.files.get(event.file_uuid)
            import_manager = self._import_manager_class(
                temp_file=temp_file,
                library_uuid=event.destination_library_uuid,
                uploaded_by_user_uuid=event.uploaded_by_user_uuid,
                imported_from=ImportFrom.WEB_INTERFACE,
            )

            await import_manager.process()

            for file in import_manager.generated_files:
                await uow.files.add(file)
            await uow.import_managers.add(import_manager)

            await uow.commit()
        return ResultImportProcess(completed=import_manager.successfully_completed)

    @handle.register
    async def _(self, event: TempEbookFileLocallyLoadedEvent) -> ResultImportProcess:
        temp_file = event.temp_file
        async with self.uow as uow:
            import_manager = self._import_manager_class(
                temp_file=temp_file,
                library_uuid=event.destination_library_uuid,
                uploaded_by_user_uuid=event.uploaded_by_user_uuid,
                imported_from=ImportFrom.CLI,
            )

            await import_manager.process()

            for file in import_manager.generated_files:
                await uow.files.add(file)
            await uow.import_managers.add(import_manager)

            await uow.commit()
        return ResultImportProcess(completed=import_manager.successfully_completed)
