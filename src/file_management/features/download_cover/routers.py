import logging
import typing
from typing import TYPE_CHECKING
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import StreamingResponse

from src.file_management import urls
from src.file_management.repositories import ImageFileReadRepo
from src.services.authentication import active_cookie_user
from src.tags import ApiTags

if TYPE_CHECKING:
    from src.accounts.users import User

logger = logging.getLogger("default")
cover_router = APIRouter()

cache_max_age = 3600 * 24


async def valid_image_uuid(
    image_uuid: UUID,
    user: "User" = Depends(active_cookie_user),
):
    user_libraries = user.get_libraries()
    has_access_to_image = await ImageFileReadRepo().image_is_cover_in_libraries(image_uuid, user_libraries)
    if has_access_to_image:
        return image_uuid
    else:
        raise HTTPException(status_code=403, detail="No permissions for image")


class StreamingResponseFactory:
    image_repo = ImageFileReadRepo()

    @classmethod
    async def from_image_uuid(
        cls,
        image_uuid: UUID,
        headers: typing.Optional[typing.Mapping[str, str]] = None,
    ) -> StreamingResponse:
        return StreamingResponse(await cls.image_repo.stream_blob(image_uuid), headers=headers)


@cover_router.get(
    urls.DOWNLOAD_COVER_IMAGE_URL,
    tags=[ApiTags.TAG_FILES],
)
async def download_cover_image(
    image_uuid: UUID = Depends(valid_image_uuid),
):
    return await StreamingResponseFactory.from_image_uuid(
        image_uuid, headers={"Cache-Control": f"public, max-age={cache_max_age}"}
    )
