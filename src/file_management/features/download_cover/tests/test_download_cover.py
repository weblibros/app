from uuid import uuid4

import pytest
from httpx import ASGITransport, AsyncClient, Cookies

from src.accounts.users.user import Membership, User
from src.books.repositories.models import BookModel, CoverModel
from src.config.database import async_session_maker
from src.file_management import urls
from src.file_management.repositories.blob_repo.services.storage_service import LocalStorageService
from src.file_management.repositories.models import FileRecord, ImageRecord
from src.libraries.membership_roles import MembershipRole
from src.libraries.repositories.models import Library, LibraryBook
from src.main import app
from src.services.jwt import JWTUserAccesToken


def user_token_cookie(user):
    cookies = Cookies()
    cookies.set("webLibrosJwtAccessToken", str(JWTUserAccesToken.encode_user(user)))
    return cookies


class TestDownloadCoverEndpoint:
    PNG_FILE_PATH = "src/file_management/tests/files/random_image.png"

    @pytest.fixture
    async def cover_image(self):
        async with async_session_maker() as session:
            file_data = {
                "size_bytes": 4771,
                "extension": "png",
                "content_type": "image/png",
                "storage_service_name": str(LocalStorageService.name),
                "path": "/tmp/not_find" + self.PNG_FILE_PATH,
                "entity_class_name": "ImageFile",
            }
            file_record_cover = FileRecord(
                uuid=uuid4(),
                **file_data,
            )
            image = ImageRecord(
                uuid=uuid4(),
                file_record_uuid=file_record_cover.uuid,
                pixel_height=200,
                pixel_width=200,
            )
            session.add_all([file_record_cover, image])

            await session.commit()

        yield image
        async with async_session_maker() as session:
            await session.delete(file_record_cover)
            # await session.delete(image)
            await session.commit()

    @pytest.fixture
    async def cover_image_with_user(self):
        book = BookModel(uuid=uuid4(), title="fake book")
        library = Library(uuid=uuid4(), name="fake lib")
        library_book = LibraryBook(library_uuid=library.uuid, book_uuid=book.uuid)
        user = User(
            uuid=uuid4(),
            is_active=True,
            memberships=[
                Membership(
                    library_uuid=uuid4(),
                    membership_role=MembershipRole.OWNER,
                ),
                Membership(
                    library_uuid=library.uuid,
                    membership_role=MembershipRole.VIEWER,
                ),
            ],
        )
        file_data = {
            "size_bytes": 4771,
            "extension": "png",
            "content_type": "image/png",
            "storage_service_name": str(LocalStorageService.name),
            "path": self.PNG_FILE_PATH,
            "entity_class_name": "ImageFile",
        }
        file_record_cover = FileRecord(
            uuid=uuid4(),
            **file_data,
        )
        image = ImageRecord(
            uuid=uuid4(),
            file_record_uuid=file_record_cover.uuid,
            pixel_height=200,
            pixel_width=200,
        )
        cover = CoverModel(book_uuid=book.uuid, image_uuid=image.uuid, static_file_path="fake")
        async with async_session_maker() as session:
            session.add_all(
                [
                    file_record_cover,
                    image,
                    book,
                    library,
                    cover,
                ]
            )
            await session.flush()
            session.add(
                library_book,
            )
            await session.commit()

        yield image, user
        async with async_session_maker() as session:
            await session.delete(file_record_cover)
            # await session.delete(cover)
            # await session.delete(image)
            await session.delete(library_book)
            await session.delete(book)
            await session.delete(library)
            await session.commit()

    async def test_download_cover_endpoint(self, cover_image_with_user):
        cover_image, user = cover_image_with_user
        async with AsyncClient(
            transport=ASGITransport(app=app), cookies=user_token_cookie(user), base_url="http://0.0.0.0:8500"
        ) as client:
            response = await client.get(
                urls.DOWNLOAD_COVER_IMAGE_URL.format(image_uuid=cover_image.uuid),
            )
        assert response.status_code == 200
        assert response.read() == open(self.PNG_FILE_PATH, "rb").read()

    async def test_unauth_download_request(self, client, cover_image):
        response = await client.get(
            urls.DOWNLOAD_COVER_IMAGE_URL.format(image_uuid=cover_image.uuid),
        )
        assert response.status_code == 401

    async def test_download_cover_endpoint_no_permissions(self, client, auth_header_user, cover_image):
        user = User(
            uuid=uuid4(),
            is_active=True,
            memberships=[
                Membership(
                    library_uuid=uuid4(),
                    membership_role=MembershipRole.OWNER,
                ),
            ],
        )
        async with AsyncClient(
            transport=ASGITransport(app=app), cookies=user_token_cookie(user), base_url="http://0.0.0.0:8500"
        ) as client:
            response = await client.get(
                urls.DOWNLOAD_COVER_IMAGE_URL.format(image_uuid=cover_image.uuid),
            )
        assert response.status_code == 403
