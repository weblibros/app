import io
from pathlib import Path
from typing import TYPE_CHECKING, Self
from unittest.mock import MagicMock
from uuid import uuid4

import pytest
from fastapi import UploadFile
from fastapi.datastructures import Headers

from src.config import settings
from src.file_management.domain import TempFile
from src.file_management.domain.events import UploadEbookFileToTempStorageCommand
from src.file_management.unit_of_work import AbstractFileUnitOfWork, InMemoryFileUnitOfWork

from ....repositories import AbstractFileRepo
from ..handlers import FailedUploadExecption, UploadTempFileHandler

if TYPE_CHECKING:
    from uuid import UUID

    from src.file_management.domain import File
dummy_ebook_file_path = "src/file_management/tests/files/dummy-ebook.epub"


def fake_ebook_as_upload_file(file_path) -> UploadFile:
    with open(file_path, "rb") as file:
        file_b = io.BytesIO(file.read())
        file_size = len(file_b.getvalue())
        upload_file = UploadFile(
            file=file_b,
            size=file_size,
            filename="Fake" + Path(file_path).suffix,
            headers=Headers(headers={"content-type": "application/epub+zip"}),
        )
    return upload_file


@pytest.fixture
def file_upload_command_with_ebook():
    return UploadEbookFileToTempStorageCommand(
        upload_file=fake_ebook_as_upload_file(dummy_ebook_file_path),
        user_uuid=uuid4(),
        library_uuid=uuid4(),
    )


class FakeUOW(AbstractFileUnitOfWork):
    class FakeRepo(AbstractFileRepo):
        async def add(self, file: "File"):
            raise Exception("fail add")

        async def get(self, uuid: "UUID") -> "File":
            raise Exception("fail get")

        async def remove(self, file: "File"):
            raise Exception("fail remove")

    def __init__(self, files_in_memory: list["File"] = [], dispatch=MagicMock()):
        self._files_in_memory = files_in_memory
        self.committed = False
        self._dispatch = dispatch

    async def __aenter__(self) -> Self:
        self.files = self.FakeRepo()
        return await super().__aenter__()

    async def _commit(self):
        self.committed = True

    async def rollback(self):
        pass


class TestUploadFileHandler:
    handler_class = UploadTempFileHandler

    async def test_upload_file_saved_in_memory(self, file_upload_command_with_ebook):
        in_memory = []
        handler = self.handler_class(InMemoryFileUnitOfWork(in_memory))
        result = await handler(file_upload_command_with_ebook)
        assert handler.uow.committed is True
        assert len(in_memory) == 1
        assert in_memory[0].uuid == result.temp_file.uuid

    async def test_correct_path_off_temp_file(self, file_upload_command_with_ebook):
        command = file_upload_command_with_ebook
        in_memory = []
        handler = self.handler_class(InMemoryFileUnitOfWork(in_memory))
        result = await handler(file_upload_command_with_ebook)
        assert str(
            Path(settings.STORAGE_BASE_DIR).joinpath(
                "temporary_uploads",
                f"user-{command.user_uuid}",
            )
        ) in str(result.temp_file.path)

    async def test_temp_file_returned_as_result(self, file_upload_command_with_ebook):
        handler = self.handler_class(InMemoryFileUnitOfWork([]))
        result = await handler(file_upload_command_with_ebook)
        assert handler.uow.committed is True
        assert isinstance(result.temp_file, TempFile)

    async def test_event_dispatch(self, file_upload_command_with_ebook):
        mock_dispatch = MagicMock()

        handler = self.handler_class(InMemoryFileUnitOfWork(files_in_memory=[], dispatch=mock_dispatch))
        await handler(file_upload_command_with_ebook)
        assert handler.uow.committed is True
        mock_dispatch.assert_called_once()

    async def test_raise_error_on_no_ebook_file(self, file_upload_command_with_ebook):
        handler = self.handler_class(FakeUOW())
        with pytest.raises(FailedUploadExecption) as e:
            await handler(file_upload_command_with_ebook)
        assert str(e.value) == "fail add"
        assert handler.uow.committed is False

    async def test_event_not_send_on_raised_error(self, file_upload_command_with_ebook):
        mock_dispatch = MagicMock()

        handler = self.handler_class(FakeUOW(dispatch=mock_dispatch))
        with pytest.raises(FailedUploadExecption) as e:
            await handler(file_upload_command_with_ebook)
        assert str(e.value) == "fail add"
        assert handler.uow.committed is False
        mock_dispatch.assert_not_called()
