import os
from pathlib import Path
from uuid import UUID

import pytest
from httpx import Headers
from sqlalchemy import delete

from src.config import settings
from src.config.database import async_session_maker
from src.file_management import urls
from src.file_management.repositories.models import FileRecord
from src.file_management.repositories.queries.selector import find_file_record_by_uuid
from src.file_management.tests.cleanup import remove_files_in_dir
from src.services.jwt import JWTUserAccesToken


@pytest.fixture()
async def auth_header_user_with_cleanup(user):
    token = JWTUserAccesToken.encode_user(user)

    yield Headers({"Authorization": f"Bearer {token}"})
    storage_dir = Path(settings.STORAGE_BASE_DIR).joinpath(
        "temporary_uploads",
        f"user-{user.uuid}",
    )

    remove_files_in_dir(storage_dir)
    async with async_session_maker() as session:
        delete_query = delete(FileRecord).where(FileRecord.path.contains(str(storage_dir)))
        await session.execute(delete_query)
        await session.commit()


class TestFileUpload:
    dummy_file_path = "src/file_management/tests/files/dummy-ebook.epub"
    overwrite_file_name = "fake_overwrite_file.epub"

    async def test_upload_file_status_code(self, client, auth_header_user_with_cleanup):
        auth_header_user = auth_header_user_with_cleanup
        with open(self.dummy_file_path, "rb") as f:
            file = {"file": f}
            response = await client.post(
                urls.EBOOK_UPLOAD_URL,
                files=file,
                headers=auth_header_user,
            )

        assert response.status_code == 201

    async def test_upload_file_without_file(self, client, auth_header_user_with_cleanup):
        auth_header_user = auth_header_user_with_cleanup
        response = await client.post(
            urls.EBOOK_UPLOAD_URL,
            headers=auth_header_user,
        )
        assert response.status_code == 422

    async def test_upload_file_without_user(
        self,
        client,
    ):
        response = await client.post(
            urls.EBOOK_UPLOAD_URL,
        )
        assert response.status_code == 401

    async def test_upload_file_correct_response_structure(self, client, auth_header_user_with_cleanup):
        auth_header_user = auth_header_user_with_cleanup
        with open(self.dummy_file_path, "rb") as f:
            file = {"file": f}
            response = await client.post(
                urls.EBOOK_UPLOAD_URL,
                files=file,
                headers=auth_header_user,
            )

        response_data = response.json()
        assert "uuid" in response_data
        assert isinstance(response_data["uuid"], str)
        assert isinstance(UUID(response_data["uuid"]), UUID)
        assert "size_bytes" in response_data
        assert isinstance(response_data["size_bytes"], int)
        assert "content_type" in response_data
        assert isinstance(response_data["content_type"], str)

    async def test_upload_file_correct_response_data(self, client, auth_header_user_with_cleanup):
        auth_header_user = auth_header_user_with_cleanup
        expected_file_name = self.overwrite_file_name
        overwrite_content_type = "test/epub+zip"
        with open(self.dummy_file_path, "rb") as f:
            file = {"file": (expected_file_name, f, overwrite_content_type)}
            response = await client.post(
                urls.EBOOK_UPLOAD_URL,
                files=file,
                headers=auth_header_user,
            )

        response_data = response.json()
        assert response_data["content_type"] != overwrite_content_type
        assert response_data["content_type"] == "application/epub+zip"

        file_stats = os.stat(self.dummy_file_path)
        assert response_data["size_bytes"] == file_stats.st_size

    async def test_record_file_stored(self, client, auth_header_user_with_cleanup):
        auth_header_user = auth_header_user_with_cleanup
        expected_file_name = self.overwrite_file_name
        overwrite_content_type = "test/epub+zip"
        with open(self.dummy_file_path, "rb") as f:
            file = {"file": (expected_file_name, f, overwrite_content_type)}
            response = await client.post(
                urls.EBOOK_UPLOAD_URL,
                files=file,
                headers=auth_header_user,
            )

        response_data = response.json()

        async with async_session_maker() as session:
            file_record = await find_file_record_by_uuid(session, UUID(response_data["uuid"]))
        assert file_record is not None
        assert isinstance(file_record, FileRecord)
