from dataclasses import dataclass
from functools import singledispatchmethod
from pathlib import Path
from uuid import uuid4

from src.config import settings
from src.file_management.domain import FileBlobUpload, FileSize, FileType, TempFile
from src.file_management.domain.events import TempEbookFileUploadedEvent, UploadEbookFileToTempStorageCommand
from src.file_management.unit_of_work import AbstractFileUnitOfWork


@dataclass
class EbookFileUploadedResult:
    temp_file: TempFile


class FailedUploadExecption(Exception): ...


class UploadTempFileHandler:
    def __init__(self, uow: AbstractFileUnitOfWork):
        self.uow = uow

    async def __call__(self, event) -> EbookFileUploadedResult:
        return await self.handle(event)

    @singledispatchmethod
    async def handle(command) -> EbookFileUploadedResult:
        raise NotImplementedError("event cannot be handled")

    @handle.register
    async def _(self, command: UploadEbookFileToTempStorageCommand) -> EbookFileUploadedResult:
        try:
            temp_file = TempFile(
                file_type=(await FileType.from_upload_file(command.upload_file)),
                file_size=(await FileSize.from_upload_file(command.upload_file)),
                blob=FileBlobUpload(command.upload_file),
                path=self._generate_path(command),
            )
            temp_file.events.append(
                TempEbookFileUploadedEvent(
                    file_uuid=temp_file.uuid,
                    uploaded_by_user_uuid=command.user_uuid,
                    destination_library_uuid=command.library_uuid,
                )
            )

            async with self.uow:
                await self.uow.files.add(temp_file)
                await self.uow.commit()
            result = EbookFileUploadedResult(temp_file=temp_file)
        except Exception as e:
            raise FailedUploadExecption(e)

        else:
            return result

    def _generate_path(self, command: UploadEbookFileToTempStorageCommand) -> Path:
        return Path(settings.STORAGE_BASE_DIR).joinpath(
            "temporary_uploads",
            f"user-{command.user_uuid}",
            f"{uuid4().hex[0:10]}-{command.upload_file.filename}",
        )
