import logging
from typing import TYPE_CHECKING
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, UploadFile, status
from pydantic import BaseModel

from src.file_management import urls
from src.file_management.domain.events import UploadEbookFileToTempStorageCommand
from src.file_management.unit_of_work import SqlAlchemyFileUnitOfWork
from src.services.authentication import active_user
from src.tags import ApiTags

from .handlers import FailedUploadExecption, UploadTempFileHandler

if TYPE_CHECKING:
    from src.accounts.users import User

logger = logging.getLogger("default")
upload_file_router = APIRouter()


class ResponseUploadSchema(BaseModel):
    uuid: UUID
    size_bytes: int
    content_type: str


@upload_file_router.post(
    urls.EBOOK_UPLOAD_URL,
    status_code=status.HTTP_201_CREATED,
    tags=[ApiTags.TAG_FILES],
)
async def upload_ebook(
    file: UploadFile,
    user: "User" = Depends(active_user),
) -> ResponseUploadSchema:
    command = UploadEbookFileToTempStorageCommand(
        upload_file=file,
        user_uuid=user.uuid,
        library_uuid=user.personal_library_uuid,
    )
    handler = UploadTempFileHandler(SqlAlchemyFileUnitOfWork())

    try:
        result = await handler(command)
    except FailedUploadExecption:
        raise HTTPException(
            status_code=415,
            detail="Failed to upload file",
        )

    logger.info(f"ebook file uploaded by user: {user.uuid}")

    return ResponseUploadSchema(
        uuid=result.temp_file.uuid,
        size_bytes=result.temp_file.size_bytes,
        content_type=result.temp_file.content_type,
    )
