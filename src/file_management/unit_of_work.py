import abc
from typing import TYPE_CHECKING, Callable, Self

from fastapi_events.dispatcher import dispatch
from sqlalchemy.ext.asyncio import AsyncSession

from src.config.database import async_session_maker
from src.events import Event

from .repositories import (
    AbstractFileRepo,
    AbstractImportManagerRepo,
    FileRepo,
    InMemoryFileRepo,
    InMemoryImportManagerRepo,
    SqlImportManagerRepo,
)

if TYPE_CHECKING:
    from .domain import File, ImportManager


class AbstractFileUnitOfWork(abc.ABC):
    files: AbstractFileRepo
    _dispatch: Callable[[Event], None]

    async def __aenter__(self) -> Self:
        return self

    async def __aexit__(self, *args):
        await self.rollback()

    async def commit(self):
        await self._commit()
        self.publish_events()

    @abc.abstractmethod
    async def _commit(self):
        raise NotImplementedError

    @abc.abstractmethod
    async def rollback(self):
        raise NotImplementedError

    def publish_events(self):
        for file in self.files.seen:
            while file.events:
                event = file.events.pop(0)
                self._dispatch(event)


def empty_dispatch(event: Event):
    pass


class InMemoryFileUnitOfWork(AbstractFileUnitOfWork):
    def __init__(self, files_in_memory: list["File"] = [], dispatch=empty_dispatch):
        self._files_in_memory = files_in_memory
        self.committed = False
        self._dispatch = dispatch

    async def __aenter__(self) -> Self:
        self.files = InMemoryFileRepo(self._files_in_memory)
        return await super().__aenter__()

    async def _commit(self):
        self.committed = True

    async def rollback(self):
        pass


class SqlAlchemyFileUnitOfWork(AbstractFileUnitOfWork):
    async_session: AsyncSession

    def __init__(self, async_sessionmaker=async_session_maker, dispatch=dispatch):
        self.async_sessionmaker = async_sessionmaker
        self._dispatch = dispatch

    async def __aenter__(self) -> Self:
        self.async_session = self.async_sessionmaker()
        self.files = FileRepo(self.async_session)
        return await super().__aenter__()

    async def __aexit__(self, *args):
        await super().__aexit__(*args)
        await self.async_session.close()

    async def _commit(self):
        await self.async_session.commit()

    async def rollback(self):
        await self.async_session.rollback()


class AbstractImportManagerUnitOfWork(abc.ABC):
    files: AbstractFileRepo
    import_managers: AbstractImportManagerRepo
    _dispatch: Callable[[Event], None]

    async def __aenter__(self) -> Self:
        return self

    async def __aexit__(self, *args):
        await self.rollback()

    async def commit(self):
        await self._commit()
        self.publish_events()

    @abc.abstractmethod
    async def _commit(self):
        raise NotImplementedError

    @abc.abstractmethod
    async def rollback(self):
        raise NotImplementedError

    def publish_events(self):
        for manager in self.import_managers.seen:
            while manager.events:
                event = manager.events.pop(0)
                self._dispatch(event)


class InMemoryImportManagerUnitOfWork(AbstractImportManagerUnitOfWork):
    def __init__(
        self,
        files_in_memory: list["File"] = [],
        managers_in_memory: list["ImportManager"] = [],
        dispatch=empty_dispatch,
    ):
        self._files_in_memory = files_in_memory
        self._managers_in_memory = managers_in_memory
        self.committed = False
        self._dispatch = dispatch

    async def __aenter__(self) -> Self:
        self.files = InMemoryFileRepo(self._files_in_memory)
        self.import_managers = InMemoryImportManagerRepo(self._managers_in_memory)
        return await super().__aenter__()

    async def _commit(self):
        self.committed = True

    async def rollback(self):
        pass


class SqlAlchemyImportManagerUnitOfWork(AbstractImportManagerUnitOfWork):
    async_session: AsyncSession

    def __init__(self, async_sessionmaker=async_session_maker, dispatch=dispatch):
        self.async_sessionmaker = async_sessionmaker
        self._dispatch = dispatch

    async def __aenter__(self) -> Self:
        self.async_session = self.async_sessionmaker()
        self.files = FileRepo(self.async_session)
        self.import_managers = SqlImportManagerRepo(self.async_session)
        return await super().__aenter__()

    async def __aexit__(self, *args):
        await super().__aexit__(*args)
        await self.async_session.close()

    async def _commit(self):
        await self.async_session.commit()

    async def rollback(self):
        await self.async_session.rollback()
