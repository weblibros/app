from src.file_management.tests.fakers import fake_small_upload_file


def test_fake_small_upload_file():
    fake_upload_file = fake_small_upload_file()
    assert fake_upload_file.size == 28
    assert fake_upload_file.content_type == "application/epub+zip"
    assert fake_upload_file.filename == "fake_small_bytes.epub"
