import io

from fastapi import UploadFile
from fastapi.datastructures import Headers


def fake_small_upload_file(fake_filename: str = "fake_small_bytes.epub") -> UploadFile:
    fake_bytes = io.BytesIO(b"some initial binary data: \x00\x01")
    fake_size = len(fake_bytes.getvalue())
    return UploadFile(
        file=fake_bytes,
        size=fake_size,
        filename=fake_filename,
        headers=Headers(headers={"content-type": "application/epub+zip"}),
    )
