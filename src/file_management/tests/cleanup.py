import os

from src.config import settings


def remove_files_in_dir(directory: str):
    if os.path.isdir(directory) and directory != settings.STORAGE_BASE_DIR:
        for file in os.listdir(directory):
            path = f"{directory}/{file}"
            if os.path.isfile(path):
                os.remove(path)
            else:
                remove_files_in_dir(path)
        os.rmdir(directory)
