import asyncio
import os
from collections.abc import AsyncGenerator
from contextlib import asynccontextmanager
from datetime import datetime
from typing import Any, Callable

import pytest
import pytest_asyncio
from httpx import ASGITransport, AsyncClient, Headers

from src.accounts.repositories import UserRepo
from src.accounts.repositories.queries.create import create_user
from src.accounts.users import User
from src.config.database import async_session_maker, get_async_session, init_test_db
from src.libraries.features.create_personal_library import create_personal_library
from src.libraries.repositories import LibraryWriteRepo
from src.main import app
from src.services.jwt import JWTToken, JWTUserAccesToken


def bearer_token_header(token: JWTToken) -> Headers:
    return Headers({"Authorization": f"Bearer {token}"})


@pytest.fixture(scope="session")
async def user() -> AsyncGenerator[User, None]:
    async with async_session_maker() as session:
        user_model = await create_user(
            session,
            email="user@fake.com",
            password="supersecret",
            is_active=True,
            email_verified_on=datetime(2000, 1, 1, 1, 00, 00, 0),
        )
        await session.commit()
    library = await create_personal_library(user_model.uuid)
    user = await UserRepo().get_by_uuid(user_model.uuid)
    yield user
    await LibraryWriteRepo().delete_library(library)
    async with async_session_maker() as session:
        await session.delete(user_model)
        await session.commit()


@pytest.fixture(scope="session")
async def unactive_user() -> AsyncGenerator[User, None]:
    async with async_session_maker() as session:
        user_model = await create_user(
            session,
            email="unactive_user@fake.com",
            password="supersecret",
            is_active=False,
        )
        await session.commit()
    library = await create_personal_library(user_model.uuid)
    user = await UserRepo().get_by_uuid(user_model.uuid)
    yield user
    await LibraryWriteRepo().delete_library(library)
    async with async_session_maker() as session:
        await session.delete(user_model)
        await session.commit()


@pytest.fixture(scope="session")
async def access_token_user(user):
    yield JWTUserAccesToken.encode_user(user)


@pytest.fixture(scope="session")
async def auth_header_user(access_token_user):
    yield bearer_token_header(access_token_user)


@pytest.fixture(scope="session")
async def auth_header_unactive_user(unactive_user):
    yield bearer_token_header(JWTUserAccesToken.encode_user(unactive_user))


@pytest.fixture(scope="session")
def event_loop():
    policy = asyncio.get_event_loop_policy()
    loop = policy.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
async def session():
    async_session = get_async_session()
    async for session in async_session:
        yield session


@pytest.fixture(scope="session", autouse=True)
async def migrate_test_db():
    await init_test_db()


@asynccontextmanager
async def _async_client_override_factory(overrides: dict[Callable[..., Any], Callable[..., Any]] = {}):
    app.dependency_overrides = overrides

    transport = ASGITransport(app=app)
    client_generator = AsyncClient(transport=transport, base_url="http://0.0.0.0:8500")
    yield client_generator
    app.dependency_overrides = {}


@pytest.fixture()
def client_factory():
    return _async_client_override_factory


@pytest_asyncio.fixture(scope="session")
async def client():
    async with _async_client_override_factory({}) as client:
        yield client


@pytest.fixture(scope="session", autouse=True)
def disable_events_for_testing():
    os.environ["FASTAPI_EVENTS_DISABLE_DISPATCH"] = "1"
