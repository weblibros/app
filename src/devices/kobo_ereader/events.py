from dataclasses import dataclass

from src.devices.service.dto import RequestDTO
from src.events import Command


@dataclass
class SyncBookToKoboDeviceCommand(Command):
    inbound_request: RequestDTO
