from fastapi import Request
from starlette.datastructures import UploadFile

from src.devices.service.dto import (
    Body,
    BodyArgs,
    BodyArgsUploadFile,
    Cookie,
    Cookies,
    EmptyBody,
    FormBody,
    Header,
    Headers,
    JsonBody,
    RequestDTO,
    RequestMethod,
    Url,
)


async def map_fastapi_request_to_dto_request(request: Request) -> RequestDTO:
    body: Body
    if request.headers.get("content-type") == "application/json":
        if await request.body():
            json_body = await request.json()
            body = JsonBody(body_args=[BodyArgs(key=key, value=value) for key, value in json_body.items()])
        else:
            body = EmptyBody(body_args=[])
    elif request.headers.get("content-type") == "application/x-www-form-urlencoded":
        form_body = await request.form()
        body_args: list[BodyArgs | BodyArgsUploadFile] = []
        for key, value in form_body.items():
            if isinstance(value, str):
                body_args.append(BodyArgs(key=key, value=value))
            elif isinstance(value, UploadFile):
                body_args.append(BodyArgsUploadFile(key=key, value=value))

        body = FormBody(body_args=body_args)
    else:
        body = EmptyBody(body_args=[])

    return RequestDTO(
        url=Url(str(request.url)),
        body=body,
        method=RequestMethod(value=request.method),
        cookies=Cookies(
            cookies=[Cookie(value=value, key=key) for key, value in request.cookies.items()],
        ),
        headers=Headers(headers=[Header(key=key, value=value) for key, value in request.headers.items()]),
    )
