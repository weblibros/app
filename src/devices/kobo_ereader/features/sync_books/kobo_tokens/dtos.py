import json
from collections import OrderedDict
from dataclasses import dataclass, field
from datetime import datetime, timezone
from typing import TYPE_CHECKING, Any, Optional
from uuid import UUID

if TYPE_CHECKING:
    from src.devices.kobo_ereader.features.sync_books.dtos import BookToSyncDto


@dataclass
class EntitlementInfo:
    skip_created: bool = False
    created_count: int = 0
    created: list[UUID] = field(default_factory=list)
    skip_modified: bool = False
    modified: list[UUID] = field(default_factory=list)

    def to_dict(self) -> OrderedDict:
        return OrderedDict(
            [
                ("SkipCreated", self.skip_created),
                ("CreatedCount", self.created_count),
                ("Created", [str(created) for created in self.created]),
                ("SkipModified", self.skip_modified),
                ("Modified", [str(modified) for modified in self.modified]),
            ]
        )

    def to_json_string(self) -> str:
        return json.dumps(self.to_dict()).replace(" ", "")


@dataclass
class KoboSyncInternalInfoState:
    is_initial: Optional[str | bool] = None
    generation_time: Optional[datetime] = None
    timestamp: datetime = datetime(1900, 1, 1, tzinfo=timezone.utc)
    check_sum: Optional[EntitlementInfo] = None
    id: UUID = UUID("00000000-0000-0000-0000-000000000000")

    def to_dict(self) -> OrderedDict:
        return OrderedDict(
            [
                ("IsInitial", self.is_initial),
                (
                    "GenerationTime",
                    self.generation_time.strftime("%Y-%m-%dT%H:%M:%SZ") if self.generation_time else None,
                ),
                ("Timestamp", self.timestamp.strftime("%Y-%m-%dT%H:%M:%SZ")),
                ("CheckSum", self.check_sum.to_json_string() if self.check_sum else None),
                ("Id", str(self.id)),
            ]
        )

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, KoboSyncInternalInfoState):
            return False
        else:
            return self.to_dict() == other.to_dict()


@dataclass
class KoboInternalSyncToken:
    subscription_entitlements: KoboSyncInternalInfoState = field(default_factory=KoboSyncInternalInfoState)
    future_subscription_entitlements: Optional[Any] = None
    entitlements: KoboSyncInternalInfoState = field(default_factory=KoboSyncInternalInfoState)
    deleted_entitlements: KoboSyncInternalInfoState = field(default_factory=KoboSyncInternalInfoState)
    reading_states: KoboSyncInternalInfoState = field(default_factory=KoboSyncInternalInfoState)
    tags: KoboSyncInternalInfoState = field(default_factory=KoboSyncInternalInfoState)
    deleted_tags: KoboSyncInternalInfoState = field(default_factory=KoboSyncInternalInfoState)
    product_metadata: KoboSyncInternalInfoState = field(default_factory=KoboSyncInternalInfoState)

    def to_dict(self) -> OrderedDict:
        return OrderedDict(
            [
                ("SubscriptionEntitlements", self.subscription_entitlements.to_dict()),
                ("FutureSubscriptionEntitlements", None),
                ("Entitlements", self.entitlements.to_dict()),
                ("DeletedEntitlements", self.deleted_entitlements.to_dict()),
                ("ReadingStates", self.reading_states.to_dict()),
                ("Tags", self.tags.to_dict()),
                ("DeletedTags", self.deleted_tags.to_dict()),
                ("ProductMetadata", self.product_metadata.to_dict()),
            ]
        )

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, KoboInternalSyncToken):
            return False
        else:
            return self.to_dict() == other.to_dict()


@dataclass
class KoboSyncToken:
    internal_sync_token: KoboInternalSyncToken = field(default_factory=KoboInternalSyncToken)
    is_continuation_token: bool = False

    def to_dict(self) -> OrderedDict:
        return OrderedDict(
            [
                ("InternalSyncToken", self.internal_sync_token.to_dict()),
                ("IsContinuationToken", self.is_continuation_token),
            ]
        )

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, KoboSyncToken):
            return False
        else:
            return self.to_dict() == other.to_dict()

    def add_books_to_sync(self, books_to_sync: list["BookToSyncDto"]):
        check_sum_entitlements = self.internal_sync_token.entitlements.check_sum or EntitlementInfo()
        for book_to_sync in books_to_sync:
            if book_to_sync.uuid not in check_sum_entitlements.created:
                check_sum_entitlements.created.append(book_to_sync.uuid)
                check_sum_entitlements.created_count += 1
        self.internal_sync_token.entitlements.check_sum = check_sum_entitlements
        self.is_continuation_token = True
