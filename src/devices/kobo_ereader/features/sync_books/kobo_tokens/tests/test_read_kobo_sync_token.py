import pytest

from ..token_readers import KoboTokenDecoder, KoboTokenReader
from .real_kobo_tokens import (
    FROM_DEVIDE_TO_KOBO_INTERNAL_SYNC_TOKEN,
    FROM_DEVIDE_TO_KOBO_SYNC_TOKEN,
    RESPONSE_FROM_KOBO_TO_DEVICE_INTERNAL_SYNC_TOKEN,
    RESPONSE_FROM_KOBO_TO_DEVICE_SYNC_TOKEN,
)


@pytest.fixture
def token_reader():
    return KoboTokenReader()


class TestKoboSyncTokenReader:
    def test_token_reader_can_decode_token_from_device(self, token_reader):
        token_info = token_reader.decode(FROM_DEVIDE_TO_KOBO_SYNC_TOKEN)
        assert token_info.token == FROM_DEVIDE_TO_KOBO_SYNC_TOKEN
        assert isinstance(token_info.header, dict)
        assert isinstance(token_info.payload, dict)

    def test_token_reader_can_decode_token_from_response(self, token_reader):
        token_info = token_reader.decode(RESPONSE_FROM_KOBO_TO_DEVICE_SYNC_TOKEN)
        assert token_info.token == RESPONSE_FROM_KOBO_TO_DEVICE_SYNC_TOKEN
        assert isinstance(token_info.header, dict)
        assert isinstance(token_info.payload, dict)

    def test_token_reader_can_decode_internal_token_from_device(self, token_reader):
        token_info = token_reader.decode(FROM_DEVIDE_TO_KOBO_INTERNAL_SYNC_TOKEN)
        assert token_info.token == FROM_DEVIDE_TO_KOBO_INTERNAL_SYNC_TOKEN
        assert isinstance(token_info.header, dict)
        assert isinstance(token_info.payload, dict)


class TestKoboSyncTokenFromDevice:
    @pytest.fixture
    def token_reader(self):
        return KoboTokenReader()

    def test_header_off_sync_token_from_device(self, token_reader):
        token_info = token_reader.decode(FROM_DEVIDE_TO_KOBO_SYNC_TOKEN)
        assert token_info.header == {
            "ptyp": "SyncToken",
            "typ": 1,
            "ver": None,
        }

    def test_payload_off_sync_token_from_device(self, token_reader):
        token_info = token_reader.decode(FROM_DEVIDE_TO_KOBO_SYNC_TOKEN)
        assert token_info.payload == {
            "InternalSyncToken": FROM_DEVIDE_TO_KOBO_INTERNAL_SYNC_TOKEN,
            "IsContinuationToken": False,
        }

    def test_full_decode_sync_token_from_device(self, token_reader):
        token_info = token_reader.full_decode(FROM_DEVIDE_TO_KOBO_SYNC_TOKEN)
        assert token_info.payload == {
            "InternalSyncToken": token_reader.decode(FROM_DEVIDE_TO_KOBO_INTERNAL_SYNC_TOKEN).payload,
            "IsContinuationToken": False,
        }

    def test_header_off_internal_sync_token_from_device(self, token_reader):
        token_info = token_reader.decode(FROM_DEVIDE_TO_KOBO_INTERNAL_SYNC_TOKEN)
        assert token_info.header == {
            "ptyp": "SyncToken",
            "typ": 1,
            "ver": "v2",
        }

    def test_payload_off_internal_sync_token_from_device(self, token_reader):
        token_info = token_reader.decode(FROM_DEVIDE_TO_KOBO_INTERNAL_SYNC_TOKEN)
        assert token_info.payload == {
            "DeletedEntitlements": {
                "CheckSum": None,
                "GenerationTime": None,
                "Id": "00000000-0000-0000-0000-000000000000",
                "IsInitial": None,
                "Timestamp": "2024-10-12T12:13:37Z",
            },
            "DeletedTags": {
                "CheckSum": None,
                "GenerationTime": None,
                "Id": "00000000-0000-0000-0000-000000000000",
                "IsInitial": None,
                "Timestamp": "1900-01-01T00:00:00Z",
            },
            "Entitlements": {
                "CheckSum": '{"SkipCreated":false,"CreatedCount":0,"Created":[],"SkipModified":false,"Modified":[]}',
                "GenerationTime": None,
                "Id": "00000000-0000-0000-0000-000000000000",
                "IsInitial": None,
                "Timestamp": "2024-10-12T12:13:37Z",
            },
            "FutureSubscriptionEntitlements": None,
            "ProductMetadata": {
                "CheckSum": None,
                "GenerationTime": None,
                "Id": "bf981a9b-b23a-4c5a-a9eb-a557d18bc7c4",
                "IsInitial": None,
                "Timestamp": "2024-10-15T10:21:10Z",
            },
            "ReadingStates": {
                "CheckSum": None,
                "GenerationTime": None,
                "Id": "d9d4fd68-bdb4-46ae-a348-f90646237082",
                "IsInitial": None,
                "Timestamp": "2024-10-12T12:13:38Z",
            },
            "SubscriptionEntitlements": {
                "CheckSum": None,
                "GenerationTime": None,
                "Id": "00000000-0000-0000-0000-000000000000",
                "IsInitial": None,
                "Timestamp": "1900-01-01T00:00:00Z",
            },
            "Tags": {
                "CheckSum": None,
                "GenerationTime": None,
                "Id": "00000000-0000-0000-0000-000000000000",
                "IsInitial": None,
                "Timestamp": "2024-10-12T12:13:37Z",
            },
        }


class TestKoboSyncTokenFromKoboResponse:
    def test_header_off_sync_token_from_response(self, token_reader):
        token_info = token_reader.decode(RESPONSE_FROM_KOBO_TO_DEVICE_SYNC_TOKEN)
        assert token_info.header == {
            "ptyp": "SyncToken",
            "typ": 1,
            "ver": None,
        }

    def test_payload_off_sync_token_from_response(self, token_reader):
        token_info = token_reader.decode(RESPONSE_FROM_KOBO_TO_DEVICE_SYNC_TOKEN)
        assert token_info.payload == {
            "InternalSyncToken": RESPONSE_FROM_KOBO_TO_DEVICE_INTERNAL_SYNC_TOKEN,
            "IsContinuationToken": True,
        }

    def test_full_decode_sync_token_from_response(self, token_reader):
        token_info = token_reader.full_decode(RESPONSE_FROM_KOBO_TO_DEVICE_SYNC_TOKEN)
        assert token_info.payload == {
            "InternalSyncToken": token_reader.decode(RESPONSE_FROM_KOBO_TO_DEVICE_INTERNAL_SYNC_TOKEN).payload,
            "IsContinuationToken": True,
        }

    def test_header_off_internal_sync_token_from_response(self, token_reader):
        token_info = token_reader.decode(RESPONSE_FROM_KOBO_TO_DEVICE_INTERNAL_SYNC_TOKEN)
        assert token_info.header == {
            "ptyp": "SyncToken",
            "typ": 1,
            "ver": "v2",
        }

    def test_payload_off_internal_sync_token_from_response(self, token_reader):
        token_info = token_reader.decode(RESPONSE_FROM_KOBO_TO_DEVICE_INTERNAL_SYNC_TOKEN)
        assert token_info.payload == {
            "DeletedEntitlements": {
                "CheckSum": None,
                "GenerationTime": None,
                "Id": "00000000-0000-0000-0000-000000000000",
                "IsInitial": None,
                "Timestamp": "2024-10-12T12:13:37Z",
            },
            "DeletedTags": {
                "CheckSum": None,
                "GenerationTime": None,
                "Id": "00000000-0000-0000-0000-000000000000",
                "IsInitial": None,
                "Timestamp": "1900-01-01T00:00:00Z",
            },
            "Entitlements": {
                "CheckSum": '{"SkipCreated":false,"CreatedCount":1,"Created":["ed9c1a54-bb19-40bf-a660-817d8b2bc4d8"],"SkipModified":false,"Modified":[]}',  # noqa: E501
                "GenerationTime": None,
                "Id": "ed9c1a54-bb19-40bf-a660-817d8b2bc4d8",
                "IsInitial": None,
                "Timestamp": "2024-12-12T20:40:02Z",
            },
            "FutureSubscriptionEntitlements": None,
            "ProductMetadata": {
                "CheckSum": None,
                "GenerationTime": None,
                "Id": "bf981a9b-b23a-4c5a-a9eb-a557d18bc7c4",
                "IsInitial": None,
                "Timestamp": "2024-10-15T10:21:10Z",
            },
            "ReadingStates": {
                "CheckSum": None,
                "GenerationTime": None,
                "Id": "99663e8f-3695-413b-b73b-9c72db52d483",
                "IsInitial": None,
                "Timestamp": "2024-12-12T20:40:02Z",
            },
            "SubscriptionEntitlements": {
                "CheckSum": None,
                "GenerationTime": None,
                "Id": "00000000-0000-0000-0000-000000000000",
                "IsInitial": None,
                "Timestamp": "1900-01-01T00:00:00Z",
            },
            "Tags": {
                "CheckSum": None,
                "GenerationTime": None,
                "Id": "6202066d-24a4-4dbf-8a64-a1672fe88764",
                "IsInitial": None,
                "Timestamp": "2024-12-12T20:40:02Z",
            },
        }


def test_payload_off_device_and_kobo_response_token_have_differnt_internal_sync_tokens(token_reader):
    from_device_token = token_reader.decode(FROM_DEVIDE_TO_KOBO_SYNC_TOKEN)
    kobo_response_token = token_reader.decode(RESPONSE_FROM_KOBO_TO_DEVICE_SYNC_TOKEN)
    from_device_internal_sync_token = token_reader.decode(from_device_token.payload["InternalSyncToken"])
    kobo_response_internal_sync_token = token_reader.decode(kobo_response_token.payload["InternalSyncToken"])
    assert from_device_internal_sync_token.payload != kobo_response_internal_sync_token.payload


class TestKoboTokenDecoder:
    decoder = KoboTokenDecoder()

    def test_decode_token_from_device(self, sync_token_from_device):
        assert self.decoder.decode(FROM_DEVIDE_TO_KOBO_SYNC_TOKEN) == sync_token_from_device

    def test_decode_token_from_response(self, sync_token_from_response):
        assert self.decoder.decode(RESPONSE_FROM_KOBO_TO_DEVICE_SYNC_TOKEN) == sync_token_from_response
