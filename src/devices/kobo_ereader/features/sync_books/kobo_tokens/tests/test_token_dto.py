from collections import OrderedDict
from uuid import uuid4

from src.devices.kobo_ereader.features.sync_books.dtos import BookToSyncDto


def test_internal_sync_token_from_device_is_correctly_formated_to_dict(sync_token_from_device):
    assert sync_token_from_device.internal_sync_token.to_dict() == OrderedDict(
        [
            (
                "SubscriptionEntitlements",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "1900-01-01T00:00:00Z"),
                        ("CheckSum", None),
                        ("Id", "00000000-0000-0000-0000-000000000000"),
                    ]
                ),
            ),
            ("FutureSubscriptionEntitlements", None),
            (
                "Entitlements",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "2024-10-12T12:13:37Z"),
                        (
                            "CheckSum",
                            '{"SkipCreated":false,"CreatedCount":0,"Created":[],"SkipModified":false,"Modified":[]}',
                        ),
                        ("Id", "00000000-0000-0000-0000-000000000000"),
                    ]
                ),
            ),
            (
                "DeletedEntitlements",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "2024-10-12T12:13:37Z"),
                        ("CheckSum", None),
                        ("Id", "00000000-0000-0000-0000-000000000000"),
                    ]
                ),
            ),
            (
                "ReadingStates",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "2024-10-12T12:13:38Z"),
                        ("CheckSum", None),
                        ("Id", "d9d4fd68-bdb4-46ae-a348-f90646237082"),
                    ]
                ),
            ),
            (
                "Tags",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "2024-10-12T12:13:37Z"),
                        ("CheckSum", None),
                        ("Id", "00000000-0000-0000-0000-000000000000"),
                    ]
                ),
            ),
            (
                "DeletedTags",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "1900-01-01T00:00:00Z"),
                        ("CheckSum", None),
                        ("Id", "00000000-0000-0000-0000-000000000000"),
                    ]
                ),
            ),
            (
                "ProductMetadata",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "2024-10-15T10:21:10Z"),
                        ("CheckSum", None),
                        ("Id", "bf981a9b-b23a-4c5a-a9eb-a557d18bc7c4"),
                    ]
                ),
            ),
        ]
    )


def test_internal_sync_token_from_response_is_correctly_formated_to_dict(sync_token_from_response):
    assert sync_token_from_response.internal_sync_token.to_dict() == OrderedDict(
        [
            (
                "SubscriptionEntitlements",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "1900-01-01T00:00:00Z"),
                        ("CheckSum", None),
                        ("Id", "00000000-0000-0000-0000-000000000000"),
                    ]
                ),
            ),
            ("FutureSubscriptionEntitlements", None),
            (
                "Entitlements",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "2024-12-12T20:40:02Z"),
                        (
                            "CheckSum",
                            '{"SkipCreated":false,"CreatedCount":1,"Created":["ed9c1a54-bb19-40bf-a660-817d8b2bc4d8"],"SkipModified":false,"Modified":[]}',
                        ),
                        ("Id", "ed9c1a54-bb19-40bf-a660-817d8b2bc4d8"),
                    ]
                ),
            ),
            (
                "DeletedEntitlements",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "2024-10-12T12:13:37Z"),
                        ("CheckSum", None),
                        ("Id", "00000000-0000-0000-0000-000000000000"),
                    ]
                ),
            ),
            (
                "ReadingStates",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "2024-12-12T20:40:02Z"),
                        ("CheckSum", None),
                        ("Id", "99663e8f-3695-413b-b73b-9c72db52d483"),
                    ]
                ),
            ),
            (
                "Tags",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "2024-12-12T20:40:02Z"),
                        ("CheckSum", None),
                        ("Id", "6202066d-24a4-4dbf-8a64-a1672fe88764"),
                    ]
                ),
            ),
            (
                "DeletedTags",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "1900-01-01T00:00:00Z"),
                        ("CheckSum", None),
                        ("Id", "00000000-0000-0000-0000-000000000000"),
                    ]
                ),
            ),
            (
                "ProductMetadata",
                OrderedDict(
                    [
                        ("IsInitial", None),
                        ("GenerationTime", None),
                        ("Timestamp", "2024-10-15T10:21:10Z"),
                        ("CheckSum", None),
                        ("Id", "bf981a9b-b23a-4c5a-a9eb-a557d18bc7c4"),
                    ]
                ),
            ),
        ]
    )


def test_add_books_to_sync_on_from_device_kobo_sync_token(sync_token_from_device):
    books_to_sync = [
        BookToSyncDto(uuid=uuid4(), title="title", slug="slug", download_url="download_url"),
        BookToSyncDto(uuid=uuid4(), title="title", slug="slug", download_url="download_url"),
    ]
    sync_token_from_device.add_books_to_sync(books_to_sync)
    assert sync_token_from_device.internal_sync_token.entitlements.check_sum.created == [
        bookto.uuid for bookto in books_to_sync
    ]
    assert sync_token_from_device.internal_sync_token.entitlements.check_sum.created_count == len(books_to_sync)


def test_add_books_to_sync_on__from_response_kobo_sync_token(sync_token_from_response):
    books_created_before = [*sync_token_from_response.internal_sync_token.entitlements.check_sum.created]
    assert len(books_created_before) == 1
    books_to_sync = [
        BookToSyncDto(uuid=uuid4(), title="title", slug="slug", download_url="download_url"),
        BookToSyncDto(uuid=uuid4(), title="title", slug="slug", download_url="download_url"),
    ]
    sync_token_from_response.add_books_to_sync(books_to_sync)
    assert sync_token_from_response.internal_sync_token.entitlements.check_sum.created == books_created_before + [
        bookto.uuid for bookto in books_to_sync
    ]
    assert sync_token_from_response.internal_sync_token.entitlements.check_sum.created_count == len(
        books_to_sync
    ) + len(books_created_before)


def test_add_books_twice_to_kobo_sync_token_does_not_duplicate_the_books(sync_token_from_device):
    books_to_sync = [
        BookToSyncDto(uuid=uuid4(), title="title", slug="slug", download_url="download_url"),
        BookToSyncDto(uuid=uuid4(), title="title", slug="slug", download_url="download_url"),
    ]
    sync_token_from_device.add_books_to_sync(books_to_sync)
    sync_token_from_device.add_books_to_sync(books_to_sync)
    assert sync_token_from_device.internal_sync_token.entitlements.check_sum.created == [
        bookto.uuid for bookto in books_to_sync
    ]
    assert sync_token_from_device.internal_sync_token.entitlements.check_sum.created_count == len(books_to_sync)
