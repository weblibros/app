import pytest

from ..token_constructors import KoboInternalTokenConstructor, KoboTokenConstructor
from .real_kobo_tokens import (
    FROM_DEVIDE_TO_KOBO_INTERNAL_SYNC_TOKEN,
    FROM_DEVIDE_TO_KOBO_SYNC_TOKEN,
    RESPONSE_FROM_KOBO_TO_DEVICE_INTERNAL_SYNC_TOKEN,
    RESPONSE_FROM_KOBO_TO_DEVICE_SYNC_TOKEN,
)


@pytest.fixture
def constructor():
    return KoboTokenConstructor(KoboInternalTokenConstructor())


class TestConstructFromDeviceKoboToken:
    token = FROM_DEVIDE_TO_KOBO_SYNC_TOKEN
    internal_token = FROM_DEVIDE_TO_KOBO_INTERNAL_SYNC_TOKEN

    def test_if_constructor_can_build_header_for_device_kobo_token(self, constructor, sync_token_from_device):
        assert constructor.build(sync_token_from_device).split(".")[0] == self.token.split(".")[0]

    def test_if_constructor_can_build_payload_for_device_kobo_token(self, constructor, sync_token_from_device):
        assert constructor.build(sync_token_from_device).split(".")[1] == self.token.split(".")[1]

    def test_if_constructor_can_build_header_for_internal_device_kobo_token(self, constructor, sync_token_from_device):
        assert (
            constructor._internal_token_constructor.build(sync_token_from_device.internal_sync_token).split(".")[0]
            == self.internal_token.split(".")[0]
        )

    def test_if_constructor_can_build_payload_for_internal_device_kobo_token(self, constructor, sync_token_from_device):
        assert (
            constructor._internal_token_constructor.build(sync_token_from_device.internal_sync_token).split(".")[1]
            == self.internal_token.split(".")[1]
        )

    def test_if_constructor_can_build_token_for_internal_device_kobo_token(self, constructor, sync_token_from_device):
        assert (
            constructor._internal_token_constructor.build(sync_token_from_device.internal_sync_token)
            == self.internal_token
        )

    def test_if_constructor_can_build_for_internal_device_kobo_token(self, constructor, sync_token_from_device):
        assert constructor.build(sync_token_from_device) == self.token


class TestConstructFromResponseKoboToken:
    token = RESPONSE_FROM_KOBO_TO_DEVICE_SYNC_TOKEN
    internal_token = RESPONSE_FROM_KOBO_TO_DEVICE_INTERNAL_SYNC_TOKEN

    def test_if_constructor_can_build_header_for_device_kobo_token(self, constructor, sync_token_from_response):
        assert constructor.build(sync_token_from_response).split(".")[0] == self.token.split(".")[0]

    def test_if_constructor_can_build_payload_for_device_kobo_token(self, constructor, sync_token_from_response):
        assert constructor.build(sync_token_from_response).split(".")[1] == self.token.split(".")[1]

    def test_if_constructor_can_build_header_for_internal_device_kobo_token(
        self, constructor, sync_token_from_response
    ):
        assert (
            constructor._internal_token_constructor.build(sync_token_from_response.internal_sync_token).split(".")[0]
            == self.internal_token.split(".")[0]
        )

    def test_if_constructor_can_build_payload_for_internal_device_kobo_token(
        self, constructor, sync_token_from_response
    ):
        assert (
            constructor._internal_token_constructor.build(sync_token_from_response.internal_sync_token).split(".")[1]
            == self.internal_token.split(".")[1]
        )

    def test_if_constructor_can_build_for_response_kobo_token(self, constructor, sync_token_from_response):
        assert constructor.build(sync_token_from_response) == self.token
