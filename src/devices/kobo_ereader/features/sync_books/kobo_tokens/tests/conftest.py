from datetime import datetime
from uuid import UUID

import pytest

from ..dtos import EntitlementInfo, KoboInternalSyncToken, KoboSyncInternalInfoState, KoboSyncToken


@pytest.fixture
def sync_token_from_device():
    return KoboSyncToken(
        is_continuation_token=False,
        internal_sync_token=KoboInternalSyncToken(
            entitlements=KoboSyncInternalInfoState(
                timestamp=datetime.fromisoformat("2024-10-12T12:13:37Z"),
                check_sum=EntitlementInfo(
                    skip_created=False,
                    created_count=0,
                    created=[],
                    skip_modified=False,
                    modified=[],
                ),
            ),
            deleted_entitlements=KoboSyncInternalInfoState(
                timestamp=datetime.fromisoformat("2024-10-12T12:13:37Z"),
            ),
            reading_states=KoboSyncInternalInfoState(
                timestamp=datetime.fromisoformat("2024-10-12T12:13:38Z"),
                id=UUID("d9d4fd68-bdb4-46ae-a348-f90646237082"),
            ),
            tags=KoboSyncInternalInfoState(
                timestamp=datetime.fromisoformat("2024-10-12T12:13:37Z"),
            ),
            product_metadata=KoboSyncInternalInfoState(
                timestamp=datetime.fromisoformat("2024-10-15T10:21:10Z"),
                id=UUID("bf981a9b-b23a-4c5a-a9eb-a557d18bc7c4"),
            ),
        ),
    )


@pytest.fixture
def sync_token_from_response():
    return KoboSyncToken(
        is_continuation_token=True,
        internal_sync_token=KoboInternalSyncToken(
            entitlements=KoboSyncInternalInfoState(
                timestamp=datetime.fromisoformat("2024-12-12T20:40:02Z"),
                check_sum=EntitlementInfo(
                    skip_created=False,
                    created_count=1,
                    created=[UUID("ed9c1a54-bb19-40bf-a660-817d8b2bc4d8")],
                    skip_modified=False,
                    modified=[],
                ),
                id=UUID("ed9c1a54-bb19-40bf-a660-817d8b2bc4d8"),
            ),
            deleted_entitlements=KoboSyncInternalInfoState(
                timestamp=datetime.fromisoformat("2024-10-12T12:13:37Z"),
            ),
            reading_states=KoboSyncInternalInfoState(
                timestamp=datetime.fromisoformat("2024-12-12T20:40:02Z"),
                id=UUID("99663e8f-3695-413b-b73b-9c72db52d483"),
            ),
            tags=KoboSyncInternalInfoState(
                timestamp=datetime.fromisoformat("2024-12-12T20:40:02Z"),
                id=UUID("6202066d-24a4-4dbf-8a64-a1672fe88764"),
            ),
            product_metadata=KoboSyncInternalInfoState(
                timestamp=datetime.fromisoformat("2024-10-15T10:21:10Z"),
                id=UUID("bf981a9b-b23a-4c5a-a9eb-a557d18bc7c4"),
            ),
        ),
    )
