import base64
import json
from dataclasses import dataclass
from datetime import datetime
from uuid import UUID

from .dtos import EntitlementInfo, KoboInternalSyncToken, KoboSyncInternalInfoState, KoboSyncToken


@dataclass
class InfoKoboSyncToken:
    token: str
    header: dict
    payload: dict


class KoboTokenReader:
    def decode(self, token: str) -> InfoKoboSyncToken:
        if token:
            header, payload = token.split(".")
            json_header = json.loads(base64.b64decode(header))
            json_payload = json.loads(base64.b64decode(payload + "=="))
            return InfoKoboSyncToken(token=token, header=json_header, payload=json_payload)
        else:
            return InfoKoboSyncToken(token=token, header={}, payload={})

    def full_decode(self, token: str) -> InfoKoboSyncToken:
        if token:
            header, payload = token.split(".")
            json_header = json.loads(base64.b64decode(header))
            json_payload = json.loads(base64.b64decode(payload + "=="))
            internal_sync_token = KoboTokenReader().decode(json_payload["InternalSyncToken"])
            full_decoded_payload = {**json_payload, "InternalSyncToken": internal_sync_token.payload}
            return InfoKoboSyncToken(token=token, header=json_header, payload=full_decoded_payload)
        else:
            return InfoKoboSyncToken(token=token, header={}, payload={})


class KoboTokenDecoderError(Exception):
    pass


class KoboTokenDecoder:
    def __init__(self, raise_exception: bool = False):
        self._raise_exception = raise_exception

    def decode(self, token: str) -> KoboSyncToken:
        if token:
            payload = self._decode_to_dict(token)
            internal_sync_token = self._decode_to_dict(payload["InternalSyncToken"])
            full_decoded_payload = {**payload, "InternalSyncToken": internal_sync_token}
            return self._to_kobo_sync_token(full_decoded_payload)
        elif self._raise_exception:
            raise KoboTokenDecoderError("Token is empty")
        else:
            return KoboSyncToken()

    def _decode_to_dict(self, token: str) -> dict:
        _, raw_payload = token.split(".")
        payload = json.loads(base64.b64decode(raw_payload + "=="))
        return payload

    def _to_kobo_sync_token(self, raw_payload: dict) -> KoboSyncToken:
        return KoboSyncToken(
            is_continuation_token=raw_payload["IsContinuationToken"],
            internal_sync_token=KoboInternalSyncToken(
                subscription_entitlements=self._to_kobo_sync_internal_info_state(
                    raw_payload["InternalSyncToken"]["SubscriptionEntitlements"]
                ),
                future_subscription_entitlements=raw_payload["InternalSyncToken"]["FutureSubscriptionEntitlements"],
                entitlements=self._to_kobo_sync_internal_info_state(raw_payload["InternalSyncToken"]["Entitlements"]),
                deleted_entitlements=self._to_kobo_sync_internal_info_state(
                    raw_payload["InternalSyncToken"]["DeletedEntitlements"]
                ),
                reading_states=self._to_kobo_sync_internal_info_state(
                    raw_payload["InternalSyncToken"]["ReadingStates"]
                ),
                tags=self._to_kobo_sync_internal_info_state(raw_payload["InternalSyncToken"]["Tags"]),
                deleted_tags=self._to_kobo_sync_internal_info_state(raw_payload["InternalSyncToken"]["DeletedTags"]),
                product_metadata=self._to_kobo_sync_internal_info_state(
                    raw_payload["InternalSyncToken"]["ProductMetadata"]
                ),
            ),
        )

    def _to_kobo_sync_internal_info_state(self, raw_payload: dict) -> KoboSyncInternalInfoState:
        return KoboSyncInternalInfoState(
            is_initial=raw_payload["IsInitial"],
            generation_time=datetime.fromisoformat(raw_payload["GenerationTime"])
            if raw_payload["GenerationTime"]
            else None,
            timestamp=datetime.fromisoformat(raw_payload["Timestamp"]),
            check_sum=self._to_entitlement_info(json.loads(raw_payload["CheckSum"]))
            if raw_payload["CheckSum"]
            else None,
            id=UUID(raw_payload["Id"]),
        )

    def _to_entitlement_info(self, raw_payload: dict) -> EntitlementInfo:
        return EntitlementInfo(
            skip_created=raw_payload["SkipCreated"],
            created_count=raw_payload["CreatedCount"],
            created=raw_payload["Created"],
            skip_modified=raw_payload["SkipModified"],
            modified=raw_payload["Modified"],
        )


kobo_token_decoder = KoboTokenDecoder()
