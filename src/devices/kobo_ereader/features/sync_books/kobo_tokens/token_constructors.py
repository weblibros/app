import base64
import json
from collections import OrderedDict

from .dtos import KoboInternalSyncToken, KoboSyncToken


class KoboInternalTokenConstructor:
    def build(self, kobo_sync_token: KoboInternalSyncToken) -> str:
        return f"{self._construct_kobo_jwt_header()}.{self._construct_kobo_jwt_payload(kobo_sync_token)}"

    def _construct_kobo_jwt_header(self):
        return base64.b64encode(json.dumps(self._kobo_jwt_header).replace(" ", "").encode()).decode()

    @property
    def _kobo_jwt_header(self):
        return OrderedDict(
            [
                ("typ", 1),
                ("ver", "v2"),
                ("ptyp", "SyncToken"),
            ]
        )

    def _construct_kobo_jwt_payload(self, kobo_sync_token: KoboInternalSyncToken) -> str:
        return (
            base64.b64encode(json.dumps(kobo_sync_token.to_dict()).replace(" ", "").encode())
            .decode()
            .removesuffix("==")
            .removesuffix("=")
        )


class KoboTokenConstructor:
    def __init__(self, internal_token_constructor: KoboInternalTokenConstructor):
        self._internal_token_constructor = internal_token_constructor

    def build(self, kobo_sync_token: KoboSyncToken) -> str:
        return f"{self._construct_kobo_jwt_header()}.{self._construct_kobo_jwt_payload(kobo_sync_token)}"

    def encode(self, kobo_sync_token: KoboSyncToken) -> str:
        return self.build(kobo_sync_token)

    def _construct_kobo_jwt_header(self):
        return base64.b64encode(json.dumps(self._kobo_jwt_header).replace(" ", "").encode()).decode()

    @property
    def _kobo_jwt_header(self):
        return OrderedDict(
            [
                ("typ", 1),
                ("ver", None),
                ("ptyp", "SyncToken"),
            ]
        )

    def _construct_kobo_jwt_payload(self, kobo_sync_token: KoboSyncToken):
        return (
            base64.b64encode(json.dumps(self._kobo_jwt_payload(kobo_sync_token)).replace(" ", "").encode())
            .decode()
            .removesuffix("==")
            .removesuffix("=")
        )

    def _kobo_jwt_payload(self, kobo_sync_token: KoboSyncToken):
        return OrderedDict(
            [
                ("InternalSyncToken", self._internal_token_constructor.build(kobo_sync_token.internal_sync_token)),
                ("IsContinuationToken", kobo_sync_token.is_continuation_token),
            ]
        )


kobo_token_encoder = KoboTokenConstructor(KoboInternalTokenConstructor())
