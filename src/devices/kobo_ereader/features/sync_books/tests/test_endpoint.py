from fastapi import Request

from src.devices import urls
from src.devices.service.dto import Body, BodyType, Cookies, Headers, RequestDTO, RequestMethod, ResponseDTO, Url

from ..handlers import SyncBookToKoboDeviceCommand, SyncBookToKoboDeviceHandler
from ..request_mapper import map_fastapi_request_to_dto_request
from ..router import handler_factory


class FakeSyncBookToKoboDeviceHandler(SyncBookToKoboDeviceHandler):
    async def handle(self, command: SyncBookToKoboDeviceCommand) -> ResponseDTO:
        return ResponseDTO(
            status_code=200,
            headers={},
            cookies={},
            content='{"message": "Device log request received"}',
            http_version="HTTP/1.1",
        )


def fake_handler_factory():
    return FakeSyncBookToKoboDeviceHandler()


def fake_dto_request(request: Request) -> RequestDTO:
    return RequestDTO(
        url=Url("http://example.com/dto-request"),
        body=Body(
            body_args=[],
            body_type=BodyType.EMPTY,
        ),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(headers=[]),
    )


class TestSyncBooksEndpoint:
    async def test_endpoint_sync_books_return_200(self, client_factory):
        async with client_factory(
            {
                handler_factory: fake_handler_factory,
                map_fastapi_request_to_dto_request: fake_dto_request,
            }
        ) as client:
            response = await client.get(urls.KOBO_DEVICE_LIBRARY_SYNC_URL)

        assert response.status_code == 200
        assert response.content == b'{"message":"Device log request received"}'
