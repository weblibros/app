from uuid import uuid4

import pytest

from src.books.domain import Book, BookFile, BookMimeType
from src.books.domain.events import BookAddedEvent
from src.books.repositories import SqlBookRepo
from src.config.database import async_session_maker
from src.file_management.repositories.models import FileRecord

from ..dtos import BookToSyncDto
from ..read_models import SqlBookSyncReadModel


@pytest.fixture
def book_to_sync():
    book = Book(
        uuid=uuid4(),
        title="fake plain title",
        title_sort="fake title sort",
        isbn=None,
        published_at=None,
        authors=[],
        identifiers=[],
        cover=None,
        files=[BookFile(uuid=uuid4(), href="/test/epub", mime_type=BookMimeType.EPUB)],
    )
    book.events.append(
        BookAddedEvent(
            book_uuid=book.uuid,
            library_uuid=uuid4(),
            title=book.title,
            title_sort=book.title_sort,
            published_at=book.published_at,
            stored_by_user_uuid=uuid4(),
        )
    )
    return book


def fake_download_link_contructor(book: Book):
    return "https://example.com/"


async def test_read_models_returns_list_of_book_to_sync_objects(book_to_sync):
    async with async_session_maker() as session:
        file_record = FileRecord(
            uuid=book_to_sync.get_file().uuid,
            path="/tmp/fake-book.epub",
            size_bytes=23,
            extension="epub",
            content_type="application/epub+zip",
            storage_service_name="local_storage_service",
            entity_class_name="EbookFile",
        )
        session.add(file_record)
        await session.flush()
        book_repo = SqlBookRepo(session_overwrite=session)
        await book_repo.add(book_to_sync)
        await session.flush()
        read_model = SqlBookSyncReadModel(
            session_overwrite=session, download_link_contructor=fake_download_link_contructor
        )
        result = await read_model.get_books_to_sync()
    assert isinstance(result, list)
    assert isinstance(result[0], BookToSyncDto)


async def test_read_models_returns_a_book_to_sync(book_to_sync):
    async with async_session_maker() as session:
        file_record = FileRecord(
            uuid=book_to_sync.get_file().uuid,
            path="/tmp/fake-book.epub",
            size_bytes=23,
            extension="epub",
            content_type="application/epub+zip",
            storage_service_name="local_storage_service",
            entity_class_name="EbookFile",
        )
        session.add(file_record)
        await session.flush()
        book_repo = SqlBookRepo(session_overwrite=session)
        await book_repo.add(book_to_sync)
        await session.flush()
        read_model = SqlBookSyncReadModel(
            session_overwrite=session, download_link_contructor=fake_download_link_contructor
        )
        result = await read_model.get_books_to_sync()
    assert len(result) == 1
    assert result[0].uuid == book_to_sync.uuid
    assert result[0].slug == book_to_sync.title.lower().replace(" ", "-")
    assert result[0].title == book_to_sync.title
    assert result[0].download_url == "https://example.com/"
