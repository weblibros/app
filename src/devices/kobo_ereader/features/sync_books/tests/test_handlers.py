from uuid import uuid4

import pytest

from src.devices.kobo_ereader.events import SyncBookToKoboDeviceCommand
from src.devices.service.dto import Body, BodyType, Cookies, Header, Headers, RequestDTO, RequestMethod, Url

from ..dtos import BookToSyncDto
from ..handlers import InjectBookToKoboDeviceHandler
from ..read_models import InMemoryBookSyncReadModel

kobo_headers = {
    "x-kobo-affiliatename": "Kobo",
    "x-kobo-appversion": "4.38.23171",
    "x-kobo-deviceid": "1111111111",
    "x-kobo-devicemodel": "Kobo Sage",
    "x-kobo-deviceos": "4.9.56",
    "x-kobo-deviceosversion": "NA",
    "x-kobo-platformid": "00000000-0000-0000-0000-000000000111",
    "x-kobo-synctoken": "",
}


@pytest.fixture
def fake_book() -> BookToSyncDto:
    return BookToSyncDto(
        uuid=uuid4(),
        title="Fake title",
        slug="fake-title",
        download_url="https://www.example.com/fake-book",
    )


@pytest.fixture
def fake_inbound_request() -> RequestDTO:
    return RequestDTO(
        url=Url("http://example.com/dto-request"),
        body=Body(
            body_args=[],
            body_type=BodyType.EMPTY,
        ),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(headers=[Header(key=key, value=value) for key, value in kobo_headers.items()]),
    )


@pytest.fixture
def handler(fake_book):
    return InjectBookToKoboDeviceHandler(
        read_model=InMemoryBookSyncReadModel([fake_book]),
    )


class TestInjectBookToKoboDeviceHandler:
    async def test_handler_returns_back_a_respons_url(self, handler, fake_inbound_request):
        command = SyncBookToKoboDeviceCommand(inbound_request=fake_inbound_request)
        response = await handler(command)
        assert response.status_code == 200

    async def test_handlers_passes_kobo_headers_to_response(self, handler, fake_inbound_request):
        command = SyncBookToKoboDeviceCommand(inbound_request=fake_inbound_request)
        response = await handler(command)
        assert response.headers["x-kobo-deviceid"] == kobo_headers["x-kobo-deviceid"]
        assert response.headers["x-kobo-devicemodel"] == kobo_headers["x-kobo-devicemodel"]
        assert response.headers["x-kobo-deviceos"] == kobo_headers["x-kobo-deviceos"]
        assert response.headers["x-kobo-deviceosversion"] == kobo_headers["x-kobo-deviceosversion"]
        assert response.headers["x-kobo-platformid"] == kobo_headers["x-kobo-platformid"]
        assert "x-kobo-synctoken" in response.headers.keys()

    async def test_handlers_returns_a_response_with_the_book_in_the_payload(
        self,
        handler,
        fake_inbound_request,
        fake_book,
    ):
        command = SyncBookToKoboDeviceCommand(inbound_request=fake_inbound_request)
        response = await handler(command)
        created_at = fake_book.created_at_str
        assert response.json()[0] == {
            "NewEntitlement": {
                "BookEntitlement": {
                    "ActivePeriod": {"From": created_at},
                    "IsRemoved": False,
                    "Status": "Active",
                    "Accessibility": "Full",
                    "CrossRevisionId": "9bae730c-23f9-3f23-be03-1f3a5ac0eef9",
                    "RevisionId": "c1d3e2fa-0dfb-4032-98eb-9f3c10f8cc51",
                    "IsHiddenFromArchive": False,
                    "Id": str(fake_book.uuid),
                    "Created": created_at,
                    "LastModified": created_at,
                    "IsLocked": False,
                    "OriginCategory": "Purchased",
                },
                "ReadingState": {
                    "EntitlementId": str(fake_book.uuid),
                    "Created": created_at,
                    "LastModified": created_at,
                    "StatusInfo": {
                        "LastModified": created_at,
                        "Status": "ReadyToRead",
                        "TimesStartedReading": 0,
                    },
                    "Statistics": {"LastModified": created_at},
                    "CurrentBookmark": {"LastModified": created_at},
                    "PriorityTimestamp": created_at,
                },
                "BookMetadata": {
                    "CrossRevisionId": "9bae730c-23f9-3f23-be03-1f3a5ac0eef9",
                    "RevisionId": "c1d3e2fa-0dfb-4032-98eb-9f3c10f8cc51",
                    "Publisher": {},
                    "PublicationDate": "2014-11-30T00:00:00.0000000",
                    "Language": "en",
                    "Isbn": "1230000283080",
                    "Genre": "180861c2-21ea-4793-952b-ecd39613adfe",
                    "Slug": "fake-title",
                    "CoverImageId": "305ab3a8-4998-47f9-a022-f7cfa4323f3f",
                    "ApplicableSubscriptions": [],
                    "IsSocialEnabled": False,
                    "WorkId": "9bae730c-23f9-3f23-be03-1f3a5ac0eef9",
                    "ExternalIds": [],
                    "IsPreOrder": False,
                    "ContributorRoles": [],
                    "IsInternetArchive": False,
                    "IsAnnotationExportEnabled": False,
                    "IsAISummaryEnabled": False,
                    "EntitlementId": str(fake_book.uuid),
                    "Title": fake_book.title,
                    "Description": "",
                    "Categories": [],
                    "DownloadUrls": [
                        {
                            "DrmType": "None",
                            "Format": "EPUB_SAMPLE",
                            "Url": fake_book.download_url,
                            "Platform": "Android",
                            "Size": 978752,
                        },
                    ],
                    "Contributors": [],
                    "Series": {},
                    "CurrentDisplayPrice": {"TotalAmount": 0, "CurrencyCode": "EUR"},
                    "CurrentLoveDisplayPrice": {"TotalAmount": 0},
                    "IsEligibleForKoboLove": False,
                    "PhoneticPronunciations": {},
                    "RelatedGroupId": "00000000-0000-0000-0000-000000000001",
                },
            }
        }
