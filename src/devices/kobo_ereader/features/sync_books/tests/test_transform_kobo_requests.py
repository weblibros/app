import pytest

from src.devices.service.dto import (
    Body,
    BodyArgs,
    BodyType,
    Cookies,
    EmptyBody,
    FormBody,
    Header,
    Headers,
    JsonBody,
    RequestDTO,
    RequestMethod,
    Url,
)
from src.devices.urls import (
    KOBO_DEVICE_AFFILIATE_URL,
    KOBO_DEVICE_ANALYTICS_GET_TESTS,
    KOBO_DEVICE_DEALS_URL,
    KOBO_DEVICE_INITIALIZATION_URL,
    KOBO_DEVICE_USER_LOYALTY_URL,
    KOBO_DEVICE_USER_PROFILE_URL,
)

from ..kobo_request_transformer import transform_to_kobo_request


@pytest.mark.parametrize(
    "inbound_url,expected_outbound_url",
    [
        (
            f"http://localhost:8000{KOBO_DEVICE_AFFILIATE_URL}",
            "https://storeapi.kobo.com/v1/affiliate",
        ),
        (
            f"http://localhost:8000{KOBO_DEVICE_AFFILIATE_URL}?PlatformID=00000000-0000-0000-0000-000000000111&SerialNumber=ecdf63198e73c9475c2cc38e0abc7333",
            "https://storeapi.kobo.com/v1/affiliate?PlatformID=00000000-0000-0000-0000-000000000111&SerialNumber=ecdf63198e73c9475c2cc38e0abc7333",
        ),
        (
            f"http://localhost:8000{KOBO_DEVICE_INITIALIZATION_URL}",
            "https://storeapi.kobo.com/v1/initialization",
        ),
        (
            f"http://localhost:8000{KOBO_DEVICE_USER_PROFILE_URL}",
            "https://storeapi.kobo.com/v1/user/profile",
        ),
        (
            f"http://localhost:8000{KOBO_DEVICE_USER_PROFILE_URL}?foo=bar&foo=baz&foo=foobar",
            "https://storeapi.kobo.com/v1/user/profile?foo=bar&foo=baz&foo=foobar",
        ),
        (
            f"http://localhost:8000{KOBO_DEVICE_USER_LOYALTY_URL}",
            "https://storeapi.kobo.com/v1/user/loyalty/benefits",
        ),
        (
            f"http://localhost:8000{KOBO_DEVICE_DEALS_URL}",
            "https://storeapi.kobo.com/v1/deals",
        ),
        (
            f"http://localhost:8000{KOBO_DEVICE_ANALYTICS_GET_TESTS}",
            "https://storeapi.kobo.com/v1/analytics/gettests",
        ),
    ],
)
def test_transform_inbound_request_url_to_outbound_kobo_url(inbound_url, expected_outbound_url):
    inbound_request = RequestDTO(
        url=Url(inbound_url),
        body=EmptyBody(),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(headers=[]),
    )
    outbound_request = transform_to_kobo_request(inbound_request)
    assert isinstance(outbound_request, RequestDTO)
    assert inbound_request != outbound_request
    assert outbound_request.url.value == expected_outbound_url


def test_transform_inbound_request_empty_body_to_outbound_kobo_body():
    inbound_request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_ANALYTICS_GET_TESTS}"),
        body=EmptyBody(),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(headers=[]),
    )
    outbound_request = transform_to_kobo_request(inbound_request)
    assert isinstance(outbound_request, RequestDTO)
    assert inbound_request != outbound_request
    assert outbound_request.body.to_dict() == {}
    assert outbound_request.body.body_type == BodyType.EMPTY
    assert isinstance(outbound_request.body, Body)


def test_transform_inbound_request_json_body_to_outbound_kobo_body():
    inbound_request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_ANALYTICS_GET_TESTS}"),
        body=JsonBody(
            body_args=[
                BodyArgs("foo", "bar"),
                BodyArgs("foobar", "baz"),
            ]
        ),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(headers=[]),
    )
    outbound_request = transform_to_kobo_request(inbound_request)
    assert isinstance(outbound_request, RequestDTO)
    assert inbound_request != outbound_request
    assert outbound_request.body.to_dict() == {
        "data": {
            "foo": "bar",
            "foobar": "baz",
        },
        "type": "JSON",
    }
    assert outbound_request.body.body_type == BodyType.JSON
    assert isinstance(outbound_request.body, Body)


def test_transform_inbound_request_form_body_to_outbound_kobo_body():
    inbound_request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_ANALYTICS_GET_TESTS}"),
        body=FormBody(
            body_args=[
                BodyArgs("foo", "bar"),
                BodyArgs("foobar", "baz"),
            ]
        ),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(headers=[]),
    )
    outbound_request = transform_to_kobo_request(inbound_request)
    assert isinstance(outbound_request, RequestDTO)
    assert inbound_request != outbound_request
    assert outbound_request.body.to_dict() == {
        "data": {
            "foo": "bar",
            "foobar": "baz",
        },
        "type": "FORM",
    }
    assert outbound_request.body.body_type == BodyType.FORM
    assert isinstance(outbound_request.body, Body)


def test_transform_inbound_request_headers_to_outbound_kobo_headers_filter_unknown_headers():
    inbound_request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_ANALYTICS_GET_TESTS}"),
        body=EmptyBody(),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(
            headers=[
                Header("foo", "bar"),
                Header("foobar", "baz"),
            ]
        ),
    )
    outbound_request = transform_to_kobo_request(inbound_request)
    assert isinstance(outbound_request, RequestDTO)
    assert inbound_request != outbound_request
    assert isinstance(outbound_request.headers, Headers)
    assert outbound_request.headers.to_dict() == {}


def test_transform_inbound_request_headers_to_outbound_kobo_headers_filters_cloudflare_headers():
    inbound_request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_ANALYTICS_GET_TESTS}"),
        body=EmptyBody(),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(
            headers=[
                Header("cf-connecting-ip", "0.0.0.0"),
                Header("cf-ipcountry", "UK"),
                Header("cf-ray", "1111111111-MAD"),
                Header("cf-visitor", '{"scheme":"https"}'),
                Header("cdn-loop", "cloudflare; loops=1"),
            ]
        ),
    )
    outbound_request = transform_to_kobo_request(inbound_request)
    assert isinstance(outbound_request, RequestDTO)
    assert inbound_request != outbound_request
    assert isinstance(outbound_request.headers, Headers)
    assert outbound_request.headers.to_dict() == {}


def test_transform_inbound_request_headers_to_outbound_kobo_headers_pass_kobo_headers():
    inbound_request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_ANALYTICS_GET_TESTS}"),
        body=EmptyBody(),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(
            headers=[
                Header(key=key, value=value)
                for key, value in {
                    "foo": "bar",
                    "foobar": "baz",
                    "x-kobo-affiliatename": "Kobo",
                    "x-kobo-appversion": "4.38.23038",
                    "x-kobo-deviceid": "11111111111111111111111111",
                    "x-kobo-devicemodel": "Kobo Sage",
                    "x-kobo-deviceos": "4.9.56",
                    "x-kobo-deviceosversion": "NA",
                    "x-kobo-platformid": "00000000-0000-0000-0000-000000000111",
                }.items()
            ],
        ),
    )
    outbound_request = transform_to_kobo_request(inbound_request)
    assert isinstance(outbound_request, RequestDTO)
    assert inbound_request != outbound_request
    assert isinstance(outbound_request.headers, Headers)
    assert outbound_request.headers.to_dict() == {
        "x-kobo-affiliatename": "Kobo",
        "x-kobo-appversion": "4.38.23038",
        "x-kobo-deviceid": "11111111111111111111111111",
        "x-kobo-devicemodel": "Kobo Sage",
        "x-kobo-deviceos": "4.9.56",
        "x-kobo-deviceosversion": "NA",
        "x-kobo-platformid": "00000000-0000-0000-0000-000000000111",
    }


def test_transform_inbound_request_headers_to_outbound_kobo_headers_filter_xforwarded_headers():
    inbound_request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_ANALYTICS_GET_TESTS}"),
        body=EmptyBody(),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(
            headers=[
                Header(key=key, value=value)
                for key, value in {
                    "x-forwarded-for": "10.42.1.0",
                    "x-forwarded-host": "example.com",
                    "x-forwarded-port": "80",
                    "x-forwarded-proto": "http",
                    "x-forwarded-server": "traefik-54d4cd7d7c-p4l4q",
                }.items()
            ],
        ),
    )
    outbound_request = transform_to_kobo_request(inbound_request)
    assert isinstance(outbound_request, RequestDTO)
    assert inbound_request != outbound_request
    assert isinstance(outbound_request.headers, Headers)
    assert outbound_request.headers.to_dict() == {}


def test_transform_inbound_request_headers_to_outbound_kobo_headers_filter_default_headers():
    inbound_request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_ANALYTICS_GET_TESTS}"),
        body=EmptyBody(),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(
            headers=[
                Header(key=key, value=value)
                for key, value in {
                    "host": "example.com",
                    "user-agent": "Mozilla/5.0 (Linux; U; Android 2.0; en-us;) AppleWebKit/538.1 (KHTML, like Gecko)",
                    "content-length": "195",
                    "accept": "application/json",
                    "accept-encoding": "gzip, br",
                    "accept-language": "en-US, *;q=0.9",
                    "content-type": "application/json",
                }.items()
            ],
        ),
    )
    outbound_request = transform_to_kobo_request(inbound_request)
    assert isinstance(outbound_request, RequestDTO)
    assert inbound_request != outbound_request
    assert isinstance(outbound_request.headers, Headers)
    assert outbound_request.headers.to_dict() == {}


def test_transform_inbound_request_headers_to_outbound_kobo_headers_pass_and_transform_auth_headers():
    inbound_request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_ANALYTICS_GET_TESTS}"),
        body=EmptyBody(),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(
            headers=[
                Header(key=key, value=value)
                for key, value in {
                    "foo": "bar",
                    "foobar": "baz",
                    "authorization": "Bearer token",
                }.items()
            ],
        ),
    )
    outbound_request = transform_to_kobo_request(inbound_request)
    assert isinstance(outbound_request, RequestDTO)
    assert inbound_request != outbound_request
    assert isinstance(outbound_request.headers, Headers)
    assert outbound_request.headers.to_dict() == {
        "authorization": "Bearer token",
    }


def test_transform_inbound_request_headers_to_outbound_kobo_headers_filter_real_ip_headers():
    inbound_request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_ANALYTICS_GET_TESTS}"),
        body=EmptyBody(),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(
            headers=[
                Header(key=key, value=value)
                for key, value in {
                    "x-real-ip": "10.42.1.0",
                }.items()
            ],
        ),
    )
    outbound_request = transform_to_kobo_request(inbound_request)
    assert isinstance(outbound_request, RequestDTO)
    assert inbound_request != outbound_request
    assert isinstance(outbound_request.headers, Headers)
    assert outbound_request.headers.to_dict() == {}
