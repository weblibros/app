from dataclasses import dataclass, field
from datetime import datetime
from uuid import UUID


@dataclass
class BookToSyncDto:
    uuid: UUID
    title: str
    slug: str
    download_url: str
    created_at: datetime = field(default_factory=datetime.now)

    @property
    def created_at_str(self):
        return self.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")
