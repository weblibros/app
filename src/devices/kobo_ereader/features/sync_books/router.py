import logging
import os

from fastapi import APIRouter, Depends
from starlette.responses import JSONResponse

from src.devices import urls
from src.devices.kobo_ereader.events import SyncBookToKoboDeviceCommand
from src.devices.service import HttpxRequestService, ProxyService
from src.devices.service.dto import RequestDTO
from src.services.jwt import JWTUserDownloadBookToken
from src.tags import ApiTags

from .handlers import InjectBookToKoboDeviceHandler, ProxySyncBookToKoboDeviceHandler, SyncBookToKoboDeviceHandler
from .kobo_request_transformer import transform_to_kobo_request
from .kobo_tokens import KoboTokenReader
from .link_constructor import LinkConstructor
from .read_models import SqlBookSyncReadModel
from .request_mapper import map_fastapi_request_to_dto_request

logger = logging.getLogger("default")

kobo_sync_books_router = APIRouter()


def handler_factory():
    if os.environ.get("EXPIRMENT_INJECT_FIRST_BOOK") in ["1", "true", "True"]:
        return InjectBookToKoboDeviceHandler(
            read_model=SqlBookSyncReadModel(
                download_link_contructor=LinkConstructor(JWTUserDownloadBookToken),
            )
        )

    else:
        return ProxySyncBookToKoboDeviceHandler(
            ProxyService(
                request_transformer=transform_to_kobo_request,
                request_service=HttpxRequestService(),
            )
        )


@kobo_sync_books_router.get(urls.KOBO_DEVICE_LIBRARY_SYNC_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
async def kobo_device_sync(
    inbound_request: RequestDTO = Depends(map_fastapi_request_to_dto_request),
    handler: SyncBookToKoboDeviceHandler = Depends(handler_factory),
):
    kobo_token_reader = KoboTokenReader()
    command = SyncBookToKoboDeviceCommand(inbound_request=inbound_request)
    kobo_response = await handler(command)

    to_log = {
        # "inbound": inbound_request.to_dict(),
        "inbound_sync_token": kobo_token_reader.full_decode(
            inbound_request.to_dict()["headers"].get("x-kobo-synctoken", "")
        ).payload,
        # "kobo_response": kobo_response.to_dict(),
        "kobo_response_sync_token": kobo_token_reader.full_decode(
            kobo_response.headers.get("x-kobo-synctoken", "")
        ).payload,
    }
    logger.info(f"Request kwargs: {to_log}")
    response = JSONResponse(
        content=kobo_response.json(),
        status_code=kobo_response.status_code,
        headers=dict([(key, value) for key, value in kobo_response.headers.items() if "x-kobo" in key]),
    )
    return response
