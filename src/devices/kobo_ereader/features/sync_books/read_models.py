import abc
from functools import partial
from typing import TYPE_CHECKING, Callable, Optional

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from src.books.repositories import SqlBookRepo
from src.books.repositories.models import BookModel
from src.config.database import async_session_manager

from .dtos import BookToSyncDto

if TYPE_CHECKING:
    from src.books.domain import Book


class BookSyncReadModel(abc.ABC):
    @abc.abstractmethod
    async def get_books_to_sync(self) -> list["BookToSyncDto"]: ...


class InMemoryBookSyncReadModel(BookSyncReadModel):
    def __init__(self, books: list["BookToSyncDto"]):
        self._books = books

    async def get_books_to_sync(self) -> list["BookToSyncDto"]:
        return self._books


class SqlBookSyncReadModel(BookSyncReadModel):
    async_session_manager = partial(async_session_manager)

    def __init__(
        self,
        download_link_contructor: Callable[["Book"], str],
        session_overwrite: Optional[AsyncSession] = None,
    ):
        self._session = session_overwrite
        self._book_repo_class = SqlBookRepo
        self._download_link_contructor = download_link_contructor

    async def get_books_to_sync(self) -> list[BookToSyncDto]:
        async with self.async_session_manager(session_overwrite=self._session) as session:
            # TODO save books to sync in a table instead off fetching first book
            first_book_row = (await session.execute(select(BookModel))).first()
            if first_book_row:
                first_book = first_book_row[0]
            else:
                raise Exception("No books found")
            book = await self._book_repo_class(session_overwrite=session).get(first_book.uuid)
            if book:
                slug = book.title.lower().replace(" ", "-")

                download_url = self._download_link_contructor(book)

                return [
                    BookToSyncDto(
                        uuid=book.uuid,
                        slug=slug,
                        title=book.title,
                        download_url=download_url,
                    )
                ]
            else:
                raise Exception("No book found")
