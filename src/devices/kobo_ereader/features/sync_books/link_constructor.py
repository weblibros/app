from typing import TYPE_CHECKING, Type
from uuid import uuid4

from src.config import settings

if TYPE_CHECKING:
    from src.books.domain import Book
    from src.services.jwt import JWTToken


class BookHasNoFilesException(Exception):
    def __init__(self, book: "Book"):
        super().__init__(f"Book with uuid {book.uuid} has no files")


class LinkConstructor:
    def __init__(self, token_class: Type["JWTToken"]):
        self._token_class = token_class

    def __call__(self, book: "Book") -> str:
        if book_file := book.get_file():
            token = self._token_class.encode(
                {
                    "user": str(uuid4()),
                    "book": str(book.uuid),
                    "file": str(book_file.uuid),
                    "title": book.title,
                }
            )
            return f"https://{settings.DOMAIN}{book_file.href}?token={token}"
        else:
            raise BookHasNoFilesException(book)
