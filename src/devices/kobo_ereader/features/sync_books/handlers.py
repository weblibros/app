import abc
import json
from typing import TYPE_CHECKING

from src.devices.kobo_ereader.events import SyncBookToKoboDeviceCommand
from src.devices.service import ProxyService
from src.devices.service.dto import ResponseDTO

from .kobo_tokens import kobo_token_decoder, kobo_token_encoder
from .read_models import BookSyncReadModel

if TYPE_CHECKING:
    from .dtos import BookToSyncDto


class SyncBookToKoboDeviceHandler(abc.ABC):
    @abc.abstractmethod
    async def handle(self, command: SyncBookToKoboDeviceCommand) -> ResponseDTO: ...

    async def __call__(self, command: SyncBookToKoboDeviceCommand) -> ResponseDTO:
        return await self.handle(command)


class ProxySyncBookToKoboDeviceHandler(SyncBookToKoboDeviceHandler):
    def __init__(self, proxy_service: ProxyService):
        self.proxy_service = proxy_service

    async def handle(self, command: SyncBookToKoboDeviceCommand) -> ResponseDTO:
        kobo_response = await self.proxy_service.proxy(command.inbound_request)
        return kobo_response


class InjectBookToKoboDeviceHandler(SyncBookToKoboDeviceHandler):
    def __init__(self, read_model: BookSyncReadModel):
        self._read_model = read_model

    async def handle(self, command: SyncBookToKoboDeviceCommand) -> ResponseDTO:
        books_to_sync = await self._read_model.get_books_to_sync()
        kobo_entitlements = [self._build_response(book) for book in books_to_sync]

        headers = self._add_books_to_sync_in_headers(command, books_to_sync)

        return ResponseDTO(
            status_code=200,
            headers=headers,
            cookies={},
            content=json.dumps(kobo_entitlements).encode(),
            http_version="HTTP/1.1",
        )

    def _add_books_to_sync_in_headers(
        self, command: SyncBookToKoboDeviceCommand, books_to_sync: list["BookToSyncDto"]
    ) -> dict:
        headers = dict(
            [(key, value) for key, value in command.inbound_request.headers.to_dict().items() if "x-kobo" in key]
        )
        kobo_sync_token = kobo_token_decoder.decode(headers.get("x-kobo-synctoken", ""))
        kobo_sync_token.add_books_to_sync(books_to_sync)
        headers["x-kobo-synctoken"] = kobo_token_encoder.encode(kobo_sync_token)
        return headers

    def _build_response(self, book: "BookToSyncDto"):
        entitlement_id = str(book.uuid)
        title = book.title
        slug = book.slug
        download_url = book.download_url
        datetime_now = book.created_at_str
        return {
            "NewEntitlement": {
                "BookEntitlement": {
                    "ActivePeriod": {"From": datetime_now},
                    "IsRemoved": False,
                    "Status": "Active",
                    "Accessibility": "Full",
                    "CrossRevisionId": "9bae730c-23f9-3f23-be03-1f3a5ac0eef9",
                    "RevisionId": "c1d3e2fa-0dfb-4032-98eb-9f3c10f8cc51",
                    "IsHiddenFromArchive": False,
                    "Id": entitlement_id,
                    "Created": datetime_now,
                    "LastModified": datetime_now,
                    "IsLocked": False,
                    "OriginCategory": "Purchased",
                },
                "ReadingState": {
                    "EntitlementId": entitlement_id,
                    "Created": datetime_now,
                    "LastModified": datetime_now,
                    "StatusInfo": {
                        "LastModified": datetime_now,
                        "Status": "ReadyToRead",
                        "TimesStartedReading": 0,
                    },
                    "Statistics": {"LastModified": datetime_now},
                    "CurrentBookmark": {"LastModified": datetime_now},
                    "PriorityTimestamp": datetime_now,
                },
                "BookMetadata": {
                    "CrossRevisionId": "9bae730c-23f9-3f23-be03-1f3a5ac0eef9",
                    "RevisionId": "c1d3e2fa-0dfb-4032-98eb-9f3c10f8cc51",
                    "Publisher": {},
                    "PublicationDate": "2014-11-30T00:00:00.0000000",
                    "Language": "en",
                    "Isbn": "1230000283080",
                    "Genre": "180861c2-21ea-4793-952b-ecd39613adfe",
                    "Slug": slug,
                    "CoverImageId": "305ab3a8-4998-47f9-a022-f7cfa4323f3f",
                    "ApplicableSubscriptions": [],
                    "IsSocialEnabled": False,
                    "WorkId": "9bae730c-23f9-3f23-be03-1f3a5ac0eef9",
                    "ExternalIds": [],
                    "IsPreOrder": False,
                    "ContributorRoles": [],
                    "IsInternetArchive": False,
                    "IsAnnotationExportEnabled": False,
                    "IsAISummaryEnabled": False,
                    "EntitlementId": entitlement_id,
                    "Title": title,
                    "Description": "",
                    "Categories": [],
                    "DownloadUrls": [
                        {
                            "DrmType": "None",
                            "Format": "EPUB_SAMPLE",
                            "Url": download_url,
                            "Platform": "Android",
                            "Size": 978752,
                        },
                    ],
                    "Contributors": [],
                    "Series": {},
                    "CurrentDisplayPrice": {"TotalAmount": 0, "CurrencyCode": "EUR"},
                    "CurrentLoveDisplayPrice": {"TotalAmount": 0},
                    "IsEligibleForKoboLove": False,
                    "PhoneticPronunciations": {},
                    "RelatedGroupId": "00000000-0000-0000-0000-000000000001",
                },
            }
        }
