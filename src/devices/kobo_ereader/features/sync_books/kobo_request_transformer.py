import re
from typing import Callable

from src.devices.kobo_ereader.features.map_url_to_kobo import transform_to_kobo_url
from src.devices.service.dto import Header, Headers, RequestDTO, Url


class KoboRequestTransformer:
    def __init__(self):
        self._url_transformer: Callable[[str], str] = transform_to_kobo_url

    def __call__(self, inbound_request: RequestDTO) -> RequestDTO:
        outbound_request = RequestDTO(
            url=self._transform_url(inbound_request.url),
            body=inbound_request.body,
            method=inbound_request.method,
            cookies=inbound_request.cookies,
            headers=self._filter_headers(inbound_request.headers),
        )
        return outbound_request

    def _transform_url(self, inbound_url: Url) -> Url:
        outbound_url = Url(self._url_transformer(inbound_url.value))
        return outbound_url

    def _filter_headers(self, inbound_headers) -> Headers:
        filter_keys = [
            "^x-kobo-.*",
            "^authorization$",
        ]
        outbound_headers: list[Header] = []
        for header in inbound_headers.headers:
            if re.search("|".join(filter_keys), header.key):
                outbound_headers.append(Header(key=header.key, value=header.value))
        return Headers(headers=outbound_headers)


transform_to_kobo_request = KoboRequestTransformer()
