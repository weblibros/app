import re
from urllib.parse import urlparse


class TransformToKoboUrl:
    _internal_kobo_url_regres = "^/api/v1/devices/kobo"

    def __call__(self, url: str) -> str:
        parsed_url = urlparse(url)
        assert re.search(self._internal_kobo_url_regres, parsed_url.path), f"Invalid url: {url}"
        split_path = re.split(self._internal_kobo_url_regres, parsed_url.path)
        assert len(split_path) == 2, f"Invalid url: {url}"
        replaced_path = f"https://storeapi.kobo.com{split_path[1]}"
        if parsed_url.query:
            replaced_path += f"?{parsed_url.query}"
        return replaced_path


transform_to_kobo_url = TransformToKoboUrl()
