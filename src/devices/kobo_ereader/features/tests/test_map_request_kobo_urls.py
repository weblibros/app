import pytest

from src.devices.urls import (
    KOBO_DEVICE_AFFILIATE_URL,
    KOBO_DEVICE_ASSETS_URL,
    KOBO_DEVICE_AUTH_DEVICE_URL,
    KOBO_DEVICE_AUTH_REFRESH_URL,
    KOBO_DEVICE_BOOK_SUPSTRIPTION_URL,
    KOBO_DEVICE_DEALS_URL,
    KOBO_DEVICE_INITIALIZATION_URL,
    KOBO_DEVICE_USER_LOYALTY_URL,
    KOBO_DEVICE_USER_PROFILE_URL,
)

from ..map_url_to_kobo import transform_to_kobo_url


@pytest.mark.parametrize(
    "url_path, expected_url",
    [
        (
            f"http://exampe.com{KOBO_DEVICE_AFFILIATE_URL}",
            "https://storeapi.kobo.com/v1/affiliate",
        ),
        (
            f"http://exampe.com{KOBO_DEVICE_AFFILIATE_URL}?PlatformID=00000000-0000-0000-0000-000000000111&SerialNumber=ecdf63198e73c9475c2cc38e0abc7333",
            "https://storeapi.kobo.com/v1/affiliate?PlatformID=00000000-0000-0000-0000-000000000111&SerialNumber=ecdf63198e73c9475c2cc38e0abc7333",
        ),
        (
            f"http://exampe.com{KOBO_DEVICE_INITIALIZATION_URL}",
            "https://storeapi.kobo.com/v1/initialization",
        ),
        (
            f"http://exampe.com{KOBO_DEVICE_USER_PROFILE_URL}",
            "https://storeapi.kobo.com/v1/user/profile",
        ),
        (
            f"http://exampe.com{KOBO_DEVICE_USER_PROFILE_URL}?foo=bar&foo=baz&foo=foobar",
            "https://storeapi.kobo.com/v1/user/profile?foo=bar&foo=baz&foo=foobar",
        ),
        (
            f"http://exampe.com{KOBO_DEVICE_USER_LOYALTY_URL}",
            "https://storeapi.kobo.com/v1/user/loyalty/benefits",
        ),
        (
            f"http://exampe.com{KOBO_DEVICE_DEALS_URL}",
            "https://storeapi.kobo.com/v1/deals",
        ),
        (
            f"http://exampe.com{KOBO_DEVICE_ASSETS_URL}",
            "https://storeapi.kobo.com/v1/assets",
        ),
        (
            f"http://exampe.com{KOBO_DEVICE_BOOK_SUPSTRIPTION_URL}",
            "https://storeapi.kobo.com/v1/products/books/subscriptions",
        ),
        (
            f"http://exampe.com{KOBO_DEVICE_AUTH_REFRESH_URL}",
            "https://storeapi.kobo.com/v1/auth/refresh",
        ),
        (
            f"http://exampe.com{KOBO_DEVICE_AUTH_DEVICE_URL}",
            "https://storeapi.kobo.com/v1/auth/device",
        ),
        (
            "http://exampe.com/api/v1/devices/kobo/v1",
            "https://storeapi.kobo.com/v1",
        ),
        (
            "http://exampe.com/api/v1/devices/kobo/v1/foo/bar?foo=bar&foo=baz&foo=foobar",
            "https://storeapi.kobo.com/v1/foo/bar?foo=bar&foo=baz&foo=foobar",
        ),
    ],
)
def test_kobo_device_urls(url_path, expected_url):
    assert transform_to_kobo_url(url_path) == expected_url


def test_url_transformer_throws_error_on_url_without_kobo_prefix():
    with pytest.raises(AssertionError):
        transform_to_kobo_url("http://exampe.com/v1/foo/bar?foo=bar&foo=baz&foo=foobar")
