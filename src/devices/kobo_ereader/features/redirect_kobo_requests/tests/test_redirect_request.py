import pytest

from src.devices.urls import (
    KOBO_DEVICE_AFFILIATE_URL,
    KOBO_DEVICE_ASSETS_URL,
    KOBO_DEVICE_AUTH_DEVICE_URL,
    KOBO_DEVICE_AUTH_REFRESH_URL,
    KOBO_DEVICE_BOOK_SUPSTRIPTION_URL,
    KOBO_DEVICE_DEALS_URL,
    KOBO_DEVICE_INITIALIZATION_URL,
    KOBO_DEVICE_USER_LOYALTY_URL,
    KOBO_DEVICE_USER_PROFILE_URL,
)

path_proxy_service_factory = "src.devices.kobo_ereader.features.proxy_kobo_requests.router.proxy_service_factory"


@pytest.mark.parametrize(
    "url_path, expected_url",
    [
        (
            KOBO_DEVICE_AFFILIATE_URL,
            "https://storeapi.kobo.com/v1/affiliate",
        ),
        (
            f"{KOBO_DEVICE_AFFILIATE_URL}?PlatformID=00000000-0000-0000-0000-000000000111&SerialNumber=ecdf63198e73c9475c2cc38e0abc7333",
            "https://storeapi.kobo.com/v1/affiliate?PlatformID=00000000-0000-0000-0000-000000000111&SerialNumber=ecdf63198e73c9475c2cc38e0abc7333",
        ),
        (
            KOBO_DEVICE_INITIALIZATION_URL,
            "https://storeapi.kobo.com/v1/initialization",
        ),
        (
            KOBO_DEVICE_USER_PROFILE_URL,
            "https://storeapi.kobo.com/v1/user/profile",
        ),
        (
            f"{KOBO_DEVICE_USER_PROFILE_URL}?foo=bar&foo=baz&foo=foobar",
            "https://storeapi.kobo.com/v1/user/profile?foo=bar&foo=baz&foo=foobar",
        ),
        (
            KOBO_DEVICE_USER_LOYALTY_URL,
            "https://storeapi.kobo.com/v1/user/loyalty/benefits",
        ),
        (
            KOBO_DEVICE_DEALS_URL,
            "https://storeapi.kobo.com/v1/deals",
        ),
        (
            KOBO_DEVICE_ASSETS_URL,
            "https://storeapi.kobo.com/v1/assets",
        ),
        (
            KOBO_DEVICE_BOOK_SUPSTRIPTION_URL,
            "https://storeapi.kobo.com/v1/products/books/subscriptions",
        ),
    ],
)
async def test_device_get_endpoint_redirects_to_kobo(url_path, expected_url, client):
    response = await client.get(url_path)
    assert response.status_code == 307
    assert response.next_request.url == expected_url


@pytest.mark.parametrize(
    "url_path, expected_url",
    [
        (
            KOBO_DEVICE_AUTH_REFRESH_URL,
            "https://storeapi.kobo.com/v1/auth/refresh",
        ),
        (
            KOBO_DEVICE_AUTH_DEVICE_URL,
            "https://storeapi.kobo.com/v1/auth/device",
        ),
    ],
)
async def test_device_post_endpoint_redirects_to_kobo(url_path, expected_url, client):
    response = await client.post(url_path)
    assert response.status_code == 307
    assert response.next_request.url == expected_url


@pytest.mark.parametrize(
    "url_path, expected_url",
    [
        (
            "/api/v1/devices/kobo/foo/bar",
            "https://storeapi.kobo.com/foo/bar",
        ),
    ],
)
async def test_device_get_endpoint_redirects_to_kobo_from_unknown_urls(url_path, expected_url, client):
    response = await client.get(url_path)
    assert response.status_code == 307
    assert response.next_request.url == expected_url
