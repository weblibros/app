import logging

from fastapi import APIRouter, Request
from fastapi.responses import RedirectResponse

from src.devices import urls
from src.devices.kobo_ereader.features.map_url_to_kobo import TransformToKoboUrl
from src.tags import ApiTags

logger = logging.getLogger("default")

kobo_redirect_router = APIRouter()


def url_transformer_factory():
    return TransformToKoboUrl()


@kobo_redirect_router.get(urls.KOBO_DEVICE_DEALS_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(urls.KOBO_DEVICE_ASSETS_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(urls.KOBO_DEVICE_AFFILIATE_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(urls.KOBO_DEVICE_INITIALIZATION_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(urls.KOBO_DEVICE_USER_PROFILE_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(urls.KOBO_DEVICE_USER_LOYALTY_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(
    urls.KOBO_DEVICE_USER_RECOMMENDATIONS_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO]
)
@kobo_redirect_router.get(
    urls.KOBO_DEVICE_USER_RECOMMENDATIONS_FEEDBACK_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO]
)
@kobo_redirect_router.post(
    urls.KOBO_DEVICE_USER_RECOMMENDATIONS_FEEDBACK_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO]
)
@kobo_redirect_router.get(urls.KOBO_DEVICE_USER_WISHLIST_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(urls.KOBO_DEVICE_BOOK_SUPSTRIPTION_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(urls.KOBO_DEVICE_BOOK_SERIES_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(urls.KOBO_DEVICE_PRODUCT_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(urls.KOBO_DEVICE_PRODUCT_NEXT_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(urls.KOBO_DEVICE_PRODUCT_PRICES_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(urls.KOBO_DEVICE_PRODUCT_FEATURED_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.get(
    urls.KOBO_DEVICE_PRODUCT_UNKNOWN_FEATURED_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO]
)
@kobo_redirect_router.get(urls.KOBO_DEVICE_PRODUCT_DAILYDEAL_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.post(urls.KOBO_DEVICE_AUTH_REFRESH_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.post(urls.KOBO_DEVICE_AUTH_DEVICE_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.delete(urls.KOBO_DEVICE_LIBRARY_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.put(urls.KOBO_DEVICE_LIBRARY_STATE_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
@kobo_redirect_router.post(urls.KOBO_DEVICE_LIBRARY_TAG_ITEMS_URL, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
async def redirect_request_to_kobo(request: Request):
    url_transformer = url_transformer_factory()
    redirect_url = url_transformer(str(request.url))
    return RedirectResponse(redirect_url)


# Ugly hack to map unknown requests from kobo device
async def redirect_unknown_request_to_kobo(request: Request):
    url_transformer = url_transformer_factory()
    redirect_url = url_transformer(str(request.url))

    to_log = {
        "unknown_request": str(request.url),
        "inbound_headers": request.headers,
        "inbound_body": await request.body(),
        "inbound_cookies": request.cookies,
        "redirect_to": redirect_url,
    }
    logger.info(f"Unmapped Request kwargs: {to_log}")
    return RedirectResponse(redirect_url)


for i in range(1, 11):
    part_string = ["/{" + f"part{j}" + "}" for j in range(i)]
    url = "/api/v1/devices/kobo" + "".join(part_string)

    kobo_redirect_router.add_api_route(
        url,
        redirect_unknown_request_to_kobo,
        methods=["GET", "POST", "PUT", "PATCH", "DELETE"],
        include_in_schema=False,
    )
    kobo_redirect_router.add_api_route(
        url + "/",
        redirect_unknown_request_to_kobo,
        methods=["GET", "POST", "PUT", "PATCH", "DELETE"],
        include_in_schema=False,
    )
