from uuid import UUID, uuid4

from src.devices.urls import KOBO_DEVICE_ANALYTICS_GET_TESTS


async def test_analytics_post_tests_request_returns_200(client):
    testkey = uuid4()
    response = await client.post(
        KOBO_DEVICE_ANALYTICS_GET_TESTS,
        json={
            "AffiliateName": "Kobo",
            "ApplicationVersion": "0.0.1",
            "PlatformId": "00000000-0000-0000-0000-00000000011",
            "SerialNumber": "fake",
            "TestKey": str(testkey),
        },
    )
    assert response.status_code == 200


async def test_analytics_post_tests_request_returns_testkey(client):
    testkey = uuid4()
    response = await client.post(
        KOBO_DEVICE_ANALYTICS_GET_TESTS,
        json={
            "AffiliateName": "Kobo",
            "ApplicationVersion": "0.0.1",
            "PlatformId": "00000000-0000-0000-0000-00000000011",
            "SerialNumber": "fake",
            "TestKey": str(testkey),
        },
    )
    assert response.status_code == 200
    assert response.json()["TestKey"] == str(testkey)


async def test_analytics_post_tests_request_returns_testkey_without_passing_variable(client):
    testkey = uuid4()
    response = await client.post(
        KOBO_DEVICE_ANALYTICS_GET_TESTS,
        json={},
    )
    assert response.status_code == 200
    testkey = response.json()["TestKey"]
    assert isinstance(UUID(testkey), UUID)
