from uuid import uuid4

from fastapi import APIRouter
from pydantic import BaseModel, Field

from src.devices.urls import KOBO_DEVICE_ANALYTICS_GET_TESTS
from src.tags import ApiTags

kobo_mocked_router = APIRouter()


class AnalyticsGetBody(BaseModel):
    AffiliateName: str = Field(default="Kobo")
    ApplicationVersion: str = Field(default="0.0.1")
    PlatformId: str = Field(default="00000000-0000-0000-0000-00000000011")
    SerialNumber: str = Field(default="111111111111")
    TestKey: str = Field(default_factory=lambda: str(uuid4()))


class AnalyticsGetTestsResponse(BaseModel):
    TestKey: str
    Result: str = "Success"
    Tests: dict = {}


@kobo_mocked_router.post(KOBO_DEVICE_ANALYTICS_GET_TESTS, tags=[ApiTags.TAG_DEVICES, ApiTags.TAG_DEVICE_KOBO])
async def mock_kobo_request_analytics_get_tests(body: AnalyticsGetBody):
    return AnalyticsGetTestsResponse(TestKey=body.TestKey)
