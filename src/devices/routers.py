from fastapi import APIRouter

from .kobo_ereader.features.mock_kobo_requests.router import kobo_mocked_router
from .kobo_ereader.features.redirect_kobo_requests.router import kobo_redirect_router
from .kobo_ereader.features.sync_books.router import kobo_sync_books_router

devices_router = APIRouter()

devices_router.include_router(kobo_sync_books_router)
devices_router.include_router(kobo_mocked_router)
devices_router.include_router(kobo_redirect_router)
