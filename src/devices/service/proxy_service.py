from abc import ABC, abstractmethod
from typing import TYPE_CHECKING, Callable, Optional

import httpx
from httpx import AsyncClient

from src.devices.service.dto import FormBody, JsonBody

from .dto import ResponseDTO

if TYPE_CHECKING:
    from .dto import RequestDTO


class RequestService(ABC):
    @abstractmethod
    async def request(self, request: "RequestDTO") -> "ResponseDTO":
        pass


class HttpxRequestService:
    _http_client: type[AsyncClient] = AsyncClient

    async def request(self, request: "RequestDTO") -> "ResponseDTO":
        async with self._http_client(cookies=request.cookies.to_dict()) as client:
            httpx_request = client.build_request(
                method=request.method,
                url=request.url.value,
                json=request.body.value if isinstance(request.body, JsonBody) else None,
                data=request.body.value if isinstance(request.body, FormBody) else None,
                headers=request.headers.to_dict(),
            )
            httpx_response = await client.send(httpx_request)
            return ResponseDTO(
                status_code=httpx_response.status_code,
                headers=self._httpx_headers_to_dict(httpx_response.headers),
                cookies=self._httpx_cookies_to_dict(httpx_response.cookies),
                content=httpx_response.content,
                http_version=httpx_response.http_version,
            )

    def _httpx_cookies_to_dict(self, cookies: httpx.Cookies) -> dict[str, str]:
        cookies_dict = {}
        for key, value in cookies.items():
            cookies_dict[key] = value
        return cookies_dict

    def _httpx_headers_to_dict(self, headers: httpx.Headers) -> dict[str, str]:
        headers_dict = {}
        for key, value in headers.items():
            headers_dict[key] = value
        return headers_dict


class ProxyService:
    _inbound_request: Optional["RequestDTO"]
    _outbound_request: Optional["RequestDTO"]

    def __init__(self, request_transformer: Callable[["RequestDTO"], "RequestDTO"], request_service: RequestService):
        self._inbound_request = None
        self._outbound_request = None
        self._request_transformer = request_transformer
        self._request_service = request_service

    async def proxy(self, request: "RequestDTO") -> "ResponseDTO":
        self._inbound_request = request
        self._outbound_request = self._request_transformer(self._inbound_request)
        return await self._request_service.request(self._outbound_request)

    @property
    def inbound_request(self) -> "RequestDTO":
        if self._inbound_request is None:
            raise RuntimeError("No inbound request found")
        else:
            return self._inbound_request

    @property
    def outbound_request(self) -> "RequestDTO":
        if self._outbound_request is None:
            raise RuntimeError("No outbound request found")
        else:
            return self._outbound_request
