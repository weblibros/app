from unittest.mock import AsyncMock

from src.devices.service import ProxyService
from src.devices.service.dto import ResponseDTO
from src.devices.service.proxy_service import RequestService
from src.devices.urls import KOBO_DEVICE_USER_PROFILE_URL

from ..dto import Body, BodyType, Cookies, Headers, RequestDTO, RequestMethod, Url


def dummy_transform_request(request: RequestDTO) -> RequestDTO:
    return RequestDTO(
        url=Url("http://example.com"),
        body=Body(
            body_args=[],
            body_type=BodyType.EMPTY,
        ),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(headers=[]),
    )


class FakeRequestService(RequestService):
    async def request(self, request: "RequestDTO") -> "ResponseDTO":
        return ResponseDTO(
            status_code=200,
            headers={},
            cookies={},
            content='{"message": "Device log request received"}',
            http_version="HTTP/1.1",
        )


def proxy_service_factory():
    request_wrapper = FakeRequestService()

    return ProxyService(
        request_transformer=dummy_transform_request,
        request_service=request_wrapper,
    )


async def test_proxy_can_read_request():
    proxy = proxy_service_factory()
    request = RequestDTO(
        url=Url("http://localhost:8000/api/v1/devices/test"),
        body=Body(
            body_args=[],
            body_type=BodyType.EMPTY,
        ),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(headers=[]),
    )
    await proxy.proxy(request)
    assert proxy.inbound_request.url.value == "http://localhost:8000/api/v1/devices/test"


async def test_proxy_can_read_transformed_outbound_request():
    proxy = proxy_service_factory()
    request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_USER_PROFILE_URL}?foo=bar&foo=baz&foo=foobar"),
        body=Body(
            body_args=[],
            body_type=BodyType.EMPTY,
        ),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(headers=[]),
    )
    await proxy.proxy(request)
    assert proxy.outbound_request.url.value == "http://example.com"


async def test_proxy_service_calls_transformed_request():
    request_wrapper = AsyncMock(RequestService)
    proxy = ProxyService(request_transformer=dummy_transform_request, request_service=request_wrapper)
    request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_USER_PROFILE_URL}?foo=bar&foo=baz&foo=foobar"),
        body=Body(
            body_args=[],
            body_type=BodyType.EMPTY,
        ),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(headers=[]),
    )
    await proxy.proxy(request)
    assert proxy.outbound_request.url.value == "http://example.com"
    request_wrapper.request.assert_awaited_once_with(proxy.outbound_request)


async def test_proxy_service_returns_outbound_request_response():
    request_wrapper = FakeRequestService()
    proxy = ProxyService(request_transformer=dummy_transform_request, request_service=request_wrapper)
    request = RequestDTO(
        url=Url(f"http://localhost:8000{KOBO_DEVICE_USER_PROFILE_URL}?foo=bar&foo=baz&foo=foobar"),
        body=Body(
            body_args=[],
            body_type=BodyType.EMPTY,
        ),
        method=RequestMethod.GET,
        cookies=Cookies(cookies=[]),
        headers=Headers(headers=[]),
    )
    response = await proxy.proxy(request)
    assert response.status_code == 200
    assert response.json() == {"message": "Device log request received"}
    assert response.headers == {}
    assert response.cookies == {}
    assert response.content == '{"message": "Device log request received"}'
    assert response.http_version == "HTTP/1.1"
