import json
from dataclasses import dataclass, field
from enum import StrEnum
from typing import Sequence
from urllib.parse import parse_qs, urlparse

from starlette.datastructures import UploadFile


class QueryParams:
    _value_store: dict[str, str | list[str]]

    def __init__(self, query_params_str: str):
        self._process_query_params(query_params_str)

    def _process_query_params(self, query_params: str):
        query_dict: dict[str, str | list[str]] = {}
        if query_params:
            parsed_query_params = parse_qs(query_params)
            for key, value in parsed_query_params.items():
                if len(value) == 1:
                    query_dict[key] = value[0]
                else:
                    query_dict[key] = value
        self._value_store = query_dict

    def to_dict(self):
        return self._value_store

    def __bool__(self):
        return bool(self._value_store)

    def __getitem__(self, key: str):
        return self._value_store[key]

    def __str__(self) -> str:
        query_params_str_list = []
        for key, value in self._value_store.items():
            if isinstance(value, list):
                for v in value:
                    query_params_str_list.append(f"{key}={v}")
            else:
                query_params_str_list.append(f"{key}={value}")
        if query_params_str_list:
            return "?" + "&".join(query_params_str_list)
        else:
            return ""

    def __add__(self, second):
        assert isinstance(second, str)
        return str(self) + second

    def __radd__(self, second):
        assert isinstance(second, str)
        return second + str(self)


class Url:
    scheme: str
    netloc: str
    path: str
    params: str
    query_params: QueryParams
    fragment: str
    _raw_value: str

    def __init__(self, url: str):
        self._raw_value = url
        self.scheme, self.netloc, self.path, self.params, raw_query, self.fragment = self._parse_url(url)
        self.query_params = QueryParams(raw_query)

    def _parse_url(self, url: str):
        # <scheme>://<netloc>/<path>;<params>?<query>#<fragment>

        parsed_url = urlparse(url)
        return (
            parsed_url.scheme,
            parsed_url.netloc,
            parsed_url.path,
            parsed_url.params,
            parsed_url.query,
            parsed_url.fragment,
        )

    @property
    def value(self):
        return self._raw_value

    def to_dict(self):
        return {
            "scheme": self.scheme,
            "netloc": self.netloc,
            "path": self.path,
            "params": self.params,
            "query_params": self.query_params.to_dict(),
            "fragement": self.fragment,
        }


@dataclass
class BodyArgs:
    key: str
    value: str

    def to_dict(self):
        return {self.key: self.value}


@dataclass
class BodyArgsUploadFile:
    key: str
    value: UploadFile

    def to_dict(self):
        return {self.key: str(self.value)}


class BodyType(StrEnum):
    JSON = "JSON"
    FORM = "FORM"
    EMPTY = "EMPTY"


@dataclass
class Body:
    body_args: Sequence[BodyArgs | BodyArgsUploadFile]
    body_type: BodyType

    @property
    def value(self):
        body_args_dict = {}
        for body_arg in self.body_args:
            body_args_dict.update(body_arg.to_dict())
        return body_args_dict

    def to_dict(self):
        return {
            "data": self.value,
            "type": self.body_type.value,
        }


@dataclass
class FormBody(Body):
    body_type: BodyType = field(default=BodyType.FORM)


@dataclass
class JsonBody(Body):
    body_type: BodyType = field(default=BodyType.JSON)


@dataclass
class EmptyBody(Body):
    body_args: Sequence[BodyArgs | BodyArgsUploadFile] = field(default_factory=list)
    body_type: BodyType = field(default=BodyType.EMPTY)

    def to_dict(self):
        return {}


class RequestMethod(StrEnum):
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    PATCH = "PATCH"
    DELETE = "DELETE"


@dataclass
class Cookie:
    key: str
    value: str


@dataclass
class Cookies:
    cookies: Sequence[Cookie]

    def to_dict(self):
        cookies_dict = {}
        for cookie in self.cookies:
            cookies_dict[cookie.key] = cookie.value
        return cookies_dict


@dataclass
class Header:
    key: str
    value: str


@dataclass
class Headers:
    headers: Sequence[Header]

    def to_dict(self):
        ignore_headers = ["x-request-id"]
        headers_dict = {}
        for header in self.headers:
            if header.key not in ignore_headers:
                headers_dict[header.key] = header.value
        return headers_dict


@dataclass
class RequestDTO:
    url: Url
    body: Body
    method: RequestMethod
    cookies: Cookies
    headers: Headers

    def to_dict(self):
        return {
            "url": self.url.to_dict(),
            "body": self.body.to_dict(),
            "method": self.method.value,
            "cookies": self.cookies.to_dict(),
            "headers": self.headers.to_dict(),
        }


class ResponseDTO:
    def __init__(
        self,
        status_code: int,
        headers: dict,
        cookies: dict,
        content: str | bytes,
        http_version: str,
    ):
        self.status_code = status_code
        self.headers = headers
        self.cookies = cookies
        self._content = content
        self.http_version = http_version

    @property
    def content(self) -> str:
        if isinstance(self._content, bytes):
            return self._content.decode("utf-8")
        else:
            return self._content

    def json(self):
        return json.loads(self.content)

    def to_dict(self):
        if self.headers.get("content-type") == "application/json":
            content = self.json()
        else:
            content = self.content

        return {
            "status_code": self.status_code,
            "headers": self.headers,
            "cookies": self.cookies,
            "content": content,
            "http_version": self.http_version,
        }
