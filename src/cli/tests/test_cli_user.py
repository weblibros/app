import pytest

from src.accounts.repositories.models import UserModel
from src.cli.user import async_create_user_from_cli, async_remove_user_from_cli


@pytest.mark.skip
async def test_cli_create_user():
    email = "test_cli_create_user@fake.com"
    user = await async_create_user_from_cli(email, password="test")
    assert isinstance(user, UserModel)
    assert user.email == email
    await async_remove_user_from_cli(email)


@pytest.mark.skip
async def test_cli_create_user_no_exp_called_twice():
    email = "test_cli_create_user_called_twice@fake.com"
    user = await async_create_user_from_cli(email, password="test")
    await async_create_user_from_cli(email, password="test")
    assert isinstance(user, UserModel)
    await async_remove_user_from_cli(email)
