import os
from asyncio import run as aiorun
from pathlib import Path
from typing import TYPE_CHECKING, Annotated
from uuid import UUID

import typer
from rich.progress import track

from src.accounts.repositories import UserRepo
from src.accounts.users import User
from src.books.domain import Book
from src.books.features.create_book.handlers import CreateBookHandler
from src.books.unit_of_work import SqlAlchemyUnitOfWork as SqlAlchemyBookUnitOfWork
from src.file_management.domain import TempFile
from src.file_management.domain.events import (
    EbookFileImported,
    ImportLocalEbookFileCommand,
    TempEbookFileLocallyLoadedEvent,
)
from src.file_management.features.import_ebook_file.handlers import ImportEbookHandler
from src.file_management.unit_of_work import SqlAlchemyImportManagerUnitOfWork
from src.libraries.features.add_book_to_library import add_book_to_library

if TYPE_CHECKING:
    from src.events import Event

typer_book = typer.Typer()


def no_dispatch(event: "Event"):
    pass


class InMemoryDispatcher:
    def __init__(self, memory: list["Event"]):
        self.memory = memory

    def __call__(self, event: "Event"):
        self.memory.append(event)


class EbookImporterv2:
    def _import_ebook_file_handler(self, memory: list["Event"]):
        ebook_file_handler = ImportEbookHandler(SqlAlchemyImportManagerUnitOfWork(dispatch=InMemoryDispatcher(memory)))
        return ebook_file_handler

    def _create_book_handler(self):
        create_book_handler = CreateBookHandler(SqlAlchemyBookUnitOfWork(dispatch=no_dispatch))
        return create_book_handler

    async def handle(self, command: ImportLocalEbookFileCommand) -> Book | None:
        temp_file = await self._genenerate_temp_file(command)
        if not self._can_import(temp_file):
            return None

        temp_file_loaded_event = TempEbookFileLocallyLoadedEvent(
            uploaded_by_user_uuid=command.user_uuid,
            destination_library_uuid=command.library_uuid,
            temp_file=temp_file,
        )
        memory_event_import_files: list["Event"] = []
        upload_file_handler = self._import_ebook_file_handler(memory_event_import_files)
        await upload_file_handler(temp_file_loaded_event)

        ebook_file_imported_event = self._find_ebook_file_imported_event(memory_event_import_files)
        create_book_handler = self._create_book_handler()
        book = await create_book_handler(ebook_file_imported_event)
        await add_book_to_library(
            book_uuid=book.uuid,
            library_uuid=command.library_uuid,
        )
        return book

    def _find_ebook_file_imported_event(self, memory: list["Event"]) -> EbookFileImported:
        for event in memory:
            if isinstance(event, EbookFileImported):
                return event

        raise ValueError("Ebook file imported event not found")

    async def _genenerate_temp_file(self, command: ImportLocalEbookFileCommand):
        return await TempFile.from_path(command.file_path)

    def _can_import(self, temp_file: TempFile) -> bool:
        return temp_file.file_type.is_ebook


def all_files_in_file_folder(file_or_folder: str) -> list[str]:
    files_in_folder = []
    if os.path.isdir(file_or_folder):
        for file_or_folder_in_sub_dir in os.listdir(file_or_folder):
            files_in_folder.extend(all_files_in_file_folder(os.path.join(file_or_folder, file_or_folder_in_sub_dir)))
    elif os.path.isfile(file_or_folder):
        files_in_folder.append(file_or_folder)

    return files_in_folder


@typer_book.command("import")
def import_folder_or_file(
    dir: Annotated[str, typer.Option(help="File or folder of ebooks")],
    user: Annotated[str, typer.Option(help="user uuid or email")],
):
    aiorun(async_import_folder_or_file(dir, user))


async def get_user(user_email_or_uuid: str) -> User:
    user_repo = UserRepo()
    try:
        user_uuid = UUID(user_email_or_uuid)
    except ValueError:
        user_uuid = None

    if user_uuid:
        return await user_repo.get_by_uuid(user_uuid)
    else:
        return await user_repo.get_by_email(user_email_or_uuid)


async def async_import_folder_or_file(file_or_folder: str, user_email_or_uuid: str):
    handler = EbookImporterv2()
    user = await get_user(user_email_or_uuid)
    files_in_folder = all_files_in_file_folder(file_or_folder)
    total = 0
    for file in track(files_in_folder, description="Importing..."):
        command = ImportLocalEbookFileCommand(
            file_path=Path(file),
            user_uuid=user.uuid,
            library_uuid=user.personal_library_uuid,
        )
        imported_book = await handler.handle(command)
        if imported_book:
            total += 1

    print(f"Imported {total} books.")
