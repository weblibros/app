import subprocess

import typer

typer_migrate = typer.Typer()


@typer_migrate.command("latest")
def migrate_alembic_to_head():
    subprocess.run(["alembic", "upgrade", "head"])
