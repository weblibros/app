from asyncio import run as aiorun

import typer

from src.cli.book import async_import_folder_or_file
from src.cli.user import async_create_user_from_cli

typer_generate = typer.Typer()
default_user = "user@fake.com"
fake_users = [
    {
        "email": default_user,
        "password": "Q12345678!",
        "is_active": True,
    },
    {
        "email": "non_active_user@fake.com",
        "password": "Q12345678!",
        "is_active": False,
    },
    {
        "email": "no_books_user@fake.com",
        "password": "Q12345678!",
        "is_active": True,
    },
]

user_fake_books = [
    {
        "email": default_user,
        "path": "./packages/e2e/dummy_files",
    },
]


async def async_generate_fake_users():
    for user_info in fake_users:
        await async_create_user_from_cli(**user_info)


async def async_import_files_for_users():
    for user_fake_book in user_fake_books:
        await async_import_folder_or_file(
            file_or_folder=user_fake_book["path"],
            user_email_or_uuid=user_fake_book["email"],
        )


async def async_generate_e2e_data():
    await async_generate_fake_users()
    await async_import_files_for_users()


@typer_generate.command("e2e-data")
def generate_e2e_data():
    aiorun(async_generate_e2e_data())
