from asyncio import run as aiorun

import typer

from src.accounts.repositories.models import UserModel
from src.accounts.repositories.queries import create, remove, selector
from src.config.database import async_session_maker
from src.libraries.features.create_personal_library import create_personal_library

typer_user = typer.Typer()


async def async_create_user_from_cli(email: str, password: str, **kwargs) -> UserModel | None:
    print(f"creating user: {email}")
    async with async_session_maker() as session:
        user_exist = await selector.find_user_by_email(session, email)
        if user_exist:
            print(f"email: {email} is already in use")
            user = None
        else:
            user = await create.create_user(session, password=password, email=email, **kwargs)
            await session.commit()
            await create_personal_library(user.uuid)
            print(f"user {user.email} created")

    return user


async def async_remove_user_from_cli(email: str):
    print(f"remove user: {email}")
    async with async_session_maker() as session:
        user = await selector.find_user_by_email(session, email)
        if user:
            await remove.remove_user(session, user)
            print(f"user {email} was removed database")
        else:
            print(f"user {email} was not found in database")


@typer_user.command("create")
def cli_create_user(email: str, password: str, **kwargs):
    aiorun(async_create_user_from_cli(password=password, email=email, **kwargs))


@typer_user.command("remove")
def cli_remove_user(email: str):
    aiorun(async_remove_user_from_cli(email=email))
