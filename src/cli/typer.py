import typer

from src.cli import book, generate, migrate, user

typer_app = typer.Typer()


typer_app.add_typer(user.typer_user, name="user")
typer_app.add_typer(book.typer_book, name="book")
typer_app.add_typer(generate.typer_generate, name="generate")
typer_app.add_typer(migrate.typer_migrate, name="migrate")
