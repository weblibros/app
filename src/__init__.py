import importlib

# base modules
importlib.import_module("src.accounts.users")
importlib.import_module("src.file_management")
importlib.import_module("src.books")
importlib.import_module("src.libraries")

# listeners modules
importlib.import_module("src.services.mail.listeners")
importlib.import_module("src.file_management.listeners")
importlib.import_module("src.file_management.features.remove_cover_image_file.listeners")
importlib.import_module("src.file_management.features.remove_ebook_file.listeners")
importlib.import_module("src.file_management.features.import_ebook_file.listeners")
importlib.import_module("src.books.features.create_book.listeners")
importlib.import_module("src.accounts.users.listeners")
importlib.import_module("src.libraries.listeners")
importlib.import_module("src.services.event_logger.listeners")
