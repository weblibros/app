from enum import StrEnum


class ApiTags(StrEnum):
    TAG_BOOKS = "Books"
    TAG_ACCOUNTS = "Accounts"
    TAG_AUTH = "Authenticate"
    TAG_INTERNAL = "Internal"
    TAG_FILES = "Files"
    TAG_DEVICES = "Devices"
    TAG_DEVICE_KOBO = "KoboEreader"
