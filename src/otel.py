from fastapi import FastAPI
from opentelemetry.sdk.resources import Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor,
    ConsoleSpanExporter,
    SimpleSpanProcessor,
)

from src.config import settings

resource = Resource(attributes={"service.name": "weblibros"})
tracer = TracerProvider(resource=resource)

if settings.OTEL_ENABLE and settings.OTEL_TRACE_EXPORTER_URL:
    from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter

    otlp_exporter = OTLPSpanExporter(
        endpoint=settings.OTEL_TRACE_EXPORTER_URL,
        insecure=settings.OTEL_TRACE_EXPORTER_INSECURE,
    )
    span_processor = BatchSpanProcessor(otlp_exporter)
    tracer.add_span_processor(span_processor)

if settings.OTEL_ENABLE and settings.OTEL_TRACE_CONSOLE_LOG:
    tracer.add_span_processor(SimpleSpanProcessor(ConsoleSpanExporter()))


def setup_opentelemetry(app: FastAPI):
    if not settings.OTEL_ENABLE:
        return

    from opentelemetry.instrumentation.fastapi import FastAPIInstrumentor
    from opentelemetry.instrumentation.requests import RequestsInstrumentor

    # https://github.com/open-telemetry/opentelemetry-python/blob/main/exporter/opentelemetry-exporter-otlp-proto-grpc/src/opentelemetry/exporter/otlp/proto/grpc/__init__.py
    FastAPIInstrumentor.instrument_app(app, tracer_provider=tracer)
    RequestsInstrumentor().instrument(tracer_provider=tracer)


def otel_sqlachemy_wrapper(engine):
    if not settings.OTEL_ENABLE:
        return engine

    from opentelemetry.instrumentation.sqlalchemy import SQLAlchemyInstrumentor

    SQLAlchemyInstrumentor().instrument(
        enable_commenter=True,
        engine=engine.sync_engine,
        tracer_provider=tracer,
    )
    return engine
