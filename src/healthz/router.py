from fastapi import APIRouter

from src.healthz.schema import HealthyRespond
from src.tags import ApiTags

healthz_router = APIRouter()


@healthz_router.get("/healthz", response_model=HealthyRespond, tags=[ApiTags.TAG_INTERNAL])
async def healthz() -> HealthyRespond:
    return HealthyRespond()
