from pydantic import BaseModel


class HealthyRespond(BaseModel):
    """
    response healthy
    """

    message: str = "healthy"
