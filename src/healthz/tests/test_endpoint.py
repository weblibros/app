async def test_healthz_endpoint(client):
    response = await client.get("/healthz")
    assert response.status_code == 200
    assert response.json() == {"message": "healthy"}
