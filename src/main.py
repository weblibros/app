from contextlib import asynccontextmanager
from typing import List

import sentry_sdk
import uvicorn
from asgi_correlation_id import CorrelationIdMiddleware
from fastapi import APIRouter, FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi_events.handlers.local import local_handler
from fastapi_events.middleware import EventHandlerASGIMiddleware
from sentry_sdk.integrations.fastapi import FastApiIntegration
from sentry_sdk.integrations.starlette import StarletteIntegration

from src.accounts.routers import account_router
from src.books.routers import books_router
from src.config import settings
from src.config.logging import log_config
from src.devices.routers import devices_router
from src.file_management.routers import file_management_router
from src.healthz.router import healthz_router
from src.otel import setup_opentelemetry

if settings.SENTRY_DNS:
    sentry_sdk.init(
        dsn=settings.SENTRY_DNS,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production,
        traces_sample_rate=1.0,
        environment=settings.ENVIRONMENT,
        integrations=[
            StarletteIntegration(transaction_style="endpoint"),
            FastApiIntegration(transaction_style="endpoint"),
        ],
    )


@asynccontextmanager
async def lifespan(app: FastAPI):
    yield


app = FastAPI(title=settings.PROJECT_NAME, lifespan=lifespan)
app.add_middleware(CorrelationIdMiddleware)
app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.CORS_ORIGIN_LIST,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.add_middleware(EventHandlerASGIMiddleware, handlers=[local_handler])


router_list: List[APIRouter] = [
    healthz_router,
    books_router,
    account_router,
    file_management_router,
    devices_router,
]

for router in router_list:
    app.include_router(router)

app.mount(
    "/",
    StaticFiles(
        directory="/static",
        html=True,
        check_dir=False,
    ),
    name="static",
)

setup_opentelemetry(app)

if __name__ == "__main__":
    is_local_development_env = settings.ENVIRONMENT == "local"
    settings.validate_jwt_settings
    uvicorn.run(
        "src.main:app",
        host=settings.UVICORN_HOST,
        port=settings.UVICORN_PORT,
        log_level=settings.LOG_LEVEL.lower(),
        log_config=log_config,
        use_colors=True,
        reload=is_local_development_env,
        proxy_headers=not is_local_development_env,
    )
