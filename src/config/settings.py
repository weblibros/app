from enum import StrEnum
from typing import Literal, Optional

from pydantic.networks import PostgresDsn
from pydantic_settings import BaseSettings, SettingsConfigDict


class LogLevel(StrEnum):  # noqa: WPS600
    """Possible log levels."""

    NOTSET = "NOTSET"
    DEBUG = "DEBUG"
    INFO = "INFO"
    WARNING = "WARNING"
    ERROR = "ERROR"
    FATAL = "FATAL"


class Settings(BaseSettings):
    """
    Application settings.

    These parameters can be configured
    with environment variables.
    """

    PROJECT_NAME: str = "Web libros"
    DOMAIN: str = "app.web-libros.com"

    UVICORN_PORT: int = 8500
    UVICORN_HOST: str = "0.0.0.0"

    # Current environment
    ENVIRONMENT: str = "production"

    LOG_LEVEL: LogLevel = LogLevel.INFO
    LOG_DB: bool = False

    SENTRY_DNS: Optional[str] = None

    DB_DSN: str | PostgresDsn = "sqlite+aiosqlite:///database.db"

    JWT_ALGORITHM: Literal["HS256", "RS256", "ES512"] = "HS256"
    JWT_ACCESS_TOKEN_EXPIRE_SECONDS: int = 60 * 5
    JWT_REFRESH_TOKEN_EXPIRE_SECONDS: int = 60 * 60 * 24
    JWT_PASSWORD_RESET_TOKEN_EXPIRE_SECONDS: int = 60 * 60 * 2
    JWT_EMAIL_ACTIVATE_TOKEN_EXPIRE_SECONDS: int = 60 * 60 * 2
    JWT_SECRET: Optional[str] = None
    JWT_PRIVATE_KEY_FILE: Optional[str] = None
    JWT_PUBLIC_KEY_FILE: Optional[str] = None

    CORS_ORIGIN_LIST: list[str] = ["*"]

    EMAIL_ENABLED: bool = False
    EMAIL_SMTP_SERVER: Optional[str] = None
    EMAIL_SMTP_PORT: int = 25
    EMAIL_TLS_ENABLED: bool = True
    EMAIL_STARTTLS: bool = False
    EMAIL_USER_LOGIN: Optional[str] = None
    EMAIL_PASSWORD_LOGIN: Optional[str] = None
    EMAIL_DEFAULT_SENDER: str = "info@mail.web-libros.com"

    ALLOW_EXPIRIMENTAL_FILETYPES: bool = False

    STORAGE_BASE_DIR: str = "/web-libros/storage"

    OTEL_ENABLE: bool = False
    OTEL_TRACE_CONSOLE_LOG: bool = False
    OTEL_TRACE_EXPORTER_INSECURE: bool = False
    OTEL_TRACE_EXPORTER_URL: Optional[str] = None
    OTEL_PYTHON_FASTAPI_EXCLUDED_URLS: str = "*/healthz"
    OTEL_INSTRUMENTATION_HTTP_CAPTURE_HEADERS_SERVER_REQUEST: str = "Accept.*,X-.*"
    OTEL_INSTRUMENTATION_HTTP_CAPTURE_HEADERS_SERVER_RESPONSE: str = "Content.*,X-.*"
    model_config = SettingsConfigDict(secrets_dir="/var/run/secrets", use_enum_values=True, validate_default=True)

    def validate_jwt_settings(self):
        supported_sync_jwt_algorithms = ["HS256"]
        supported_async_jwt_algorithms = ["RS256"]
        supported_jwt_algorithms = supported_sync_jwt_algorithms + supported_async_jwt_algorithms
        jwt_algorithm_is_set = self.JWT_ALGORITHM in supported_jwt_algorithms
        jwt_algorithm_is_sync = self.JWT_ALGORITHM in supported_sync_jwt_algorithms
        jwt_algorithm_is_async = self.JWT_ALGORITHM in supported_async_jwt_algorithms
        if not jwt_algorithm_is_set:
            raise Exception(
                f"The JWT_ALGORITHM needs to be set with one of the following values: {supported_jwt_algorithms}"
            )

        if jwt_algorithm_is_sync and self.JWT_SECRET is None:
            raise Exception(f"The jwt algorithm {self.JWT_ALGORITHM} needs JWT_SECRET")
        if jwt_algorithm_is_async and (self.JWT_PUBLIC_KEY_FILE is None or self.JWT_PRIVATE_KEY_FILE is None):
            raise Exception(
                f"The jwt algorithm {self.JWT_ALGORITHM} needs JWT_PUBLIC_KEY_FILE and JWT_PRIVATE_KEY_FILE"
            )


settings = Settings()
