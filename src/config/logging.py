import logging

from pydantic import BaseModel

from src.config import settings

LOG_FORMAT: str = "%(levelprefix)s %(asctime)s | [%(correlation_id)s] | %(message)s"
LOG_LEVEL: str = settings.LOG_LEVEL


class HealthCheckFilter(logging.Filter):
    def filter(self, record):
        return record.getMessage().find("/healthz") == -1


class LogConfig(BaseModel):
    """Logging configuration to be set for the server"""

    # Logging config
    version: int = 1
    disable_existing_loggers: bool = False
    filters: dict = {
        "correlation_id": {
            "()": "asgi_correlation_id.CorrelationIdFilter",
            "uuid_length": 32,
            "default_value": "-",
        },
        "healthcheck_filter": {"()": HealthCheckFilter},
    }
    formatters: dict = {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": LOG_FORMAT,
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
        "access": {
            "()": "uvicorn.logging.AccessFormatter",
            "fmt": LOG_FORMAT,
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    }
    handlers: dict = {
        "default": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
            "filters": ["correlation_id", "healthcheck_filter"],
        },
        "access": {
            "formatter": "access",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
            "filters": ["correlation_id", "healthcheck_filter"],
        },
    }
    loggers: dict = {
        "default": {
            "handlers": ["default"],
            "level": LOG_LEVEL,
        },
        "uvicorn.access": {
            "level": "INFO",
            "handlers": ["default"],
            "propagate": False,
        },
        "uvicorn.error": {
            "level": "INFO",
            "handlers": ["default"],
            "propagate": False,
        },
        "uvicorn": {
            "level": "INFO",
            "handlers": ["default"],
            "propagate": False,
        },
    }


log_config = LogConfig().model_dump()
