class TableNames:
    BOOK_TABLE_NAME = "books"
    BOOK_FILERECORDS_TABLE_NAME = "book_filerecords"
    BOOK_COVER_TABLE_NAME = "book_covers"

    FILERECORD_TABLE_NAME = "file_records"
    IMAGE_FILERECORD_TABLE_NAME = "image_filerecords"

    IMPORT_MANAGER_TABLE_NAME = "file_import_managers"
    IMPORT_STEP_TABLE_NAME = "file_import_steps"

    USER_TABLE_NAME = "accounts_users"

    IDENTIFIER_TABLE_NAME = "identifiers"
    IDENTIFIER_BOOK_TABLE_NAME = "identifiers_books"

    AUTHOR_TABLE_NAMES = "authors"
    AUTHOR_BOOK_TABLE_NAMES = "authors_books"

    LIBRARY_TABLE_NAME = "libraries"
    LIBRARY_MEMBER_TABLE_NAME = "library_members"
    LIBRARY_BOOK_TABLE_NAME = "library_books"
