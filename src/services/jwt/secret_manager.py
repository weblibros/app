from src.config import settings


class JwtAlgorithmNotSupported(Exception):
    def __init__(self, algorithm: str):
        super().__init__(f"Algorithm {algorithm} is not supported")


class JWTSecretManager:
    def __init__(self):
        self._encoding_secret = None
        self._decoding_secret = None

    def __read_file(self, file_path) -> str:
        with open(file_path, "r") as file:
            contents = file.read()
        return contents

    @property
    def encoding_secret(self) -> str:
        if self._encoding_secret:
            return self._encoding_secret
        elif settings.JWT_ALGORITHM == "HS256":
            assert settings.JWT_SECRET is not None
            encoding_secret = settings.JWT_SECRET
            self._encoding_secret = encoding_secret
            return encoding_secret
        elif settings.JWT_ALGORITHM == "RS256" or settings.JWT_ALGORITHM == "ES512":
            encoding_secret = self.__read_file(settings.JWT_PRIVATE_KEY_FILE)
            self._encoding_secret = encoding_secret
            return encoding_secret
        else:
            raise JwtAlgorithmNotSupported(settings.JWT_ALGORITHM)

    @property
    def decoding_secret(self) -> str:
        if self._decoding_secret:
            return self._decoding_secret
        elif settings.JWT_ALGORITHM == "HS256":
            assert settings.JWT_SECRET is not None
            decoding_secret = settings.JWT_SECRET
            self._decoding_secret = decoding_secret
            return decoding_secret
        elif settings.JWT_ALGORITHM == "RS256" or settings.JWT_ALGORITHM == "ES512":
            decoding_secret = self.__read_file(settings.JWT_PUBLIC_KEY_FILE)
            self._decoding_secret = decoding_secret
            return decoding_secret
        else:
            raise JwtAlgorithmNotSupported(settings.JWT_ALGORITHM)

    @property
    def algorithm(self) -> str:
        return settings.JWT_ALGORITHM


jwt_secret_manager = JWTSecretManager()
