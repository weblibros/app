from uuid import UUID

from src.services.jwt import JWTUserDownloadBookToken


def test_download_book_token_can_be_encoded():
    token = JWTUserDownloadBookToken.encode(
        {
            "user": "b74acd8f-8a5a-4f27-a6a9-b9b1c9d6f2cb",
            "book": "b74acd8f-8a5a-4f27-a6a9-b9b1c9d6f2cb",
            "file": "b74acd8f-8a5a-4f27-a6a9-b9b1c9d6f2cb",
            "title": "fake title",
            "type": "user-download-book",
        }
    )

    assert token.is_valid() is True
    assert token.is_valid_token_type() is True
    assert token.payload.token_type == "user-download-book"
    assert token.payload.user == UUID("b74acd8f-8a5a-4f27-a6a9-b9b1c9d6f2cb")
    assert token.payload.book == UUID("b74acd8f-8a5a-4f27-a6a9-b9b1c9d6f2cb")
    assert token.payload.file == UUID("b74acd8f-8a5a-4f27-a6a9-b9b1c9d6f2cb")
    assert token.payload.title == "fake title"
