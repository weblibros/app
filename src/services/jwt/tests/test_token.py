from datetime import datetime, timedelta
from uuid import uuid4

import pytest
from jose import JWTError, jwt

from ..token import JWTToken


def test_encode():
    token = JWTToken.encode({"test": "test"})
    assert isinstance(token, JWTToken)
    raw_token = str(token)
    assert len(raw_token.split(".")) == 3
    claims = jwt.get_unverified_claims(raw_token)
    header = jwt.get_unverified_header(raw_token)
    assert claims["test"] == "test"
    assert "exp" in claims.keys()
    assert "iss" in claims.keys()
    assert "jti" in claims.keys()
    assert "iat" in claims.keys()
    assert header == {"alg": "ES512", "typ": "JWT"}


def test_no_string_input():
    with pytest.raises(AssertionError):
        JWTToken(dict())


def test_create_jwttoken():
    test_payload = {
        "test": "test_encode",
        "exp": datetime.utcnow() + timedelta(seconds=3600),
        "iat": datetime.utcnow(),
        "iss": "web-libros",
        "jti": str(uuid4()),
    }
    raw_jwt_token = jwt.encode(
        test_payload,
        "secret",
        algorithm="HS256",
    )

    token = JWTToken(raw_jwt_token)

    assert dict(token._unverified_claims) == test_payload


def test_valid_jwttoken():
    class JWTTestSecretManager:
        encoding_secret = "devsecret"
        decoding_secret = "devsecret"
        algorithm = "HS256"

    test_payload = {
        "test": "test_encode",
        "exp": datetime.utcnow() + timedelta(seconds=3600),
        "iat": datetime.utcnow(),
        "iss": "web-libros",
        "jti": str(uuid4()),
    }
    raw_jwt_token = jwt.encode(
        test_payload,
        JWTTestSecretManager.encoding_secret,
        algorithm=JWTTestSecretManager.algorithm,
    )

    token = JWTToken(raw_jwt_token)
    token._jwt_secret_manager = JWTTestSecretManager()

    assert token.is_valid() is True


def test_unvalid_jwttoken_wrong_encryption():
    class JWTTestSecretManager:
        encoding_secret = "devsecret"
        decoding_secret = "devsecret"
        algorithm = "HS256"

    test_payload = {
        "test": "test_encode",
        "exp": datetime.utcnow() + timedelta(seconds=3600),
        "iat": datetime.utcnow(),
        "iss": "web-libros",
        "jti": str(uuid4()),
    }
    raw_jwt_token = jwt.encode(
        test_payload,
        "notcorrect secret",
        algorithm=JWTTestSecretManager.algorithm,
    )

    token = JWTToken(raw_jwt_token)
    token._jwt_secret_manager = JWTTestSecretManager()

    assert token.is_valid() is False


def test_unvalid_jwttoken_wrong_encryption_raise_error():
    class JWTTestSecretManager:
        encoding_secret = "devsecret"
        decoding_secret = "devsecret"
        algorithm = "HS256"

    test_payload = {
        "test": "test_encode",
        "exp": datetime.utcnow() + timedelta(seconds=3600),
        "iat": datetime.utcnow(),
        "iss": "web-libros",
        "jti": str(uuid4()),
    }
    raw_jwt_token = jwt.encode(
        test_payload,
        "notcorrect secret",
        algorithm=JWTTestSecretManager.algorithm,
    )

    token = JWTToken(raw_jwt_token)
    token._jwt_secret_manager = JWTTestSecretManager()

    with pytest.raises(JWTError):
        token.is_valid(raise_error=True)


def test_exire_jwttoken():
    class JWTTestSecretManager:
        encoding_secret = "devsecret"
        decoding_secret = "devsecret"
        algorithm = "HS256"

    test_payload = {
        "test": "test_encode",
        "exp": datetime.utcnow() - timedelta(seconds=3600),
        "iat": datetime.utcnow(),
        "iss": "web-libros",
        "jti": str(uuid4()),
    }
    raw_jwt_token = jwt.encode(
        test_payload,
        JWTTestSecretManager.encoding_secret,
        algorithm=JWTTestSecretManager.algorithm,
    )

    token = JWTToken(raw_jwt_token)
    token._jwt_secret_manager = JWTTestSecretManager()

    assert token.is_expired() is True


def test_unvalid_jwttoken_incorrect_payload_raise_error():
    class JWTTestSecretManager:
        encoding_secret = "devsecret"
        decoding_secret = "devsecret"
        algorithm = "HS256"

    test_payload = {
        "test": "test_encode",
        # "exp": datetime.utcnow() + timedelta(seconds=3600),
        # "iat": datetime.utcnow(),
        # "iss": "web-libros",
        # "jti": str(uuid4()),
    }
    raw_jwt_token = jwt.encode(
        test_payload,
        JWTTestSecretManager.encoding_secret,
        algorithm=JWTTestSecretManager.algorithm,
    )

    token = JWTToken(raw_jwt_token)
    token._jwt_secret_manager = JWTTestSecretManager()

    with pytest.raises(JWTError):
        token.is_valid(raise_error=True)
