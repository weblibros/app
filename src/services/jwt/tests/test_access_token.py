from uuid import uuid4

from src.accounts.users import User
from src.services.jwt import JWTUserAccesToken


def test_user_acces_token():
    user = User(
        uuid=uuid4(),
        is_active=True,
        memberships=[],
    )
    token = JWTUserAccesToken.encode_user(user)

    assert token.payload.user == user.uuid
    assert token.payload.is_active is True
    assert token.payload.token_type.value == "access"


def test_un_active_access_token():
    unactive_user = User(
        uuid=uuid4(),
        is_active=False,
        memberships=[],
    )
    token = JWTUserAccesToken.encode_user(unactive_user)

    assert token.payload.user == unactive_user.uuid
    assert token.payload.is_active is False
    assert token.payload.token_type.value == "access"
