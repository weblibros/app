from uuid import uuid4

import pytest

from src.accounts.users import User
from src.services.jwt import JWTPasswordResetToken, JWTUserAccesToken, JWTUserActivateToken, JWTUserRefreshToken


@pytest.mark.parametrize(
    "jwt_class, token_type_value",
    [
        (JWTUserAccesToken, "access"),
        (JWTUserRefreshToken, "refresh"),
        (JWTPasswordResetToken, "password-reset"),
        (JWTUserActivateToken, "email-activate"),
    ],
)
def test_correct_token_type(jwt_class, token_type_value):
    user = User(uuid=uuid4(), is_active=True, memberships=[])
    token_from_user = jwt_class.encode_user(user)

    assert token_from_user.is_valid_token_type() is True
    assert token_from_user.is_valid() is True
    assert str(token_from_user.payload.token_type) == token_type_value

    token_from_string = jwt_class(str(token_from_user))

    assert token_from_string.is_valid_token_type() is True
    assert token_from_string.is_valid() is True
    assert str(token_from_user.payload.token_type) == token_type_value


@pytest.mark.parametrize(
    "jwt_class, incorrect_jwt_class",
    [
        (JWTUserAccesToken, JWTUserActivateToken),
        (JWTUserRefreshToken, JWTUserAccesToken),
        (JWTPasswordResetToken, JWTUserRefreshToken),
        (JWTUserActivateToken, JWTPasswordResetToken),
    ],
)
def test_in_correct_token_type(jwt_class, incorrect_jwt_class):
    user = User(uuid=uuid4(), is_active=True, memberships=[])
    token_from_user = jwt_class.encode_user(user)

    assert token_from_user.is_valid_token_type() is True
    assert token_from_user.is_valid() is True

    token_from_string = incorrect_jwt_class(str(token_from_user))

    assert token_from_string.is_valid_token_type() is False
    assert token_from_string.is_valid() is False
