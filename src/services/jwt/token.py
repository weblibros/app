from dataclasses import dataclass
from datetime import datetime, timedelta
from enum import StrEnum
from typing import TYPE_CHECKING, Optional, Self, Type
from uuid import UUID, uuid4

from jose import ExpiredSignatureError, JWTError, jwt
from jose.exceptions import JWTClaimsError

from src.config import settings
from src.libraries.membership_roles import MembershipRole

from .secret_manager import jwt_secret_manager

if TYPE_CHECKING:
    from src.accounts.users import User

    from .secret_manager import JWTSecretManager


class TokenType(StrEnum):
    ACCESS_TOKEN = "access"
    REFRESH_TOKEN = "refresh"
    PASSWORD_RESET_TOKEN = "password-reset"
    EMAIL_ACTIVATE_TOKEN = "email-activate"
    DOWNLOAD_BOOK_TOKEN = "user-download-book"


@dataclass
class JwtPayload:
    exp: int
    iat: int
    jti: str
    iss: str

    @classmethod
    def from_unverified_claims(cls, unverified_claims: dict) -> Self:
        return cls(
            exp=unverified_claims["exp"],
            iat=unverified_claims["iat"],
            iss=unverified_claims.get("iss", "web-libros"),
            jti=unverified_claims["jti"],
        )


@dataclass
class JwtUserPayload(JwtPayload):
    user: UUID
    token_type: TokenType

    @classmethod
    def from_unverified_claims(cls, unverified_claims: dict) -> Self:
        return cls(
            exp=unverified_claims["exp"],
            iat=unverified_claims["iat"],
            iss=unverified_claims.get("iss", "web-libros"),
            jti=unverified_claims["jti"],
            user=UUID(unverified_claims["user"]),
            token_type=TokenType(unverified_claims["type"]),
        )


@dataclass
class JwtUserMembership:
    library_uuid: UUID
    membership_role: MembershipRole

    @classmethod
    def from_dict(cls, raw_dict: dict) -> Self:
        return cls(
            library_uuid=UUID(raw_dict["library"]),
            membership_role=MembershipRole(raw_dict["role"]),
        )


@dataclass
class JwtAccessPayload(JwtPayload):
    user: UUID
    is_active: bool
    token_type: TokenType
    memberships: list[JwtUserMembership]

    @classmethod
    def from_unverified_claims(cls, unverified_claims: dict) -> Self:
        memberships = unverified_claims.get("memberships", [])
        return cls(
            exp=unverified_claims["exp"],
            iat=unverified_claims["iat"],
            iss=unverified_claims.get("iss", "web-libros"),
            jti=unverified_claims["jti"],
            user=UUID(unverified_claims["user"]),
            is_active=unverified_claims["isActive"],
            token_type=TokenType(unverified_claims["type"]),
            memberships=[JwtUserMembership.from_dict(membership_dict) for membership_dict in memberships],
        )


class InvalidJwtTokenType(Exception):
    """exception when jwt token type is not correct"""

    def __init__(
        self,
        expected_token_type: Optional[TokenType] = None,
        given_token_type: str = "",
    ):
        if expected_token_type:
            self.message = f"expected following token type {expected_token_type.value} was given {given_token_type}"
        else:
            self.message = f"an token type was given {given_token_type} when none was expected"
        self.expected_token_type = expected_token_type

    def __str__(self):
        return str(self.message)


class JWTToken:
    token_type: Optional[TokenType] = None
    jwt_payload_class: Type[JwtPayload]
    _jwt_secret_manager: "JWTSecretManager" = jwt_secret_manager
    expire_time_seconds: int = 60 * 5

    __decode_option: dict = {
        "require_iat": True,
        "require_exp": True,
        "require_iss": True,
        "require_jti": True,
        "leeway": 60,
    }

    def __init__(self, token_string: str):
        assert isinstance(token_string, str)
        self.__raw_token = token_string
        self.__header: dict[str, str] = jwt.get_unverified_header(token_string)
        self._unverified_claims: dict = jwt.get_unverified_claims(token_string)

    @property
    def header(self):
        return self.__header

    @property
    def payload(self):
        return self.jwt_payload_class.from_unverified_claims(self._unverified_claims)

    def is_valid(self, raise_error=False) -> bool:
        is_valid_encoding = self._is_valid_encoding(raise_error)
        is_valid_token_type = self.is_valid_token_type()
        if raise_error is True and is_valid_token_type is False:
            raise InvalidJwtTokenType(
                expected_token_type=self.token_type,
                given_token_type=self._unverified_claims.get("type", ""),
            )
        return is_valid_encoding is True and is_valid_token_type is True

    def _is_valid_encoding(self, raise_error=False) -> bool:
        secret = self._jwt_secret_manager.decoding_secret
        algorithm = self._jwt_secret_manager.algorithm
        try:
            jwt.decode(
                token=self.__raw_token,
                key=secret,
                options=self.__decode_option,
                algorithms=algorithm,
            )
        except (JWTError, ExpiredSignatureError, JWTClaimsError) as e:
            if raise_error:
                raise e
            return False
        else:
            return True

    def is_valid_token_type(self) -> bool:
        raw_token_type = self._unverified_claims.get("type")
        is_correct_token_type = False

        if self.token_type is None:
            is_correct_token_type = raw_token_type is None
        elif raw_token_type == str(self.token_type):
            is_correct_token_type = True

        return is_correct_token_type

    def is_expired(self) -> bool:
        try:
            self.is_valid(raise_error=True)
        except ExpiredSignatureError:
            return False
        finally:
            return True

    @classmethod
    def _default_payload(cls) -> dict:
        default_payload = {
            "exp": datetime.utcnow() + timedelta(seconds=cls.expire_time_seconds),
            "iat": datetime.utcnow(),
            "iss": "web-libros",
            "jti": uuid4().hex,
        }
        if cls.token_type:
            default_payload["type"] = cls.token_type
        return default_payload

    @classmethod
    def encode(cls, to_encode: dict) -> Self:
        claim = {**cls._default_payload(), **to_encode}
        secret = cls._jwt_secret_manager.encoding_secret
        algorithm = cls._jwt_secret_manager.algorithm
        jwt_str = jwt.encode(claim, secret, algorithm=algorithm)
        return cls(jwt_str)

    def __str__(self):
        return self.__raw_token

    def __repr__(self):
        return f"{self.__class__.__name__}({self.__raw_token})"


class JWTUserAccesToken(JWTToken):
    token_type = TokenType.ACCESS_TOKEN
    expire_time_seconds = settings.JWT_ACCESS_TOKEN_EXPIRE_SECONDS
    jwt_payload_class = JwtAccessPayload

    @classmethod
    def encode_user(cls, user: "User") -> Self:
        to_encode = {
            "user": str(user.uuid),
            "isActive": user.is_active,
            "memberships": [
                {
                    "library": str(membership.library_uuid),
                    "role": str(membership.membership_role),
                }
                for membership in user.memberships
            ],
        }
        return cls.encode(to_encode)


class JWTUserRefreshToken(JWTToken):
    token_type = TokenType.REFRESH_TOKEN
    expire_time_seconds = settings.JWT_REFRESH_TOKEN_EXPIRE_SECONDS
    jwt_payload_class = JwtUserPayload

    @classmethod
    def encode_user(cls, user: "User") -> Self:
        to_encode = {"user": str(user.uuid)}
        return cls.encode(to_encode)


class JWTPasswordResetToken(JWTToken):
    token_type = TokenType.PASSWORD_RESET_TOKEN
    expire_time_seconds = settings.JWT_PASSWORD_RESET_TOKEN_EXPIRE_SECONDS
    jwt_payload_class = JwtUserPayload

    @classmethod
    def encode_user(cls, user: "User") -> Self:
        to_encode = {"user": str(user.uuid)}
        return cls.encode(to_encode)


class JWTUserActivateToken(JWTToken):
    token_type = TokenType.EMAIL_ACTIVATE_TOKEN
    expire_time_seconds = settings.JWT_EMAIL_ACTIVATE_TOKEN_EXPIRE_SECONDS
    jwt_payload_class = JwtUserPayload

    @classmethod
    def encode_user(cls, user: "User") -> Self:
        to_encode = {"user": str(user.uuid)}
        return cls.encode(to_encode)


@dataclass
class JwtUserDownloadBookPayload(JwtPayload):
    user: UUID
    book: UUID
    title: str
    file: UUID
    token_type: TokenType

    @classmethod
    def from_unverified_claims(cls, unverified_claims: dict) -> Self:
        return cls(
            exp=unverified_claims["exp"],
            iat=unverified_claims["iat"],
            iss=unverified_claims.get("iss", "web-libros"),
            jti=unverified_claims["jti"],
            user=UUID(unverified_claims["user"]),
            book=UUID(unverified_claims["book"]),
            file=UUID(unverified_claims["file"]),
            title=unverified_claims["title"],
            token_type=TokenType(unverified_claims["type"]),
        )


class JWTUserDownloadBookToken(JWTToken):
    token_type = TokenType.DOWNLOAD_BOOK_TOKEN
    expire_time_seconds = 60 * 60
    jwt_payload_class = JwtUserDownloadBookPayload
