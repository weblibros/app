from typing import Annotated
from uuid import UUID

from fastapi import Cookie, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import ExpiredSignatureError, JWTError
from jose.exceptions import JWTClaimsError

from src.accounts import urls
from src.accounts.users import User
from src.services.jwt import InvalidJwtTokenType, JWTUserAccesToken

oauth2_scheme = OAuth2PasswordBearer(tokenUrl=urls.PASSWORD_LOGIN_USER_URL)


def valid_jwt_access_token(token: str) -> JWTUserAccesToken:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    expired_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Credentials are expired",
        headers={"WWW-Authenticate": "Bearer"},
    )
    not_an_access_token = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Token is not of type access",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        jwt_token = JWTUserAccesToken(token_string=token)
        jwt_token.is_valid(raise_error=True)
    except ExpiredSignatureError:
        raise expired_exception
    except (JWTError, JWTClaimsError):
        raise credentials_exception
    except InvalidJwtTokenType:
        raise not_an_access_token

    return jwt_token


def valid_body_token(token: str = Depends(oauth2_scheme)) -> JWTUserAccesToken:
    return valid_jwt_access_token(token)


def valid_cookie_token(webLibrosJwtAccessToken: Annotated[str | None, Cookie()] = None) -> JWTUserAccesToken:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="cookie 'webLibrosJwtAccessToken' not found",
    )
    if webLibrosJwtAccessToken:
        return valid_jwt_access_token(webLibrosJwtAccessToken)
    else:
        raise credentials_exception


def user_is_active(user: User) -> User:
    unactive_user_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="User is not activated",
    )
    if user.is_active is not True:
        raise unactive_user_exception
    return user


def active_user(token: JWTUserAccesToken = Depends(valid_body_token)) -> User:
    user = User.from_access_token(token)
    return user_is_active(user)


def active_cookie_user(token: JWTUserAccesToken = Depends(valid_cookie_token)) -> User:
    user = User.from_access_token(token)
    return user_is_active(user)


def request_user_uuid(
    user: User = Depends(active_user),
) -> UUID:
    return user.uuid
