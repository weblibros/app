from datetime import datetime, timedelta
from uuid import uuid4

import pytest
from fastapi import HTTPException
from jose import jwt

from src.accounts.users import User
from src.services.jwt import JWTToken, JWTUserAccesToken

from ..fastapi_auth_injections import request_user_uuid, valid_jwt_access_token


def test_valid_jwt_access_token(access_token_user):
    raw_token = str(access_token_user)
    valid_token = valid_jwt_access_token(raw_token)

    assert isinstance(valid_token, JWTUserAccesToken)
    assert valid_token is not access_token_user
    assert valid_token.payload == access_token_user.payload


def test_unvalid_jwt_access_token():
    test_payload = {
        "user": str(uuid4()),
        "type": "access",
        "exp": datetime.utcnow() + timedelta(seconds=3600),
        "iat": datetime.utcnow(),
        "iss": "web-libros",
        "jti": str(uuid4()),
    }
    raw_token = jwt.encode(
        test_payload,
        "notcorrect secret",
        algorithm="HS256",
    )
    with pytest.raises(HTTPException) as e:
        valid_jwt_access_token(raw_token)

    assert e.value.detail == "Could not validate credentials"
    assert e.value.status_code == 401


def test_expired_jwt_access_token():
    test_payload = {
        "user": str(uuid4()),
        "type": "access",
        "exp": datetime.utcnow() - timedelta(seconds=3600),
        "iat": datetime.utcnow(),
        "iss": "web-libros",
        "jti": str(uuid4()),
    }
    raw_token = str(JWTToken.encode(test_payload))
    with pytest.raises(HTTPException) as e:
        valid_jwt_access_token(raw_token)

    assert e.value.detail == "Credentials are expired"
    assert e.value.status_code == 401


def test_jwt_no_access_token():
    test_payload = {
        "user": str(uuid4()),
        "type": "no-access",
        "exp": datetime.utcnow() + timedelta(seconds=3600),
        "iat": datetime.utcnow(),
        "iss": "web-libros",
        "jti": str(uuid4()),
        "is_active": False,
    }
    raw_token = str(JWTToken.encode(test_payload))
    with pytest.raises(HTTPException) as e:
        valid_jwt_access_token(raw_token)

    assert e.value.detail == "Token is not of type access"
    assert e.value.status_code == 401


def test_request_user_uuid():
    user = User(
        uuid=uuid4(),
        is_active=True,
        memberships=[],
    )
    user_uuid = request_user_uuid(user)
    assert user_uuid == user.uuid
