import logging

from fastapi_events.handlers.local import local_handler
from fastapi_events.typing import Event

logger = logging.getLogger("default")


@local_handler.register(event_name="*")
def log_event(event: Event):
    event_name, payload = event
    logger.info(f"event: {event_name} - payload: {payload} ")
