from fastapi_events.handlers.local import local_handler
from fastapi_events.typing import Event

from src.accounts.users.service import reset_password_by_email, send_activation_email
from src.services.mail.events import MailEvents


@local_handler.register(event_name=str(MailEvents.RESET_PASSWORD))
async def handle_events_reset_password(
    event: Event,
):
    _, payload = event
    await reset_password_by_email(payload["user_uuid"])


@local_handler.register(event_name=str(MailEvents.EMAIL_VALIDATION))
async def handle_events_activation(
    event: Event,
):
    _, payload = event
    await send_activation_email(payload["user_uuid"])
