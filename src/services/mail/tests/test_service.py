from src.services.mail.fastmail import fast_mail_service
from src.services.mail.service import EmailService


async def test_email_service():
    email_service = EmailService(receiver_email="test_email_service@test.com")
    fast_mail_service.config.SUPPRESS_SEND = 1
    with fast_mail_service.record_messages() as outbox:
        await email_service.send_password_reset_email("fakelink")

    assert len(outbox) == 1
    assert outbox[0]["from"] == "info@mail.web-libros.com"
    assert outbox[0]["To"] == "test_email_service@test.com"
    assert outbox[0]["Subject"] == "Reset User Password WebLibros"
