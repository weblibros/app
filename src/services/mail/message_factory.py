from fastapi_mail import MessageSchema, MessageType
from pydantic import EmailStr


class MailMessageFactory:
    def __init__(
        self,
        email_recipients: list[EmailStr],
    ):
        self.email_recipients = email_recipients

    def password_reset(self, link: str) -> MessageSchema:
        html = f"""
        <p>click on the button to reset your password</p>
        <a href="{link}">CLICK</a>
        """
        message = MessageSchema(
            subject="Reset User Password WebLibros",
            recipients=self.email_recipients,
            body=html,
            subtype=MessageType.html,
        )

        return message

    def user_activate_message(self, link: str) -> MessageSchema:
        html = f"""
        <p>click on the button to active your user</p>
        <a href="{link}">ACTIVATE</a>
        """
        message = MessageSchema(
            subject="Activate user WebLibros",
            recipients=self.email_recipients,
            body=html,
            subtype=MessageType.html,
        )

        return message
