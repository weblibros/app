from pydantic import EmailStr

from src.services.mail.fastmail import FastMailProtocol, fast_mail_service
from src.services.mail.message_factory import MailMessageFactory


class EmailService:
    message_factory_class = MailMessageFactory
    fast_mail_service: FastMailProtocol = fast_mail_service

    def __init__(
        self,
        receiver_email: EmailStr,
    ):
        email_recipient = receiver_email
        self.message_factory = self.message_factory_class(email_recipients=[email_recipient])

    async def send_password_reset_email(self, link):
        message = self.message_factory.password_reset(link)
        await self.fast_mail_service.send_message(message)

    async def send_user_activation_email(self, link):
        message = self.message_factory.user_activate_message(link)
        await self.fast_mail_service.send_message(message)
