from enum import StrEnum
from uuid import UUID

from fastapi_events.registry.payload_schema import registry as payload_schema
from pydantic import BaseModel


class MailEvents(StrEnum):
    RESET_PASSWORD = "RESET_PASSWORD"
    EMAIL_VALIDATION = "EMAIL_VALIDATION"


@payload_schema.register(event_name=MailEvents.RESET_PASSWORD)
@payload_schema.register(event_name=MailEvents.EMAIL_VALIDATION)
class SendMailPayload(BaseModel):
    user_uuid: UUID
