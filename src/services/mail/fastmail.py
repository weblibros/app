import logging
from typing import Optional, Protocol

from fastapi_mail import ConnectionConfig, FastMail, MessageSchema
from pydantic import SecretStr

from src.config import settings

logger = logging.getLogger(__name__)


class FastMailProtocol(Protocol):
    async def send_message(self, message: MessageSchema, template_name: Optional[str] = None) -> None: ...


class NoEmailSetFastMail:
    async def send_message(self, message: MessageSchema, template_name: Optional[str] = None) -> None:
        logger.info(f"Email is disabled no email senf for {message} with template {template_name}")


def process_email_config() -> ConnectionConfig:
    use_tls = settings.EMAIL_TLS_ENABLED
    smpt_server = settings.EMAIL_SMTP_SERVER
    smtp_port = settings.EMAIL_SMTP_PORT
    email_user_login = settings.EMAIL_USER_LOGIN
    email_password_login = settings.EMAIL_PASSWORD_LOGIN
    default_from_email = settings.EMAIL_DEFAULT_SENDER
    mail_starttls = settings.EMAIL_STARTTLS

    if smpt_server is None:
        raise Exception("setting EMAIL_SMTP_SERVER is not set")

    if email_user_login is None:
        raise Exception("setting EMAIL_USER_LOGIN is not set")

    if email_password_login is None:
        raise Exception("setting EMAIL_PASSWORD_LOGIN is not set")
    config = ConnectionConfig(
        MAIL_USERNAME=email_user_login,
        MAIL_PASSWORD=SecretStr(email_password_login),
        MAIL_FROM=default_from_email,
        MAIL_PORT=smtp_port,
        MAIL_SERVER=smpt_server,
        MAIL_STARTTLS=mail_starttls,
        MAIL_SSL_TLS=use_tls,
        USE_CREDENTIALS=True,
        VALIDATE_CERTS=True,
    )
    return config


def fast_mail_factory() -> FastMailProtocol:
    email_enabled = settings.EMAIL_ENABLED
    if email_enabled:
        return FastMail(process_email_config())
    else:
        return NoEmailSetFastMail()


fast_mail_service = fast_mail_factory()
