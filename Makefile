
CONTAINER_NAME_BACKEND=web_libros
CONTAINER_NAME_FRONTEND=spa_web_libros
CONTAINER_NAME_POSTGRES=postgres
POSTGRES_VOLUME=web_libros_postgres_volume
COMPOSE_LOCAL=docker-compose/main.yaml \
							-f docker-compose/spa.yaml \
							-f docker-compose/postgres.yaml \
							-f docker-compose/mail.yaml




## DOCKER COMPOSE
.PHONY: docker
docker:
	docker compose -f $(COMPOSE_LOCAL) up ${ARGS}
.PHONY: docker-d
docker-d:
	ARGS=-d make docker
.PHONY: docker-build
docker-build:
	docker compose -f $(COMPOSE_LOCAL) build ${ARGS}
.PHONY: docker-build-clean
docker-build-clean:
	ARGS=--no-cache make docker-build
.PHONY: docker-stop
docker-stop:
	docker compose -f $(COMPOSE_LOCAL) stop ${ARGS}
.PHONY: docker-tracing
docker-tracing:
	docker compose -f $(COMPOSE_LOCAL) -f docker-compose-tracing.yaml up ${ARGS}
##---------------

## DOCKER
.PHONY: exec-backend
exec-backend:
	docker exec -it $(CONTAINER_NAME_BACKEND) bash ${ARGS}
.PHONY: exec-spa
exec-spa:
	docker exec -it $(CONTAINER_NAME_FRONTEND) bash ${ARGS}
##---------------

## playwright
.PHONY: e2e-open
e2e-open:
	playwright open http://0.0.0.0:5500 --color-scheme dark --browser chromium
.PHONY: e2e-install
e2e-install:
	pnpm --filter e2e exec playwright install chromium firefox
## storybook
.PHONY: stories
stories:
	pnpm storybook

.PHONY: stories-build
stories-build:
	pnpm build-storybook

## test
.PHONY: test
test:
	make test-backend
	make test-spa

.PHONY: test-backend
test-backend:
	ARGS='-c "pytest ${APP} -vvs"' make exec-backend

.PHONY: test-web-ui
test-web-ui:
	pnpm test

.PHONY: test-e2e
test-e2e:
	pnpm test-e2e

.PHONY: test-e2e-headed
test-e2e-headed:
	pnpm test-e2e-headed

.PHONY: test-e2e-ui
test-e2e-ui:
	pnpm test-e2e-ui

.PHONY: pop-test-data
pop-test-data:
	ARGS='-c "python cli.py migrate latest && python cli.py generate e2e-data"' make exec-backend

## CLEANUP
clean-spa:
	docker container stop $(CONTAINER_NAME_FRONTEND)
	docker container rm $(CONTAINER_NAME_FRONTEND)
	docker volume rm web_libros_node_modules
	docker volume rm web_libros_node_modules_ui
	docker volume rm web_libros_nuxt_modules

clean-postgres:
	docker container stop $(CONTAINER_NAME_POSTGRES)
	docker container rm $(CONTAINER_NAME_POSTGRES)
	docker volume rm $(POSTGRES_VOLUME)
clean-py-cache:
	find . | grep -E "(__pycache__|\.pyc|\.pyo)$ " | xargs rm -rf
##---------------
