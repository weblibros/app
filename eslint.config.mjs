import prettier from "eslint-plugin-prettier";
import path from "node:path";
import { fileURLToPath } from "node:url";
import { FlatCompat } from "@eslint/eslintrc";
import { createConfigForNuxt } from "@nuxt/eslint-config/flat";
import eslint from "@eslint/js";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
	baseDirectory: __dirname,
	recommendedConfig: eslint.configs.recommended,
	allConfig: eslint.configs.all,
});
const nuxtConfig = await createConfigForNuxt({});

export default [
	{
		ignores: [
			"**/coverage",
			"**/.coverage",
			"**/public",
			"*/node_modules",
			"*/dist",
			"*/storybook-static",
			"**/.nuxt",
			"**/.output",
		],
	},
	...nuxtConfig,
	...compat.extends("prettier", "plugin:storybook/recommended"),
	{
		plugins: {
			prettier,
		},

		rules: {
			"no-redeclare": "off",
			"no-unused-vars": "off",
			"no-undef": "off",
			"prettier/prettier": "error",
			"@typescript-eslint/ban-ts-comment": "off",
			"@typescript-eslint/no-require-imports": "off",
			"vue/multi-word-component-names": "off",
			"vue/no-multiple-template-root": "off",
			"vue/no-v-for-template-key": "off",

			"vue/no-reserved-props": [
				"error",
				{
					vueVersion: 3,
				},
			],
		},
	},
];
