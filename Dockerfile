ARG PYTHON_VERSION=3.12.7-slim-bullseye
# define an alias for the specfic python version used in this file.
FROM python:${PYTHON_VERSION} AS base-python

ARG APP_HOME=/app
ARG ARCH=x86_64

ENV PYTHONUNBUFFERED=1 \
  # prevents python creating .pyc files
  PYTHONDONTWRITEBYTECODE=1 \
  \
  # pip
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  \
  UV_NO_CACHE=1 \
  UV_SYSTEM_PYTHON=true \
  UV_PROJECT_ENVIRONMENT="/root/.venv" \
  \
  PATH="/root/.venv/bin:$PATH" \
  \
  CALIBRE_VERSION="6.17.0"


# Install apt packages
RUN apt-get update && apt-get install --no-install-recommends -y \
  # dependencies for building Python packages
  build-essential \
  curl \
  file \
  imagemagick \
  # dependencies for calibre
  libxcb-xinput-dev \
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && rm -rf /var/lib/apt/lists/*

RUN curl -sSL https://download.calibre-ebook.com/${CALIBRE_VERSION}/calibre-${CALIBRE_VERSION}-${ARCH}.txz -o /tmp/calibre-bin.txz && \
  mkdir /usr/local/bin/calibre && \
  tar xvf /tmp/calibre-bin.txz -C /usr/local/bin/calibre && \
  rm /tmp/calibre-bin.txz && \
  ln -sf /usr/local/bin/calibre/ebook-meta /usr/local/bin/ebook-meta && \
  ln -sf /usr/local/bin/calibre/ebook-convert /usr/local/bin/ebook-convert && \
  # remove unused file to reduce image size
  rm -R /usr/local/bin/calibre/translations && \
  cd /usr/local/bin/calibre/lib && ls -p | grep -v "/" | grep "libQt.*" | xargs rm && cd && \
  cd /usr/local/bin/calibre/lib/calibre-extensions && ls -p | grep -v "/" | grep "PyQt6.*" | xargs rm && cd
COPY --from=ghcr.io/astral-sh/uv:0.6.5 /uv /uvx /bin/

WORKDIR ${APP_HOME}

# create default storage directory
RUN mkdir -p /web-libros/storage /static

COPY uv.lock pyproject.toml ./

RUN --mount=type=cache,target=/root/.cache/uv \
  --mount=type=bind,source=uv.lock,target=uv.lock \
  --mount=type=bind,source=pyproject.toml,target=pyproject.toml \
  uv sync --frozen --no-install-project

HEALTHCHECK CMD curl --fail http://localhost:8500/healthz || exit 1

# copy application code to WORKDIR
COPY . .

ENTRYPOINT ["commands/entrypoint.sh"]

CMD ["commands/start_command.sh"]


FROM node:22-slim AS ui-builder
WORKDIR /app
RUN corepack enable \
  && corepack prepare pnpm@latest-8 --activate
COPY package.json \
  pnpm-lock.yaml \
  pnpm-workspace.yaml \
  .npmrc \
  ./
COPY packages/ui/package.json packages/ui/package.json
RUN pnpm --filter @web-libros/ui install
COPY packages/ui packages/ui

RUN pnpm generate

FROM base-python AS prod-python
COPY --from=ui-builder /app/packages/ui/.output/public /static
