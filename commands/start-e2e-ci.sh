#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

echo "cmd: start the application"
echo "cmd: prepare e2e environment"
python cli.py migrate latest
python cli.py generate e2e-data
echo "cmd: e2e environment prepared"
echo "cmd: start server"
python -m src.main
