#!/bin/bash

set -o errexit
set -o nounset

coverage run -m pytest  -svv
coverage xml
coverage report
coverage html
