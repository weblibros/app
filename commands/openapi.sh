
#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

echo "cmd: generate openapi file"
python -m src.openapi

