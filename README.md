# Web Libros

## About

This is a application to manage your digital books.

## setup

### setup docker

`make docker`

### setup e2e

- install e2e packages `cd e2e && pnpm install && cd ..`
- install playwright packages `make test-e2e-install`

## run tests

unit & integration tests

```bash
make docker-d
make test-backend
```

e2e test

```bash
make docker-d
make pop-test-data
make test-2e2-install
make test-e2e-ui
```

## roadmap

- [x] containerasation of the project
  - [x] create a docker and docker compose file for developement [calibre-web]
  - [x] create a docker and docker compose file for developement [Backend]
  - [x] create a docker and docker compose file for developement [Frontend]
- [x] setup python backend
  - [x] write test with pytest
  - [x] setup CI pipelines
  - [x] Use SQLModel
  - [x] enable migrations Alembic
  - [x] write first endpoints for authenication (WIP)
    - [x] authentication endpoint
    - [x] token refresh endpoint
    - [x] reset password endpoint
    - [x] onboarding endpoint
- [x] setup vue frontend
  - [x] Use Vue3 for the frontend
  - [x] create Login logic
  - [x] frontend unit testing
  - [x] frontend testing with cypress
  - [x] setup CI pipelines
  - [x] setup storybook
- [x] CD
  - [x] auto deploy Backend
  - [x] auto deploy Frontend
- [ ] features
  - [ ] invite users endpoint
  - [ ] Libary: view all your ebooks
  - [ ] upload new ebook
  - [ ] import calibre books
  - [ ] sync functionality with kobo ereaders
  - [ ] convert formats ebooks
  - [ ] share books with other users
  - [ ] create book shels
  - [ ] ...

## database migrations

for the databse migration Alembic is used.

new migrations are created with the following command

```
alembic revision --autogenerate -m "very smart comment"
```

migrate to latest version

```
alembic upgrade head
```
