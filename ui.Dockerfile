FROM node:22-slim

# make the 'app' folder the current working directory
WORKDIR /app

RUN corepack enable \
    && corepack prepare pnpm@latest-8 --activate
COPY package.json \
      pnpm-lock.yaml \
      pnpm-workspace.yaml \
      .npmrc \
      ./
COPY packages/ui/package.json packages/ui/package.json

RUN pnpm --filter @web-libros/ui install

COPY packages/ui packages/ui

