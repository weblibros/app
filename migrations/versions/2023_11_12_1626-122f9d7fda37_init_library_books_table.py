"""init library books table

Revision ID: 122f9d7fda37
Revises: be3d8b1e28e4
Create Date: 2023-11-12 16:26:22.610105

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "122f9d7fda37"
down_revision = "be3d8b1e28e4"
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "library_books",
        sa.Column("uuid", sa.Uuid(), nullable=False),
        sa.Column("library_uuid", sa.Uuid(), nullable=False),
        sa.Column("book_uuid", sa.Uuid(), nullable=False),
        sa.Column(
            "created_at",
            sa.DateTime(),
            server_default=sa.text("CURRENT_TIMESTAMP"),
            nullable=False,
        ),
        sa.Column(
            "updated_at",
            sa.DateTime(),
            server_default=sa.text("CURRENT_TIMESTAMP"),
            nullable=False,
        ),
        sa.ForeignKeyConstraint(["book_uuid"], ["books.uuid"], ondelete="CASCADE"),
        sa.ForeignKeyConstraint(["library_uuid"], ["libraries.uuid"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("uuid"),
        sa.UniqueConstraint("book_uuid", name="_book_uc"),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("library_books")
    # ### end Alembic commands ###
