"""fill in entity_class_name

Revision ID: d99eeee340d7
Revises: bbb0f4746acd
Create Date: 2024-08-01 16:34:23.756315

"""

from alembic import op

# revision identifiers, used by Alembic.
revision = "d99eeee340d7"
down_revision = "bbb0f4746acd"
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute("UPDATE file_records SET entity_class_name = 'File' WHERE entity_class_name IS NULL")
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
