"""extend import step table with name and version

Revision ID: 72b786b6d76d
Revises: 7d0ab8b5acd0
Create Date: 2024-09-29 14:00:01.789608

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "72b786b6d76d"
down_revision = "7d0ab8b5acd0"
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "file_import_steps",
        sa.Column("step_name", sa.String(length=50), nullable=True),
    )
    op.add_column(
        "file_import_steps",
        sa.Column("step_version", sa.String(length=10), nullable=True),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("file_import_steps", "step_version")
    op.drop_column("file_import_steps", "step_name")
    # ### end Alembic commands ###
