# Json Web Tokens

For authentication of users the application uses JWT Tokens  

## secret key

For the simple setup we can us a single secret key set the following env variables
"""bash
    JWT_ALGORITHM=HS256
    JWT_SECRET=<very secret key>
"""

## secret key pair

It's recommened to use a public/private key pair.
With the following command you can create pair
"""bash
# ES512
# private key
openssl ecparam -genkey -name secp521r1 -noout -out ./.secret/ecdsa-p521-private.pem
# public key
openssl ec -in ./.secret/ecdsa-p521-private.pem -pubout -out ./.secret/ecdsa-p521-public.pem
"""

use the following environment variables
"""bash
    JWT_ALGORITHM=HS256
    JWT_PRIVATE_KEY_FILE=<path to private key>
    JWT_PUBLIC_KEY_FILE=<path to public key>
"""
